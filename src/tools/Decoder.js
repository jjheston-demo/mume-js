export default class Decoder {

  static ansiCodes() {
    return {
      // text style attributes
      '\\[0m'  : 'off',
      '\\[1m'  : 'bold background_transparent',
      '\\[4m'  : 'underline background_transparent',
      // foreground colors - dark
      '\\[30m' : 'black background_transparent',
      '\\[31m' : 'red background_transparent',
      '\\[32m' : 'green background_transparent',
      '\\[33m' : 'yellow background_transparent',
      '\\[34m' : 'blue background_transparent',
      '\\[35m' : 'magenta background_transparent',
      '\\[36m' : 'cyan background_transparent',
      '\\[37m' : 'white background_transparent',
      // foreground colors - bright
      '\\[90m' : 'bright_black background_transparent',
      '\\[91m' : 'bright_red background_transparent',
      '\\[92m' : 'bright_green background_transparent',
      '\\[93m' : 'bright_yellow background_transparent',
      '\\[94m' : 'bright_blue background_transparent',
      '\\[95m' : 'bright_magenta background_transparent',
      '\\[96m' : 'bright_cyan background_transparent',
      '\\[97m' : 'bright_white background_transparent',
      // bold + dark colors
      '\\[30;1m' : 'bold black background_transparent',
      '\\[31;1m' : 'bold red background_transparent',
      '\\[32;1m' : 'bold green background_transparent',
      '\\[33;1m' : 'bold yellow background_transparent',
      '\\[34;1m' : 'bold blue background_transparent',
      '\\[35;1m' : 'bold magenta background_transparent',
      '\\[36;1m' : 'bold cyan background_transparent',
      '\\[37;1m' : 'bold white background_transparent',
      // bold + bright colors
      '\\[90;1m' : 'bold bright_black background_transparent',
      '\\[91;1m' : 'bold bright_red background_transparent',
      '\\[92;1m' : 'bold bright_green background_transparent',
      '\\[93;1m' : 'bold bright_yellow background_transparent',
      '\\[94;1m' : 'bold bright_blue background_transparent',
      '\\[95;1m' : 'bold bright_magenta background_transparent',
      '\\[96;1m' : 'bold bright_cyan background_transparent',
      '\\[97;1m' : 'bold bright_white background_transparent',

      '\\[97;44;1m' : 'bold bright_white background_blue',

      '\\[0;31m' : 'bold red background_transparent',

      '\\[37;44m' : 'white background_blue',


    }
  }

  static mudOutputToHtml(data) {
    let output = data;
    output = this.stripArtifacts(output);
    /*
    is this ANSI or XML? I need to write a function to check for
    this later. Assuming ANSI for now.
    */
    const isXml = true;
    if( isXml ) {
      output = this.xmlToHtml(output);
    } else {
      // console.log('running else');
      output = this.ansiToHtml(output);
    }
    return output;
  }

  static mudOutputToPlainText(data) {
    let output = data;
    const isXml = false;
    if( isXml ) {
      output = this.xmlToPlainText(output);
    } else {
      // console.log('running else');
      output = this.ansiToPlainText(output);
    }
    return output;
  }

  static xmlToHtml(data) {
    // this is literally all I need. browser renders XML just fine.
    let output = data;
    output = this.renderAnsiCodes(output);
    return output;
  }

  static ansiToHtml(data) {
    const ansiCodes = this.ansiCodes();
    let output = data;
    // replace < and > with &gt; and &lt;
    output = this.tagsToEntities(output);
    let spanCount = 0;
    // loop through every ansi code and do a search and replace

    // EXAMPLE
    // [1mPlayers[0m
    //
    // STEP 0 - example start
    // """ [1mPlayers[0m
    //
    // STEP 1 - for each ansiCode, run output.replace(/key/g)
    // """ <span class="bold">Players<span class="off">
    //
    // STEP 2 - keep a count of # of spans injected during state 1.
    //   - then append that many "</span>"s to output
    // """ <span class="bold">Players<span class="off"></span></span>
    for(const code in ansiCodes) {
      if( ansiCodes.hasOwnProperty(code) ){
        // console.log('if hasOwnProperty checks out');

        const codeRegex = new RegExp(code, 'g');
        if( codeRegex.test(data) ) {
          spanCount++;
          const color = ansiCodes[code];
          output = output.replace(codeRegex, '<span class="' + color + '">' );
        }
      }
    }
    const a = '</span>'.repeat(spanCount);
    output += a;
    return output;
  }

  static ansiToPlainText(data) {
    let output = data;
    output = this.stripArtifacts(output);
    output = this.stripAnsiCodes(output);
    return output;
  }

  static tagsToEntities(data) {
    return data.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;') ;
  }

  static stripArtifacts(data) {
    let output = data;
    output = output.replace(/��/g, '');
    return output;
  }

  static stripAnsiCodes(data) {
    const regex = /\[([\d]{1,2})m/g;
    if( regex.test(data) ){
      const matchesCount = data.match(regex).length;
      data = data.replace( regex, "" );
    }
    return data;
  }

  static renderAnsiCodes(data) {
    const ansiCodes = this.ansiCodes();
    let output = data;
    // replace < and > with &gt; and &lt;
    let spanCount = 0;
    // loop through every ansi code and do a search and replace

    // EXAMPLE
    // [1mPlayers[0m
    //
    // STEP 0 - example start
    // """ [1mPlayers[0m
    //
    // STEP 1 - for each ansiCode, run output.replace(/key/g)
    // """ <span class="bold">Players<span class="off">
    //
    // STEP 2 - keep a count of # of spans injected during state 1.
    //   - then append that many "</span>"s to output
    // """ <span class="bold">Players<span class="off"></span></span>
    for(const code in ansiCodes) {
      if( ansiCodes.hasOwnProperty(code) ){
        // console.log('if hasOwnProperty checks out');

        const codeRegex = new RegExp(code, 'g');
        if( codeRegex.test(data) ) {
          spanCount++;
          const color = ansiCodes[code];
          output = output.replace(codeRegex, '<span class="' + color + '">' );
        }
      }
    }
    const a = '</span>'.repeat(spanCount);
    output += a;
    return output;
  }
}