export default class MapTool {
  // when constructor runs, autoload last saved map
  constructor() {
    this.map = null;
    this.mapName = '';
    this.currentRoom = {};

    // load with no params loads most recently saved map in localStorage
    this.load();
  }
  createEmptyMap(xLength, yLength, xStartIndex, yStartIndex) {
    xStartIndex = (xStartIndex) ? xStartIndex : 0;
    yStartIndex = (yStartIndex) ? yStartIndex : 0;

    let xStopIndex = xStartIndex + xLength - 1;
    let yStopIndex = yStartIndex + yLength - 1;

    // console.log('xStartIndex');
    // console.log(xStartIndex);
    // console.log('yStartIndex');
    // console.log(yStartIndex);


    // if no xLength, yLength provided, default to 100 by 100
    let map = [];


    for (var y = yStopIndex; y >= yStartIndex; y--) {
      let yRow = []; // yRow is a row of rooms with all the same y coordinate value
      for (var x = xStartIndex; x < (xStopIndex + 1); x++) {
        const room = {x:x, y:y};
        yRow.push(room);
      }
      map.push(yRow); 
    }
    this.map = map;
  }
  // load map data from local storage
  loadLocalMap() {

  }
  // save map data to local storage
  saveLocalMap() {

  }

  // load map data from file
  loadFile(mapName) {
    let map = require('../data/maps/' + mapName + '-master')['default'];
    this.map = map;
  }
  // load map from local storage
  load(mapName) {
    // if mapname is empty, load most recent
    /*
    const stateString = JSON.stringify(state);
    // 2. create key and store
    const storageKey = 'mapToolData_' + this.mapName;
    localStorage.setItem(storageKey, stateString);
    localStorage.setItem('lastMapToolKey', storageKey);
    */

    if(localStorage.getItem('lastMapToolKey')) {
      let thisKey = localStorage.getItem('lastMapToolKey');
      let state = JSON.parse(localStorage.getItem(thisKey));
      this.map = state.map;
      this.mapName = state.mapName;
      this.currentRoom = state.currentRoom;
    } else {
      // do nothing for now
    }

  }
  // provided props.x, props.x and some other props, update room with prop.x and prop.y on this.map
  setRoom(props) {
    // 1. grab old room
    let room;
    const map = this.map;
    const { xIndex, yIndex } = this.getXYIndexes(props.x, props.y);
    // notice y comes first, then x
    room = map[yIndex][xIndex];

    // 2. iterate through props and copy everything but x and y
    for (let [key, value] of Object.entries(props)) {
      room[key] = value;      
    }
    this.map[yIndex][xIndex] = room;
  }
  set(props) {
    this.setRoom(props);
  }
  setRooms(roomsArray) {
    for (let i = 0; i < roomsArray.length; i++) {
      let room = roomsArray[i];
      this.setRoom(room);
    }
  }
  // get array indexes/keys based on map coordinates (y axis is inverted, normalize neg. values, etc.)
  getXYIndexes(xCoordinate, yCoordinate) {
    const map = this.map;
    // first we need to find maximum x and y values used in our map room coordinates. 
    // these can be +pos or -neg, and are needed to calculate array offset
    const rowCount = map.length;
    const firstRow = map[0];
    const lastRow = map[map.length-1]
    const firstRowFirstRoom = firstRow[0];
    const firstRowRange = firstRow.length;
    const firstRowLastRoom = firstRow[firstRowRange-1]; 
    const lastRowFirstRoom = lastRow[0];
                                     // Some example values from first map 'noc.js'
    const loX = firstRowFirstRoom.x; // -20
    const hiX = firstRowLastRoom.x;  //  30
    const loY = lastRowFirstRoom.y;  // -12
    const hiY = firstRowLastRoom.y;  //  10
    // example values from first map 'noc.js'
    // var xOnMapObj = xRequested + 20;
    // var yOnMapObj = 10 - yRequested;
    const xOnMapObj = xCoordinate - loX;
    const yOnMapObj = hiY - yCoordinate;
    return {xIndex: xOnMapObj, yIndex: yOnMapObj};
  }
  // translate coordinates and grab a copy of a room
  getRoom(props) {  
    let room;
    const map = this.map;
    const { xIndex, yIndex } = this.getXYIndexes(props.x, props.y);
    // notice y comes first, then x
    room = map[yIndex][xIndex];
    return room;
  }
  get(props) {
    return this.getRoom(props);
  }
  // save this map object to local storage
  save() {
    // 1. create state object to store
    const state = {
      map: this.map,
      mapName: this.mapName,
      currentRoom: this.currentRoom,
    };
    const stateString = JSON.stringify(state);

    // 2. create key and store
    const storageKey = 'mapToolData_' + this.mapName;
    localStorage.setItem(storageKey, stateString);
    localStorage.setItem('lastMapToolKey', storageKey);
  }
  // order user to look/do whatever to grab name, desc, terrain, exits, etc
  scan() {
    // first create empty room object
    const room = {
      name: '',
      description: '',
      exits: {},
      terrain: '',
    }
    // step 1 - send commands to refresh data to MUD
    // then run 
    // * app.input('look')
    // * app.input('exit')
    window.app.input('look');
    window.app.input('exit');

    // step 2 - new data is picked up from mudOutput responding to look, exit commands
    // i'll either use a callback function for input ()
  }
}