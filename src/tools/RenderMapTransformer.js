export default class RenderMapTransformer {
  constructor() {

  }
  static convert(masterMapArray) {
    let renderMapArray = masterMapArray.slice();
    for (var i = 0; i < renderMapArray.length; i++) {
      var yRow = renderMapArray[i];


      for (var j = 0; j < yRow.length; j++) {
        var room = yRow[j];

        // exit related properties we need to render:
        if( room.exits ) {

          // EXITS STRING: up/down - we just need to render up and down on map so ignore others
          var exitsString = '';
          if(room.exits.up) {
            exitsString+= 'up ';
          }
          if(room.exits && room.exits.down) {
            exitsString+= 'down ';
          }
          exitsString = exitsString.trim();

          // WALLS STRING - 
          var wallsString = '';
          if(! room.exits.north) {
            wallsString+= 'north ';
          }
          if(! room.exits.east) {
            wallsString+= 'east ';
          }
          if(! room.exits.south) {
            wallsString+= 'south ';
          }
          if(! room.exits.west) {
            wallsString+= 'west ';
          }
          wallsString = wallsString.trim();

          // DOORS STRING
          var doorsString = '';
          if(room.exits.north && room.exits.north.door) {
            doorsString+= 'north ';
          }
          if(room.exits.east && room.exits.east.door) {
            doorsString+= 'east ';
          }
          if(room.exits.south && room.exits.south.door) {
            doorsString+= 'south ';
          }        
          if(room.exits.west && room.exits.west.door) {
            doorsString+= 'west ';
          }
          doorsString = doorsString.trim();

          // PIPES - skip for now, these have to be hard-coded currently
          delete room.exits;

          room.exits = exitsString;
          room.walls = wallsString;
          room.doors = doorsString;
        }// if room.exits

        delete room.id;
        delete room.name;
        delete room.map;
        delete room.description;
      }// for j
    }// for i 

    return renderMapArray;
  }// static convert
}