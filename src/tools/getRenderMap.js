

import noc from './data/maps/noc-master';

(function() {
  var a1 = noc;
  var xmap = a1.slice();
  window.xmap = xmap;

  console.log('xmap');
  console.log(xmap);
  // console.log('xmap');
  // console.log(xmap);

  // CREATE RENDER MAP:

  for (var i = 0; i < xmap.length; i++) {
    var yRow = xmap[i];


    for (var j = 0; j < yRow.length; j++) {
      var room = yRow[j];
      // if no id, we strip the room, no x or y even, unless its first or last room in a yRow
      // hoping this saves resources
      

      // if ((!room.id) &&  j!== 0 && j !== (yRow.length-1) ) {
      //   delete room.x;
      //   delete room.y;
      //   continue;
      // }


      // exit related properties we need to render:
      if( room.exits ) {


        // EXITS STRING: up/down - we just need to render up and down on map so ignore others
        var exitsString = '';
        if(room.exits.up) {
          exitsString+= 'up ';
        }
        if(room.exits && room.exits.down) {
          exitsString+= 'down ';
        }
        exitsString = exitsString.trim();


        // WALLS STRING - 
        var wallsString = '';
        if(! room.exits.north) {
          wallsString+= 'north ';
        }
        if(! room.exits.east) {
          wallsString+= 'east ';
        }
        if(! room.exits.south) {
          wallsString+= 'south ';
        }
        if(! room.exits.west) {
          wallsString+= 'west ';
        }
        wallsString = wallsString.trim();

        // DOORS STRING
        var doorsString = '';
        if(room.exits.north && room.exits.north.door) {
          doorsString+= 'north ';
        }
        if(room.exits.east && room.exits.east.door) {
          doorsString+= 'east ';
        }
        if(room.exits.south && room.exits.south.door) {
          doorsString+= 'south ';
        }        
        if(room.exits.west && room.exits.west.door) {
          doorsString+= 'west ';
        }
        doorsString = doorsString.trim();

        // PIPES - skip for now, these have to be hard-coded currently

        delete room.exits;

        room.exits = exitsString;
        room.walls = wallsString;
        room.doors = doorsString;


      }// if room.exits

      delete room.id;
      delete room.name;
      delete room.map;




      // delete irrelevant info to save space:

      // delete old 'exits'
      // delete 'id'
      // delete 'name'
      // delete 'map'











      // create the following data attributes for render

      // * renderExits (we just need up and down)
      // * renderWalls (based on exits)
      // * renderDoors

      // delete the following:
      // * room.name
      // * 

      // NOTE: we might actually be able to delete x and y for empty rooms
    }
  }




}())