import React from 'react';
import $ from 'jquery';

export default class MapperRoom extends React.Component {

  constructor(props) {
    super(props);
    this.handleDoubleClick = this.handleDoubleClick.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.markers !== nextProps.markers) {
      return true;
    } else {
      return false;
    }
  }

  render() {
    const room = this.props.room;
    const markers = this.props.markers;

    if (room.terrain && room.terrain.length) {
      return (
        <div
          onDoubleClick={ this.handleDoubleClick }
          className={'x' + room.x}
          data-x={room.x}
          data-y={room.y}
          data-name={room.name}
          data-terrain={room.terrain}
          data-exits={room.exits}
          data-walls={room.walls}
          data-doors={room.doors}
          data-flags={room.flags} /* like 'hasWater' */
          data-markers={markers} /* like "isCurrentRoom" or "isSelectedRoom" */
        >
          <div className="layer terrain"></div>
          <div className="layer exits"></div>
          <div className="layer walls"></div>
          <div className="layer doors"></div>
          <div className="layer flags"></div>
          <div className="layer markers"></div>
        </div>
      )
    } else { // empty room, don't create as many DOM sub elements
      return (
        <div
          className={'x' + room.x}
          key={room.x + ',' + room.y}
          data-x={room.x}
          data-y={room.y}
        >
        </div>
      )
    }
  }// render(){...}

  handleDoubleClick(event) {
    const roomNode = event.target.parentNode;
    const x = parseInt(roomNode.getAttribute('data-x'));
    const y = parseInt(roomNode.getAttribute('data-y'));
    this.props.handleMapperRoomDoubleClick(x, y);        
  }

}