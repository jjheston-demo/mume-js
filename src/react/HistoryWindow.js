import React from 'react';

export default class HistoryWindow extends React.Component {
  constructor(props) {
    super(props);
    // this.state = {
      // historyEntries: this.props.historyEntries
    // }
    this.handleDoubleClick = this.handleDoubleClick.bind(this);
  }
  componentDidUpdate() {
    this.$HistoryWindow.scrollTop = 99999999999;
  }
  handleDoubleClick(event) {
    this.props.handleHistoryWindowDoubleClick();
  }
  render() {
    // console.log('HistoryWindow.render() runs');
    const historyEntries = this.props.historyEntries || [];
    const entriesMarkup = historyEntries.map( (entry, i) => {
      const data = entry.data;
        return (
          <div className="HistoryEntry"
            key={entry.time}
            data-type={entry.type}
            dangerouslySetInnerHTML={createMarkup(data)}
            />
        );
    });
    return (
      <div
        className="HistoryWindow"
        ref={ (div) => {this.$HistoryWindow = div;} }
        onDoubleClick={ this.handleDoubleClick }
        >
        {entriesMarkup}
      </div>
    );
  }
}

function createMarkup(data) {
  return {__html: data};
}