import React from 'react';
import HistoryWindow from './HistoryWindow';
import UserInputForm from './UserInputForm';
import MapperWindow from './MapperWindow';

export default class MudClientReactApp extends React.Component {
  constructor(props) {
    super(props);
    this.handleUserInputFormSubmit = this.handleUserInputFormSubmit.bind(this);
    this.handleHistoryWindowDoubleClick = this.handleHistoryWindowDoubleClick.bind(this);    
    this.handleMapperRoomDoubleClick = this.handleMapperRoomDoubleClick.bind(this);     
  }
  componentDidUpdate() {
    // console.log('MudClientReactApp.componentDidUpdate() runs');
  }
  handleHistoryWindowDoubleClick() {
    this.refs.$UserInputForm.selectTextInput();
  }
  handleUserInputFormSubmit(value) {
    window.app.input(value);
  }
  handleMapperRoomDoubleClick(x, y) {
    console.log('x, y');
    console.log(x + ',' + y);
    // window.app.maintainers.currentMap.updateState({x: x, y: y});
    window.app.maintainers.currentMap.updateState({x: x, y: y, mapName: this.props.currentMap.mapName});
  }  
  render() {
    // console.log('MudClientReactApp.render()');
    return (
      <div className="Client">
        <div className="leftColumn">
          <HistoryWindow
            historyEntries={ this.props.historyEntries }
            handleHistoryWindowDoubleClick={ this.handleHistoryWindowDoubleClick }
          />
          <UserInputForm
            ref="$UserInputForm"
            handleUserInputFormSubmit={ this.handleUserInputFormSubmit }
          />
        </div>
        {/**/}
        <MapperWindow
          currentMap={ this.props.currentMap }
          handleMapperRoomDoubleClick={ this.handleMapperRoomDoubleClick }
        />
        {/**/}
      </div>
      
    )
  }
}