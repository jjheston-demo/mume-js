import React from 'react';
import $ from 'jquery';
import MapperRoom from './MapperRoom';

export default class MapperWindow extends React.Component {
  constructor(props) {
    super(props);
    this.centerMap = this.centerMap.bind(this);
    this.drawPipesOnCanvas = this.drawPipesOnCanvas.bind(this);
  }
  componentDidMount() {
    this.centerMap();
    // this.drawPipesOnCanvas();
  }
  componentDidUpdate() {
    this.centerMap();
  }
  render() {
    let mapName = (this.props && this.props.currentMap && this.props.currentMap.mapName) ? this.props.currentMap.mapName : 'goblins2oc' ;
    const rMap = require('../data/maps/' + mapName + '-render')['default'];
    const mapRows = rMap.length;
    const mapCols = rMap[0].length;

    // const mapRows = this.props.currentMap.mapRenderData.length;
    // const mapCols = this.props.currentMap.mapRenderData[0].length;

    const squareSize = 32; // in pixels

    return (
      <div
        className="MapperWindow"
        ref={ (div) => {this.$MapperWindow = div;} }        
        >
        <div className="mapWrapper">
          <div id="map" className="map">
            <div className="markupGrid" style={{ width: (mapCols*squareSize) + 'px', height: (mapRows*squareSize) + 'px' }}>
              {this.gridRows()}
            </div>
          </div>
          <canvas id="mapCanvas"className="mapCanvas" width={mapCols * squareSize} height={mapRows * squareSize}></canvas>        
        </div>
      </div>        
    )
  }
  centerMap() {
    var windowCenterX = $('.mapWrapper').width() / 2;
    var windowCenterY = $('.mapWrapper').height() / 2;
    var $container = $(".mapWrapper");
    var $room = $('.markupGrid [data-markers*=isCurrentRoom]');


    if( $container.length && $room.length ) {
      $container.scrollTop(  $('.markupGrid [data-markers*=isCurrentRoom]').offset()['top'] + 32 - windowCenterY - $('.markupGrid').offset()['top'] );
      $container.scrollLeft(  $('.markupGrid [data-markers*=isCurrentRoom]').offset()['left'] + 24 - windowCenterX - $('.markupGrid').offset()['left'] );      
    }
  }
  drawPipesOnCanvas() {
    const canvas = document.getElementById('mapCanvas');
    const context = canvas.getContext('2d');

    let storedLines = [];

    if (this.props.currentMap.mapName === 'noc') {

      storedLines = [
        {'x1': 216, 'y1': 103, 'x2': 360, 'y2':217},
        {'x1': 392, 'y1': 217, 'x2': 473, 'y2':264},
        {'x1': 456, 'y1': 281, 'x2': 345, 'y2':553},
        {'x1': 456, 'y1': 409, 'x2': 601, 'y2':584},
        {'x1': 623, 'y1': 448, 'x2': 720, 'y2':576},
        {'x1': 680, 'y1': 441, 'x2': 793, 'y2':584},
        {'x1': 601, 'y1': 520, 'x2': 807, 'y2':376},
        {'x1': 321, 'y1': 593, 'x2': 384, 'y2':593},
      ];

    } else if (this.props.currentMap.mapName === 'goblins2oc') {

    }


    context.beginPath();
    context.strokeStyle = '#b0b0b0';
    for (var i = 0; i < storedLines.length; i++) {
      var v = storedLines[i];
      context.moveTo(v.x1, v.y1);
      context.lineTo(v.x2, v.y2);
      context.stroke();
    }
  }
  gridRows() {
  // return this.props.arr.map( (rows, i) => {

    let mapName = (this.props && this.props.currentMap && this.props.currentMap.mapName) ? this.props.currentMap.mapName : 'goblins2oc' ;
    const rMap = require('../data/maps/' + mapName + '-render')['default'];

    return rMap.map( (rows, i) => {
    // return this.props.currentMap.mapRenderData.map( (rows, i) => {
      const y = rows[i]['y'];
      let row = rows.map( (room, j) => {
        const _x = parseInt(room.x);
        const _y = parseInt(y);
        let markers = '';
        // if the room being rendered is the current room user is in, add appropriate markup data
        if (_x === parseInt(this.props.currentMap.x) && _y === parseInt(this.props.currentMap.y )) {
          markers+= ' isCurrentRoom';
        }

        return (
          <MapperRoom  
            key={room.x + ',' + room.y}
            room={room}
            markers={markers}
            handleMapperRoomDoubleClick={ this.props.handleMapperRoomDoubleClick }
          />          
        )

      });
      return (
        <div 
          className={'y' + y }
          key={'y' + y }
          >
          {row}
        </div>
      )
    });
  }
}