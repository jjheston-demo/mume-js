/**
 * So, we're dealing with managing user input history just within this component.
 * User command history is not getting passed to Controller or anything. Might change this later
 */

import React from 'react';

export default class UserInputForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: '' };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleKeyUp = this.handleKeyUp.bind(this);

    this.history = [];
    this.pointerIndex = 0;

  }
  componentDidMount() {
    this.selectTextInput();
  }
  handleChange(event) {
    this.setState({value: event.target.value});
  }
  handleSubmit(event) {
    event.preventDefault();
    const value = this.state.value;

    this.history.push(value);
    this.pointerIndex = this.history.length -1;

    this.props.handleUserInputFormSubmit(value);
    this.textInput.select();
  }
  render() {
    // console.log('UserInputForm.render() runs');
    return (
      <form className="UserInputForm" onSubmit={this.handleSubmit}>
        <input type="text"
          value={this.state.value}
          onChange={this.handleChange}
          onKeyUp={this.handleKeyUp}
          ref={ (input) => {this.textInput = input;} }
          />
        <input type="submit" value="Submit" />
      </form>
    );
  }
  handleKeyUp(event) {
    // if (event.keyCode == 13) { }// Return Key

    // "Up Arrow" key replaces current input text with previous command in history
    if (event.keyCode == 38) {  
      // console.log('Up Arrow key pressed');
      const message = this.getPreviousMessage();
      // this.textInput.value = message;
      this.setState({value: message});
      this.textInput.focus();
      this.textInput.select();
    }
    // "Down Arrow" key replaces current input text wiht next command in history or empties contents
    if (event.keyCode == 40) { // down arrow key
      // console.log('Down Arrow key pressed');
      const message = this.getNextMessage();
      this.setState({value: message});
      // this.textInput.value = message;
      this.textInput.focus();
      this.textInput.select();
    }

  }
  selectTextInput() {
    this.textInput.select();
  }
  addMessage(message) {
    // whenever user submits a new message, pointerIndex is set to max/most recent
    this.history.push(message);
    this.pointerIndex = this.history.length - 1;
  }
  // When Up Arrow key is pressed
  getPreviousMessage() {
    let message = '';
    let requestedIndex = this.pointerIndex - 1;

    // requested index in range, replace input_bar text with previous entry in history
    if (requestedIndex >= 0 && requestedIndex <= this.history.length -1 ) {
      this.pointerIndex = requestedIndex;
      message = this.history[requestedIndex];
    } 
    // else don't do anything

    return message;
  }
  // When Down Arrow key is pressed
  getNextMessage() {
    let message = '';
    var requestedIndex = this.pointerIndex + 1;
    // requested index in range, replace input_bar text with next entry in history
    if (requestedIndex  >= 0 && requestedIndex <= this.history.length -1) {
      this.pointerIndex = requestedIndex;
      message = this.history[requestedIndex];      
    }    

    return message;
  }

}