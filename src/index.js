import './assets/reset.css';
import './assets/index.css';
import './assets/ansiColors.css';
import './assets/MapperWindow.css';

import $ from 'jquery';
window.$ = $;

import Controller from './controller/Controller';

const app = new Controller();
window.app = app;


import map2 from './data/maps/valewest-master';
import RenderMapTransformer from './tools/RenderMapTransformer';
(function() {

    let mapB = JSON.parse(JSON.stringify(map2));
    const xyrender = RenderMapTransformer.convert(mapB);
    window.xyrender = xyrender; 

}())


/*

import MapTool from './tools/MapTool';

(function() {

  let xmap = new MapTool();
  window.xmap = xmap;  

  // create an empty map, 0,0 to 49, 49
  xmap.createEmptyMap(50, 50); 

  // 'valewest' first room: nw corner room 
  xmap.setRooms([


    {
      "x": 10,
      "y": 40,
      "map": "valewest",
      "name": "\u001bAn Old Riverbed\u001b",
      "description": "\u001bThe small river runs north-south here. A dark forest looms on its western bank.\u001b\r\n\u001bVast plains stretch before you to the east. Turning around, you see the high\u001b\r\n\u001bpeaks of the ominous Misty Mountains. The long mountain range stretches from\u001b\r\n\u001bAngrenost, also known as Isengard, in the south, to Carn Dum, the capital of\u001b\r\n\u001bAngmar, the land of the Witch-king, in the north. You have heard many\u001b\r\n\u001bfrightening tales about these mountains - about Moria, where no man dares to\u001b\r\n\u001btravel, about Mount Gundabad, the orkish capital in the north, and about the\u001b\r\n\u001bhorrid goblins of the Goblin-gate, somewhere in the High Pass just west of\u001b\r\n\u001bhere. The river is less supported by water than it used to be and it slowly\u001b\r\n\u001bturns into land.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false,
          "pipeTo": {
            "map": 'goblins2oc',
            "x": 89,
            "y": 40,
          }

        }
      },
      "terrain": "field",
    },

    {
      "x": 10,
      "y": 39,
      "map": "valewest",      
      "name": "\u001bAn Old Riverbed\u001b",
      "description": "\u001bTo the east of you is the dark pine forest which separates you from the foot of\u001b\r\n\u001bthe Misty Mountains. The small river emerges from the forest here, starting\u001b\r\n\u001bits way east to the large river which eventually could bring you to Gondor and\u001b\r\n\u001bthe ocean beyond. The river is less supported by water than it used to be and\u001b\r\n\u001bit slowly turns into land.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false,
          "pipeTo": {
            "map": 'goblins2oc',
            "x": 89,
            "y": 39,
          }
        }
      },
      "terrain": "field",
    },



    {
      "x": 10,
      "y": 38,
      "map": "valewest",
      "name": "\u001bForest River\u001b",
      "description": "\u001bHere the small river leaves the surrounding forest and makes its way through\u001b\r\n\u001bthe grassy plains west of the river Anduin. The water is clear and seems quite\u001b\r\n\u001bsafe to drink.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false,
          "pipeTo": {
            "map": 'goblins2oc',
            "x": 89,
            "y": 38,
          }
        }
      },
      "terrain": "water",
    },

    {
      "x": 10,
      "y": 37,
      "map": "valewest",
      "name": "\u001bDark Forest\u001b",
      "description": "\u001bYou are now deep inside the forest. Old rotten logs, mixed with dead leaves\u001b\r\n\u001band some brightly coloured flowers lie among the high trees. It is very\u001b\r\n\u001bdark in here. You unsuccessfully try to find the sources of the sounds that\u001b\r\n\u001breach you. You can hear boars grunting, birds singing and an occasional\u001b\r\n\u001bbranch breaking.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false,
          "pipeTo": {
            "map": 'goblins2oc',
            "x": 89,
            "y": 37,
          }
        }
      },
      "terrain": "forest",

    },


    {
      "x": 10,
      "y": 35,
      "map": "valewest",
      "name": "\u001bThick Forest\u001b",
      "description": "\u001bThis dark and gloomy forest covers some of the foothills of the Misty\u001b\r\n\u001bmountains. Somewhere to the east, the land gradually flattens into the Vale\u001b\r\n\u001bof Anduin, but here it is still quite rugged.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false,
          "pipeTo": {
            "map": 'goblins2oc',
            "x": 89,
            "y": 35,
          }
        }
      },
      "terrain": "forest",
    },


    {
      "x": 10,
      "y": 34,
      "map": "valewest",
      "name": "\u001bThick Forest\u001b",
      "description": "\u001bThe forest is more dense. High trees mixed with lower brush form a very hard\u001b\r\n\u001bterrain to move in. Looking around you, you see the tracks of wild animals such\u001b\r\n\u001bboars and deers. You also see tracks of something bigger - man or beast... \u001b\r\n\u001bLooking back, you get the comforting view of the road to the south and the west\u001b\r\n\u001band realise that at least you are not hopelessly lost yet. \u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false,
          "pipeTo": {
            "map": 'goblins2oc',
            "x": 89,
            "y": 34,
          }
        }
      },
      "terrain": "forest",
    },


    {
      "x": 10,
      "y": 33,
      "map": "valewest",
      "name": "\u001bRoad to the Old Ford\u001b",
      "description": "\u001bThe road makes its way through a dark forest towards an unknown destiny in the\u001b\r\n\u001beast. Looking along the road, you can see that it leads down to the river\u001b\r\n\u001bAnduin, which it crosses by a ford, known to be rather dangerous. Beyond the\u001b\r\n\u001briver the road leads into Mirkwood and to the lands beyond.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "west": {
          "door": false,
          "pipeTo": {
            "map": 'goblins2oc',
            "x": 89,
            "y": 33,
          }
        }
      },
      "terrain": "road",
    },


    {
      "x": 11,
      "y": 40,
      "map": "valewest",
      "name": "\u001bShadowy Plains\u001b",
      "description": "\u001bThese plains stretch for many miles to the east. In the evenings they fall\u001b\r\n\u001bunder the dark shadows of the Misty Mountains. Immediately south, a dark forest\u001b\r\n\u001bseems to be encroaching upon these plains, as if attempting a conquest.\u001b\r\n\u001bAlthough a small river can be seen to the west, the sounds of rushing water\u001b\r\n\u001bseem to be coming from the south.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "field",
    },



    {
      "x": 11,
      "y": 39,
      "map": "valewest",
      "name": "\u001bDark Forest\u001b",
      "description": "\u001bA sombre silence lies upon this forest. Almost every life form seems to have \u001b\r\n\u001bfled from the darkness that shrouds this place. The high, closely intertwined \u001b\r\n\u001btreetops ensure that little light makes its way down to the forest floor. \u001b\r\n\u001bHowever, there seems to be another, less obvious reason for the lingering \u001b\r\n\u001bdarkness.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "forest",
    },


    {
      "x": 11,
      "y": 38,
      "map": "valewest",
      "name": "\u001bForest River\u001b",
      "description": "\u001bThe river that flows from the west turns south here, trying to escape the\u001b\r\n\u001bclutches of the dark forest in the north. It rushes toward the grasslands to\u001b\r\n\u001bthe south-east in its journey towards the great river Anduin. Sloping gently,\u001b\r\n\u001bthe water carries some debris, but becomes ever more clear as it progresses\u001b\r\n\u001bdownstream. Some gnarled logs create a maze for the water to run through, as if\u001b\r\n\u001bthe logs were the arms of the forest, trying in vain to hinder the river's\u001b\r\n\u001bflow.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "water",
    },


    {
      "x": 11,
      "y": 37,
      "map": "valewest",
      "name": "\u001bBend in the Forest River\u001b",
      "description": "\u001bThe river flows in from the north, abruptly changing course to head eastwards,\u001b\r\n\u001bas if it couldn't make up its mind about what to do. The southern edge of the\u001b\r\n\u001briver bank at the bend is rocky and the water swells and gurgles as it lashes\u001b\r\n\u001bagainst the rocks. A dark forest looms ominously to the south threatening to\u001b\r\n\u001bdevour the river.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "water",
    },

    {
      "x": 11,
      "y": 36,
      "map": "valewest",
      "name": "\u001bDark Forest\u001b",
      "description": "\u001bThe trees in this area of the forest seem younger than in other parts of it,\u001b\r\n\u001bbut still have weathered many seasons. Their lush foliage blocks out any \u001b\r\n\u001bnatural sunlight. The floor of the forest consists mostly of fallen leaves\u001b\r\n\u001band twigs. Smaller plants and shrubs would be choked out by the massive roots\u001b\r\n\u001bof the towering trees, and would not receive the light they need to survive.\u001b\r\n\u001bThe same might be true for other things that enter here.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "forest",
    },


    {
      "x": 11,
      "y": 35,
      "map": "valewest",
      "name": "\u001bDark Forest\u001b",
      "description": "\u001bThe forest is thick as the trees crowd each other. The trees are an even\u001b\r\n\u001bmixture of old, wizened ones and younger, less hardened ones. With the passing\u001b\r\n\u001bwind the tree branches sway, looking as if they are talking to each other; it \u001b\r\n\u001bis almost possible to hear them whispering.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "forest",
    },


    {
      "x": 11,
      "y": 34,
      "map": "valewest",
      "name": "\u001bWolf Glade\u001b",
      "description": "\u001bSeveral large trees stand around this glade, providing a natural cloak. The\u001b\r\n\u001bground is worn and packed, as if many large and heavy creatures had travelled\u001b\r\n\u001bthrough here. Some trees bear large scratches like scars, evidence that some of\u001b\r\n\u001bthe visitors were quite fierce creatures with large, sharp claws. If those\u001b\r\n\u001bcreatures were marking their territory it might be unwise to stay here for\u001b\r\n\u001blong lest they return.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        },
        "up": {
          "door": false
        }
      },
      "terrain": "forest",
    },



    {
      "x": 11,
      "y": 33,
      "map": "valewest",
      "name": "\u001bBleeding Forest\u001b",
      "description": "\u001bThe trees that stand here are worn and scarred, like weary soldiers after a \u001b\r\n\u001bbloody battle. Many other trees lie on the ground; some have been cut, some\u001b\r\n\u001bbroken, and some uprooted altogether. Smaller branches and twigs litter the\u001b\r\n\u001barea, with dead leaves and wood splinters covering the earth. As the wind blows\u001b\r\n\u001bby, it creates a low, resonating sound, with the pitch shifting up as the wind\u001b\r\n\u001bdies down. This place looks like a battlefield, as if the forces of nature had\u001b\r\n\u001bmet in some sort of violent clash, only to have a tragic finale ending in\u001b\r\n\u001bsorrow and despair.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "forest",
    },


  ]);


}())
*/