class Action {
  constructor(type, instructions) {
    this.type = type;
    this.instructions = instructions;
  }
  get() {
    return this;
  }
  attachData(payload) {
    this.data = payload;
  }  
}

class Trigger {
 /**
  * @param {String || RegExp} pattern
  * @param {String || function || Array} response
  */   
  constructor(pattern, response) {
    // 1st - calc and store matchType
    if (pattern instanceof RegExp) {
      this.matchType = 'regExp';
    } else if (typeof(pattern) === 'string') {
      this.matchType = 'subString';
    } else {
      // throw an error
    }
    this.pattern = pattern;
    
    // 2nd - build list of response actions
    this.responseActions = [];
    if (response.constructor === Array) {
      for (var i = 0; i < response.length; i++) {
        const action = this.parseAction(response[i]);
        this.responseActions.push(action);
      }
    } else {
      this.responseActions.push(this.parseAction(response));      
    }
  }
 /**
  * Checks for pattern matches against `data` string and returns relevant
  * Actions if match is found
  * @param {String} data
  */
  check(data) {
    let actions = [];
    if (this.matchType === 'regExp') {
      if (this.pattern.test(data)) { // see if data string contains instance of regex
        actions = actions.concat(this.responseActions);
      } 
    } else if (this.matchType === 'subString') {
      if( data.indexOf(this.pattern) !== -1 ) { // if `pattern` substring exists in data string
        actions = actions.concat(this.responseActions);
      }
    }
    // loop through actions. if any action was passed a callback function, lets attach haystack data to it    
    for (var i = 0; i < actions.length; i++) {
      const action = actions[i];
      if (action.type === 'runCallbackFunction') {
        action.attachData(data);
      }
    }

    return actions;
  }
  parseAction(instructions) {
    let type = '';
    // if its a string
    if( typeof(instructions) === 'string' ) {
      type = 'writeMessage';
    } else if (typeof(instructions) === 'function') {
      type = 'runCallbackFunction';
    } else {
      // throw an error
    }
    const action = new Action(type, instructions);
    return action;
  }
}

class Maintainer {
  constructor(app) {
    this.app = app;
    this.bindDataEvents();
  }
  bindDataEvents() {
    this.app.socket.on('mudOutput', (outputData) => {
      // console.log('mudOutput');
      this.handleMudOutput(outputData);
    });
    this.app.socket.on('userInput', (event) => {
      const inputText = event.detail;
      this.handleUserInput(inputText);
    });
  }
  handleMudOutput(outputData) {
    const actions = this.checkMudOutputTriggers(outputData);
    this.runActions(actions);
  }
  handleUserInput(inputData) {
    // do nothing by default
  }
  checkMudOutputTriggers(outputData) { 
    let actions = [];
    // pull in triggers here
    for (let i = 0; i < this.mudOutputTriggers.length; i++) {
      const thisTriggersActions = this.mudOutputTriggers[i].check(outputData);
      actions = actions.concat( thisTriggersActions );
    } 
    return actions;   
  }  
  runActions(actions) {
    for (var i = 0; i < actions.length; i++) {
      const action = actions[i];

      switch(action.type) {
        case 'writeMessage':
          this.writeToMudServer(action.instructions);
          break;
        case 'runCallbackFunction':
          // we pass call callback functions the haystack by default in case they need to do something with that data. like grabbing room name for currentRoom state update
          action.instructions(action.data);
          break;
      }
    }
  }
  writeToMudServer(command) {
    this.app.socket.emit('finalUserInput', command);
  }   
}

export {Action, Trigger, Maintainer};