import {Action, Trigger, Maintainer} from './Maintainer';

export default class SessionMaintainer extends Maintainer {
  constructor(app) {
    super(app, app);
    // console.log('running SessionMaintainer.constructor()');

    let d = new Date();
    const t = d.getTime();

    // pull state from localStorage through app.state, or use these hard-coded defaults
    this.state = (app.state && app.state['session']) ? app.state.session : {
      previousSessionFound: false,
      xmlModeIsOn: false,
      isLoggedIn: false,          
      heroName:   '',
      clavis: '',       // should be pulled from user config
      time: t, // time of last state update
    };

    this.loadTriggers();
  }

  loadTriggers() {
    // console.log('running SessionMaintainer.loadTriggers()');
    this.mudOutputTriggers = [
      // autologin triggers
      // new Trigger('By what name do you wish to be known?', this.state.characterName),
      // new Trigger('By what name do you wish to be known?', ''),
      // new Trigger('Account pass phrase', this.state.clavis ),
      // new Trigger('Account pass phrase', '' ),

      // new Trigger('seems to have recovered', 'bash'),
      // as soon as I log in, XML mode needs to be turned on for tracking movement in Mapper
      new Trigger('Reconnecting.', [
        'change xml',
        () => {
          this.updateState({'isLoggedIn': true});
        },
      ]),
      new Trigger('Welcome to the land of Middle-earth.', [
        // xml mode is usually off by default when I reconnect
        'change xml',
        // when hero logs in, make them LOOK and check EXITS to refresh mapper data
        // 'look', // NOTE: for some reason, look and exit don't get run from here, but they do from 'XML mode is now on' Trigger 
        // 'exits',
      ]),
      new Trigger('XML mode is now off.', 'change xml'),
      new Trigger('XML mode is now on.', [
          () => {
            this.updateState({ 'xmlModeIsOn': true });
          },
          'look',
          'exit',
      ]),
      // user global triggers
      new Trigger('*** Re', ' '), // automatically skip through page continue prompts
      // user 
    ];    
  }

 /**
  * SessionMaintainer.updateState() does *not* update its own state independantly
  * of the master Controller.state. Every Maintainer has its own updateState() method which
  * passes that Maintainer's data through the master Controller.updateState() method.
  */
  updateState(payload, callback) {
    const key = Object.keys(payload)[0];
    const payloadValue = payload[key];
    // 1. update maintainers internal state data
    this.state[key] = payloadValue;
    const updatedMaintainerState = this.state;
    // 2. update app master state
    this.app.updateState( {'session': updatedMaintainerState } );
  }


}
