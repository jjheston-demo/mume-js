import {Action, Trigger, Maintainer} from './Maintainer';

class Mapper {
  constructor(mapName, x, y) {
    this.map = require('../data/maps/' + mapName + '-master')['default'];
    this.mapName = mapName;
    this.x = x;
    this.y = y;
  }

  // updates x, y, and potentially mapName and map based on 
  trackMovement(movementDirection, roomDepartedData, roomEnteredData) {
    var newX;
    var newY;
    var newMapName;
    var newMap;
    let oldX = this.x;
    let oldY = this.y;
    let oldRoomMaster = this.getRoom({x: oldX, y: oldY });
    let oldMapName = oldRoomMaster.map;

    //----------------------------------------------------------------------------
    // Option 1 - If hard-coded tracking info exits in exit.pipeTo, just use that.
    //----------------------------------------------------------------------------
    // check for hard-coded exits first, which exist as pipes. prop name is 'pipeTo'
    // pipes handle tracking non-obvious movement, like when north isn't just y++
    // pipes also handle an exit which leads to a different map
    if( oldRoomMaster.exits && oldRoomMaster.exits[movementDirection] && oldRoomMaster.exits[movementDirection].pipeTo ) {
    // if( oldRoomMaster.exits[movementDirection].pipeTo ) {
      newX = oldRoomMaster.exits[movementDirection].pipeTo.x;
      newY = oldRoomMaster.exits[movementDirection].pipeTo.y;
      // if map is specified in pipe
      newMapName = (oldRoomMaster.exits[movementDirection].pipeTo.map) ? oldRoomMaster.exits[movementDirection].pipeTo.map : oldMapName ;
      // if map name has changed, load in a different map
      if( newMapName !== oldMapName ) {
        this.map = require('../data/maps/' + newMapName + '-master')['default']

      }

    //---------------------------------------------------------------
    // Option 2 - Try and guess next location based on grid geography
    //---------------------------------------------------------------
    } else {
      ({newX, newY} = this.guessNextLocation(movementDirection, roomDepartedData, roomEnteredData, oldRoomMaster));
      newMapName = oldMapName;
    }

    // if no hard exits,
    this.x = newX;
    this.y = newY;
    this.mapName = newMapName;
    return {newX, newY, newMapName}; 
  }

  guessNextLocation(movementDirection, roomDepartedData, roomEnteredData, oldRoomMaster) {
    // let newX = 0;
    // let newY = 0;
    // let newMapName = 'noc';
    let oldX = oldRoomMaster.x;
    let oldY = oldRoomMaster.y;
    let oldMapName = oldRoomMaster.map;

    let newX = oldX;
    let newY = oldY;
    let newMapName = oldMapName;

    switch(movementDirection) {
      case 'north':
        newY++;
        break;
      case 'south':
        newY--;
        break;
      case 'east':
        newX++;
        break;
      case 'west':
        newX--;
        break;
      case 'up':
        ({ newX, newY } = this.guessNextVerticalLocation(movementDirection, roomDepartedData, roomEnteredData, oldRoomMaster));
        break;
      case 'down':
        ({ newX, newY } = this.guessNextVerticalLocation(movementDirection, roomDepartedData, roomEnteredData, oldRoomMaster));
        break;
    }

    return { newX: newX, newY: newY, newMapName:newMapName, };
  }

  guessNextVerticalLocation(movementDirection, roomDepartedData, roomEnteredData, oldRoomMaster) {
    let oldX = oldRoomMaster.x;
    let oldY = oldRoomMaster.y;

    let newX = oldX;
    let newY = oldY;

    const oppositeDirection = (movementDirection === 'up') ? 'down' : 'up';

    const nRoom = this.getRoom({ x: oldX, y: oldY+1 });
    if (nRoom && nRoom.exits && nRoom.exits[oppositeDirection] ) {
      newY++;
      return {newX, newY};
    }
    const eRoom = this.getRoom({ x: oldX+1, y: oldY });
    if (eRoom && eRoom.exits && eRoom.exits[oppositeDirection] ) {
      newX++;
      return {newX, newY};
    }        
    const sRoom = this.getRoom({ x:oldX, y: oldY-1 });
    if (sRoom && sRoom.exits && sRoom.exits[oppositeDirection] ) {
      newY--;
      return {newX, newY};
    }
    const wRoom = this.getRoom({ x:oldX-1, y:oldY });
    if (wRoom && wRoom.exits && wRoom.exits[oppositeDirection] ) {
      newX--;
      return {newX, newY};
    }  

    return {newX, newY};
  }
  // translate coordinates and grab a copy of a room
  getRoom(props) {  
    let room;
    const map = this.map;
    const { xIndex, yIndex } = this.getXYIndexes(props.x, props.y);
    // notice y comes first, then x
    room = map[yIndex][xIndex];
    return room;
  }
  // get array indexes/keys based on map coordinates (y axis is inverted, normalize neg. values, etc.)
  getXYIndexes(xCoordinate, yCoordinate) {
    const map = this.map;
    // first we need to find maximum x and y values used in our map room coordinates. 
    // these can be +pos or -neg, and are needed to calculate array offset
    const rowCount = map.length;
    const firstRow = map[0];
    const lastRow = map[map.length-1]
    const firstRowFirstRoom = firstRow[0];
    const firstRowRange = firstRow.length;
    const firstRowLastRoom = firstRow[firstRowRange-1]; 
    const lastRowFirstRoom = lastRow[0];
                                     // Some example values from first map 'noc.js'
    const loX = firstRowFirstRoom.x; // -20
    const hiX = firstRowLastRoom.x;  //  30
    const loY = lastRowFirstRoom.y;  // -12
    const hiY = firstRowLastRoom.y;  //  10

    // example values from first map 'noc.js'
    // var xOnMapObj = xRequested + 20;
    // var yOnMapObj = 10 - yRequested;
    const xOnMapObj = xCoordinate - loX;
    const yOnMapObj = hiY - yCoordinate;

    return {xIndex: xOnMapObj, yIndex: yOnMapObj};
  }
  updateState(){}
}


export default class CurrentMapMaintainer extends Maintainer {
  constructor(app) {
    super(app, app);

    let localData = {};
    if( localStorage.getItem('currentMap') ){
      localData = JSON.parse(localStorage.getItem('currentMap'));
    }

    // this.state = (app.state && app.state['currentMap']) ? app.state['currentMap'] : {
    this.state = {
      mapName: (localData.mapName) ? localData.mapName : 'noc',
      x: (localData.x) ? localData.x : 0,
      y: (localData.y) ? localData.y : 0,
      // mapRenderData: (localData.mapName) ? this.getMapFile(localData.mapName + '-render') : this.getMapFile('noc-render'),
    };

    this.mapper = new Mapper(this.state.mapName, this.state.x, this.state.y);

    this.mudOutputTriggers = []; // unfortunately this has to stay here, for now until I update Maintainer constructor with this as an overridable default
  }


  getMapFile(mapName) {
    return require('../data/maps/' + mapName)['default'];
  }

  handleMovement(movementDirection, roomDepartedData, roomEnteredData) {
    let newX, newY, newMapName;
    ( {newX, newY, newMapName} = this.mapper.trackMovement(movementDirection, roomDepartedData, roomEnteredData) );

    this.updateState({
      x: newX,
      y: newY,
      mapName: newMapName,
    });
  }

  updateState(payload, callback) {
    // const key = Object.keys(payload)[0];
    // const payloadValue = payload[key];
    const newState = Object.assign( {}, this.state );

    for (var i = 0; i < Object.keys(payload).length; i++) {
      const updateKey = Object.keys(payload)[i];
      newState[updateKey] = payload[updateKey];
    }
    // update mapper to new x and y - this needs to be moved to Mapper.trackMovement() method... nvm, it has to handle mapper room double clicks too
    this.mapper.x = newState.x;
    this.mapper.y = newState.y;

    // ADD THIS BACK IN BELOW, and remove mapName parameter from mapper room double click handler
    // if mapName isn't set, try and recover
    // if(! newState.mapName ) { // like undefined
      // newState.mapName = this.state.mapName;
    // }

    // if mapName has changed, we need to load in a new renderable map object
    if( this.state.mapName !== newState.mapName ) {
      // newState.mapRenderData = this.getMapFile(newState.mapName + '-render');
    }


    this.app.updateState( {'currentMap': newState} );
    // save new state to localStorage
    var a = JSON.stringify({ x: newState.x, y: newState.y, mapName: newState.mapName });
    localStorage.setItem('currentMap', a);
  }
}