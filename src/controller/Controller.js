import io from 'socket.io-client';

import React from 'react';
import ReactDOM from 'react-dom';
import MudClientReactApp from '../react/MudClientReactApp';

import ControllerBase from './ControllerBase';
import HistoryEntriesMaintainer from './HistoryEntriesMaintainer';
import SessionMaintainer from './SessionMaintainer';
import CurrentRoomMaintainer from './CurrentRoomMaintainer';
import CurrentMapMaintainer from './CurrentMapMaintainer';

// import noc from '../data/maps/noc-master';

export default class Controller extends ControllerBase {
  constructor() {
    super();
    this.socket = new io('http://localhost:3002');

    // try and load a saved master state object from localStorage when browser loads.
    // otherwise, leave it undefined, so the that state is loaded from default data defined in each Maintainer's constructor 
    this.savedState = this.loadState() || undefined; 

    // Create our State Maintainers, which respond to various events including 
    // MUD output and user input. Maintainers update state data *and* run side effects. 
    // In this system, the Maintainers really do all the work for our Controller.
    this.maintainers = {
      session:        new SessionMaintainer(this), // run login commands, handle logout side effects
      historyEntries: new HistoryEntriesMaintainer(this), 
      currentRoom:    new CurrentRoomMaintainer(this),
      currentMap:     new CurrentMapMaintainer(this),
      // hero:           new HeroMaintainer(this),
    };

    this.state = this.savedState || {
      session:        this.maintainers.session.state,
      historyEntries: this.maintainers.historyEntries.state,
      currentRoom:    this.maintainers.currentRoom.state,
      currentMap:     this.maintainers.currentMap.state,
      // hero:       this.maintainers.hero.state
    };
  }

  // input 'props' format should be just like react's .setState() parameter input

  /**
   * Controller.updateState() should 
   * 
   */ 
  updateState(payload, callback) {
    const key = Object.keys(payload)[0]; // BUG!!! payload is throwing an error here when it gets passed in as "null" or "undefined"
    const payloadValue = payload[key];
    // 1. update internal state data
    this.state[key] = payloadValue;
    const updatedAppState = this.state;
    // 1.5 - run callback AFTER state update but BEFORE render
    if(typeof callback === "function") {
      callback();
    }
    // 2. store state in localStorage
    // this.saveState(updatedAppState);

    // 3. finally, always run ReactDOM.render() after state update
    this.render(updatedAppState);
    // this.render(updatedAppState, callback);
  }

  render(updatedAppState, callback) {
    //------------------------------------
    // NEXT: TRANSFORM STATE FOR REACT HERE
    //------------------------------------
    // * currentMap: delete map data objects before passing to react
    //   
    const ui = ReactDOM.render(
      <MudClientReactApp 
        historyEntries={updatedAppState.historyEntries}
        currentMap={updatedAppState.currentMap}
      />,
      document.getElementById('root')
    );
    window.ui = ui;    
  }
  // retrieve and return saved app master state object from localStorage, or return false if no valid state object was found in localStorage
  // eventually may load state from server
  // RIGHT NOW LOADSTATE JUST RETURNS FALSE
  loadState() {
    var localStorageLoaded = false;
    if( localStorageLoaded ) {
      let state = {};
      // add here - update `state` object data from localStorage
      return state;
    } else {
      return false;
    }
  }
  // save app state in local storage
  saveState() {

  }

  getState(propertyKey) {
    return this.state[propertyKey];
  }

}