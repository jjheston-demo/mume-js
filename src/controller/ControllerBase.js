export default class ControllerBase {
  constructor() {

  }
  // This method allows user/developer to submit user input from web browser 
  // console via ctl.input('look'), for example
  input(inputText) {
    this.handleUserInput(inputText);
  }
  // Receive entire user input string and passes it into the system, where 
  // it undergoes command splitting, alias replacement, is added to history, etc.
  handleUserInput(inputText) {
    // dispatch copy of inputText to rest of system so it can be added to
    // history by HistoryEntriesMaintainer
    const userInputEvent = new CustomEvent('userInput', {detail: inputText});
    dispatchEvent(userInputEvent);
    // split inputText into seperate commands by semi-colon
    const inputCommands = inputText.split(';')
    for (var i = 0; i < inputCommands.length; i++) {
      const inputCommand = inputCommands[i];
      this.writeUserInputToMudServer(inputCommand);
    }
  }
  writeUserInputToMudServer(inputCommandText) {
    this.socket.emit('finalUserInput', inputCommandText);
  }
}