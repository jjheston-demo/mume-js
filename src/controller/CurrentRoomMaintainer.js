import {Action, Trigger, Maintainer} from './Maintainer';
import Decoder from '../tools/Decoder';

export default class CurrentRoomMaintainer extends Maintainer {

  constructor(app) {
    super(app, app);

    // make sure to set initial state first, before attaching/creating triggers
    // otherwise useful data gathered by triggers
    // during startup could get overwritten by initialState declaration
    this.state = (app.state && app.state['currentRoom']) ? app.state.currentRoom : {
      name: undefined,
      description: undefined,
      terrain: undefined,
      exits: undefined,
      // matchingGlobalId: undefined,      
    };

    // this.systemEventTriggers = [];

    // load triggers after initial state is established
    this.mudOutputTriggers = [

      // NOTE: when hero logs in, we need to make sure client auto runs 'look' and 'exits'
      // commands to refresh mapper data. This is in our SessionMaintainer's triggers, currently

      // State Trigger 1: MOVEMENT
      new Trigger('<movement dir=', (mudOutputData) => {
        // console.log('MOVEMENT Trigger');
        // NOTE: this trigger needs to in turn trigger Mapper updates

        const regex = /\<movement dir\=([^\/]+)\/\>/;
        const movementDirection = mudOutputData.match(regex)[1];
        // console.log('movementDirection');
        // console.log(movementDirection);
        let roomLeft = Object.assign({}, this.state);
        let room = {};
        room.name = this.extractName(mudOutputData);
        room.description = this.extractDescription(mudOutputData);
        room.exits = this.extractExits(mudOutputData);
        room.terrain = this.extractTerrain(mudOutputData);

        this.updateState({'room': room}, () => {
          const roomEntered = room;
          window.app.maintainers.currentMap.handleMovement(movementDirection, roomLeft, roomEntered);
        });
      }),
      // State Trigger : 'LOOK' COMMAND
        // <room><name> is first chars of 1st line of mudOutput
      new Trigger(/^\<room\>\<name\>/, (mudOutputData) => {
        // console.log('LOOK command Trigger');
        let room = Object.assign({},this.state);
        room.name = this.extractName(mudOutputData);
        room.description = this.extractDescription(mudOutputData);
        room.terrain = this.extractTerrain(mudOutputData);
        this.updateState({'room': room});
      }),
      // State Trigger : 'EXIT' COMMAND
        // if mudOutput's first line's first characters are '<exits>'
      new Trigger(/^\<exits\>/, (mudOutputData) => {
        // console.log('EXIT command Trigger');
        let room = Object.assign({},this.state);
        room.exits = this.extractExits(mudOutputData);
        room.terrain = this.extractTerrain(mudOutputData);        
        this.updateState({'room': room});
      }),
      // State Trigger : 'SEARCH' COMMAND

    ];
  }
  extractName(mudOutputData) {
    const regExp = /\<name\>(.+)\<\/name\>/;
    const plainTextData = Decoder.ansiToPlainText(mudOutputData);
    const matches = plainTextData.match(regExp);
    const name = matches[1];    
    return name;
  }
  extractDescription(mudOutputData) {
    const regExp = /\<description\>([\s\S]*?)<\/description>/gm;
    const plainTextData = Decoder.ansiToPlainText(mudOutputData);
    const matches = plainTextData.match(regExp);
    let desc;
    if (matches) {
      desc = matches[0];
      desc = desc.replace('<description>', '').replace('</description>', '');   
    } else {
      desc = '';
    }
    return desc;
  }
  extractExits(mudOutputData) {
    const regExp = /\<exits\>([\s\S]*?)<\/exits>/gm;
    const plainTextData = Decoder.ansiToPlainText(mudOutputData);
    const matches = plainTextData.match(regExp);
    let exitsString;
    if (matches) {
      exitsString = matches[0].toLowerCase();
    } else {
      exitsString = '';
    }
    var exits = {};
    checkExit('north');
    checkExit('east');
    checkExit('south');
    checkExit('west');
    checkExit('up');
    checkExit('down');
    return exits;

    function checkExit(direction) {
      if (exitsString.indexOf(direction) !== -1) { 
        exits[direction]= {};
        if(exitsString.indexOf('[' + direction + ']') !== -1 || exitsString.indexOf('(' + direction + ')') !== -1 ) { // if [name] or (name) are found
          exits[direction].door = {};
        } else {
          exits[direction].door = false;
        }
      }
    }
  }
  extractTerrain(mudOutputData) {
    const regExp = /\<prompt\>(.+)\<\/prompt\>/;
    // 1. strip color codes and whatever is in necessary using Decoder module
    const plainTextData = Decoder.ansiToPlainText(mudOutputData);
    
    let a;
    if(plainTextData.match(regExp)) {
      a = plainTextData.match(regExp)[1];
    } else {
      a = '';
    }

    // 2. get 2nd visible data character inside prompt (after removing color codes etc.)
    let thisTerrainSymbol = a.substring(2,3);
    // 3. after I have terrainSymbol, match it to terrain name
    const terrainSymbols = {
      '[': 'indoors',
      '#': 'city',
      '.': 'field',
      'f': 'forest',
      '(': 'hills',
      '<': 'mountains',
      // '&lt;': 'mountains',
      '%': 'shallowWater',
      '~': 'water',
      'W': 'rapids',
      'U': 'underwater',
      '+': 'road',
      ':': 'brush',
      '=': 'tunnel',
      'O': 'cavern',
    };
    // make sure matching property name exists for value who stored in thisTerrainSymbol
    let terrain = (!! terrainSymbols[thisTerrainSymbol]) ? terrainSymbols[thisTerrainSymbol]:'';

    return terrain;
  }

  // NOTE: in CurrentRoomMaintainer.updateState(),
    // at the end, after state is updated, we need to notify MapperMaintainer
    // so it can update its data based on CurrentRoom data
  // input 'props' format should be just like react's .setState() parameter input
  updateState(payload, callback) {
    const key = Object.keys(payload)[0];
    const payloadValue = payload[key];

    // if state hasn't changed, don't post a state update to Controller
    // for example, if you LOOK again in a room you've already gathered all room info from
    if( JSON.stringify(payloadValue) === JSON.stringify(this.state) ){
      return false;
    }
    // else...
    // 1. update internal state data
    this.state = payloadValue;
    // this.state[key] = payloadValue;
    // const updatedMaintainerState= this.state;
    // 2. update app master state
    this.app.updateState( {'currentRoom': payloadValue}, callback );
  }
}

