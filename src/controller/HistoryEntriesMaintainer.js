import {Action, Trigger, Maintainer} from './Maintainer';
import Decoder from '../tools/Decoder';

export default class HistoryEntriesMaintainer extends Maintainer {
  constructor(app) {
    super(app, app);
    this.app = app;

    this.maxLen = 100;

    // load state from localStorage or use these hard-coded defaults
    this.state = (app.state && app.state['historyEntries']) ? app.state.historyEntries : [
      new HistoryEntry('appMessage', 'Initializing...'),
    ];
    
  }

  updateState(payload, callback) {
    const key = Object.keys(payload)[0]; // key is always 'historyEntries'
    let payloadValue = payload[key]; // payload value is updated historyEntries state array
    // limit entries to this.maxLen, discarding oldest entries
    if (payloadValue.length > this.maxLen) {
      var difference = payloadValue.length - this.maxLen;
      payloadValue.splice( 0, difference );      
    }

    // 1. update internal state data
    this.state = payloadValue;
    // 2. update app master state
    const updatedMaintainerState = this.state;
    this.app.updateState( {'historyEntries': updatedMaintainerState } );
  }
  handleMudOutput(outputData) {
    const entry = new HistoryEntry('mudOutput', outputData);
    let newHistoryEntries = this.state.slice(0);
    newHistoryEntries.push(entry);
    this.updateState({ 'historyEntries': newHistoryEntries });
  }
  handleUserInput(inputData) {
    const entry = new HistoryEntry('userInput', inputData);
    let newHistoryEntries = this.state.slice(0);
    newHistoryEntries.push(entry);
    this.updateState({ 'historyEntries': newHistoryEntries });
  }
}





class HistoryEntry {
 /**
  * @param {String} type - 'userInput', 'mudOutput', 'appMessage', or 'debugLog'
  */   
  constructor(type, data) {
    /*
     * If type is mudOutput, let's run it through Decoder to 
     * remove artifact characters and render colors. Later we'll use a "transformer"
     * to create a different state object that React will use,
     */
    this.type = type;
    if (type === 'mudOutput') {
      this.data = Decoder.mudOutputToHtml(data);
    } else {
      this.data = data;
    }
    const d = new Date();
    this.time = d.getTime();
    return this;
  }  
}