export default [
  [
    {
      "x": -20,
      "y": 10
    },
    {
      "x": -19,
      "y": 10
    },
    {
      "x": -18,
      "y": 10
    },
    {
      "x": -17,
      "y": 10
    },
    {
      "x": -16,
      "y": 10
    },
    {
      "x": -15,
      "y": 10
    },
    {
      "x": -14,
      "y": 10
    },
    {
      "x": -13,
      "y": 10
    },
    {
      "x": -12,
      "y": 10
    },
    {
      "x": -11,
      "y": 10
    },
    {
      "x": -10,
      "y": 10
    },
    {
      "x": -9,
      "y": 10
    },
    {
      "x": -8,
      "y": 10
    },
    {
      "x": -7,
      "y": 10
    },
    {
      "x": -6,
      "y": 10
    },
    {
      "x": -5,
      "y": 10
    },
    {
      "x": -4,
      "y": 10
    },
    {
      "x": -3,
      "y": 10
    },
    {
      "x": -2,
      "y": 10
    },
    {
      "x": -1,
      "y": 10
    },
    {
      "x": 0,
      "y": 10
    },
    {
      "x": 1,
      "y": 10
    },
    {
      "x": 2,
      "y": 10
    },
    {
      "x": 3,
      "y": 10
    },
    {
      "x": 4,
      "y": 10
    },
    {
      "x": 5,
      "y": 10
    },
    {
      "x": 6,
      "y": 10
    },
    {
      "x": 7,
      "y": 10
    },
    {
      "x": 8,
      "y": 10
    },
    {
      "x": 9,
      "y": 10
    },
    {
      "x": 10,
      "y": 10
    },
    {
      "x": 11,
      "y": 10
    },
    {
      "x": 12,
      "y": 10
    },
    {
      "x": 13,
      "y": 10
    },
    {
      "x": 14,
      "y": 10
    },
    {
      "x": 15,
      "y": 10
    },
    {
      "x": 16,
      "y": 10
    },
    {
      "x": 17,
      "y": 10
    },
    {
      "x": 18,
      "y": 10
    },
    {
      "x": 19,
      "y": 10
    },
    {
      "x": 20,
      "y": 10
    },
    {
      "x": 21,
      "y": 10
    },
    {
      "x": 22,
      "y": 10
    },
    {
      "x": 23,
      "y": 10
    },
    {
      "x": 24,
      "y": 10
    },
    {
      "x": 25,
      "y": 10
    },
    {
      "x": 26,
      "y": 10
    },
    {
      "x": 27,
      "y": 10
    },
    {
      "x": 28,
      "y": 10
    },
    {
      "x": 29,
      "y": 10
    },
    {
      "x": 30,
      "y": 10
    }
  ],
  [
    {
      "x": -20,
      "y": 9
    },
    {
      "x": -19,
      "y": 9
    },
    {
      "x": -18,
      "y": 9
    },
    {
      "x": -17,
      "y": 9
    },
    {
      "x": -16,
      "y": 9
    },
    {
      "x": -15,
      "y": 9
    },
    {
      "x": -14,
      "y": 9
    },
    {
      "x": -13,
      "y": 9
    },
    {
      "x": -12,
      "y": 9
    },
    {
      "x": -11,
      "y": 9
    },
    {
      "x": -10,
      "y": 9
    },
    {
      "x": -9,
      "y": 9
    },
    {
      "x": -8,
      "y": 9
    },
    {
      "x": -7,
      "y": 9
    },
    {
      "x": -6,
      "y": 9
    },
    {
      "x": -5,
      "y": 9
    },
    {
      "x": -4,
      "y": 9
    },
    {
      "x": -3,
      "y": 9
    },
    {
      "x": -2,
      "y": 9
    },
    {
      "x": -1,
      "y": 9
    },
    {
      "x": 0,
      "y": 9
    },
    {
      "x": 1,
      "y": 9
    },
    {
      "x": 2,
      "y": 9
    },
    {
      "x": 3,
      "y": 9
    },
    {
      "x": 4,
      "y": 9
    },
    {
      "x": 5,
      "y": 9
    },
    {
      "x": 6,
      "y": 9
    },
    {
      "x": 7,
      "y": 9
    },
    {
      "x": 8,
      "y": 9
    },
    {
      "x": 9,
      "y": 9
    },
    {
      "x": 10,
      "y": 9
    },
    {
      "x": 11,
      "y": 9
    },
    {
      "x": 12,
      "y": 9
    },
    {
      "x": 13,
      "y": 9
    },
    {
      "x": 14,
      "y": 9
    },
    {
      "x": 15,
      "y": 9
    },
    {
      "x": 16,
      "y": 9
    },
    {
      "x": 17,
      "y": 9
    },
    {
      "x": 18,
      "y": 9
    },
    {
      "x": 19,
      "y": 9
    },
    {
      "x": 20,
      "y": 9
    },
    {
      "x": 21,
      "y": 9
    },
    {
      "x": 22,
      "y": 9
    },
    {
      "x": 23,
      "y": 9
    },
    {
      "x": 24,
      "y": 9
    },
    {
      "x": 25,
      "y": 9
    },
    {
      "x": 26,
      "y": 9
    },
    {
      "x": 27,
      "y": 9
    },
    {
      "x": 28,
      "y": 9
    },
    {
      "x": 29,
      "y": 9
    },
    {
      "x": 30,
      "y": 9
    }
  ],
  [
    {
      "x": -20,
      "y": 8
    },
    {
      "x": -19,
      "y": 8
    },
    {
      "x": -18,
      "y": 8
    },
    {
      "x": -17,
      "y": 8
    },
    {
      "x": -16,
      "y": 8
    },
    {
      "x": -15,
      "y": 8
    },
    {
      "x": -14,
      "y": 8
    },
    {
      "x": -13,
      "y": 8
    },
    {
      "x": -12,
      "y": 8
    },
    {
      "x": -11,
      "y": 8
    },
    {
      "x": -10,
      "y": 8
    },
    {
      "x": -9,
      "y": 8
    },
    {
      "x": -8,
      "y": 8
    },
    {
      "x": -7,
      "y": 8
    },
    {
      "x": -6,
      "y": 8
    },
    {
      "x": -5,
      "y": 8
    },
    {
      "x": -4,
      "y": 8
    },
    {
      "x": -3,
      "y": 8
    },
    {
      "x": -2,
      "y": 8
    },
    {
      "x": -1,
      "y": 8
    },
    {
      "x": 0,
      "y": 8
    },
    {
      "x": 1,
      "y": 8
    },
    {
      "x": 2,
      "y": 8
    },
    {
      "x": 3,
      "y": 8
    },
    {
      "x": 4,
      "y": 8
    },
    {
      "x": 5,
      "y": 8
    },
    {
      "x": 6,
      "y": 8
    },
    {
      "x": 7,
      "y": 8
    },
    {
      "x": 8,
      "y": 8
    },
    {
      "x": 9,
      "y": 8
    },
    {
      "x": 10,
      "y": 8
    },
    {
      "x": 11,
      "y": 8
    },
    {
      "name": "A Cave",
      "terrain": "indoors",
      "exits": {
        "east": {}
      },
      "flags": "",
      "x": 12,
      "y": 8,
      "map": "noc",
      "id": 1000269
    },
    {
      "name": "Goblins' Pass",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 13,
      "y": 8,
      "map": "noc",
      "id": 1000270
    },
    {
      "name": "Goblins' Pass",
      "terrain": "indoors",
      "exits": {
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 14,
      "y": 8,
      "map": "noc",
      "id": 1000271
    },
    {
      "name": "A Small Inlet",
      "terrain": "water",
      "exits": {
        "east": {}
      },
      "flags": "",
      "x": 15,
      "y": 8,
      "map": "noc",
      "id": 1000272
    },
    {
      "name": "Dark Lake",
      "terrain": "water",
      "exits": {
        "east": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 16,
      "y": 8,
      "map": "noc",
      "id": 1000273
    },
    {
      "name": "Dark Lake",
      "terrain": "water",
      "exits": {
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 17,
      "y": 8,
      "map": "noc",
      "id": 1000274
    },
    {
      "name": "A Cave",
      "terrain": "shallow_water",
      "exits": {
        "east": {},
        "south": {}
      },
      "flags": "",
      "x": 18,
      "y": 8,
      "map": "noc",
      "id": 1000275
    },
    {
      "name": "A Cave",
      "terrain": "shallow_water",
      "exits": {
        "west": {}
      },
      "flags": "",
      "x": 19,
      "y": 8,
      "map": "noc",
      "id": 1000276
    },
    {
      "x": 20,
      "y": 8
    },
    {
      "x": 21,
      "y": 8
    },
    {
      "x": 22,
      "y": 8
    },
    {
      "x": 23,
      "y": 8
    },
    {
      "x": 24,
      "y": 8
    },
    {
      "x": 25,
      "y": 8
    },
    {
      "x": 26,
      "y": 8
    },
    {
      "x": 27,
      "y": 8
    },
    {
      "x": 28,
      "y": 8
    },
    {
      "x": 29,
      "y": 8
    },
    {
      "x": 30,
      "y": 8
    }
  ],
  [
    {
      "x": -20,
      "y": 7
    },
    {
      "x": -19,
      "y": 7
    },
    {
      "x": -18,
      "y": 7
    },
    {
      "x": -17,
      "y": 7
    },
    {
      "x": -16,
      "y": 7
    },
    {
      "x": -15,
      "y": 7
    },
    {
      "x": -14,
      "y": 7,
      "name": "Limestone Tunnel",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "up": {
          "pipeTo": {
            "x": -9,
            "y": 4
          }
        }
      },
      "flags": "",
      "map": "noc",
      "id": 1000257
    },
    {
      "name": "Limestone Tunnel",
      "terrain": "tunnel",
      "exits": {
        "west": {},
        "down": {}
      },
      "flags": "",
      "x": -13,
      "y": 7,
      "map": "noc",
      "id": 1000258
    },
    {
      "x": -12,
      "y": 7
    },
    {
      "x": -11,
      "y": 7
    },
    {
      "x": -10,
      "y": 7
    },
    {
      "x": -9,
      "y": 7
    },
    {
      "x": -8,
      "y": 7
    },
    {
      "x": -7,
      "y": 7
    },
    {
      "x": -6,
      "y": 7
    },
    {
      "x": -5,
      "y": 7
    },
    {
      "x": -4,
      "y": 7
    },
    {
      "x": -3,
      "y": 7
    },
    {
      "x": -2,
      "y": 7
    },
    {
      "x": -1,
      "y": 7
    },
    {
      "x": 0,
      "y": 7
    },
    {
      "x": 1,
      "y": 7
    },
    {
      "x": 2,
      "y": 7
    },
    {
      "x": 3,
      "y": 7
    },
    {
      "x": 4,
      "y": 7
    },
    {
      "x": 5,
      "y": 7
    },
    {
      "x": 6,
      "y": 7
    },
    {
      "x": 7,
      "y": 7
    },
    {
      "x": 8,
      "y": 7
    },
    {
      "x": 9,
      "y": 7
    },
    {
      "name": "A Cave",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "south": {}
      },
      "flags": "",
      "x": 10,
      "y": 7,
      "map": "noc",
      "id": 1000259
    },
    {
      "name": "A Cave",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 11,
      "y": 7,
      "map": "noc",
      "id": 1000260
    },
    {
      "name": "A Tunnel",
      "terrain": "indoors",
      "exits": {
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 12,
      "y": 7,
      "map": "noc",
      "id": 1000261
    },
    {
      "name": "Goblins' Pass",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": 13,
      "y": 7,
      "map": "noc",
      "id": 1000262
    },
    {
      "name": "Goblins' Pass",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": 14,
      "y": 7,
      "map": "noc",
      "id": 1000263
    },
    {
      "name": "A Rocky Cove",
      "terrain": "indoors",
      "exits": {
        "east": {}
      },
      "flags": "",
      "x": 15,
      "y": 7,
      "map": "noc",
      "id": 1000264
    },
    {
      "name": "Dark Lake",
      "terrain": "water",
      "exits": {
        "north": {},
        "west": {}
      },
      "flags": "",
      "x": 16,
      "y": 7,
      "map": "noc",
      "id": 1000265
    },
    {
      "name": "A Cave",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {}
      },
      "flags": "",
      "x": 17,
      "y": 7,
      "map": "noc",
      "id": 1000266
    },
    {
      "name": "A Cavern",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 18,
      "y": 7,
      "map": "noc",
      "id": 1000267
    },
    {
      "name": "A Cave",
      "terrain": "indoors",
      "exits": {
        "west": {}
      },
      "flags": "",
      "x": 19,
      "y": 7,
      "map": "noc",
      "id": 1000268
    },
    {
      "x": 20,
      "y": 7
    },
    {
      "x": 21,
      "y": 7
    },
    {
      "x": 22,
      "y": 7
    },
    {
      "x": 23,
      "y": 7
    },
    {
      "x": 24,
      "y": 7
    },
    {
      "x": 25,
      "y": 7
    },
    {
      "x": 26,
      "y": 7
    },
    {
      "x": 27,
      "y": 7
    },
    {
      "x": 28,
      "y": 7
    },
    {
      "x": 29,
      "y": 7
    },
    {
      "x": 30,
      "y": 7
    }
  ],
  [
    {
      "x": -20,
      "y": 6
    },
    {
      "x": -19,
      "y": 6
    },
    {
      "x": -18,
      "y": 6
    },
    {
      "x": -17,
      "y": 6
    },
    {
      "x": -16,
      "y": 6
    },
    {
      "x": -15,
      "y": 6
    },
    {
      "x": -14,
      "y": 6
    },
    {
      "x": -13,
      "y": 6,
      "name": "Darkness",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "up": {
          "pipeTo": {
            "x": -13,
            "y": 7
          }
        }
      },
      "flags": "",
      "map": "noc",
      "id": 1000243
    },
    {
      "x": -12,
      "y": 6,
      "name": "Darkness",
      "terrain": "tunnel",
      "exits": {
        "west": {},
        "down": {
          "pipeTo": {
            "x": -12,
            "y": 5
          }
        }
      },
      "flags": "",
      "map": "noc",
      "id": 1000244
    },
    {
      "x": -11,
      "y": 6
    },
    {
      "x": -10,
      "y": 6
    },
    {
      "x": -9,
      "y": 6
    },
    {
      "name": "A Pool",
      "terrain": "water",
      "exits": {
        "east": {
          "door": true
        },
        "south": {}
      },
      "flags": "",
      "x": -8,
      "y": 6,
      "map": "noc",
      "id": 1000245
    },
    {
      "name": "Hidden Cave",
      "terrain": "water",
      "exits": {
        "west": {
          "door": true
        },
        "up": {
          "pipeTo": {
            "x":82,
            "y":46,
            "map": "mmnorth",
          }
        },
      },
      "flags": "",
      "x": -7,
      "y": 6,
      "map": "noc",
      "id": 1000246
    },
    {
      "x": -6,
      "y": 6
    },
    {
      "x": -5,
      "y": 6
    },
    {
      "x": -4,
      "y": 6
    },
    {
      "x": -3,
      "y": 6
    },
    {
      "x": -2,
      "y": 6
    },
    {
      "x": -1,
      "y": 6
    },
    {
      "x": 0,
      "y": 6
    },
    {
      "x": 1,
      "y": 6
    },
    {
      "x": 2,
      "y": 6
    },
    {
      "x": 3,
      "y": 6
    },
    {
      "x": 4,
      "y": 6
    },
    {
      "x": 5,
      "y": 6
    },
    {
      "x": 6,
      "y": 6
    },
    {
      "x": 7,
      "y": 6
    },
    {
      "x": 8,
      "y": 6
    },
    {
      "x": 9,
      "y": 6
    },
    {
      "name": "A Tunnel",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {},
        "south": {}
      },
      "flags": "",
      "x": 10,
      "y": 6,
      "map": "noc",
      "id": 1000247
    },
    {
      "name": "A Cave",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": 11,
      "y": 6,
      "map": "noc",
      "id": 1000248
    },
    {
      "name": "A Tunnel",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 12,
      "y": 6,
      "map": "noc",
      "id": 1000249
    },
    {
      "name": "Goblins' Pass",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": 13,
      "y": 6,
      "map": "noc",
      "id": 1000250
    },
    {
      "name": "Goblins' Pass",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {}
      },
      "flags": "",
      "x": 14,
      "y": 6,
      "map": "noc",
      "id": 1000251
    },
    {
      "name": "Goblins' Pass",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": 15,
      "y": 6,
      "map": "noc",
      "id": 1000252
    },
    {
      "name": "Goblins' Pass",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": 16,
      "y": 6,
      "map": "noc",
      "id": 1000253
    },
    {
      "name": "Goblins' Pass",
      "terrain": "indoors",
      "exits": {
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 17,
      "y": 6,
      "map": "noc",
      "id": 1000254
    },
    {
      "name": "A Tunnel",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {},
        "south": {
          "door": true
        }
      },
      "flags": "",
      "x": 18,
      "y": 6,
      "map": "noc",
      "id": 1000255
    },
    {
      "name": "A Cave",
      "terrain": "indoors",
      "exits": {
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 19,
      "y": 6,
      "map": "noc",
      "id": 1000256
    },
    {
      "x": 20,
      "y": 6
    },
    {
      "x": 21,
      "y": 6
    },
    {
      "x": 22,
      "y": 6
    },
    {
      "x": 23,
      "y": 6
    },
    {
      "x": 24,
      "y": 6
    },
    {
      "x": 25,
      "y": 6
    },
    {
      "x": 26,
      "y": 6
    },
    {
      "x": 27,
      "y": 6
    },
    {
      "x": 28,
      "y": 6
    },
    {
      "x": 29,
      "y": 6
    },
    {
      "x": 30,
      "y": 6
    }
  ],
  [
    {
      "x": -20,
      "y": 5
    },
    {
      "x": -19,
      "y": 5
    },
    {
      "x": -18,
      "y": 5
    },
    {
      "x": -17,
      "y": 5
    },
    {
      "x": -16,
      "y": 5
    },
    {
      "x": -15,
      "y": 5
    },
    {
      "name": "An Underground Stream",
      "terrain": "shallow_water",
      "exits": {
        "east": {},
        "south": {}
      },
      "flags": "",
      "x": -14,
      "y": 5,
      "map": "noc",
      "id": 1000227
    },
    {
      "name": "An Underground Stream",
      "terrain": "shallow_water",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": -13,
      "y": 5,
      "map": "noc",
      "id": 1000228
    },
    {
      "x": -12,
      "y": 5,
      "name": "Silvery Cavern",
      "terrain": "cavern",
      "exits": {
        "west": {},
        "up": {
          "pipeTo": {
            "x": -12,
            "y": 6
          }
        }
      },
      "flags": "",
      "map": "noc",
      "id": 1000229
    },
    {
      "x": -11,
      "y": 5
    },
    {
      "x": -10,
      "y": 5
    },
    {
      "x": -9,
      "y": 5
    },
    {
      "name": "Wet Tunnel",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": -8,
      "y": 5,
      "map": "noc",
      "id": 1000230
    },
    {
      "x": -7,
      "y": 5
    },
    {
      "name": "Small Cave",
      "terrain": "cavern",
      "exits": {
        "east": {
          "door": true
        },
        "south": {}
      },
      "flags": "",
      "x": -6,
      "y": 5,
      "map": "noc",
      "id": 1000231
    },
    {
      "name": "Orc Guard Room",
      "terrain": "cavern",
      "exits": {
        "south": {
          "door": true
        },
        "west": {
          "door": true
        },
        "up": {
          "pipeTo": {
            "map": "mmnorth",
            "x":87,
            "y":48,
          }
        }
      },
      "flags": "",
      "x": -5,
      "y": 5,
      "map": "noc",
      "id": 1000232
    },
    {
      "x": -4,
      "y": 5
    },
    {
      "x": -3,
      "y": 5
    },
    {
      "x": -2,
      "y": 5
    },
    {
      "x": -1,
      "y": 5
    },
    {
      "x": 0,
      "y": 5
    },
    {
      "x": 1,
      "y": 5
    },
    {
      "x": 2,
      "y": 5
    },
    {
      "x": 3,
      "y": 5
    },
    {
      "x": 4,
      "y": 5
    },
    {
      "x": 5,
      "y": 5
    },
    {
      "x": 6,
      "y": 5
    },
    {
      "x": 7,
      "y": 5
    },
    {
      "x": 8,
      "y": 5
    },
    {
      "x": 9,
      "y": 5
    },
    {
      "name": "Tunnel",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": 10,
      "y": 5,
      "map": "noc",
      "id": 1000233
    },
    {
      "name": "A Cave",
      "terrain": "indoors",
      "exits": {
        "east": {}
      },
      "flags": "",
      "x": 11,
      "y": 5,
      "map": "noc",
      "id": 1000234
    },
    {
      "name": "A Cavern",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 12,
      "y": 5,
      "map": "noc",
      "id": 1000235
    },
    {
      "name": "Goblins' Pass",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {},
        "south": {}
      },
      "flags": "",
      "x": 13,
      "y": 5,
      "map": "noc",
      "id": 1000236
    },
    {
      "name": "A Cave",
      "terrain": "indoors",
      "exits": {
        "west": {}
      },
      "flags": "",
      "x": 14,
      "y": 5,
      "map": "noc",
      "id": 1000237
    },
    {
      "name": "A Tunnel Mouth",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "south": {}
      },
      "flags": "",
      "x": 15,
      "y": 5,
      "map": "noc",
      "id": 1000238
    },
    {
      "name": "A Tunnel",
      "terrain": "indoors",
      "exits": {
        "east": {
          "door": true
        },
        "west": {}
      },
      "flags": "",
      "x": 16,
      "y": 5,
      "map": "noc",
      "id": 1000239
    },
    {
      "name": "Goblins' Pass",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "west": {
          "door": true
        },
        "north": {},
      },
      "flags": "",
      "x": 17,
      "y": 5,
      "map": "noc",
      "id": 1000240
    },
    {
      "name": "Goblins' Pass",
      "terrain": "indoors",
      "exits": {
        "north": {
          "door": true
        },
        "east": {
          "door": true
        },
        "south": {},
        "west": {}
      },
      "x": 18,
      "y": 5,
      "map": "noc",
      "id": 1000241
    },
    {
      "name": "A Cavern",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "west": {
          "door": true
        }
      },
      "flags": "",
      "x": 19,
      "y": 5,
      "map": "noc",
      "id": 1000242
    },
    {
      "x": 20,
      "y": 5
    },
    {
      "x": 21,
      "y": 5
    },
    {
      "x": 22,
      "y": 5
    },
    {
      "x": 23,
      "y": 5
    },
    {
      "x": 24,
      "y": 5
    },
    {
      "x": 25,
      "y": 5
    },
    {
      "x": 26,
      "y": 5
    },
    {
      "x": 27,
      "y": 5
    },
    {
      "x": 28,
      "y": 5
    },
    {
      "x": 29,
      "y": 5
    },
    {
      "x": 30,
      "y": 5
    }
  ],
  [
    {
      "x": -20,
      "y": 4
    },
    {
      "x": -19,
      "y": 4
    },
    {
      "x": -18,
      "y": 4
    },
    {
      "x": -17,
      "y": 4
    },
    {
      "x": -16,
      "y": 4
    },
    {
      "x": -15,
      "y": 4
    },
    {
      "name": "An Underground River",
      "terrain": "shallow_water",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": -14,
      "y": 4,
      "map": "noc",
      "id": 1000202
    },
    {
      "x": -13,
      "y": 4
    },
    {
      "x": -12,
      "y": 4
    },
    {
      "x": -11,
      "y": 4
    },
    {
      "x": -10,
      "y": 4
    },
    {
      "x": -9,
      "y": 4,
      "name": "Limestone Cave",
      "terrain": "cavern",
      "exits": {
        "east": {},
        "down": {
          "pipeTo": {
            "x": -14,
            "y": 7
          }
        }
      },
      "flags": "",
      "map": "noc",
      "id": 1000203
    },
    {
      "x": -8,
      "y": 4,
      "name": "Underground Junction",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {},
        "south": {},
        "west": {},
        "down": {
          "pipeTo": {
            "x": -6,
            "y": 2
          }
        }
      },
      "flags": "",
      "map": "noc",
      "id": 1000204
    },
    {
      "name": "Sloping Tunnel",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": -7,
      "y": 4,
      "map": "noc",
      "id": 1000205
    },
    {
      "name": "A Worn Passageway",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "west": {}
      },
      "flags": "",
      "x": -6,
      "y": 4,
      "map": "noc",
      "id": 1000206
    },
    {
      "name": "Barricade in a Passage",
      "terrain": "tunnel",
      "exits": {
        "north": {
          "door": true
        },
        "east": {}
      },
      "flags": "",
      "x": -5,
      "y": 4,
      "map": "noc",
      "id": 1000207
    },
    {
      "name": "A Steep Tunnel",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": -4,
      "y": 4,
      "map": "noc",
      "id": 1000208
    },
    {
      "name": "A Steep Tunnel",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": -3,
      "y": 4,
      "map": "noc",
      "id": 1000209
    },
    {
      "name": "Sloping Tunnel",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": -2,
      "y": 4,
      "map": "noc",
      "id": 1000210
    },
    {
      "name": "A Sloping Tunnel",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": -1,
      "y": 4,
      "map": "noc",
      "id": 1000211
    },
    {
      "name": "A Sloping Tunnel",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": 0,
      "y": 4,
      "map": "noc",
      "id": 1000212
    },
    {
      "name": "Tunnel Bend",
      "terrain": "tunnel",
      "exits": {
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 1,
      "y": 4,
      "map": "noc",
      "id": 1000213
    },
    {
      "x": 2,
      "y": 4
    },
    {
      "name": "A Blasted Furnace",
      "terrain": "cavern",
      "exits": {
        "south": {}
      },
      "flags": "shop",
      "x": 3,
      "y": 4,
      "map": "noc",
      "id": 1000214
    },
    {
      "x": 4,
      "y": 4
    },
    {
      "x": 5,
      "y": 4
    },
    {
      "x": 6,
      "y": 4
    },
    {
      "name": "Forest Clearing",
      "terrain": "field",
      "exits": {
        "east": {}
      },
      "flags": "",
      "x": 7,
      "y": 4,
      "map": "noc",
      "id": 1000215
    },
    {
      "name": "Damp Cave",
      "terrain": "indoors",
      "exits": {
        "south": {
          "door": true
        },
        "west": {}
      },
      "flags": "",
      "x": 8,
      "y": 4,
      "map": "noc",
      "id": 1000216
    },
    {
      "x": 9,
      "y": 4
    },
    {
      "name": "A Cavern",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {},
        "south": {}
      },
      "flags": "",
      "x": 10,
      "y": 4,
      "map": "noc",
      "id": 1000217
    },
    {
      "name": "A Cave",
      "terrain": "indoors",
      "exits": {
        "west": {}
      },
      "flags": "",
      "x": 11,
      "y": 4,
      "map": "noc",
      "id": 1000218
    },
    {
      "name": "A Cave",
      "terrain": "indoors",
      "exits": {
        "north": {}
      },
      "flags": "",
      "x": 12,
      "y": 4,
      "map": "noc",
      "id": 1000219
    },
    {
      "name": "A Tunnel Mouth",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": 13,
      "y": 4,
      "map": "noc",
      "id": 1000220
    },
    {
      "name": "The Burrow",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "south": {}
      },
      "flags": "",
      "x": 14,
      "y": 4,
      "map": "noc",
      "id": 1000221
    },
    {
      "name": "The Burrow",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": 15,
      "y": 4,
      "map": "noc",
      "id": 1000222
    },
    {
      "name": "The Burrow",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 16,
      "y": 4,
      "map": "noc",
      "id": 1000223
    },
    {
      "name": "The Burrow",
      "terrain": "tunnel",
      "exits": {
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 17,
      "y": 4,
      "map": "noc",
      "id": 1000224
    },
    {
      "name": "Goblins' Pass",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {}
      },
      "flags": "",
      "x": 18,
      "y": 4,
      "map": "noc",
      "id": 1000225
    },
    {
      "name": "Goblins End",
      "terrain": "indoors",
      "exits": {
        "west": {},
        "down": {
          "door": true
        }
      },
      "flags": "watchtower",
      "x": 19,
      "y": 4,
      "map": "noc",
      "id": 1000226
    },
    {
      "x": 20,
      "y": 4
    },
    {
      "x": 21,
      "y": 4
    },
    {
      "x": 22,
      "y": 4
    },
    {
      "x": 23,
      "y": 4
    },
    {
      "x": 24,
      "y": 4
    },
    {
      "x": 25,
      "y": 4
    },
    {
      "x": 26,
      "y": 4
    },
    {
      "x": 27,
      "y": 4
    },
    {
      "x": 28,
      "y": 4
    },
    {
      "x": 29,
      "y": 4
    },
    {
      "x": 30,
      "y": 4
    }
  ],
  [
    {
      "x": -20,
      "y": 3
    },
    {
      "x": -19,
      "y": 3
    },
    {
      "x": -18,
      "y": 3
    },
    {
      "x": -17,
      "y": 3
    },
    {
      "x": -16,
      "y": 3
    },
    {
      "x": -15,
      "y": 3
    },
    {
      "x": -14,
      "y": 3,
      "name": "A Dark Lake",
      "terrain": "shallow_water",
      "exits": {
        "north": {},
        "down": {
          "pipeTo": {
            "x": -13,
            "y": 3
          }
        }
      },
      "flags": "",
      "map": "noc",
      "id": 1000179
    },
    {
      "x": -13,
      "y": 3,
      "name": "A Natural Stairway",
      "terrain": "tunnel",
      "exits": {
        "east": {
          "door": true
        },
        "up": {
          "pipeTo": {
            "x": -14,
            "y": 3
          }
        }
      },
      "flags": "",
      "map": "noc",
      "id": 1000180
    },
    {
      "x": -12,
      "y": 3,
      "name": "Hidden Cavern",
      "terrain": "cavern",
      "exits": {
        "east": {},
        "west": {
          "door": true
        },
        "down": {
          "door": true,
          "pipeTo": {
            "x": -12,
            "y": 2
          }
        }
      },
      "flags": "",
      "map": "noc",
      "id": 1000181
    },
    {
      "name": "A Side Tunnel in the Cavern",
      "terrain": "tunnel",
      "exits": {
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": -11,
      "y": 3,
      "map": "noc",
      "id": 1000182
    },
    {
      "x": -10,
      "y": 3
    },
    {
      "x": -9,
      "y": 3
    },
    {
      "name": "Stone Tunnel",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "south": {
          "door": true
        }
      },
      "flags": "",
      "x": -8,
      "y": 3,
      "map": "noc",
      "id": 1000183
    },
    {
      "x": -7,
      "y": 3
    },
    {
      "x": -6,
      "y": 3
    },
    {
      "x": -5,
      "y": 3
    },
    {
      "x": -4,
      "y": 3
    },
    {
      "name": "A Steep Tunnel",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": -3,
      "y": 3,
      "map": "noc",
      "id": 1000184
    },
    {
      "x": -2,
      "y": 3
    },
    {
      "x": -1,
      "y": 3
    },
    {
      "x": 0,
      "y": 3
    },
    {
      "name": "Tunnel Fork",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "east": {},
        "south": {}
      },
      "flags": "",
      "x": 1,
      "y": 3,
      "map": "noc",
      "id": 1000185
    },
    {
      "name": "East Fork",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": 2,
      "y": 3,
      "map": "noc",
      "id": 1000186
    },
    {
      "name": "Cavern",
      "terrain": "cavern",
      "exits": {
        "north": {},
        "east": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 3,
      "y": 3,
      "map": "noc",
      "id": 1000187
    },
    {
      "name": "A Cavern",
      "terrain": "cavern",
      "exits": {
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 4,
      "y": 3,
      "map": "noc",
      "id": 1000188
    },
    {
      "name": "Golfimbul's Gruesome Grotto",
      "terrain": "cavern",
      "exits": {
        "up": {}
      },
      "flags": "",
      "x": 5,
      "y": 3,
      "map": "noc",
      "id": 1000189
    },
    {
      "x": 6,
      "y": 3
    },
    {
      "x": 7,
      "y": 3
    },
    {
      "name": "A Tunnel Bend",
      "terrain": "tunnel",
      "exits": {
        "north": {
          "door": true
        },
        "east": {},
        "south": {}
      },
      "flags": "",
      "x": 8,
      "y": 3,
      "map": "noc",
      "id": 1000190
    },
    {
      "name": "A Worn Tunnel",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": 9,
      "y": 3,
      "map": "noc",
      "id": 1000191
    },
    {
      "name": "The Burrow",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": 10,
      "y": 3,
      "map": "noc",
      "id": 1000192
    },
    {
      "name": "The Burrow",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 11,
      "y": 3,
      "map": "noc",
      "id": 1000193
    },
    {
      "name": "The Burrow",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": 12,
      "y": 3,
      "map": "noc",
      "id": 1000194
    },
    {
      "name": "The Burrow Intersection",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "east": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 13,
      "y": 3,
      "map": "noc",
      "id": 1000195
    },
    {
      "name": "The Burrow",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": 14,
      "y": 3,
      "map": "noc",
      "id": 1000196
    },
    {
      "name": "A Tunnel Mouth",
      "terrain": "indoors",
      "exits": {
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 15,
      "y": 3,
      "map": "noc",
      "id": 1000197
    },
    {
      "name": "A Tunnel Mouth",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": 16,
      "y": 3,
      "map": "noc",
      "id": 1000198
    },
    {
      "name": "The Burrow",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "east": {}
      },
      "flags": "",
      "x": 17,
      "y": 3,
      "map": "noc",
      "id": 1000199
    },
    {
      "name": "The Burrow",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": 18,
      "y": 3,
      "map": "noc",
      "id": 1000200
    },
    {
      "name": "The Wolf's Gate",
      "terrain": "indoors",
      "exits": {
        "east": {
          "door": true,
          "pipeTo": {
            "map": "goblins2oc",
            "x": 78,
            "y": 41,
          }
        },
        "south": {},
        "west": {},
        "up": {
          "door": true
        }
      },
      "flags": "",
      "x": 19,
      "y": 3,
      "map": "noc",
      "id": 1000201
    },
    {
      "x": 20,
      "y": 3
    },
    {
      "x": 21,
      "y": 3
    },
    {
      "x": 22,
      "y": 3
    },
    {
      "x": 23,
      "y": 3
    },
    {
      "x": 24,
      "y": 3
    },
    {
      "x": 25,
      "y": 3
    },
    {
      "x": 26,
      "y": 3
    },
    {
      "x": 27,
      "y": 3
    },
    {
      "x": 28,
      "y": 3
    },
    {
      "x": 29,
      "y": 3
    },
    {
      "x": 30,
      "y": 3
    }
  ],
  [
    {
      "x": -20,
      "y": 2
    },
    {
      "x": -19,
      "y": 2
    },
    {
      "x": -18,
      "y": 2
    },
    {
      "x": -17,
      "y": 2
    },
    {
      "x": -16,
      "y": 2
    },
    {
      "x": -15,
      "y": 2
    },
    {
      "x": -14,
      "y": 2
    },
    {
      "x": -13,
      "y": 2
    },
    {
      "x": -12,
      "y": 2,
      "name": "Hall of the Arcane",
      "terrain": "cavern",
      "exits": {
        "up": {
          "door": true,
          "pipeTo": {
            "x": -12,
            "y": 3
          }
        }
      },
      "flags": "guildmaster",
      "map": "noc",
      "id": 1000159
    },
    {
      "name": "A Circular Cave",
      "terrain": "cavern",
      "exits": {
        "north": {}
      },
      "flags": "",
      "x": -11,
      "y": 2,
      "map": "noc",
      "id": 1000160
    },
    {
      "x": -10,
      "y": 2
    },
    {
      "x": -9,
      "y": 2
    },
    {
      "name": "Stone Cave",
      "terrain": "indoors",
      "exits": {
        "north": {
          "door": true
        },
        "south": {}
      },
      "flags": "",
      "x": -8,
      "y": 2,
      "map": "noc",
      "id": 1000161
    },
    {
      "x": -7,
      "y": 2
    },
    {
      "x": -6,
      "y": 2,
      "name": "A Long, Sloping Tunnel",
      "terrain": "tunnel",
      "exits": {
        "south": {},
        "up": {
          "pipeTo": {
            "x": -8,
            "y": 4
          }
        },
        "down": {
          "pipeTo": {
            "x": -10,
            "y": -7
          }
        }
      },
      "flags": "",
      "map": "noc",
      "id": 1000162
    },
    {
      "x": -5,
      "y": 2
    },
    {
      "x": -4,
      "y": 2
    },
    {
      "name": "A Steep Tunnel",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": -3,
      "y": 2,
      "map": "noc",
      "id": 1000163
    },
    {
      "x": -2,
      "y": 2
    },
    {
      "x": -1,
      "y": 2
    },
    {
      "name": "Orc Living Area",
      "terrain": "cavern",
      "exits": {
        "east": {},
        "south": {}
      },
      "flags": "",
      "x": 0,
      "y": 2,
      "map": "noc",
      "id": 1000164
    },
    {
      "name": "A Tunnel",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "west": {}
      },
      "flags": "",
      "x": 1,
      "y": 2,
      "map": "noc",
      "id": 1000165
    },
    {
      "x": 2,
      "y": 2
    },
    {
      "name": "Cavern",
      "terrain": "cavern",
      "exits": {
        "north": {},
        "east": {},
        "south": {}
      },
      "flags": "",
      "x": 3,
      "y": 2,
      "map": "noc",
      "id": 1000166
    },
    {
      "name": "Cavern",
      "terrain": "cavern",
      "exits": {
        "north": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 4,
      "y": 2,
      "map": "noc",
      "id": 1000167
    },
    {
      "name": "Torture Chamber",
      "terrain": "cavern",
      "exits": {
        "south": {
          "door": true
        },
        "down": {}
      },
      "flags": "",
      "x": 5,
      "y": 2,
      "map": "noc",
      "id": 1000168
    },
    {
      "x": 6,
      "y": 2
    },
    {
      "x": 7,
      "y": 2
    },
    {
      "name": "A Tunnel",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": 8,
      "y": 2,
      "map": "noc",
      "id": 1000169
    },
    {
      "x": 9,
      "y": 2
    },
    {
      "name": "A Cavern",
      "terrain": "cavern",
      "exits": {
        "east": {}
      },
      "flags": "",
      "x": 10,
      "y": 2,
      "map": "noc",
      "id": 1000170
    },
    {
      "name": "A Natural Crack",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 11,
      "y": 2,
      "map": "noc",
      "id": 1000171
    },
    {
      "x": 12,
      "y": 2
    },
    {
      "name": "A Tunnel Mouth",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {},
        "south": {}
      },
      "flags": "",
      "x": 13,
      "y": 2,
      "map": "noc",
      "id": 1000172
    },
    {
      "name": "A Tunnel",
      "terrain": "indoors",
      "exits": {
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 14,
      "y": 2,
      "map": "noc",
      "id": 1000173
    },
    {
      "name": "A Tunnel",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": 15,
      "y": 2,
      "map": "noc",
      "id": 1000174
    },
    {
      "name": "A Tunnel",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {}
      },
      "flags": "",
      "x": 16,
      "y": 2,
      "map": "noc",
      "id": 1000175
    },
    {
      "name": "A Tunnel",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 17,
      "y": 2,
      "map": "noc",
      "id": 1000176
    },
    {
      "name": "A Cavern",
      "terrain": "indoors",
      "exits": {
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 18,
      "y": 2,
      "map": "noc",
      "id": 1000177
    },
    {
      "name": "Guards Post",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "south": {
          "door": true
        }
      },
      "flags": "",
      "x": 19,
      "y": 2,
      "map": "noc",
      "id": 1000178
    },
    {
      "x": 20,
      "y": 2
    },
    {
      "x": 21,
      "y": 2
    },
    {
      "x": 22,
      "y": 2
    },
    {
      "x": 23,
      "y": 2
    },
    {
      "x": 24,
      "y": 2
    },
    {
      "x": 25,
      "y": 2
    },
    {
      "x": 26,
      "y": 2
    },
    {
      "x": 27,
      "y": 2
    },
    {
      "x": 28,
      "y": 2
    },
    {
      "x": 29,
      "y": 2
    },
    {
      "x": 30,
      "y": 2
    }
  ],
  [
    {
      "x": -20,
      "y": 1
    },
    {
      "x": -19,
      "y": 1
    },
    {
      "x": -18,
      "y": 1
    },
    {
      "x": -17,
      "y": 1
    },
    {
      "x": -16,
      "y": 1
    },
    {
      "x": -15,
      "y": 1
    },
    {
      "x": -14,
      "y": 1
    },
    {
      "x": -13,
      "y": 1
    },
    {
      "x": -12,
      "y": 1
    },
    {
      "x": -11,
      "y": 1
    },
    {
      "x": -10,
      "y": 1
    },
    {
      "x": -9,
      "y": 1
    },
    {
      "name": "Dark Passage",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": -8,
      "y": 1,
      "map": "noc",
      "id": 1000134
    },
    {
      "name": "Lair of the Spider",
      "terrain": "cavern",
      "exits": {
        "south": {}
      },
      "flags": "",
      "x": -7,
      "y": 1,
      "map": "noc",
      "id": 1000135
    },
    {
      "name": "A Long, Twisting Tunnel",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": -6,
      "y": 1,
      "map": "noc",
      "id": 1000136
    },
    {
      "x": -5,
      "y": 1
    },
    {
      "x": -4,
      "y": 1
    },
    {
      "name": "A Tunnel Bend",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "east": {}
      },
      "flags": "",
      "x": -3,
      "y": 1,
      "map": "noc",
      "id": 1000137
    },
    {
      "name": "A Steep Tunnel",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": -2,
      "y": 1,
      "map": "noc",
      "id": 1000138
    },
    {
      "name": "A Tunnel Bend",
      "terrain": "tunnel",
      "exits": {
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": -1,
      "y": 1,
      "map": "noc",
      "id": 1000139
    },
    {
      "name": "Orc Living Area",
      "terrain": "cavern",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": 0,
      "y": 1,
      "map": "noc",
      "id": 1000140
    },
    {
      "x": 1,
      "y": 1
    },
    {
      "name": "The Commissary",
      "terrain": "cavern",
      "exits": {
        "east": {},
        "south": {}
      },
      "flags": "shop",
      "x": 2,
      "y": 1,
      "map": "noc",
      "id": 1000141
    },
    {
      "name": "Cavern",
      "terrain": "cavern",
      "exits": {
        "north": {},
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": 3,
      "y": 1,
      "map": "noc",
      "id": 1000142
    },
    {
      "name": "Cavern",
      "terrain": "cavern",
      "exits": {
        "north": {},
        "east": {},
        "west": {}
      },
      "flags": "water",
      "x": 4,
      "y": 1,
      "map": "noc",
      "id": 1000143
    },
    {
      "name": "A Worn Tunnel",
      "terrain": "tunnel",
      "exits": {
        "north": {
          "door": true
        },
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": 5,
      "y": 1,
      "map": "noc",
      "id": 1000144
    },
    {
      "name": "A Worn Tunnel",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": 6,
      "y": 1,
      "map": "noc",
      "id": 1000145
    },
    {
      "name": "A Worn Tunnel",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": 7,
      "y": 1,
      "map": "noc",
      "id": 1000146
    },
    {
      "name": "A Tunnel Bend",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "west": {}
      },
      "flags": "",
      "x": 8,
      "y": 1,
      "map": "noc",
      "id": 1000147
    },
    {
      "x": 9,
      "y": 1
    },
    {
      "name": "A Dark Pit",
      "terrain": "indoors",
      "exits": {
        "up": {
          "door": true
        }
      },
      "flags": "",
      "x": 10,
      "y": 1,
      "map": "noc",
      "id": 1000148
    },
    {
      "name": "A Tunnel",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {},
        "south": {
          "door": true
        },
        "down": {
          "door": true
        }
      },
      "flags": "",
      "x": 11,
      "y": 1,
      "map": "noc",
      "id": 1000149
    },
    {
      "name": "A Cave",
      "terrain": "indoors",
      "exits": {
        "west": {}
      },
      "flags": "",
      "x": 12,
      "y": 1,
      "map": "noc",
      "id": 1000150
    },
    {
      "name": "A Tunnel",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": 13,
      "y": 1,
      "map": "noc",
      "id": 1000151
    },
    {
      "name": "A Tunnel",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {
          "door": true
        }
      },
      "flags": "",
      "x": 14,
      "y": 1,
      "map": "noc",
      "id": 1000152
    },
    {
      "name": "A Tunnel",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {},
        "south": {},
        "west": {
          "door": true
        }
      },
      "flags": "",
      "x": 15,
      "y": 1,
      "map": "noc",
      "id": 1000153
    },
    {
      "name": "A Cave",
      "terrain": "cavern",
      "exits": {
        "west": {}
      },
      "flags": "",
      "x": 16,
      "y": 1,
      "map": "noc",
      "id": 1000154
    },
    {
      "name": "A Tunnel",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": 17,
      "y": 1,
      "map": "noc",
      "id": 1000155
    },
    {
      "name": "A Cave",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {}
      },
      "flags": "",
      "x": 18,
      "y": 1,
      "map": "noc",
      "id": 1000156
    },
    {
      "name": "A Tunnel",
      "terrain": "indoors",
      "exits": {
        "north": {
          "door": true
        },
        "east": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 19,
      "y": 1,
      "map": "noc",
      "id": 1000157
    },
    {
      "name": "Warg Kennels",
      "terrain": "indoors",
      "exits": {
        "west": {}
      },
      "flags": "guildmaster",
      "x": 20,
      "y": 1,
      "map": "noc",
      "id": 1000158
    },
    {
      "x": 21,
      "y": 1
    },
    {
      "x": 22,
      "y": 1
    },
    {
      "x": 23,
      "y": 1
    },
    {
      "x": 24,
      "y": 1
    },
    {
      "x": 25,
      "y": 1
    },
    {
      "x": 26,
      "y": 1
    },
    {
      "x": 27,
      "y": 1
    },
    {
      "x": 28,
      "y": 1
    },
    {
      "x": 29,
      "y": 1
    },
    {
      "x": 30,
      "y": 1
    }
  ],
  [
    {
      "x": -20,
      "y": 0
    },
    {
      "x": -19,
      "y": 0
    },
    {
      "x": -18,
      "y": 0
    },
    {
      "x": -17,
      "y": 0
    },
    {
      "x": -16,
      "y": 0
    },
    {
      "x": -15,
      "y": 0
    },
    {
      "x": -14,
      "y": 0
    },
    {
      "x": -13,
      "y": 0
    },
    {
      "x": -12,
      "y": 0
    },
    {
      "x": -11,
      "y": 0
    },
    {
      "x": -10,
      "y": 0
    },
    {
      "x": -9,
      "y": 0
    },
    {
      "name": "Webbed Tunnel",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "east": {}
      },
      "flags": "",
      "x": -8,
      "y": 0,
      "map": "noc",
      "id": 1000116
    },
    {
      "name": "Webbed Tunnel",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "west": {}
      },
      "flags": "",
      "x": -7,
      "y": 0,
      "map": "noc",
      "id": 1000117
    },
    {
      "name": "Narrow Tunnel",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": -6,
      "y": 0,
      "map": "noc",
      "id": 1000118
    },
    {
      "x": -5,
      "y": 0
    },
    {
      "x": -4,
      "y": 0
    },
    {
      "x": -3,
      "y": 0
    },
    {
      "x": -2,
      "y": 0
    },
    {
      "name": "A Steep Tunnel",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": -1,
      "y": 0,
      "map": "noc",
      "id": 1000119
    },
    {
      "x": 0,
      "y": 0,
      "name": "Orc Sleeping Warrens",
      "terrain": "cavern",
      "exits": {
        "north": {},
        "down": {
          "pipeTo": {
            "x": 1,
            "y": 0
          }
        }
      },
      "flags": "rent",
      "map": "noc",
      "id": 1000120
    },
    {
      "x": 1,
      "y": 0,
      "name": "Humid Cave",
      "terrain": "cavern",
      "exits": {
        "up": {
          "pipeTo": {
            "x": 0,
            "y": 0
          }
        }
      },
      "flags": "",
      "map": "noc",
      "id": 1000121
    },
    {
      "name": "Slaughter House",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {
          "door": true
        }
      },
      "flags": "",
      "x": 2,
      "y": 0,
      "map": "noc",
      "id": 1000122
    },
    {
      "name": "Slave Pen",
      "terrain": "indoors",
      "exits": {
        "west": {
          "door": true
        }
      },
      "flags": "",
      "x": 3,
      "y": 0,
      "map": "noc",
      "id": 1000123
    },
    {
      "x": 4,
      "y": 0
    },
    {
      "x": 5,
      "y": 0
    },
    {
      "x": 6,
      "y": 0
    },
    {
      "x": 7,
      "y": 0
    },
    {
      "x": 8,
      "y": 0
    },
    {
      "x": 9,
      "y": 0
    },
    {
      "name": "A Cave",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "south": {}
      },
      "flags": "",
      "x": 10,
      "y": 0,
      "map": "noc",
      "id": 1000124
    },
    {
      "name": "A Cave",
      "terrain": "indoors",
      "exits": {
        "north": {
          "door": true
        },
        "east": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 11,
      "y": 0,
      "map": "noc",
      "id": 1000125
    },
    {
      "name": "A Cave",
      "terrain": "indoors",
      "exits": {
        "west": {}
      },
      "flags": "",
      "x": 12,
      "y": 0,
      "map": "noc",
      "id": 1000126
    },
    {
      "name": "A Cavern",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {},
        "south": {}
      },
      "flags": "",
      "x": 13,
      "y": 0,
      "map": "noc",
      "id": 1000127
    },
    {
      "name": "A Tunnel",
      "terrain": "indoors",
      "exits": {
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 14,
      "y": 0,
      "map": "noc",
      "id": 1000128
    },
    {
      "name": "A Tunnel",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {},
        "south": {}
      },
      "flags": "",
      "x": 15,
      "y": 0,
      "map": "noc",
      "id": 1000129
    },
    {
      "name": "A Cave",
      "terrain": "cavern",
      "exits": {
        "west": {}
      },
      "flags": "",
      "x": 16,
      "y": 0,
      "map": "noc",
      "id": 1000130
    },
    {
      "name": "A Tunnel",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {}
      },
      "flags": "",
      "x": 17,
      "y": 0,
      "map": "noc",
      "id": 1000131
    },
    {
      "name": "A Tunnel",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": 18,
      "y": 0,
      "map": "noc",
      "id": 1000132
    },
    {
      "name": "A Tunnel",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "south": {
          "door": true
        },
        "west": {}
      },
      "flags": "",
      "x": 19,
      "y": 0,
      "map": "noc",
      "id": 1000133
    },
    {
      "x": 20,
      "y": 0
    },
    {
      "x": 21,
      "y": 0
    },
    {
      "x": 22,
      "y": 0
    },
    {
      "x": 23,
      "y": 0
    },
    {
      "x": 24,
      "y": 0
    },
    {
      "x": 25,
      "y": 0
    },
    {
      "x": 26,
      "y": 0
    },
    {
      "x": 27,
      "y": 0
    },
    {
      "x": 28,
      "y": 0
    },
    {
      "x": 29,
      "y": 0
    },
    {
      "x": 30,
      "y": 0
    }
  ],
  [
    {
      "x": -20,
      "y": -1
    },
    {
      "x": -19,
      "y": -1
    },
    {
      "x": -18,
      "y": -1
    },
    {
      "x": -17,
      "y": -1
    },
    {
      "x": -16,
      "y": -1
    },
    {
      "x": -15,
      "y": -1
    },
    {
      "x": -14,
      "y": -1
    },
    {
      "x": -13,
      "y": -1
    },
    {
      "x": -12,
      "y": -1
    },
    {
      "x": -11,
      "y": -1
    },
    {
      "x": -10,
      "y": -1
    },
    {
      "x": -9,
      "y": -1
    },
    {
      "x": -8,
      "y": -1
    },
    {
      "x": -7,
      "y": -1
    },
    {
      "name": "Narrow Tunnel",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": -6,
      "y": -1,
      "map": "noc",
      "id": 1000094
    },
    {
      "x": -5,
      "y": -1
    },
    {
      "x": -4,
      "y": -1
    },
    {
      "x": -3,
      "y": -1
    },
    {
      "name": "A Tunnel Bend",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "south": {}
      },
      "flags": "",
      "x": -2,
      "y": -1,
      "map": "noc",
      "id": 1000095
    },
    {
      "name": "A Tunnel Junction",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": -1,
      "y": -1,
      "map": "noc",
      "id": 1000096
    },
    {
      "name": "The Great Hall",
      "terrain": "cavern",
      "exits": {
        "east": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 0,
      "y": -1,
      "map": "noc",
      "id": 1000097
    },
    {
      "name": "The Great Hall",
      "terrain": "cavern",
      "exits": {
        "east": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 1,
      "y": -1,
      "map": "noc",
      "id": 1000098
    },
    {
      "name": "The Great Hall",
      "terrain": "cavern",
      "exits": {
        "east": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 2,
      "y": -1,
      "map": "noc",
      "id": 1000099
    },
    {
      "name": "The Great Hall",
      "terrain": "cavern",
      "exits": {
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 3,
      "y": -1,
      "map": "noc",
      "id": 1000100
    },
    {
      "x": 4,
      "y": -1
    },
    {
      "x": 5,
      "y": -1,
      "name": "A Small Ledge",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "down": {
          "pipeTo": {
            "x": -2,
            "y": -6
          }
        }
      },
      "flags": "",
      "map": "noc",
      "id": 1000101
    },
    {
      "name": "A Narrow Tunnel",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": 6,
      "y": -1,
      "map": "noc",
      "id": 1000102
    },
    {
      "name": "A Narrow Tunnel",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": 7,
      "y": -1,
      "map": "noc",
      "id": 1000103
    },
    {
      "name": "A Narrow Tunnel",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": 8,
      "y": -1,
      "map": "noc",
      "id": 1000104
    },
    {
      "name": "A Narrow Tunnel",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": 9,
      "y": -1,
      "map": "noc",
      "id": 1000105
    },
    {
      "name": "A Cave",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "west": {}
      },
      "flags": "",
      "x": 10,
      "y": -1,
      "map": "noc",
      "id": 1000106
    },
    {
      "name": "A Cave",
      "terrain": "indoors",
      "exits": {
        "north": {}
      },
      "flags": "",
      "x": 11,
      "y": -1,
      "map": "noc",
      "id": 1000107
    },
    {
      "name": "A Cave",
      "terrain": "indoors",
      "exits": {
        "east": {}
      },
      "flags": "",
      "x": 12,
      "y": -1,
      "map": "noc",
      "id": 1000108
    },
    {
      "name": "A Tunnel",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "west": {}
      },
      "flags": "",
      "x": 13,
      "y": -1,
      "map": "noc",
      "id": 1000109
    },
    {
      "name": "A Cave",
      "terrain": "indoors",
      "exits": {
        "north": {}
      },
      "flags": "",
      "x": 14,
      "y": -1,
      "map": "noc",
      "id": 1000110
    },
    {
      "name": "A Tunnel",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {}
      },
      "flags": "",
      "x": 15,
      "y": -1,
      "map": "noc",
      "id": 1000111
    },
    {
      "name": "A Cave",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": 16,
      "y": -1,
      "map": "noc",
      "id": 1000112
    },
    {
      "name": "Guard Cave",
      "terrain": "cavern",
      "exits": {
        "south": {
          "door": true
        },
        "west": {}
      },
      "flags": "",
      "x": 17,
      "y": -1,
      "map": "noc",
      "id": 1000113
    },
    {
      "x": 18,
      "y": -1
    },
    {
      "name": "Dungeon",
      "terrain": "indoors",
      "exits": {
        "north": {
          "door": true
        },
        "down": {
          "door": true
        }
      },
      "flags": "",
      "x": 19,
      "y": -1,
      "map": "noc",
      "id": 1000114
    },
    {
      "name": "A Pit",
      "terrain": "shallow_water",
      "exits": {
        "up": {
          "door": true
        }
      },
      "flags": "",
      "x": 20,
      "y": -1,
      "map": "noc",
      "id": 1000115
    },
    {
      "x": 21,
      "y": -1
    },
    {
      "x": 22,
      "y": -1
    },
    {
      "x": 23,
      "y": -1
    },
    {
      "x": 24,
      "y": -1
    },
    {
      "x": 25,
      "y": -1
    },
    {
      "x": 26,
      "y": -1
    },
    {
      "x": 27,
      "y": -1
    },
    {
      "x": 28,
      "y": -1
    },
    {
      "x": 29,
      "y": -1
    },
    {
      "x": 30,
      "y": -1
    }
  ],
  [
    {
      "x": -20,
      "y": -2
    },
    {
      "x": -19,
      "y": -2
    },
    {
      "x": -18,
      "y": -2
    },
    {
      "x": -17,
      "y": -2
    },
    {
      "x": -16,
      "y": -2
    },
    {
      "x": -15,
      "y": -2
    },
    {
      "x": -14,
      "y": -2
    },
    {
      "x": -13,
      "y": -2
    },
    {
      "x": -12,
      "y": -2
    },
    {
      "x": -11,
      "y": -2
    },
    {
      "x": -10,
      "y": -2
    },
    {
      "x": -9,
      "y": -2
    },
    {
      "x": -8,
      "y": -2
    },
    {
      "x": -7,
      "y": -2
    },
    {
      "x": -6,
      "y": -2,
      "name": "Narrow Tunnel",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "south": {},
        "down": {
          "pipeTo": {
            "x": -2,
            "y": -8
          }
        }
      },
      "flags": "",
      "map": "noc",
      "id": 1000084
    },
    {
      "x": -5,
      "y": -2
    },
    {
      "x": -4,
      "y": -2
    },
    {
      "name": "Hidden Cave",
      "terrain": "indoors",
      "exits": {
        "south": {
          "door": true
        }
      },
      "flags": "guildmaster",
      "x": -3,
      "y": -2,
      "map": "noc",
      "id": 1000085
    },
    {
      "name": "A Steep Tunnel",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": -2,
      "y": -2,
      "map": "noc",
      "id": 1000086
    },
    {
      "x": -1,
      "y": -2
    },
    {
      "name": "The Great Hall",
      "terrain": "cavern",
      "exits": {
        "north": {},
        "east": {},
        "south": {}
      },
      "flags": "",
      "x": 0,
      "y": -2,
      "map": "noc",
      "id": 1000087
    },
    {
      "name": "The Great Hall",
      "terrain": "cavern",
      "exits": {
        "north": {},
        "east": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 1,
      "y": -2,
      "map": "noc",
      "id": 1000088
    },
    {
      "name": "The Great Hall",
      "terrain": "cavern",
      "exits": {
        "north": {},
        "east": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 2,
      "y": -2,
      "map": "noc",
      "id": 1000089
    },
    {
      "name": "The Great Hall",
      "terrain": "cavern",
      "exits": {
        "east": {
          "door": true
        },
        "north": {},
        "west": {},
        "south": {},
      },
      "flags": "",
      "x": 3,
      "y": -2,
      "map": "noc",
      "id": 1000090
    },
    {
      "name": "Treasure Chamber",
      "terrain": "indoors",
      "exits": {
        "west": {
          "door": true
        }
      },
      "flags": "",
      "x": 4,
      "y": -2,
      "map": "noc",
      "id": 1000091
    },
    {
      "x": 5,
      "y": -2
    },
    {
      "x": 6,
      "y": -2
    },
    {
      "x": 7,
      "y": -2
    },
    {
      "x": 8,
      "y": -2
    },
    {
      "x": 9,
      "y": -2
    },
    {
      "x": 10,
      "y": -2
    },
    {
      "x": 11,
      "y": -2
    },
    {
      "x": 12,
      "y": -2
    },
    {
      "x": 13,
      "y": -2
    },
    {
      "x": 14,
      "y": -2
    },
    {
      "x": 15,
      "y": -2
    },
    {
      "x": 16,
      "y": -2
    },
    {
      "name": "Entrance to Mines",
      "terrain": "indoors",
      "exits": {
        "north": {
          "door": true
        },
        "east": {}
      },
      "flags": "",
      "x": 17,
      "y": -2,
      "map": "noc",
      "id": 1000092
    },
    {
      "name": "Mineshaft",
      "terrain": "indoors",
      "exits": {
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 18,
      "y": -2,
      "map": "noc",
      "id": 1000093
    },
    {
      "x": 19,
      "y": -2
    },
    {
      "x": 20,
      "y": -2
    },
    {
      "x": 21,
      "y": -2
    },
    {
      "x": 22,
      "y": -2
    },
    {
      "x": 23,
      "y": -2
    },
    {
      "x": 24,
      "y": -2
    },
    {
      "x": 25,
      "y": -2
    },
    {
      "x": 26,
      "y": -2
    },
    {
      "x": 27,
      "y": -2
    },
    {
      "x": 28,
      "y": -2
    },
    {
      "x": 29,
      "y": -2
    },
    {
      "x": 30,
      "y": -2
    }
  ],
  [
    {
      "x": -20,
      "y": -3
    },
    {
      "x": -19,
      "y": -3
    },
    {
      "x": -18,
      "y": -3
    },
    {
      "x": -17,
      "y": -3
    },
    {
      "x": -16,
      "y": -3
    },
    {
      "x": -15,
      "y": -3
    },
    {
      "x": -14,
      "y": -3
    },
    {
      "x": -13,
      "y": -3
    },
    {
      "x": -12,
      "y": -3
    },
    {
      "x": -11,
      "y": -3
    },
    {
      "x": -10,
      "y": -3
    },
    {
      "x": -9,
      "y": -3
    },
    {
      "x": -8,
      "y": -3
    },
    {
      "x": -7,
      "y": -3
    },
    {
      "name": "Narrow Tunnel",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": -6,
      "y": -3,
      "map": "noc",
      "id": 1000071
    },
    {
      "x": -5,
      "y": -3
    },
    {
      "name": "A Large Cavern",
      "terrain": "cavern",
      "exits": {
        "east": {}
      },
      "flags": "",
      "x": -4,
      "y": -3,
      "map": "noc",
      "id": 1000072
    },
    {
      "name": "Roughly Carved Tunnel",
      "terrain": "tunnel",
      "exits": {
        "north": {
          "door": true
        },
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": -3,
      "y": -3,
      "map": "noc",
      "id": 1000073
    },
    {
      "name": "A Steep Tunnel",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": -2,
      "y": -3,
      "map": "noc",
      "id": 1000074
    },
    {
      "name": "Chamber of Combat",
      "terrain": "cavern",
      "exits": {
        "east": {},
        "south": {}
      },
      "flags": "guildmaster",
      "x": -1,
      "y": -3,
      "map": "noc",
      "id": 1000075
    },
    {
      "name": "The Great Hall",
      "terrain": "cavern",
      "exits": {
        "north": {},
        "east": {},
        "south": {},
        "west": {},
      },
      "flags": "",
      "x": 0,
      "y": -3,
      "map": "noc",
      "id": 1000076
    },
    {
      "x": 1,
      "y": -3,
      "name": "The Great Hall",
      "terrain": "cavern",
      "exits": {
        "down": {
          "door": true,
          "pipeTo": {
            "x": 4,
            "y": -8
          }
        },
        "north": {},
        "east": {},
        "south": {},
        "west": {},
      },
      "flags": "quest",
      "map": "noc",
      "id": 1000077
    },
    {
      "name": "The Great Hall",
      "terrain": "cavern",
      "exits": {
        "north": {},
        "east": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 2,
      "y": -3,
      "map": "noc",
      "id": 1000078
    },
    {
      "name": "The Great Hall",
      "terrain": "cavern",
      "exits": {
        "north": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 3,
      "y": -3,
      "map": "noc",
      "id": 1000079
    },
    {
      "x": 4,
      "y": -3
    },
    {
      "x": 5,
      "y": -3
    },
    {
      "x": 6,
      "y": -3
    },
    {
      "x": 7,
      "y": -3
    },
    {
      "x": 8,
      "y": -3
    },
    {
      "x": 9,
      "y": -3
    },
    {
      "x": 10,
      "y": -3
    },
    {
      "x": 11,
      "y": -3
    },
    {
      "x": 12,
      "y": -3
    },
    {
      "x": 13,
      "y": -3
    },
    {
      "x": 14,
      "y": -3
    },
    {
      "x": 15,
      "y": -3
    },
    {
      "name": "Collapsed Tunnel",
      "terrain": "indoors",
      "exits": {
        "east": {}
      },
      "flags": "",
      "x": 16,
      "y": -3,
      "map": "noc",
      "id": 1000080
    },
    {
      "name": "Mineshaft",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": 17,
      "y": -3,
      "map": "noc",
      "id": 1000081
    },
    {
      "name": "At a Checkpoint",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 18,
      "y": -3,
      "map": "noc",
      "id": 1000082
    },
    {
      "name": "Abandoned Foreman's Office",
      "terrain": "indoors",
      "exits": {
        "south": {
          "door": true
        }
      },
      "flags": "",
      "x": 19,
      "y": -3,
      "map": "noc",
      "id": 1000083
    },
    {
      "x": 20,
      "y": -3
    },
    {
      "x": 21,
      "y": -3
    },
    {
      "x": 22,
      "y": -3
    },
    {
      "x": 23,
      "y": -3
    },
    {
      "x": 24,
      "y": -3
    },
    {
      "x": 25,
      "y": -3
    },
    {
      "x": 26,
      "y": -3
    },
    {
      "x": 27,
      "y": -3
    },
    {
      "x": 28,
      "y": -3
    },
    {
      "x": 29,
      "y": -3
    },
    {
      "x": 30,
      "y": -3
    }
  ],
  [
    {
      "x": -20,
      "y": -4
    },
    {
      "x": -19,
      "y": -4
    },
    {
      "x": -18,
      "y": -4
    },
    {
      "x": -17,
      "y": -4
    },
    {
      "x": -16,
      "y": -4
    },
    {
      "x": -15,
      "y": -4
    },
    {
      "x": -14,
      "y": -4
    },
    {
      "x": -13,
      "y": -4
    },
    {
      "x": -12,
      "y": -4
    },
    {
      "x": -11,
      "y": -4
    },
    {
      "x": -10,
      "y": -4
    },
    {
      "x": -9,
      "y": -4
    },
    {
      "x": -8,
      "y": -4
    },
    {
      "x": -7,
      "y": -4
    },
    {
      "name": "Circular Chamber",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "east": {}
      },
      "flags": "",
      "x": -6,
      "y": -4,
      "map": "noc",
      "id": 1000059
    },
    {
      "name": "Narrow Tunnel",
      "terrain": "tunnel",
      "exits": {
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": -5,
      "y": -4,
      "map": "noc",
      "id": 1000060
    },
    {
      "x": -4,
      "y": -4
    },
    {
      "name": "Demolished Living Quarters",
      "terrain": "indoors",
      "exits": {
        "east": {}
      },
      "flags": "",
      "x": -3,
      "y": -4,
      "map": "noc",
      "id": 1000061
    },
    {
      "name": "A Steep Tunnel",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": -2,
      "y": -4,
      "map": "noc",
      "id": 1000062
    },
    {
      "x": -1,
      "y": -4
    },
    {
      "name": "The Great Hall",
      "terrain": "cavern",
      "exits": {
        "north": {},
        "east": {}
      },
      "flags": "",
      "x": 0,
      "y": -4,
      "map": "noc",
      "id": 1000063
    },
    {
      "name": "The Great Hall",
      "terrain": "cavern",
      "exits": {
        "north": {},
        "east": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 1,
      "y": -4,
      "map": "noc",
      "id": 1000064
    },
    {
      "name": "The Great Hall",
      "terrain": "cavern",
      "exits": {
        "north": {},
        "east": {},
        "south": {
          "door": true
        },
        "west": {}
      },
      "flags": "",
      "x": 2,
      "y": -4,
      "map": "noc",
      "id": 1000065
    },
    {
      "name": "The Great Hall",
      "terrain": "cavern",
      "exits": {
        "north": {},
        "east": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 3,
      "y": -4,
      "map": "noc",
      "id": 1000066
    },
    {
      "name": "Harkz's Horrible Sleeping Chamber",
      "terrain": "cavern",
      "exits": {
        "west": {}
      },
      "flags": "",
      "x": 4,
      "y": -4,
      "map": "noc",
      "id": 1000067
    },
    {
      "x": 5,
      "y": -4
    },
    {
      "x": 6,
      "y": -4
    },
    {
      "x": 7,
      "y": -4
    },
    {
      "x": 8,
      "y": -4
    },
    {
      "x": 9,
      "y": -4
    },
    {
      "x": 10,
      "y": -4
    },
    {
      "x": 11,
      "y": -4
    },
    {
      "x": 12,
      "y": -4
    },
    {
      "x": 13,
      "y": -4
    },
    {
      "x": 14,
      "y": -4
    },
    {
      "x": 15,
      "y": -4
    },
    {
      "x": 16,
      "y": -4
    },
    {
      "name": "Drafty Tunnel",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "up": {
          "pipeTo": {
            "map": "goblins2oc",
            "x":74,
            "y":38,
          }
        }
      },
      "flags": "",
      "x": 17,
      "y": -4,
      "map": "noc",
      "id": 1000068
    },
    {
      "name": "Ore Pit",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 18,
      "y": -4,
      "map": "noc",
      "id": 1000069
    },
    {
      "name": "Mineshaft",
      "terrain": "indoors",
      "exits": {
        "north": {
          "door": true
        },
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 19,
      "y": -4,
      "map": "noc",
      "id": 1000070
    },
    {
      "x": 20,
      "y": -4
    },
    {
      "x": 21,
      "y": -4
    },
    {
      "x": 22,
      "y": -4
    },
    {
      "x": 23,
      "y": -4
    },
    {
      "x": 24,
      "y": -4
    },
    {
      "x": 25,
      "y": -4
    },
    {
      "x": 26,
      "y": -4
    },
    {
      "x": 27,
      "y": -4
    },
    {
      "x": 28,
      "y": -4
    },
    {
      "x": 29,
      "y": -4
    },
    {
      "x": 30,
      "y": -4
    }
  ],
  [
    {
      "x": -20,
      "y": -5
    },
    {
      "x": -19,
      "y": -5
    },
    {
      "x": -18,
      "y": -5
    },
    {
      "x": -17,
      "y": -5
    },
    {
      "x": -16,
      "y": -5
    },
    {
      "x": -15,
      "y": -5
    },
    {
      "name": "Rocky Cave",
      "terrain": "cavern",
      "exits": {
        "east": {
          "door": true
        }
      },
      "flags": "",
      "x": -14,
      "y": -5,
      "map": "noc",
      "id": 1000045
    },
    {
      "name": "Rocky Passage",
      "terrain": "tunnel",
      "exits": {
        "south": {},
        "west": {
          "door": true
        }
      },
      "flags": "",
      "x": -13,
      "y": -5,
      "map": "noc",
      "id": 1000046
    },
    {
      "x": -12,
      "y": -5
    },
    {
      "x": -11,
      "y": -5
    },
    {
      "x": -10,
      "y": -5
    },
    {
      "x": -9,
      "y": -5
    },
    {
      "x": -8,
      "y": -5
    },
    {
      "x": -7,
      "y": -5
    },
    {
      "x": -6,
      "y": -5
    },
    {
      "name": "Caved-in Bend in a Tunnel",
      "terrain": "tunnel",
      "exits": {
        "north": {}
      },
      "flags": "",
      "x": -5,
      "y": -5,
      "map": "noc",
      "id": 1000047
    },
    {
      "name": "Caved-in Tunnel",
      "terrain": "tunnel",
      "exits": {
        "east": {}
      },
      "flags": "",
      "x": -4,
      "y": -5,
      "map": "noc",
      "id": 1000048
    },
    {
      "name": "A Steep Tunnel",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": -3,
      "y": -5,
      "map": "noc",
      "id": 1000049
    },
    {
      "name": "A Tunnel Junction",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": -2,
      "y": -5,
      "map": "noc",
      "id": 1000050
    },
    {
      "name": "The Kennels",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "south": {
          "door": true
        }
      },
      "flags": "",
      "x": -1,
      "y": -5,
      "map": "noc",
      "id": 1000051
    },
    {
      "name": "The Kennels",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "south": {
          "door": true
        },
        "west": {}
      },
      "flags": "",
      "x": 0,
      "y": -5,
      "map": "noc",
      "id": 1000052
    },
    {
      "name": "The Kennels",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "south": {
          "door": true
        },
        "west": {}
      },
      "flags": "",
      "x": 1,
      "y": -5,
      "map": "noc",
      "id": 1000053
    },
    {
      "name": "Guard Barracks",
      "terrain": "indoors",
      "exits": {
        "north": {
          "door": true
        }
      },
      "flags": "",
      "x": 2,
      "y": -5,
      "map": "noc",
      "id": 1000054
    },
    {
      "name": "The Gambling Den",
      "terrain": "cavern",
      "exits": {
        "north": {}
      },
      "flags": "",
      "x": 3,
      "y": -5,
      "map": "noc",
      "id": 1000055
    },
    {
      "x": 4,
      "y": -5
    },
    {
      "x": 5,
      "y": -5
    },
    {
      "x": 6,
      "y": -5
    },
    {
      "x": 7,
      "y": -5
    },
    {
      "x": 8,
      "y": -5
    },
    {
      "x": 9,
      "y": -5
    },
    {
      "x": 10,
      "y": -5
    },
    {
      "x": 11,
      "y": -5
    },
    {
      "x": 12,
      "y": -5
    },
    {
      "x": 13,
      "y": -5
    },
    {
      "x": 14,
      "y": -5
    },
    {
      "x": 15,
      "y": -5
    },
    {
      "x": 16,
      "y": -5
    },
    {
      "name": "Mineshaft",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "south": {}
      },
      "flags": "",
      "x": 17,
      "y": -5,
      "map": "noc",
      "id": 1000056
    },
    {
      "name": "Slave Pits",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": 18,
      "y": -5,
      "map": "noc",
      "id": 1000057
    },
    {
      "name": "Mineshaft",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 19,
      "y": -5,
      "map": "noc",
      "id": 1000058
    },
    {
      "x": 20,
      "y": -5
    },
    {
      "x": 21,
      "y": -5
    },
    {
      "x": 22,
      "y": -5
    },
    {
      "x": 23,
      "y": -5
    },
    {
      "x": 24,
      "y": -5
    },
    {
      "x": 25,
      "y": -5
    },
    {
      "x": 26,
      "y": -5
    },
    {
      "x": 27,
      "y": -5
    },
    {
      "x": 28,
      "y": -5
    },
    {
      "x": 29,
      "y": -5
    },
    {
      "x": 30,
      "y": -5
    }
  ],
  [
    {
      "x": -20,
      "y": -6
    },
    {
      "x": -19,
      "y": -6
    },
    {
      "x": -18,
      "y": -6
    },
    {
      "x": -17,
      "y": -6
    },
    {
      "x": -16,
      "y": -6
    },
    {
      "x": -15,
      "y": -6
    },
    {
      "x": -14,
      "y": -6
    },
    {
      "name": "Rocky Bend",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "east": {}
      },
      "flags": "",
      "x": -13,
      "y": -6,
      "map": "noc",
      "id": 1000035
    },
    {
      "name": "Rocky Passage",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": -12,
      "y": -6,
      "map": "noc",
      "id": 1000036
    },
    {
      "name": "Deep Passage",
      "terrain": "tunnel",
      "exits": {
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": -11,
      "y": -6,
      "map": "noc",
      "id": 1000037
    },
    {
      "x": -10,
      "y": -6
    },
    {
      "x": -9,
      "y": -6
    },
    {
      "x": -8,
      "y": -6
    },
    {
      "x": -7,
      "y": -6
    },
    {
      "x": -6,
      "y": -6
    },
    {
      "x": -5,
      "y": -6
    },
    {
      "x": -4,
      "y": -6
    },
    {
      "x": -3,
      "y": -6
    },
    {
      "x": -2,
      "y": -6,
      "name": "Below the Ledge",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "up": {
          "pipeTo": {
            "x": 5,
            "y": -1
          }
        }
      },
      "flags": "",
      "map": "noc",
      "id": 1000038
    },
    {
      "name": "An Animal Pen",
      "terrain": "indoors",
      "exits": {
        "north": {
          "door": true
        }
      },
      "flags": "",
      "x": -1,
      "y": -6,
      "map": "noc",
      "id": 1000039
    },
    {
      "name": "An Animal Pen",
      "terrain": "indoors",
      "exits": {
        "north": {
          "door": true
        }
      },
      "flags": "",
      "x": 0,
      "y": -6,
      "map": "noc",
      "id": 1000040
    },
    {
      "name": "An Animal Pen",
      "terrain": "indoors",
      "exits": {
        "north": {
          "door": true
        }
      },
      "flags": "",
      "x": 1,
      "y": -6,
      "map": "noc",
      "id": 1000041
    },
    {
      "x": 2,
      "y": -6
    },
    {
      "x": 3,
      "y": -6
    },
    {
      "x": 4,
      "y": -6
    },
    {
      "x": 5,
      "y": -6
    },
    {
      "x": 6,
      "y": -6
    },
    {
      "x": 7,
      "y": -6
    },
    {
      "x": 8,
      "y": -6
    },
    {
      "x": 9,
      "y": -6
    },
    {
      "x": 10,
      "y": -6
    },
    {
      "x": 11,
      "y": -6
    },
    {
      "x": 12,
      "y": -6
    },
    {
      "x": 13,
      "y": -6
    },
    {
      "x": 14,
      "y": -6
    },
    {
      "x": 15,
      "y": -6
    },
    {
      "x": 16,
      "y": -6
    },
    {
      "name": "Cobwebbed Tunnel",
      "terrain": "city",
      "exits": {
        "north": {}
      },
      "flags": "",
      "x": 17,
      "y": -6,
      "map": "noc",
      "id": 1000042
    },
    {
      "name": "Underground Stream",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "south": {}
      },
      "flags": "water",
      "x": 18,
      "y": -6,
      "map": "noc",
      "id": 1000043
    },
    {
      "name": "Sharp Bend",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "west": {}
      },
      "flags": "",
      "x": 19,
      "y": -6,
      "map": "noc",
      "id": 1000044
    },
    {
      "x": 20,
      "y": -6
    },
    {
      "x": 21,
      "y": -6
    },
    {
      "x": 22,
      "y": -6
    },
    {
      "x": 23,
      "y": -6
    },
    {
      "x": 24,
      "y": -6
    },
    {
      "x": 25,
      "y": -6
    },
    {
      "x": 26,
      "y": -6
    },
    {
      "x": 27,
      "y": -6
    },
    {
      "x": 28,
      "y": -6
    },
    {
      "x": 29,
      "y": -6
    },
    {
      "x": 30,
      "y": -6
    }
  ],
  [
    {
      "x": -20,
      "y": -7
    },
    {
      "x": -19,
      "y": -7
    },
    {
      "x": -18,
      "y": -7
    },
    {
      "x": -17,
      "y": -7
    },
    {
      "x": -16,
      "y": -7
    },
    {
      "x": -15,
      "y": -7
    },
    {
      "x": -14,
      "y": -7
    },
    {
      "x": -13,
      "y": -7
    },
    {
      "x": -12,
      "y": -7
    },
    {
      "name": "Passage into the Depths",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "south": {},
        "up": {}
      },
      "flags": "",
      "x": -11,
      "y": -7,
      "map": "noc",
      "id": 1000031
    },
    {
      "x": -10,
      "y": -7,
      "name": "A Deeply Plunging Tunnel",
      "terrain": "tunnel",
      "exits": {
        "up": {
          "pipeTo": {
            "x": -6,
            "y": 2
          }
        },
        "down": {
          "pipeTo": {
            "x": -11,
            "y": -7
          }
        }
      },
      "flags": "",
      "map": "noc",
      "id": 1000032
    },
    {
      "x": -9,
      "y": -7
    },
    {
      "x": -8,
      "y": -7
    },
    {
      "x": -7,
      "y": -7
    },
    {
      "x": -6,
      "y": -7
    },
    {
      "x": -5,
      "y": -7
    },
    {
      "x": -4,
      "y": -7
    },
    {
      "x": -3,
      "y": -7
    },
    {
      "x": -2,
      "y": -7
    },
    {
      "x": -1,
      "y": -7
    },
    {
      "x": 0,
      "y": -7
    },
    {
      "x": 1,
      "y": -7
    },
    {
      "x": 2,
      "y": -7
    },
    {
      "x": 3,
      "y": -7
    },
    {
      "x": 4,
      "y": -7
    },
    {
      "x": 5,
      "y": -7
    },
    {
      "x": 6,
      "y": -7
    },
    {
      "x": 7,
      "y": -7
    },
    {
      "x": 8,
      "y": -7
    },
    {
      "x": 9,
      "y": -7
    },
    {
      "x": 10,
      "y": -7
    },
    {
      "x": 11,
      "y": -7
    },
    {
      "x": 12,
      "y": -7
    },
    {
      "x": 13,
      "y": -7
    },
    {
      "x": 14,
      "y": -7
    },
    {
      "x": 15,
      "y": -7
    },
    {
      "x": 16,
      "y": -7
    },
    {
      "x": 17,
      "y": -7
    },
    {
      "name": "Quarry Overlook",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "east": {}
      },
      "flags": "",
      "x": 18,
      "y": -7,
      "map": "noc",
      "id": 1000033
    },
    {
      "name": "Midst of a Quarry",
      "terrain": "indoors",
      "exits": {
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 19,
      "y": -7,
      "map": "noc",
      "id": 1000034
    },
    {
      "x": 20,
      "y": -7
    },
    {
      "x": 21,
      "y": -7
    },
    {
      "x": 22,
      "y": -7
    },
    {
      "x": 23,
      "y": -7
    },
    {
      "x": 24,
      "y": -7
    },
    {
      "x": 25,
      "y": -7
    },
    {
      "x": 26,
      "y": -7
    },
    {
      "x": 27,
      "y": -7
    },
    {
      "x": 28,
      "y": -7
    },
    {
      "x": 29,
      "y": -7
    },
    {
      "x": 30,
      "y": -7
    }
  ],
  [
    {
      "x": -20,
      "y": -8
    },
    {
      "x": -19,
      "y": -8
    },
    {
      "x": -18,
      "y": -8
    },
    {
      "x": -17,
      "y": -8
    },
    {
      "x": -16,
      "y": -8
    },
    {
      "x": -15,
      "y": -8
    },
    {
      "x": -14,
      "y": -8
    },
    {
      "x": -13,
      "y": -8
    },
    {
      "x": -12,
      "y": -8
    },
    {
      "x": -11,
      "y": -8,
      "name": "Silent Tunnel",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "east": {
          "door": true,
          "pipeTo": {
            "x": -8,
            "y": -8
          }
        },
        "south": {}
      },
      "flags": "",
      "map": "noc",
      "id": 1000022
    },
    {
      "x": -10,
      "y": -8
    },
    {
      "x": -9,
      "y": -8
    },
    {
      "x": -8,
      "y": -8,
      "name": "Side Tunnel",
      "terrain": "tunnel",
      "exits": {
        "south": {},
        "west": {
          "door": true,
          "pipeTo": {
            "x": -11,
            "y": -8
          }
        }
      },
      "flags": "",
      "map": "noc",
      "id": 1000023
    },
    {
      "x": -7,
      "y": -8
    },
    {
      "x": -6,
      "y": -8
    },
    {
      "x": -5,
      "y": -8
    },
    {
      "x": -4,
      "y": -8
    },
    {
      "name": "A Crudely Carved Passage",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "south": {}
      },
      "flags": "",
      "x": -3,
      "y": -8,
      "map": "noc",
      "id": 1000024
    },
    {
      "x": -2,
      "y": -8,
      "name": "Broken Pit",
      "terrain": "indoors",
      "exits": {
        "east": {},
        "west": {},
        "up": {
          "pipeTo": {
            "x": -6,
            "y": -2
          }
        }
      },
      "flags": "",
      "map": "noc",
      "id": 1000025
    },
    {
      "x": -1,
      "y": -8,
      "name": "A Dead End",
      "terrain": "tunnel",
      "exits": {
        "west": {},
        "down": {
          "door": true,
          "pipeTo": {
            "x": 0,
            "y": -8
          }
        }
      },
      "flags": "",
      "map": "noc",
      "id": 1000026
    },
    {
      "name": "Rocky Hole",
      "terrain": "indoors",
      "exits": {
        "up": {
          "door": true
        }
      },
      "flags": "",
      "x": 0,
      "y": -8,
      "map": "noc",
      "id": 1000027
    },
    {
      "x": 1,
      "y": -8
    },
    {
      "name": "Inner Chamber",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": 2,
      "y": -8,
      "map": "noc",
      "id": 1000028
    },
    {
      "x": 3,
      "y": -8
    },
    {
      "x": 4,
      "y": -8,
      "name": "A Small Cave",
      "terrain": "indoors",
      "exits": {
        "south": {},
        "up": {
          "door": true,
          "pipeTo": {
            "x": 1,
            "y": -3
          }
        }
      },
      "flags": "",
      "map": "noc",
      "id": 1000029
    },
    {
      "x": 5,
      "y": -8
    },
    {
      "x": 6,
      "y": -8
    },
    {
      "x": 7,
      "y": -8
    },
    {
      "x": 8,
      "y": -8
    },
    {
      "x": 9,
      "y": -8
    },
    {
      "x": 10,
      "y": -8
    },
    {
      "x": 11,
      "y": -8
    },
    {
      "x": 12,
      "y": -8
    },
    {
      "x": 13,
      "y": -8
    },
    {
      "x": 14,
      "y": -8
    },
    {
      "x": 15,
      "y": -8
    },
    {
      "x": 16,
      "y": -8
    },
    {
      "x": 17,
      "y": -8
    },
    {
      "x": 18,
      "y": -8
    },
    {
      "name": "Narrow Opening",
      "terrain": "indoors",
      "exits": {
        "north": {},
        "down": {}
      },
      "flags": "",
      "x": 19,
      "y": -8,
      "map": "noc",
      "id": 1000030
    },
    {
      "x": 20,
      "y": -8
    },
    {
      "x": 21,
      "y": -8
    },
    {
      "x": 22,
      "y": -8
    },
    {
      "x": 23,
      "y": -8
    },
    {
      "x": 24,
      "y": -8
    },
    {
      "x": 25,
      "y": -8
    },
    {
      "x": 26,
      "y": -8
    },
    {
      "x": 27,
      "y": -8
    },
    {
      "x": 28,
      "y": -8
    },
    {
      "x": 29,
      "y": -8
    },
    {
      "x": 30,
      "y": -8
    }
  ],
  [
    {
      "x": -20,
      "y": -9
    },
    {
      "x": -19,
      "y": -9
    },
    {
      "x": -18,
      "y": -9
    },
    {
      "x": -17,
      "y": -9
    },
    {
      "x": -16,
      "y": -9
    },
    {
      "x": -15,
      "y": -9
    },
    {
      "x": -14,
      "y": -9
    },
    {
      "x": -13,
      "y": -9
    },
    {
      "x": -12,
      "y": -9
    },
    {
      "name": "Bend in the Tunnel",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "east": {}
      },
      "flags": "",
      "x": -11,
      "y": -9,
      "map": "noc",
      "id": 1000010
    },
    {
      "name": "Silent Tunnel",
      "terrain": "tunnel",
      "exits": {
        "up": {
          "door": true
        },
        "west": {},
      },
      "flags": "",
      "x": -10,
      "y": -9,
      "map": "noc",
      "id": 1000011
    },
    {
      "name": "Hidden Room",
      "terrain": "indoors",
      "exits": {
        "down": {
          "door": true
        }
      },
      "flags": "",
      "x": -9,
      "y": -9,
      "map": "noc",
      "id": 1000012
    },
    {
      "name": "Rough Tunnel",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "east": {}
      },
      "flags": "",
      "x": -8,
      "y": -9,
      "map": "noc",
      "id": 1000013
    },
    {
      "name": "End of the Tunnel",
      "terrain": "tunnel",
      "exits": {
        "west": {},
        "down": {}
      },
      "flags": "",
      "x": -7,
      "y": -9,
      "map": "noc",
      "id": 1000014
    },
    {
      "x": -6,
      "y": -9
    },
    {
      "x": -5,
      "y": -9
    },
    {
      "x": -4,
      "y": -9
    },
    {
      "name": "A Crudely Carved Passage",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "south": {}
      },
      "flags": "",
      "x": -3,
      "y": -9,
      "map": "noc",
      "id": 1000015
    },
    {
      "name": "Cave of the Old Trolls",
      "terrain": "indoors",
      "exits": {
        "east": {}
      },
      "flags": "",
      "x": -2,
      "y": -9,
      "map": "noc",
      "id": 1000016
    },
    {
      "name": "Lair of the Trolls",
      "terrain": "cavern",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": -1,
      "y": -9,
      "map": "noc",
      "id": 1000017
    },
    {
      "name": "Lair of the Trolls",
      "terrain": "cavern",
      "exits": {
        "south": {},
        "west": {}
      },
      "flags": "",
      "x": 0,
      "y": -9,
      "map": "noc",
      "id": 1000018
    },
    {
      "x": 1,
      "y": -9
    },
    {
      "name": "Chamber of Despair",
      "terrain": "indoors",
      "exits": {
        "north": {}
      },
      "flags": "",
      "x": 2,
      "y": -9,
      "map": "noc",
      "id": 1000019
    },
    {
      "x": 3,
      "y": -9
    },
    {
      "name": "A Cave",
      "terrain": "indoors",
      "exits": {
        "north": {}
      },
      "flags": "",
      "x": 4,
      "y": -9,
      "map": "noc",
      "id": 1000020
    },
    {
      "x": 5,
      "y": -9
    },
    {
      "x": 6,
      "y": -9
    },
    {
      "x": 7,
      "y": -9
    },
    {
      "x": 8,
      "y": -9
    },
    {
      "x": 9,
      "y": -9
    },
    {
      "x": 10,
      "y": -9
    },
    {
      "x": 11,
      "y": -9
    },
    {
      "x": 12,
      "y": -9
    },
    {
      "x": 13,
      "y": -9
    },
    {
      "x": 14,
      "y": -9
    },
    {
      "x": 15,
      "y": -9
    },
    {
      "x": 16,
      "y": -9
    },
    {
      "x": 17,
      "y": -9
    },
    {
      "x": 18,
      "y": -9
    },
    {
      "x": 19,
      "y": -9,
      "name": "Hall of Murals",
      "terrain": "indoors",
      "exits": {
        "south": {
          "door": true
        },
        "up": {
          "pipeTo": {
            "x": 19,
            "y": -8
          }
        }
      },
      "flags": "",
      "map": "noc",
      "id": 1000021
    },
    {
      "x": 20,
      "y": -9
    },
    {
      "x": 21,
      "y": -9
    },
    {
      "x": 22,
      "y": -9
    },
    {
      "x": 23,
      "y": -9
    },
    {
      "x": 24,
      "y": -9
    },
    {
      "x": 25,
      "y": -9
    },
    {
      "x": 26,
      "y": -9
    },
    {
      "x": 27,
      "y": -9
    },
    {
      "x": 28,
      "y": -9
    },
    {
      "x": 29,
      "y": -9
    },
    {
      "x": 30,
      "y": -9
    }
  ],
  [
    {
      "x": -20,
      "y": -10
    },
    {
      "x": -19,
      "y": -10
    },
    {
      "x": -18,
      "y": -10
    },
    {
      "x": -17,
      "y": -10
    },
    {
      "x": -16,
      "y": -10
    },
    {
      "x": -15,
      "y": -10
    },
    {
      "x": -14,
      "y": -10
    },
    {
      "x": -13,
      "y": -10
    },
    {
      "x": -12,
      "y": -10
    },
    {
      "x": -11,
      "y": -10
    },
    {
      "x": -10,
      "y": -10
    },
    {
      "x": -9,
      "y": -10
    },
    {
      "x": -8,
      "y": -10
    },
    {
      "name": "Narrow Tunnel",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "up": {}
      },
      "flags": "",
      "x": -7,
      "y": -10,
      "map": "noc",
      "id": 1000000
    },
    {
      "name": "A Tunnel",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": -6,
      "y": -10,
      "map": "noc",
      "id": 1000001
    },
    {
      "name": "A Large Cave",
      "terrain": "tunnel",
      "exits": {
        "west": {}
      },
      "flags": "",
      "x": -5,
      "y": -10,
      "map": "noc",
      "id": 1000002
    },
    {
      "x": -4,
      "y": -10
    },
    {
      "name": "A Crudely Carved Passage",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "east": {}
      },
      "flags": "",
      "x": -3,
      "y": -10,
      "map": "noc",
      "id": 1000003
    },
    {
      "name": "A Crudely Carved Passage",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": -2,
      "y": -10,
      "map": "noc",
      "id": 1000004
    },
    {
      "name": "A Filthy Passage",
      "terrain": "tunnel",
      "exits": {
        "east": {},
        "west": {}
      },
      "flags": "",
      "x": -1,
      "y": -10,
      "map": "noc",
      "id": 1000005
    },
    {
      "name": "A Filthy Passage",
      "terrain": "tunnel",
      "exits": {
        "north": {},
        "west": {}
      },
      "flags": "",
      "x": 0,
      "y": -10,
      "map": "noc",
      "id": 1000006
    },
    {
      "x": 1,
      "y": -10
    },
    {
      "x": 2,
      "y": -10
    },
    {
      "x": 3,
      "y": -10
    },
    {
      "x": 4,
      "y": -10
    },
    {
      "x": 5,
      "y": -10
    },
    {
      "x": 6,
      "y": -10
    },
    {
      "x": 7,
      "y": -10
    },
    {
      "x": 8,
      "y": -10
    },
    {
      "x": 9,
      "y": -10
    },
    {
      "x": 10,
      "y": -10
    },
    {
      "x": 11,
      "y": -10
    },
    {
      "x": 12,
      "y": -10
    },
    {
      "x": 13,
      "y": -10
    },
    {
      "x": 14,
      "y": -10
    },
    {
      "x": 15,
      "y": -10
    },
    {
      "x": 16,
      "y": -10
    },
    {
      "x": 17,
      "y": -10
    },
    {
      "x": 18,
      "y": -10
    },
    {
      "name": "Well of the Ancients",
      "terrain": "indoors",
      "exits": {
        "north": {
          "door": true
        },
        "down": {}
      },
      "flags": "",
      "x": 19,
      "y": -10,
      "map": "noc",
      "id": 1000007
    },
    {
      "name": "Down a Well",
      "terrain": "underwater",
      "exits": {
        "up": {},
        "down": {}
      },
      "flags": "",
      "x": 20,
      "y": -10,
      "map": "noc",
      "id": 1000008
    },
    {
      "name": "Well Bottom",
      "terrain": "underwater",
      "exits": {
        "up": {}
      },
      "flags": "treasure",
      "x": 21,
      "y": -10,
      "map": "noc",
      "id": 1000009
    },
    {
      "x": 22,
      "y": -10
    },
    {
      "x": 23,
      "y": -10
    },
    {
      "x": 24,
      "y": -10
    },
    {
      "x": 25,
      "y": -10
    },
    {
      "x": 26,
      "y": -10
    },
    {
      "x": 27,
      "y": -10
    },
    {
      "x": 28,
      "y": -10
    },
    {
      "x": 29,
      "y": -10
    },
    {
      "x": 30,
      "y": -10
    }
  ],
  [
    {
      "x": -20,
      "y": -11
    },
    {
      "x": -19,
      "y": -11
    },
    {
      "x": -18,
      "y": -11
    },
    {
      "x": -17,
      "y": -11
    },
    {
      "x": -16,
      "y": -11
    },
    {
      "x": -15,
      "y": -11
    },
    {
      "x": -14,
      "y": -11
    },
    {
      "x": -13,
      "y": -11
    },
    {
      "x": -12,
      "y": -11
    },
    {
      "x": -11,
      "y": -11
    },
    {
      "x": -10,
      "y": -11
    },
    {
      "x": -9,
      "y": -11
    },
    {
      "x": -8,
      "y": -11
    },
    {
      "x": -7,
      "y": -11
    },
    {
      "x": -6,
      "y": -11
    },
    {
      "x": -5,
      "y": -11
    },
    {
      "x": -4,
      "y": -11
    },
    {
      "x": -3,
      "y": -11
    },
    {
      "x": -2,
      "y": -11
    },
    {
      "x": -1,
      "y": -11
    },
    {
      "x": 0,
      "y": -11
    },
    {
      "x": 1,
      "y": -11
    },
    {
      "x": 2,
      "y": -11
    },
    {
      "x": 3,
      "y": -11
    },
    {
      "x": 4,
      "y": -11
    },
    {
      "x": 5,
      "y": -11
    },
    {
      "x": 6,
      "y": -11
    },
    {
      "x": 7,
      "y": -11
    },
    {
      "x": 8,
      "y": -11
    },
    {
      "x": 9,
      "y": -11
    },
    {
      "x": 10,
      "y": -11
    },
    {
      "x": 11,
      "y": -11
    },
    {
      "x": 12,
      "y": -11
    },
    {
      "x": 13,
      "y": -11
    },
    {
      "x": 14,
      "y": -11
    },
    {
      "x": 15,
      "y": -11
    },
    {
      "x": 16,
      "y": -11
    },
    {
      "x": 17,
      "y": -11
    },
    {
      "x": 18,
      "y": -11
    },
    {
      "x": 19,
      "y": -11
    },
    {
      "x": 20,
      "y": -11
    },
    {
      "x": 21,
      "y": -11
    },
    {
      "x": 22,
      "y": -11
    },
    {
      "x": 23,
      "y": -11
    },
    {
      "x": 24,
      "y": -11
    },
    {
      "x": 25,
      "y": -11
    },
    {
      "x": 26,
      "y": -11
    },
    {
      "x": 27,
      "y": -11
    },
    {
      "x": 28,
      "y": -11
    },
    {
      "x": 29,
      "y": -11
    },
    {
      "x": 30,
      "y": -11
    }
  ],
  [
    {
      "x": -20,
      "y": -12
    },
    {
      "x": -19,
      "y": -12
    },
    {
      "x": -18,
      "y": -12
    },
    {
      "x": -17,
      "y": -12
    },
    {
      "x": -16,
      "y": -12
    },
    {
      "x": -15,
      "y": -12
    },
    {
      "x": -14,
      "y": -12
    },
    {
      "x": -13,
      "y": -12
    },
    {
      "x": -12,
      "y": -12
    },
    {
      "x": -11,
      "y": -12
    },
    {
      "x": -10,
      "y": -12
    },
    {
      "x": -9,
      "y": -12
    },
    {
      "x": -8,
      "y": -12
    },
    {
      "x": -7,
      "y": -12
    },
    {
      "x": -6,
      "y": -12
    },
    {
      "x": -5,
      "y": -12
    },
    {
      "x": -4,
      "y": -12
    },
    {
      "x": -3,
      "y": -12
    },
    {
      "x": -2,
      "y": -12
    },
    {
      "x": -1,
      "y": -12
    },
    {
      "x": 0,
      "y": -12
    },
    {
      "x": 1,
      "y": -12
    },
    {
      "x": 2,
      "y": -12
    },
    {
      "x": 3,
      "y": -12
    },
    {
      "x": 4,
      "y": -12
    },
    {
      "x": 5,
      "y": -12
    },
    {
      "x": 6,
      "y": -12
    },
    {
      "x": 7,
      "y": -12
    },
    {
      "x": 8,
      "y": -12
    },
    {
      "x": 9,
      "y": -12
    },
    {
      "x": 10,
      "y": -12
    },
    {
      "x": 11,
      "y": -12
    },
    {
      "x": 12,
      "y": -12
    },
    {
      "x": 13,
      "y": -12
    },
    {
      "x": 14,
      "y": -12
    },
    {
      "x": 15,
      "y": -12
    },
    {
      "x": 16,
      "y": -12
    },
    {
      "x": 17,
      "y": -12
    },
    {
      "x": 18,
      "y": -12
    },
    {
      "x": 19,
      "y": -12
    },
    {
      "x": 20,
      "y": -12
    },
    {
      "x": 21,
      "y": -12
    },
    {
      "x": 22,
      "y": -12
    },
    {
      "x": 23,
      "y": -12
    },
    {
      "x": 24,
      "y": -12
    },
    {
      "x": 25,
      "y": -12
    },
    {
      "x": 26,
      "y": -12
    },
    {
      "x": 27,
      "y": -12
    },
    {
      "x": 28,
      "y": -12
    },
    {
      "x": 29,
      "y": -12
    },
    {
      "x": 30,
      "y": -12
    }
  ]
]