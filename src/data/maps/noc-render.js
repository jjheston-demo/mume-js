export default [
  [
    {
      "x": -20,
      "y": 10
    },
    {
      "x": -19,
      "y": 10
    },
    {
      "x": -18,
      "y": 10
    },
    {
      "x": -17,
      "y": 10
    },
    {
      "x": -16,
      "y": 10
    },
    {
      "x": -15,
      "y": 10
    },
    {
      "x": -14,
      "y": 10
    },
    {
      "x": -13,
      "y": 10
    },
    {
      "x": -12,
      "y": 10
    },
    {
      "x": -11,
      "y": 10
    },
    {
      "x": -10,
      "y": 10
    },
    {
      "x": -9,
      "y": 10
    },
    {
      "x": -8,
      "y": 10
    },
    {
      "x": -7,
      "y": 10
    },
    {
      "x": -6,
      "y": 10
    },
    {
      "x": -5,
      "y": 10
    },
    {
      "x": -4,
      "y": 10
    },
    {
      "x": -3,
      "y": 10
    },
    {
      "x": -2,
      "y": 10
    },
    {
      "x": -1,
      "y": 10
    },
    {
      "x": 0,
      "y": 10
    },
    {
      "x": 1,
      "y": 10
    },
    {
      "x": 2,
      "y": 10
    },
    {
      "x": 3,
      "y": 10
    },
    {
      "x": 4,
      "y": 10
    },
    {
      "x": 5,
      "y": 10
    },
    {
      "x": 6,
      "y": 10
    },
    {
      "x": 7,
      "y": 10
    },
    {
      "x": 8,
      "y": 10
    },
    {
      "x": 9,
      "y": 10
    },
    {
      "x": 10,
      "y": 10
    },
    {
      "x": 11,
      "y": 10
    },
    {
      "x": 12,
      "y": 10
    },
    {
      "x": 13,
      "y": 10
    },
    {
      "x": 14,
      "y": 10
    },
    {
      "x": 15,
      "y": 10
    },
    {
      "x": 16,
      "y": 10
    },
    {
      "x": 17,
      "y": 10
    },
    {
      "x": 18,
      "y": 10
    },
    {
      "x": 19,
      "y": 10
    },
    {
      "x": 20,
      "y": 10
    },
    {
      "x": 21,
      "y": 10
    },
    {
      "x": 22,
      "y": 10
    },
    {
      "x": 23,
      "y": 10
    },
    {
      "x": 24,
      "y": 10
    },
    {
      "x": 25,
      "y": 10
    },
    {
      "x": 26,
      "y": 10
    },
    {
      "x": 27,
      "y": 10
    },
    {
      "x": 28,
      "y": 10
    },
    {
      "x": 29,
      "y": 10
    },
    {
      "x": 30,
      "y": 10
    }
  ],
  [
    {
      "x": -20,
      "y": 9
    },
    {
      "x": -19,
      "y": 9
    },
    {
      "x": -18,
      "y": 9
    },
    {
      "x": -17,
      "y": 9
    },
    {
      "x": -16,
      "y": 9
    },
    {
      "x": -15,
      "y": 9
    },
    {
      "x": -14,
      "y": 9
    },
    {
      "x": -13,
      "y": 9
    },
    {
      "x": -12,
      "y": 9
    },
    {
      "x": -11,
      "y": 9
    },
    {
      "x": -10,
      "y": 9
    },
    {
      "x": -9,
      "y": 9
    },
    {
      "x": -8,
      "y": 9
    },
    {
      "x": -7,
      "y": 9
    },
    {
      "x": -6,
      "y": 9
    },
    {
      "x": -5,
      "y": 9
    },
    {
      "x": -4,
      "y": 9
    },
    {
      "x": -3,
      "y": 9
    },
    {
      "x": -2,
      "y": 9
    },
    {
      "x": -1,
      "y": 9
    },
    {
      "x": 0,
      "y": 9
    },
    {
      "x": 1,
      "y": 9
    },
    {
      "x": 2,
      "y": 9
    },
    {
      "x": 3,
      "y": 9
    },
    {
      "x": 4,
      "y": 9
    },
    {
      "x": 5,
      "y": 9
    },
    {
      "x": 6,
      "y": 9
    },
    {
      "x": 7,
      "y": 9
    },
    {
      "x": 8,
      "y": 9
    },
    {
      "x": 9,
      "y": 9
    },
    {
      "x": 10,
      "y": 9
    },
    {
      "x": 11,
      "y": 9
    },
    {
      "x": 12,
      "y": 9
    },
    {
      "x": 13,
      "y": 9
    },
    {
      "x": 14,
      "y": 9
    },
    {
      "x": 15,
      "y": 9
    },
    {
      "x": 16,
      "y": 9
    },
    {
      "x": 17,
      "y": 9
    },
    {
      "x": 18,
      "y": 9
    },
    {
      "x": 19,
      "y": 9
    },
    {
      "x": 20,
      "y": 9
    },
    {
      "x": 21,
      "y": 9
    },
    {
      "x": 22,
      "y": 9
    },
    {
      "x": 23,
      "y": 9
    },
    {
      "x": 24,
      "y": 9
    },
    {
      "x": 25,
      "y": 9
    },
    {
      "x": 26,
      "y": 9
    },
    {
      "x": 27,
      "y": 9
    },
    {
      "x": 28,
      "y": 9
    },
    {
      "x": 29,
      "y": 9
    },
    {
      "x": 30,
      "y": 9
    }
  ],
  [
    {
      "x": -20,
      "y": 8
    },
    {
      "x": -19,
      "y": 8
    },
    {
      "x": -18,
      "y": 8
    },
    {
      "x": -17,
      "y": 8
    },
    {
      "x": -16,
      "y": 8
    },
    {
      "x": -15,
      "y": 8
    },
    {
      "x": -14,
      "y": 8
    },
    {
      "x": -13,
      "y": 8
    },
    {
      "x": -12,
      "y": 8
    },
    {
      "x": -11,
      "y": 8
    },
    {
      "x": -10,
      "y": 8
    },
    {
      "x": -9,
      "y": 8
    },
    {
      "x": -8,
      "y": 8
    },
    {
      "x": -7,
      "y": 8
    },
    {
      "x": -6,
      "y": 8
    },
    {
      "x": -5,
      "y": 8
    },
    {
      "x": -4,
      "y": 8
    },
    {
      "x": -3,
      "y": 8
    },
    {
      "x": -2,
      "y": 8
    },
    {
      "x": -1,
      "y": 8
    },
    {
      "x": 0,
      "y": 8
    },
    {
      "x": 1,
      "y": 8
    },
    {
      "x": 2,
      "y": 8
    },
    {
      "x": 3,
      "y": 8
    },
    {
      "x": 4,
      "y": 8
    },
    {
      "x": 5,
      "y": 8
    },
    {
      "x": 6,
      "y": 8
    },
    {
      "x": 7,
      "y": 8
    },
    {
      "x": 8,
      "y": 8
    },
    {
      "x": 9,
      "y": 8
    },
    {
      "x": 10,
      "y": 8
    },
    {
      "x": 11,
      "y": 8
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 12,
      "y": 8,
      "exits": "",
      "walls": "north south west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 13,
      "y": 8,
      "exits": "",
      "walls": "north",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 14,
      "y": 8,
      "exits": "",
      "walls": "north east",
      "doors": ""
    },
    {
      "terrain": "water",
      "flags": "",
      "x": 15,
      "y": 8,
      "exits": "",
      "walls": "north south west",
      "doors": ""
    },
    {
      "terrain": "water",
      "flags": "",
      "x": 16,
      "y": 8,
      "exits": "",
      "walls": "north",
      "doors": ""
    },
    {
      "terrain": "water",
      "flags": "",
      "x": 17,
      "y": 8,
      "exits": "",
      "walls": "north east",
      "doors": ""
    },
    {
      "terrain": "shallow_water",
      "flags": "",
      "x": 18,
      "y": 8,
      "exits": "",
      "walls": "north west",
      "doors": ""
    },
    {
      "terrain": "shallow_water",
      "flags": "",
      "x": 19,
      "y": 8,
      "exits": "",
      "walls": "north east south",
      "doors": ""
    },
    {
      "x": 20,
      "y": 8
    },
    {
      "x": 21,
      "y": 8
    },
    {
      "x": 22,
      "y": 8
    },
    {
      "x": 23,
      "y": 8
    },
    {
      "x": 24,
      "y": 8
    },
    {
      "x": 25,
      "y": 8
    },
    {
      "x": 26,
      "y": 8
    },
    {
      "x": 27,
      "y": 8
    },
    {
      "x": 28,
      "y": 8
    },
    {
      "x": 29,
      "y": 8
    },
    {
      "x": 30,
      "y": 8
    }
  ],
  [
    {
      "x": -20,
      "y": 7
    },
    {
      "x": -19,
      "y": 7
    },
    {
      "x": -18,
      "y": 7
    },
    {
      "x": -17,
      "y": 7
    },
    {
      "x": -16,
      "y": 7
    },
    {
      "x": -15,
      "y": 7
    },
    {
      "x": -14,
      "y": 7,
      "terrain": "tunnel",
      "flags": "",
      "exits": "up",
      "walls": "north south west",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -13,
      "y": 7,
      "exits": "down",
      "walls": "north east south",
      "doors": ""
    },
    {
      "x": -12,
      "y": 7
    },
    {
      "x": -11,
      "y": 7
    },
    {
      "x": -10,
      "y": 7
    },
    {
      "x": -9,
      "y": 7
    },
    {
      "x": -8,
      "y": 7
    },
    {
      "x": -7,
      "y": 7
    },
    {
      "x": -6,
      "y": 7
    },
    {
      "x": -5,
      "y": 7
    },
    {
      "x": -4,
      "y": 7
    },
    {
      "x": -3,
      "y": 7
    },
    {
      "x": -2,
      "y": 7
    },
    {
      "x": -1,
      "y": 7
    },
    {
      "x": 0,
      "y": 7
    },
    {
      "x": 1,
      "y": 7
    },
    {
      "x": 2,
      "y": 7
    },
    {
      "x": 3,
      "y": 7
    },
    {
      "x": 4,
      "y": 7
    },
    {
      "x": 5,
      "y": 7
    },
    {
      "x": 6,
      "y": 7
    },
    {
      "x": 7,
      "y": 7
    },
    {
      "x": 8,
      "y": 7
    },
    {
      "x": 9,
      "y": 7
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 10,
      "y": 7,
      "exits": "",
      "walls": "north west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 11,
      "y": 7,
      "exits": "",
      "walls": "north",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 12,
      "y": 7,
      "exits": "",
      "walls": "north east",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 13,
      "y": 7,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 14,
      "y": 7,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 15,
      "y": 7,
      "exits": "",
      "walls": "north south west",
      "doors": ""
    },
    {
      "terrain": "water",
      "flags": "",
      "x": 16,
      "y": 7,
      "exits": "",
      "walls": "east south",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 17,
      "y": 7,
      "exits": "",
      "walls": "south west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 18,
      "y": 7,
      "exits": "",
      "walls": "",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 19,
      "y": 7,
      "exits": "",
      "walls": "north east south",
      "doors": ""
    },
    {
      "x": 20,
      "y": 7
    },
    {
      "x": 21,
      "y": 7
    },
    {
      "x": 22,
      "y": 7
    },
    {
      "x": 23,
      "y": 7
    },
    {
      "x": 24,
      "y": 7
    },
    {
      "x": 25,
      "y": 7
    },
    {
      "x": 26,
      "y": 7
    },
    {
      "x": 27,
      "y": 7
    },
    {
      "x": 28,
      "y": 7
    },
    {
      "x": 29,
      "y": 7
    },
    {
      "x": 30,
      "y": 7
    }
  ],
  [
    {
      "x": -20,
      "y": 6
    },
    {
      "x": -19,
      "y": 6
    },
    {
      "x": -18,
      "y": 6
    },
    {
      "x": -17,
      "y": 6
    },
    {
      "x": -16,
      "y": 6
    },
    {
      "x": -15,
      "y": 6
    },
    {
      "x": -14,
      "y": 6
    },
    {
      "x": -13,
      "y": 6,
      "terrain": "tunnel",
      "flags": "",
      "exits": "up",
      "walls": "north south west",
      "doors": ""
    },
    {
      "x": -12,
      "y": 6,
      "terrain": "tunnel",
      "flags": "",
      "exits": "down",
      "walls": "north east south",
      "doors": ""
    },
    {
      "x": -11,
      "y": 6
    },
    {
      "x": -10,
      "y": 6
    },
    {
      "x": -9,
      "y": 6
    },
    {
      "terrain": "water",
      "flags": "",
      "x": -8,
      "y": 6,
      "exits": "",
      "walls": "north west",
      "doors": "east"
    },
    {
      "terrain": "water",
      "flags": "",
      "x": -7,
      "y": 6,
      "exits": "up",
      "walls": "north east south",
      "doors": "west"
    },
    {
      "x": -6,
      "y": 6
    },
    {
      "x": -5,
      "y": 6
    },
    {
      "x": -4,
      "y": 6
    },
    {
      "x": -3,
      "y": 6
    },
    {
      "x": -2,
      "y": 6
    },
    {
      "x": -1,
      "y": 6
    },
    {
      "x": 0,
      "y": 6
    },
    {
      "x": 1,
      "y": 6
    },
    {
      "x": 2,
      "y": 6
    },
    {
      "x": 3,
      "y": 6
    },
    {
      "x": 4,
      "y": 6
    },
    {
      "x": 5,
      "y": 6
    },
    {
      "x": 6,
      "y": 6
    },
    {
      "x": 7,
      "y": 6
    },
    {
      "x": 8,
      "y": 6
    },
    {
      "x": 9,
      "y": 6
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 10,
      "y": 6,
      "exits": "",
      "walls": "west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 11,
      "y": 6,
      "exits": "",
      "walls": "south",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 12,
      "y": 6,
      "exits": "",
      "walls": "east",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 13,
      "y": 6,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 14,
      "y": 6,
      "exits": "",
      "walls": "south west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 15,
      "y": 6,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 16,
      "y": 6,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 17,
      "y": 6,
      "exits": "",
      "walls": "north east",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 18,
      "y": 6,
      "exits": "",
      "walls": "west",
      "doors": "south"
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 19,
      "y": 6,
      "exits": "",
      "walls": "north east",
      "doors": ""
    },
    {
      "x": 20,
      "y": 6
    },
    {
      "x": 21,
      "y": 6
    },
    {
      "x": 22,
      "y": 6
    },
    {
      "x": 23,
      "y": 6
    },
    {
      "x": 24,
      "y": 6
    },
    {
      "x": 25,
      "y": 6
    },
    {
      "x": 26,
      "y": 6
    },
    {
      "x": 27,
      "y": 6
    },
    {
      "x": 28,
      "y": 6
    },
    {
      "x": 29,
      "y": 6
    },
    {
      "x": 30,
      "y": 6
    }
  ],
  [
    {
      "x": -20,
      "y": 5
    },
    {
      "x": -19,
      "y": 5
    },
    {
      "x": -18,
      "y": 5
    },
    {
      "x": -17,
      "y": 5
    },
    {
      "x": -16,
      "y": 5
    },
    {
      "x": -15,
      "y": 5
    },
    {
      "terrain": "shallow_water",
      "flags": "",
      "x": -14,
      "y": 5,
      "exits": "",
      "walls": "north west",
      "doors": ""
    },
    {
      "terrain": "shallow_water",
      "flags": "",
      "x": -13,
      "y": 5,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "x": -12,
      "y": 5,
      "terrain": "cavern",
      "flags": "",
      "exits": "up",
      "walls": "north east south",
      "doors": ""
    },
    {
      "x": -11,
      "y": 5
    },
    {
      "x": -10,
      "y": 5
    },
    {
      "x": -9,
      "y": 5
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -8,
      "y": 5,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "x": -7,
      "y": 5
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": -6,
      "y": 5,
      "exits": "",
      "walls": "north west",
      "doors": "east"
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": -5,
      "y": 5,
      "exits": "up",
      "walls": "north east",
      "doors": "south west"
    },
    {
      "x": -4,
      "y": 5
    },
    {
      "x": -3,
      "y": 5
    },
    {
      "x": -2,
      "y": 5
    },
    {
      "x": -1,
      "y": 5
    },
    {
      "x": 0,
      "y": 5
    },
    {
      "x": 1,
      "y": 5
    },
    {
      "x": 2,
      "y": 5
    },
    {
      "x": 3,
      "y": 5
    },
    {
      "x": 4,
      "y": 5
    },
    {
      "x": 5,
      "y": 5
    },
    {
      "x": 6,
      "y": 5
    },
    {
      "x": 7,
      "y": 5
    },
    {
      "x": 8,
      "y": 5
    },
    {
      "x": 9,
      "y": 5
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 10,
      "y": 5,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 11,
      "y": 5,
      "exits": "",
      "walls": "north south west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 12,
      "y": 5,
      "exits": "",
      "walls": "east",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 13,
      "y": 5,
      "exits": "",
      "walls": "west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 14,
      "y": 5,
      "exits": "",
      "walls": "north east south",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 15,
      "y": 5,
      "exits": "",
      "walls": "north west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 16,
      "y": 5,
      "exits": "",
      "walls": "north south",
      "doors": "east"
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 17,
      "y": 5,
      "exits": "",
      "walls": "south",
      "doors": "west"
    },
    {
      "terrain": "indoors",
      "x": 18,
      "y": 5,
      "exits": "",
      "walls": "",
      "doors": "north east"
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 19,
      "y": 5,
      "exits": "",
      "walls": "east south",
      "doors": "west"
    },
    {
      "x": 20,
      "y": 5
    },
    {
      "x": 21,
      "y": 5
    },
    {
      "x": 22,
      "y": 5
    },
    {
      "x": 23,
      "y": 5
    },
    {
      "x": 24,
      "y": 5
    },
    {
      "x": 25,
      "y": 5
    },
    {
      "x": 26,
      "y": 5
    },
    {
      "x": 27,
      "y": 5
    },
    {
      "x": 28,
      "y": 5
    },
    {
      "x": 29,
      "y": 5
    },
    {
      "x": 30,
      "y": 5
    }
  ],
  [
    {
      "x": -20,
      "y": 4
    },
    {
      "x": -19,
      "y": 4
    },
    {
      "x": -18,
      "y": 4
    },
    {
      "x": -17,
      "y": 4
    },
    {
      "x": -16,
      "y": 4
    },
    {
      "x": -15,
      "y": 4
    },
    {
      "terrain": "shallow_water",
      "flags": "",
      "x": -14,
      "y": 4,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "x": -13,
      "y": 4
    },
    {
      "x": -12,
      "y": 4
    },
    {
      "x": -11,
      "y": 4
    },
    {
      "x": -10,
      "y": 4
    },
    {
      "x": -9,
      "y": 4,
      "terrain": "cavern",
      "flags": "",
      "exits": "down",
      "walls": "north south west",
      "doors": ""
    },
    {
      "x": -8,
      "y": 4,
      "terrain": "indoors",
      "flags": "",
      "exits": "down",
      "walls": "",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": -7,
      "y": 4,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -6,
      "y": 4,
      "exits": "",
      "walls": "east south",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -5,
      "y": 4,
      "exits": "",
      "walls": "south west",
      "doors": "north"
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -4,
      "y": 4,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -3,
      "y": 4,
      "exits": "",
      "walls": "north",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -2,
      "y": 4,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -1,
      "y": 4,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 0,
      "y": 4,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 1,
      "y": 4,
      "exits": "",
      "walls": "north east",
      "doors": ""
    },
    {
      "x": 2,
      "y": 4
    },
    {
      "terrain": "cavern",
      "flags": "shop",
      "x": 3,
      "y": 4,
      "exits": "",
      "walls": "north east west",
      "doors": ""
    },
    {
      "x": 4,
      "y": 4
    },
    {
      "x": 5,
      "y": 4
    },
    {
      "x": 6,
      "y": 4
    },
    {
      "terrain": "field",
      "flags": "",
      "x": 7,
      "y": 4,
      "exits": "",
      "walls": "north south west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 8,
      "y": 4,
      "exits": "",
      "walls": "north east",
      "doors": "south"
    },
    {
      "x": 9,
      "y": 4
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 10,
      "y": 4,
      "exits": "",
      "walls": "west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 11,
      "y": 4,
      "exits": "",
      "walls": "north east south",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 12,
      "y": 4,
      "exits": "",
      "walls": "east south west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 13,
      "y": 4,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 14,
      "y": 4,
      "exits": "",
      "walls": "north west",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 15,
      "y": 4,
      "exits": "",
      "walls": "south",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 16,
      "y": 4,
      "exits": "",
      "walls": "north",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 17,
      "y": 4,
      "exits": "",
      "walls": "north east",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 18,
      "y": 4,
      "exits": "",
      "walls": "south west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "watchtower",
      "x": 19,
      "y": 4,
      "exits": "down",
      "walls": "north east south",
      "doors": ""
    },
    {
      "x": 20,
      "y": 4
    },
    {
      "x": 21,
      "y": 4
    },
    {
      "x": 22,
      "y": 4
    },
    {
      "x": 23,
      "y": 4
    },
    {
      "x": 24,
      "y": 4
    },
    {
      "x": 25,
      "y": 4
    },
    {
      "x": 26,
      "y": 4
    },
    {
      "x": 27,
      "y": 4
    },
    {
      "x": 28,
      "y": 4
    },
    {
      "x": 29,
      "y": 4
    },
    {
      "x": 30,
      "y": 4
    }
  ],
  [
    {
      "x": -20,
      "y": 3
    },
    {
      "x": -19,
      "y": 3
    },
    {
      "x": -18,
      "y": 3
    },
    {
      "x": -17,
      "y": 3
    },
    {
      "x": -16,
      "y": 3
    },
    {
      "x": -15,
      "y": 3
    },
    {
      "x": -14,
      "y": 3,
      "terrain": "shallow_water",
      "flags": "",
      "exits": "down",
      "walls": "east south west",
      "doors": ""
    },
    {
      "x": -13,
      "y": 3,
      "terrain": "tunnel",
      "flags": "",
      "exits": "up",
      "walls": "north south west",
      "doors": "east"
    },
    {
      "x": -12,
      "y": 3,
      "terrain": "cavern",
      "flags": "",
      "exits": "down",
      "walls": "north south",
      "doors": "west"
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -11,
      "y": 3,
      "exits": "",
      "walls": "north east",
      "doors": ""
    },
    {
      "x": -10,
      "y": 3
    },
    {
      "x": -9,
      "y": 3
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": -8,
      "y": 3,
      "exits": "",
      "walls": "east west",
      "doors": "south"
    },
    {
      "x": -7,
      "y": 3
    },
    {
      "x": -6,
      "y": 3
    },
    {
      "x": -5,
      "y": 3
    },
    {
      "x": -4,
      "y": 3
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -3,
      "y": 3,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "x": -2,
      "y": 3
    },
    {
      "x": -1,
      "y": 3
    },
    {
      "x": 0,
      "y": 3
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 1,
      "y": 3,
      "exits": "",
      "walls": "west",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 2,
      "y": 3,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 3,
      "y": 3,
      "exits": "",
      "walls": "",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 4,
      "y": 3,
      "exits": "",
      "walls": "north east",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 5,
      "y": 3,
      "exits": "up",
      "walls": "north east south west",
      "doors": ""
    },
    {
      "x": 6,
      "y": 3
    },
    {
      "x": 7,
      "y": 3
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 8,
      "y": 3,
      "exits": "",
      "walls": "west",
      "doors": "north"
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 9,
      "y": 3,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 10,
      "y": 3,
      "exits": "",
      "walls": "south",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 11,
      "y": 3,
      "exits": "",
      "walls": "north",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 12,
      "y": 3,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 13,
      "y": 3,
      "exits": "",
      "walls": "",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 14,
      "y": 3,
      "exits": "",
      "walls": "south",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 15,
      "y": 3,
      "exits": "",
      "walls": "north east",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 16,
      "y": 3,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 17,
      "y": 3,
      "exits": "",
      "walls": "south west",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 18,
      "y": 3,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 19,
      "y": 3,
      "exits": "up",
      "walls": "north",
      "doors": "east"
    },
    {
      "x": 20,
      "y": 3
    },
    {
      "x": 21,
      "y": 3
    },
    {
      "x": 22,
      "y": 3
    },
    {
      "x": 23,
      "y": 3
    },
    {
      "x": 24,
      "y": 3
    },
    {
      "x": 25,
      "y": 3
    },
    {
      "x": 26,
      "y": 3
    },
    {
      "x": 27,
      "y": 3
    },
    {
      "x": 28,
      "y": 3
    },
    {
      "x": 29,
      "y": 3
    },
    {
      "x": 30,
      "y": 3
    }
  ],
  [
    {
      "x": -20,
      "y": 2
    },
    {
      "x": -19,
      "y": 2
    },
    {
      "x": -18,
      "y": 2
    },
    {
      "x": -17,
      "y": 2
    },
    {
      "x": -16,
      "y": 2
    },
    {
      "x": -15,
      "y": 2
    },
    {
      "x": -14,
      "y": 2
    },
    {
      "x": -13,
      "y": 2
    },
    {
      "x": -12,
      "y": 2,
      "terrain": "cavern",
      "flags": "guildmaster",
      "exits": "up",
      "walls": "north east south west",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": -11,
      "y": 2,
      "exits": "",
      "walls": "east south west",
      "doors": ""
    },
    {
      "x": -10,
      "y": 2
    },
    {
      "x": -9,
      "y": 2
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": -8,
      "y": 2,
      "exits": "",
      "walls": "east west",
      "doors": "north"
    },
    {
      "x": -7,
      "y": 2
    },
    {
      "x": -6,
      "y": 2,
      "terrain": "tunnel",
      "flags": "",
      "exits": "up down",
      "walls": "north east west",
      "doors": ""
    },
    {
      "x": -5,
      "y": 2
    },
    {
      "x": -4,
      "y": 2
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -3,
      "y": 2,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "x": -2,
      "y": 2
    },
    {
      "x": -1,
      "y": 2
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 0,
      "y": 2,
      "exits": "",
      "walls": "north west",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 1,
      "y": 2,
      "exits": "",
      "walls": "east south",
      "doors": ""
    },
    {
      "x": 2,
      "y": 2
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 3,
      "y": 2,
      "exits": "",
      "walls": "west",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 4,
      "y": 2,
      "exits": "",
      "walls": "east",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 5,
      "y": 2,
      "exits": "down",
      "walls": "north east west",
      "doors": "south"
    },
    {
      "x": 6,
      "y": 2
    },
    {
      "x": 7,
      "y": 2
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 8,
      "y": 2,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "x": 9,
      "y": 2
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 10,
      "y": 2,
      "exits": "",
      "walls": "north south west",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 11,
      "y": 2,
      "exits": "",
      "walls": "east",
      "doors": ""
    },
    {
      "x": 12,
      "y": 2
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 13,
      "y": 2,
      "exits": "",
      "walls": "west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 14,
      "y": 2,
      "exits": "",
      "walls": "north east",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 15,
      "y": 2,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 16,
      "y": 2,
      "exits": "",
      "walls": "south west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 17,
      "y": 2,
      "exits": "",
      "walls": "north",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 18,
      "y": 2,
      "exits": "",
      "walls": "north east",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 19,
      "y": 2,
      "exits": "",
      "walls": "east west",
      "doors": "south"
    },
    {
      "x": 20,
      "y": 2
    },
    {
      "x": 21,
      "y": 2
    },
    {
      "x": 22,
      "y": 2
    },
    {
      "x": 23,
      "y": 2
    },
    {
      "x": 24,
      "y": 2
    },
    {
      "x": 25,
      "y": 2
    },
    {
      "x": 26,
      "y": 2
    },
    {
      "x": 27,
      "y": 2
    },
    {
      "x": 28,
      "y": 2
    },
    {
      "x": 29,
      "y": 2
    },
    {
      "x": 30,
      "y": 2
    }
  ],
  [
    {
      "x": -20,
      "y": 1
    },
    {
      "x": -19,
      "y": 1
    },
    {
      "x": -18,
      "y": 1
    },
    {
      "x": -17,
      "y": 1
    },
    {
      "x": -16,
      "y": 1
    },
    {
      "x": -15,
      "y": 1
    },
    {
      "x": -14,
      "y": 1
    },
    {
      "x": -13,
      "y": 1
    },
    {
      "x": -12,
      "y": 1
    },
    {
      "x": -11,
      "y": 1
    },
    {
      "x": -10,
      "y": 1
    },
    {
      "x": -9,
      "y": 1
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": -8,
      "y": 1,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": -7,
      "y": 1,
      "exits": "",
      "walls": "north east west",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -6,
      "y": 1,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "x": -5,
      "y": 1
    },
    {
      "x": -4,
      "y": 1
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -3,
      "y": 1,
      "exits": "",
      "walls": "south west",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -2,
      "y": 1,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -1,
      "y": 1,
      "exits": "",
      "walls": "north east",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 0,
      "y": 1,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "x": 1,
      "y": 1
    },
    {
      "terrain": "cavern",
      "flags": "shop",
      "x": 2,
      "y": 1,
      "exits": "",
      "walls": "north west",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 3,
      "y": 1,
      "exits": "",
      "walls": "south",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "water",
      "x": 4,
      "y": 1,
      "exits": "",
      "walls": "south",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 5,
      "y": 1,
      "exits": "",
      "walls": "south",
      "doors": "north"
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 6,
      "y": 1,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 7,
      "y": 1,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 8,
      "y": 1,
      "exits": "",
      "walls": "east south",
      "doors": ""
    },
    {
      "x": 9,
      "y": 1
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 10,
      "y": 1,
      "exits": "up",
      "walls": "north east south west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 11,
      "y": 1,
      "exits": "down",
      "walls": "west",
      "doors": "south"
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 12,
      "y": 1,
      "exits": "",
      "walls": "north east south",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 13,
      "y": 1,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 14,
      "y": 1,
      "exits": "",
      "walls": "south west",
      "doors": "east"
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 15,
      "y": 1,
      "exits": "",
      "walls": "",
      "doors": "west"
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 16,
      "y": 1,
      "exits": "",
      "walls": "north east south",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 17,
      "y": 1,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 18,
      "y": 1,
      "exits": "",
      "walls": "south west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 19,
      "y": 1,
      "exits": "",
      "walls": "",
      "doors": "north"
    },
    {
      "terrain": "indoors",
      "flags": "guildmaster",
      "x": 20,
      "y": 1,
      "exits": "",
      "walls": "north east south",
      "doors": ""
    },
    {
      "x": 21,
      "y": 1
    },
    {
      "x": 22,
      "y": 1
    },
    {
      "x": 23,
      "y": 1
    },
    {
      "x": 24,
      "y": 1
    },
    {
      "x": 25,
      "y": 1
    },
    {
      "x": 26,
      "y": 1
    },
    {
      "x": 27,
      "y": 1
    },
    {
      "x": 28,
      "y": 1
    },
    {
      "x": 29,
      "y": 1
    },
    {
      "x": 30,
      "y": 1
    }
  ],
  [
    {
      "x": -20,
      "y": 0
    },
    {
      "x": -19,
      "y": 0
    },
    {
      "x": -18,
      "y": 0
    },
    {
      "x": -17,
      "y": 0
    },
    {
      "x": -16,
      "y": 0
    },
    {
      "x": -15,
      "y": 0
    },
    {
      "x": -14,
      "y": 0
    },
    {
      "x": -13,
      "y": 0
    },
    {
      "x": -12,
      "y": 0
    },
    {
      "x": -11,
      "y": 0
    },
    {
      "x": -10,
      "y": 0
    },
    {
      "x": -9,
      "y": 0
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -8,
      "y": 0,
      "exits": "",
      "walls": "south west",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -7,
      "y": 0,
      "exits": "",
      "walls": "east south",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -6,
      "y": 0,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "x": -5,
      "y": 0
    },
    {
      "x": -4,
      "y": 0
    },
    {
      "x": -3,
      "y": 0
    },
    {
      "x": -2,
      "y": 0
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -1,
      "y": 0,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "x": 0,
      "y": 0,
      "terrain": "cavern",
      "flags": "rent",
      "exits": "down",
      "walls": "east south west",
      "doors": ""
    },
    {
      "x": 1,
      "y": 0,
      "terrain": "cavern",
      "flags": "",
      "exits": "up",
      "walls": "north east south west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 2,
      "y": 0,
      "exits": "",
      "walls": "south west",
      "doors": "east"
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 3,
      "y": 0,
      "exits": "",
      "walls": "north east south",
      "doors": "west"
    },
    {
      "x": 4,
      "y": 0
    },
    {
      "x": 5,
      "y": 0
    },
    {
      "x": 6,
      "y": 0
    },
    {
      "x": 7,
      "y": 0
    },
    {
      "x": 8,
      "y": 0
    },
    {
      "x": 9,
      "y": 0
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 10,
      "y": 0,
      "exits": "",
      "walls": "north west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 11,
      "y": 0,
      "exits": "",
      "walls": "",
      "doors": "north"
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 12,
      "y": 0,
      "exits": "",
      "walls": "north east south",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 13,
      "y": 0,
      "exits": "",
      "walls": "west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 14,
      "y": 0,
      "exits": "",
      "walls": "north east",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 15,
      "y": 0,
      "exits": "",
      "walls": "west",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 16,
      "y": 0,
      "exits": "",
      "walls": "north east south",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 17,
      "y": 0,
      "exits": "",
      "walls": "south west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 18,
      "y": 0,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 19,
      "y": 0,
      "exits": "",
      "walls": "east",
      "doors": "south"
    },
    {
      "x": 20,
      "y": 0
    },
    {
      "x": 21,
      "y": 0
    },
    {
      "x": 22,
      "y": 0
    },
    {
      "x": 23,
      "y": 0
    },
    {
      "x": 24,
      "y": 0
    },
    {
      "x": 25,
      "y": 0
    },
    {
      "x": 26,
      "y": 0
    },
    {
      "x": 27,
      "y": 0
    },
    {
      "x": 28,
      "y": 0
    },
    {
      "x": 29,
      "y": 0
    },
    {
      "x": 30,
      "y": 0
    }
  ],
  [
    {
      "x": -20,
      "y": -1
    },
    {
      "x": -19,
      "y": -1
    },
    {
      "x": -18,
      "y": -1
    },
    {
      "x": -17,
      "y": -1
    },
    {
      "x": -16,
      "y": -1
    },
    {
      "x": -15,
      "y": -1
    },
    {
      "x": -14,
      "y": -1
    },
    {
      "x": -13,
      "y": -1
    },
    {
      "x": -12,
      "y": -1
    },
    {
      "x": -11,
      "y": -1
    },
    {
      "x": -10,
      "y": -1
    },
    {
      "x": -9,
      "y": -1
    },
    {
      "x": -8,
      "y": -1
    },
    {
      "x": -7,
      "y": -1
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -6,
      "y": -1,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "x": -5,
      "y": -1
    },
    {
      "x": -4,
      "y": -1
    },
    {
      "x": -3,
      "y": -1
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -2,
      "y": -1,
      "exits": "",
      "walls": "north west",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -1,
      "y": -1,
      "exits": "",
      "walls": "south",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 0,
      "y": -1,
      "exits": "",
      "walls": "north",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 1,
      "y": -1,
      "exits": "",
      "walls": "north",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 2,
      "y": -1,
      "exits": "",
      "walls": "north",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 3,
      "y": -1,
      "exits": "",
      "walls": "north east",
      "doors": ""
    },
    {
      "x": 4,
      "y": -1
    },
    {
      "x": 5,
      "y": -1,
      "terrain": "indoors",
      "flags": "",
      "exits": "down",
      "walls": "north south west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 6,
      "y": -1,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 7,
      "y": -1,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 8,
      "y": -1,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 9,
      "y": -1,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 10,
      "y": -1,
      "exits": "",
      "walls": "east south",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 11,
      "y": -1,
      "exits": "",
      "walls": "east south west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 12,
      "y": -1,
      "exits": "",
      "walls": "north south west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 13,
      "y": -1,
      "exits": "",
      "walls": "east south",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 14,
      "y": -1,
      "exits": "",
      "walls": "east south west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 15,
      "y": -1,
      "exits": "",
      "walls": "south west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 16,
      "y": -1,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 17,
      "y": -1,
      "exits": "",
      "walls": "north east",
      "doors": "south"
    },
    {
      "x": 18,
      "y": -1
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 19,
      "y": -1,
      "exits": "down",
      "walls": "east south west",
      "doors": "north"
    },
    {
      "terrain": "shallow_water",
      "flags": "",
      "x": 20,
      "y": -1,
      "exits": "up",
      "walls": "north east south west",
      "doors": ""
    },
    {
      "x": 21,
      "y": -1
    },
    {
      "x": 22,
      "y": -1
    },
    {
      "x": 23,
      "y": -1
    },
    {
      "x": 24,
      "y": -1
    },
    {
      "x": 25,
      "y": -1
    },
    {
      "x": 26,
      "y": -1
    },
    {
      "x": 27,
      "y": -1
    },
    {
      "x": 28,
      "y": -1
    },
    {
      "x": 29,
      "y": -1
    },
    {
      "x": 30,
      "y": -1
    }
  ],
  [
    {
      "x": -20,
      "y": -2
    },
    {
      "x": -19,
      "y": -2
    },
    {
      "x": -18,
      "y": -2
    },
    {
      "x": -17,
      "y": -2
    },
    {
      "x": -16,
      "y": -2
    },
    {
      "x": -15,
      "y": -2
    },
    {
      "x": -14,
      "y": -2
    },
    {
      "x": -13,
      "y": -2
    },
    {
      "x": -12,
      "y": -2
    },
    {
      "x": -11,
      "y": -2
    },
    {
      "x": -10,
      "y": -2
    },
    {
      "x": -9,
      "y": -2
    },
    {
      "x": -8,
      "y": -2
    },
    {
      "x": -7,
      "y": -2
    },
    {
      "x": -6,
      "y": -2,
      "terrain": "tunnel",
      "flags": "",
      "exits": "down",
      "walls": "east west",
      "doors": ""
    },
    {
      "x": -5,
      "y": -2
    },
    {
      "x": -4,
      "y": -2
    },
    {
      "terrain": "indoors",
      "flags": "guildmaster",
      "x": -3,
      "y": -2,
      "exits": "",
      "walls": "north east west",
      "doors": "south"
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -2,
      "y": -2,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "x": -1,
      "y": -2
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 0,
      "y": -2,
      "exits": "",
      "walls": "west",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 1,
      "y": -2,
      "exits": "",
      "walls": "",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 2,
      "y": -2,
      "exits": "",
      "walls": "",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 3,
      "y": -2,
      "exits": "",
      "walls": "",
      "doors": "east"
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 4,
      "y": -2,
      "exits": "",
      "walls": "north east south",
      "doors": "west"
    },
    {
      "x": 5,
      "y": -2
    },
    {
      "x": 6,
      "y": -2
    },
    {
      "x": 7,
      "y": -2
    },
    {
      "x": 8,
      "y": -2
    },
    {
      "x": 9,
      "y": -2
    },
    {
      "x": 10,
      "y": -2
    },
    {
      "x": 11,
      "y": -2
    },
    {
      "x": 12,
      "y": -2
    },
    {
      "x": 13,
      "y": -2
    },
    {
      "x": 14,
      "y": -2
    },
    {
      "x": 15,
      "y": -2
    },
    {
      "x": 16,
      "y": -2
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 17,
      "y": -2,
      "exits": "",
      "walls": "south west",
      "doors": "north"
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 18,
      "y": -2,
      "exits": "",
      "walls": "north east",
      "doors": ""
    },
    {
      "x": 19,
      "y": -2
    },
    {
      "x": 20,
      "y": -2
    },
    {
      "x": 21,
      "y": -2
    },
    {
      "x": 22,
      "y": -2
    },
    {
      "x": 23,
      "y": -2
    },
    {
      "x": 24,
      "y": -2
    },
    {
      "x": 25,
      "y": -2
    },
    {
      "x": 26,
      "y": -2
    },
    {
      "x": 27,
      "y": -2
    },
    {
      "x": 28,
      "y": -2
    },
    {
      "x": 29,
      "y": -2
    },
    {
      "x": 30,
      "y": -2
    }
  ],
  [
    {
      "x": -20,
      "y": -3
    },
    {
      "x": -19,
      "y": -3
    },
    {
      "x": -18,
      "y": -3
    },
    {
      "x": -17,
      "y": -3
    },
    {
      "x": -16,
      "y": -3
    },
    {
      "x": -15,
      "y": -3
    },
    {
      "x": -14,
      "y": -3
    },
    {
      "x": -13,
      "y": -3
    },
    {
      "x": -12,
      "y": -3
    },
    {
      "x": -11,
      "y": -3
    },
    {
      "x": -10,
      "y": -3
    },
    {
      "x": -9,
      "y": -3
    },
    {
      "x": -8,
      "y": -3
    },
    {
      "x": -7,
      "y": -3
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -6,
      "y": -3,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "x": -5,
      "y": -3
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": -4,
      "y": -3,
      "exits": "",
      "walls": "north south west",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -3,
      "y": -3,
      "exits": "",
      "walls": "south",
      "doors": "north"
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -2,
      "y": -3,
      "exits": "",
      "walls": "east",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "guildmaster",
      "x": -1,
      "y": -3,
      "exits": "",
      "walls": "north west",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 0,
      "y": -3,
      "exits": "",
      "walls": "",
      "doors": ""
    },
    {
      "x": 1,
      "y": -3,
      "terrain": "cavern",
      "flags": "quest",
      "exits": "down",
      "walls": "",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 2,
      "y": -3,
      "exits": "",
      "walls": "",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 3,
      "y": -3,
      "exits": "",
      "walls": "east",
      "doors": ""
    },
    {
      "x": 4,
      "y": -3
    },
    {
      "x": 5,
      "y": -3
    },
    {
      "x": 6,
      "y": -3
    },
    {
      "x": 7,
      "y": -3
    },
    {
      "x": 8,
      "y": -3
    },
    {
      "x": 9,
      "y": -3
    },
    {
      "x": 10,
      "y": -3
    },
    {
      "x": 11,
      "y": -3
    },
    {
      "x": 12,
      "y": -3
    },
    {
      "x": 13,
      "y": -3
    },
    {
      "x": 14,
      "y": -3
    },
    {
      "x": 15,
      "y": -3
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 16,
      "y": -3,
      "exits": "",
      "walls": "north south west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 17,
      "y": -3,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 18,
      "y": -3,
      "exits": "",
      "walls": "east",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 19,
      "y": -3,
      "exits": "",
      "walls": "north east west",
      "doors": "south"
    },
    {
      "x": 20,
      "y": -3
    },
    {
      "x": 21,
      "y": -3
    },
    {
      "x": 22,
      "y": -3
    },
    {
      "x": 23,
      "y": -3
    },
    {
      "x": 24,
      "y": -3
    },
    {
      "x": 25,
      "y": -3
    },
    {
      "x": 26,
      "y": -3
    },
    {
      "x": 27,
      "y": -3
    },
    {
      "x": 28,
      "y": -3
    },
    {
      "x": 29,
      "y": -3
    },
    {
      "x": 30,
      "y": -3
    }
  ],
  [
    {
      "x": -20,
      "y": -4
    },
    {
      "x": -19,
      "y": -4
    },
    {
      "x": -18,
      "y": -4
    },
    {
      "x": -17,
      "y": -4
    },
    {
      "x": -16,
      "y": -4
    },
    {
      "x": -15,
      "y": -4
    },
    {
      "x": -14,
      "y": -4
    },
    {
      "x": -13,
      "y": -4
    },
    {
      "x": -12,
      "y": -4
    },
    {
      "x": -11,
      "y": -4
    },
    {
      "x": -10,
      "y": -4
    },
    {
      "x": -9,
      "y": -4
    },
    {
      "x": -8,
      "y": -4
    },
    {
      "x": -7,
      "y": -4
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -6,
      "y": -4,
      "exits": "",
      "walls": "south west",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -5,
      "y": -4,
      "exits": "",
      "walls": "north east",
      "doors": ""
    },
    {
      "x": -4,
      "y": -4
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": -3,
      "y": -4,
      "exits": "",
      "walls": "north south west",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -2,
      "y": -4,
      "exits": "",
      "walls": "east",
      "doors": ""
    },
    {
      "x": -1,
      "y": -4
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 0,
      "y": -4,
      "exits": "",
      "walls": "south west",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 1,
      "y": -4,
      "exits": "",
      "walls": "",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 2,
      "y": -4,
      "exits": "",
      "walls": "",
      "doors": "south"
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 3,
      "y": -4,
      "exits": "",
      "walls": "",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 4,
      "y": -4,
      "exits": "",
      "walls": "north east south",
      "doors": ""
    },
    {
      "x": 5,
      "y": -4
    },
    {
      "x": 6,
      "y": -4
    },
    {
      "x": 7,
      "y": -4
    },
    {
      "x": 8,
      "y": -4
    },
    {
      "x": 9,
      "y": -4
    },
    {
      "x": 10,
      "y": -4
    },
    {
      "x": 11,
      "y": -4
    },
    {
      "x": 12,
      "y": -4
    },
    {
      "x": 13,
      "y": -4
    },
    {
      "x": 14,
      "y": -4
    },
    {
      "x": 15,
      "y": -4
    },
    {
      "x": 16,
      "y": -4
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 17,
      "y": -4,
      "exits": "up",
      "walls": "north south west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 18,
      "y": -4,
      "exits": "",
      "walls": "",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 19,
      "y": -4,
      "exits": "",
      "walls": "east",
      "doors": "north"
    },
    {
      "x": 20,
      "y": -4
    },
    {
      "x": 21,
      "y": -4
    },
    {
      "x": 22,
      "y": -4
    },
    {
      "x": 23,
      "y": -4
    },
    {
      "x": 24,
      "y": -4
    },
    {
      "x": 25,
      "y": -4
    },
    {
      "x": 26,
      "y": -4
    },
    {
      "x": 27,
      "y": -4
    },
    {
      "x": 28,
      "y": -4
    },
    {
      "x": 29,
      "y": -4
    },
    {
      "x": 30,
      "y": -4
    }
  ],
  [
    {
      "x": -20,
      "y": -5
    },
    {
      "x": -19,
      "y": -5
    },
    {
      "x": -18,
      "y": -5
    },
    {
      "x": -17,
      "y": -5
    },
    {
      "x": -16,
      "y": -5
    },
    {
      "x": -15,
      "y": -5
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": -14,
      "y": -5,
      "exits": "",
      "walls": "north south west",
      "doors": "east"
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -13,
      "y": -5,
      "exits": "",
      "walls": "north east",
      "doors": "west"
    },
    {
      "x": -12,
      "y": -5
    },
    {
      "x": -11,
      "y": -5
    },
    {
      "x": -10,
      "y": -5
    },
    {
      "x": -9,
      "y": -5
    },
    {
      "x": -8,
      "y": -5
    },
    {
      "x": -7,
      "y": -5
    },
    {
      "x": -6,
      "y": -5
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -5,
      "y": -5,
      "exits": "",
      "walls": "east south west",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -4,
      "y": -5,
      "exits": "",
      "walls": "north south west",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -3,
      "y": -5,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -2,
      "y": -5,
      "exits": "",
      "walls": "east",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": -1,
      "y": -5,
      "exits": "",
      "walls": "north west",
      "doors": "south"
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 0,
      "y": -5,
      "exits": "",
      "walls": "north",
      "doors": "south"
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 1,
      "y": -5,
      "exits": "",
      "walls": "east",
      "doors": "south"
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 2,
      "y": -5,
      "exits": "",
      "walls": "east south west",
      "doors": "north"
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 3,
      "y": -5,
      "exits": "",
      "walls": "east south west",
      "doors": ""
    },
    {
      "x": 4,
      "y": -5
    },
    {
      "x": 5,
      "y": -5
    },
    {
      "x": 6,
      "y": -5
    },
    {
      "x": 7,
      "y": -5
    },
    {
      "x": 8,
      "y": -5
    },
    {
      "x": 9,
      "y": -5
    },
    {
      "x": 10,
      "y": -5
    },
    {
      "x": 11,
      "y": -5
    },
    {
      "x": 12,
      "y": -5
    },
    {
      "x": 13,
      "y": -5
    },
    {
      "x": 14,
      "y": -5
    },
    {
      "x": 15,
      "y": -5
    },
    {
      "x": 16,
      "y": -5
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 17,
      "y": -5,
      "exits": "",
      "walls": "north west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 18,
      "y": -5,
      "exits": "",
      "walls": "south",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 19,
      "y": -5,
      "exits": "",
      "walls": "east",
      "doors": ""
    },
    {
      "x": 20,
      "y": -5
    },
    {
      "x": 21,
      "y": -5
    },
    {
      "x": 22,
      "y": -5
    },
    {
      "x": 23,
      "y": -5
    },
    {
      "x": 24,
      "y": -5
    },
    {
      "x": 25,
      "y": -5
    },
    {
      "x": 26,
      "y": -5
    },
    {
      "x": 27,
      "y": -5
    },
    {
      "x": 28,
      "y": -5
    },
    {
      "x": 29,
      "y": -5
    },
    {
      "x": 30,
      "y": -5
    }
  ],
  [
    {
      "x": -20,
      "y": -6
    },
    {
      "x": -19,
      "y": -6
    },
    {
      "x": -18,
      "y": -6
    },
    {
      "x": -17,
      "y": -6
    },
    {
      "x": -16,
      "y": -6
    },
    {
      "x": -15,
      "y": -6
    },
    {
      "x": -14,
      "y": -6
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -13,
      "y": -6,
      "exits": "",
      "walls": "south west",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -12,
      "y": -6,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -11,
      "y": -6,
      "exits": "",
      "walls": "north east",
      "doors": ""
    },
    {
      "x": -10,
      "y": -6
    },
    {
      "x": -9,
      "y": -6
    },
    {
      "x": -8,
      "y": -6
    },
    {
      "x": -7,
      "y": -6
    },
    {
      "x": -6,
      "y": -6
    },
    {
      "x": -5,
      "y": -6
    },
    {
      "x": -4,
      "y": -6
    },
    {
      "x": -3,
      "y": -6
    },
    {
      "x": -2,
      "y": -6,
      "terrain": "indoors",
      "flags": "",
      "exits": "up",
      "walls": "east south west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": -1,
      "y": -6,
      "exits": "",
      "walls": "east south west",
      "doors": "north"
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 0,
      "y": -6,
      "exits": "",
      "walls": "east south west",
      "doors": "north"
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 1,
      "y": -6,
      "exits": "",
      "walls": "east south west",
      "doors": "north"
    },
    {
      "x": 2,
      "y": -6
    },
    {
      "x": 3,
      "y": -6
    },
    {
      "x": 4,
      "y": -6
    },
    {
      "x": 5,
      "y": -6
    },
    {
      "x": 6,
      "y": -6
    },
    {
      "x": 7,
      "y": -6
    },
    {
      "x": 8,
      "y": -6
    },
    {
      "x": 9,
      "y": -6
    },
    {
      "x": 10,
      "y": -6
    },
    {
      "x": 11,
      "y": -6
    },
    {
      "x": 12,
      "y": -6
    },
    {
      "x": 13,
      "y": -6
    },
    {
      "x": 14,
      "y": -6
    },
    {
      "x": 15,
      "y": -6
    },
    {
      "x": 16,
      "y": -6
    },
    {
      "terrain": "city",
      "flags": "",
      "x": 17,
      "y": -6,
      "exits": "",
      "walls": "east south west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "water",
      "x": 18,
      "y": -6,
      "exits": "",
      "walls": "north west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 19,
      "y": -6,
      "exits": "",
      "walls": "east south",
      "doors": ""
    },
    {
      "x": 20,
      "y": -6
    },
    {
      "x": 21,
      "y": -6
    },
    {
      "x": 22,
      "y": -6
    },
    {
      "x": 23,
      "y": -6
    },
    {
      "x": 24,
      "y": -6
    },
    {
      "x": 25,
      "y": -6
    },
    {
      "x": 26,
      "y": -6
    },
    {
      "x": 27,
      "y": -6
    },
    {
      "x": 28,
      "y": -6
    },
    {
      "x": 29,
      "y": -6
    },
    {
      "x": 30,
      "y": -6
    }
  ],
  [
    {
      "x": -20,
      "y": -7
    },
    {
      "x": -19,
      "y": -7
    },
    {
      "x": -18,
      "y": -7
    },
    {
      "x": -17,
      "y": -7
    },
    {
      "x": -16,
      "y": -7
    },
    {
      "x": -15,
      "y": -7
    },
    {
      "x": -14,
      "y": -7
    },
    {
      "x": -13,
      "y": -7
    },
    {
      "x": -12,
      "y": -7
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -11,
      "y": -7,
      "exits": "up",
      "walls": "east west",
      "doors": ""
    },
    {
      "x": -10,
      "y": -7,
      "terrain": "tunnel",
      "flags": "",
      "exits": "up down",
      "walls": "north east south west",
      "doors": ""
    },
    {
      "x": -9,
      "y": -7
    },
    {
      "x": -8,
      "y": -7
    },
    {
      "x": -7,
      "y": -7
    },
    {
      "x": -6,
      "y": -7
    },
    {
      "x": -5,
      "y": -7
    },
    {
      "x": -4,
      "y": -7
    },
    {
      "x": -3,
      "y": -7
    },
    {
      "x": -2,
      "y": -7
    },
    {
      "x": -1,
      "y": -7
    },
    {
      "x": 0,
      "y": -7
    },
    {
      "x": 1,
      "y": -7
    },
    {
      "x": 2,
      "y": -7
    },
    {
      "x": 3,
      "y": -7
    },
    {
      "x": 4,
      "y": -7
    },
    {
      "x": 5,
      "y": -7
    },
    {
      "x": 6,
      "y": -7
    },
    {
      "x": 7,
      "y": -7
    },
    {
      "x": 8,
      "y": -7
    },
    {
      "x": 9,
      "y": -7
    },
    {
      "x": 10,
      "y": -7
    },
    {
      "x": 11,
      "y": -7
    },
    {
      "x": 12,
      "y": -7
    },
    {
      "x": 13,
      "y": -7
    },
    {
      "x": 14,
      "y": -7
    },
    {
      "x": 15,
      "y": -7
    },
    {
      "x": 16,
      "y": -7
    },
    {
      "x": 17,
      "y": -7
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 18,
      "y": -7,
      "exits": "",
      "walls": "south west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 19,
      "y": -7,
      "exits": "",
      "walls": "north east",
      "doors": ""
    },
    {
      "x": 20,
      "y": -7
    },
    {
      "x": 21,
      "y": -7
    },
    {
      "x": 22,
      "y": -7
    },
    {
      "x": 23,
      "y": -7
    },
    {
      "x": 24,
      "y": -7
    },
    {
      "x": 25,
      "y": -7
    },
    {
      "x": 26,
      "y": -7
    },
    {
      "x": 27,
      "y": -7
    },
    {
      "x": 28,
      "y": -7
    },
    {
      "x": 29,
      "y": -7
    },
    {
      "x": 30,
      "y": -7
    }
  ],
  [
    {
      "x": -20,
      "y": -8
    },
    {
      "x": -19,
      "y": -8
    },
    {
      "x": -18,
      "y": -8
    },
    {
      "x": -17,
      "y": -8
    },
    {
      "x": -16,
      "y": -8
    },
    {
      "x": -15,
      "y": -8
    },
    {
      "x": -14,
      "y": -8
    },
    {
      "x": -13,
      "y": -8
    },
    {
      "x": -12,
      "y": -8
    },
    {
      "x": -11,
      "y": -8,
      "terrain": "tunnel",
      "flags": "",
      "exits": "",
      "walls": "west",
      "doors": "east"
    },
    {
      "x": -10,
      "y": -8
    },
    {
      "x": -9,
      "y": -8
    },
    {
      "x": -8,
      "y": -8,
      "terrain": "tunnel",
      "flags": "",
      "exits": "",
      "walls": "north east",
      "doors": "west"
    },
    {
      "x": -7,
      "y": -8
    },
    {
      "x": -6,
      "y": -8
    },
    {
      "x": -5,
      "y": -8
    },
    {
      "x": -4,
      "y": -8
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -3,
      "y": -8,
      "exits": "",
      "walls": "north west",
      "doors": ""
    },
    {
      "x": -2,
      "y": -8,
      "terrain": "indoors",
      "flags": "",
      "exits": "up",
      "walls": "north south",
      "doors": ""
    },
    {
      "x": -1,
      "y": -8,
      "terrain": "tunnel",
      "flags": "",
      "exits": "down",
      "walls": "north east south",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 0,
      "y": -8,
      "exits": "up",
      "walls": "north east south west",
      "doors": ""
    },
    {
      "x": 1,
      "y": -8
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 2,
      "y": -8,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "x": 3,
      "y": -8
    },
    {
      "x": 4,
      "y": -8,
      "terrain": "indoors",
      "flags": "",
      "exits": "up",
      "walls": "north east west",
      "doors": ""
    },
    {
      "x": 5,
      "y": -8
    },
    {
      "x": 6,
      "y": -8
    },
    {
      "x": 7,
      "y": -8
    },
    {
      "x": 8,
      "y": -8
    },
    {
      "x": 9,
      "y": -8
    },
    {
      "x": 10,
      "y": -8
    },
    {
      "x": 11,
      "y": -8
    },
    {
      "x": 12,
      "y": -8
    },
    {
      "x": 13,
      "y": -8
    },
    {
      "x": 14,
      "y": -8
    },
    {
      "x": 15,
      "y": -8
    },
    {
      "x": 16,
      "y": -8
    },
    {
      "x": 17,
      "y": -8
    },
    {
      "x": 18,
      "y": -8
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 19,
      "y": -8,
      "exits": "down",
      "walls": "east south west",
      "doors": ""
    },
    {
      "x": 20,
      "y": -8
    },
    {
      "x": 21,
      "y": -8
    },
    {
      "x": 22,
      "y": -8
    },
    {
      "x": 23,
      "y": -8
    },
    {
      "x": 24,
      "y": -8
    },
    {
      "x": 25,
      "y": -8
    },
    {
      "x": 26,
      "y": -8
    },
    {
      "x": 27,
      "y": -8
    },
    {
      "x": 28,
      "y": -8
    },
    {
      "x": 29,
      "y": -8
    },
    {
      "x": 30,
      "y": -8
    }
  ],
  [
    {
      "x": -20,
      "y": -9
    },
    {
      "x": -19,
      "y": -9
    },
    {
      "x": -18,
      "y": -9
    },
    {
      "x": -17,
      "y": -9
    },
    {
      "x": -16,
      "y": -9
    },
    {
      "x": -15,
      "y": -9
    },
    {
      "x": -14,
      "y": -9
    },
    {
      "x": -13,
      "y": -9
    },
    {
      "x": -12,
      "y": -9
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -11,
      "y": -9,
      "exits": "",
      "walls": "south west",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -10,
      "y": -9,
      "exits": "up",
      "walls": "north east south",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": -9,
      "y": -9,
      "exits": "down",
      "walls": "north east south west",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -8,
      "y": -9,
      "exits": "",
      "walls": "south west",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -7,
      "y": -9,
      "exits": "down",
      "walls": "north east south",
      "doors": ""
    },
    {
      "x": -6,
      "y": -9
    },
    {
      "x": -5,
      "y": -9
    },
    {
      "x": -4,
      "y": -9
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -3,
      "y": -9,
      "exits": "",
      "walls": "east west",
      "doors": ""
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": -2,
      "y": -9,
      "exits": "",
      "walls": "north south west",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": -1,
      "y": -9,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "cavern",
      "flags": "",
      "x": 0,
      "y": -9,
      "exits": "",
      "walls": "north east",
      "doors": ""
    },
    {
      "x": 1,
      "y": -9
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 2,
      "y": -9,
      "exits": "",
      "walls": "east south west",
      "doors": ""
    },
    {
      "x": 3,
      "y": -9
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 4,
      "y": -9,
      "exits": "",
      "walls": "east south west",
      "doors": ""
    },
    {
      "x": 5,
      "y": -9
    },
    {
      "x": 6,
      "y": -9
    },
    {
      "x": 7,
      "y": -9
    },
    {
      "x": 8,
      "y": -9
    },
    {
      "x": 9,
      "y": -9
    },
    {
      "x": 10,
      "y": -9
    },
    {
      "x": 11,
      "y": -9
    },
    {
      "x": 12,
      "y": -9
    },
    {
      "x": 13,
      "y": -9
    },
    {
      "x": 14,
      "y": -9
    },
    {
      "x": 15,
      "y": -9
    },
    {
      "x": 16,
      "y": -9
    },
    {
      "x": 17,
      "y": -9
    },
    {
      "x": 18,
      "y": -9
    },
    {
      "x": 19,
      "y": -9,
      "terrain": "indoors",
      "flags": "",
      "exits": "up",
      "walls": "north east west",
      "doors": "south"
    },
    {
      "x": 20,
      "y": -9
    },
    {
      "x": 21,
      "y": -9
    },
    {
      "x": 22,
      "y": -9
    },
    {
      "x": 23,
      "y": -9
    },
    {
      "x": 24,
      "y": -9
    },
    {
      "x": 25,
      "y": -9
    },
    {
      "x": 26,
      "y": -9
    },
    {
      "x": 27,
      "y": -9
    },
    {
      "x": 28,
      "y": -9
    },
    {
      "x": 29,
      "y": -9
    },
    {
      "x": 30,
      "y": -9
    }
  ],
  [
    {
      "x": -20,
      "y": -10
    },
    {
      "x": -19,
      "y": -10
    },
    {
      "x": -18,
      "y": -10
    },
    {
      "x": -17,
      "y": -10
    },
    {
      "x": -16,
      "y": -10
    },
    {
      "x": -15,
      "y": -10
    },
    {
      "x": -14,
      "y": -10
    },
    {
      "x": -13,
      "y": -10
    },
    {
      "x": -12,
      "y": -10
    },
    {
      "x": -11,
      "y": -10
    },
    {
      "x": -10,
      "y": -10
    },
    {
      "x": -9,
      "y": -10
    },
    {
      "x": -8,
      "y": -10
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -7,
      "y": -10,
      "exits": "up",
      "walls": "north south west",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -6,
      "y": -10,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -5,
      "y": -10,
      "exits": "",
      "walls": "north east south",
      "doors": ""
    },
    {
      "x": -4,
      "y": -10
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -3,
      "y": -10,
      "exits": "",
      "walls": "south west",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -2,
      "y": -10,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": -1,
      "y": -10,
      "exits": "",
      "walls": "north south",
      "doors": ""
    },
    {
      "terrain": "tunnel",
      "flags": "",
      "x": 0,
      "y": -10,
      "exits": "",
      "walls": "east south",
      "doors": ""
    },
    {
      "x": 1,
      "y": -10
    },
    {
      "x": 2,
      "y": -10
    },
    {
      "x": 3,
      "y": -10
    },
    {
      "x": 4,
      "y": -10
    },
    {
      "x": 5,
      "y": -10
    },
    {
      "x": 6,
      "y": -10
    },
    {
      "x": 7,
      "y": -10
    },
    {
      "x": 8,
      "y": -10
    },
    {
      "x": 9,
      "y": -10
    },
    {
      "x": 10,
      "y": -10
    },
    {
      "x": 11,
      "y": -10
    },
    {
      "x": 12,
      "y": -10
    },
    {
      "x": 13,
      "y": -10
    },
    {
      "x": 14,
      "y": -10
    },
    {
      "x": 15,
      "y": -10
    },
    {
      "x": 16,
      "y": -10
    },
    {
      "x": 17,
      "y": -10
    },
    {
      "x": 18,
      "y": -10
    },
    {
      "terrain": "indoors",
      "flags": "",
      "x": 19,
      "y": -10,
      "exits": "down",
      "walls": "east south west",
      "doors": "north"
    },
    {
      "terrain": "underwater",
      "flags": "",
      "x": 20,
      "y": -10,
      "exits": "up down",
      "walls": "north east south west",
      "doors": ""
    },
    {
      "terrain": "underwater",
      "flags": "treasure",
      "x": 21,
      "y": -10,
      "exits": "up",
      "walls": "north east south west",
      "doors": ""
    },
    {
      "x": 22,
      "y": -10
    },
    {
      "x": 23,
      "y": -10
    },
    {
      "x": 24,
      "y": -10
    },
    {
      "x": 25,
      "y": -10
    },
    {
      "x": 26,
      "y": -10
    },
    {
      "x": 27,
      "y": -10
    },
    {
      "x": 28,
      "y": -10
    },
    {
      "x": 29,
      "y": -10
    },
    {
      "x": 30,
      "y": -10
    }
  ],
  [
    {
      "x": -20,
      "y": -11
    },
    {
      "x": -19,
      "y": -11
    },
    {
      "x": -18,
      "y": -11
    },
    {
      "x": -17,
      "y": -11
    },
    {
      "x": -16,
      "y": -11
    },
    {
      "x": -15,
      "y": -11
    },
    {
      "x": -14,
      "y": -11
    },
    {
      "x": -13,
      "y": -11
    },
    {
      "x": -12,
      "y": -11
    },
    {
      "x": -11,
      "y": -11
    },
    {
      "x": -10,
      "y": -11
    },
    {
      "x": -9,
      "y": -11
    },
    {
      "x": -8,
      "y": -11
    },
    {
      "x": -7,
      "y": -11
    },
    {
      "x": -6,
      "y": -11
    },
    {
      "x": -5,
      "y": -11
    },
    {
      "x": -4,
      "y": -11
    },
    {
      "x": -3,
      "y": -11
    },
    {
      "x": -2,
      "y": -11
    },
    {
      "x": -1,
      "y": -11
    },
    {
      "x": 0,
      "y": -11
    },
    {
      "x": 1,
      "y": -11
    },
    {
      "x": 2,
      "y": -11
    },
    {
      "x": 3,
      "y": -11
    },
    {
      "x": 4,
      "y": -11
    },
    {
      "x": 5,
      "y": -11
    },
    {
      "x": 6,
      "y": -11
    },
    {
      "x": 7,
      "y": -11
    },
    {
      "x": 8,
      "y": -11
    },
    {
      "x": 9,
      "y": -11
    },
    {
      "x": 10,
      "y": -11
    },
    {
      "x": 11,
      "y": -11
    },
    {
      "x": 12,
      "y": -11
    },
    {
      "x": 13,
      "y": -11
    },
    {
      "x": 14,
      "y": -11
    },
    {
      "x": 15,
      "y": -11
    },
    {
      "x": 16,
      "y": -11
    },
    {
      "x": 17,
      "y": -11
    },
    {
      "x": 18,
      "y": -11
    },
    {
      "x": 19,
      "y": -11
    },
    {
      "x": 20,
      "y": -11
    },
    {
      "x": 21,
      "y": -11
    },
    {
      "x": 22,
      "y": -11
    },
    {
      "x": 23,
      "y": -11
    },
    {
      "x": 24,
      "y": -11
    },
    {
      "x": 25,
      "y": -11
    },
    {
      "x": 26,
      "y": -11
    },
    {
      "x": 27,
      "y": -11
    },
    {
      "x": 28,
      "y": -11
    },
    {
      "x": 29,
      "y": -11
    },
    {
      "x": 30,
      "y": -11
    }
  ],
  [
    {
      "x": -20,
      "y": -12
    },
    {
      "x": -19,
      "y": -12
    },
    {
      "x": -18,
      "y": -12
    },
    {
      "x": -17,
      "y": -12
    },
    {
      "x": -16,
      "y": -12
    },
    {
      "x": -15,
      "y": -12
    },
    {
      "x": -14,
      "y": -12
    },
    {
      "x": -13,
      "y": -12
    },
    {
      "x": -12,
      "y": -12
    },
    {
      "x": -11,
      "y": -12
    },
    {
      "x": -10,
      "y": -12
    },
    {
      "x": -9,
      "y": -12
    },
    {
      "x": -8,
      "y": -12
    },
    {
      "x": -7,
      "y": -12
    },
    {
      "x": -6,
      "y": -12
    },
    {
      "x": -5,
      "y": -12
    },
    {
      "x": -4,
      "y": -12
    },
    {
      "x": -3,
      "y": -12
    },
    {
      "x": -2,
      "y": -12
    },
    {
      "x": -1,
      "y": -12
    },
    {
      "x": 0,
      "y": -12
    },
    {
      "x": 1,
      "y": -12
    },
    {
      "x": 2,
      "y": -12
    },
    {
      "x": 3,
      "y": -12
    },
    {
      "x": 4,
      "y": -12
    },
    {
      "x": 5,
      "y": -12
    },
    {
      "x": 6,
      "y": -12
    },
    {
      "x": 7,
      "y": -12
    },
    {
      "x": 8,
      "y": -12
    },
    {
      "x": 9,
      "y": -12
    },
    {
      "x": 10,
      "y": -12
    },
    {
      "x": 11,
      "y": -12
    },
    {
      "x": 12,
      "y": -12
    },
    {
      "x": 13,
      "y": -12
    },
    {
      "x": 14,
      "y": -12
    },
    {
      "x": 15,
      "y": -12
    },
    {
      "x": 16,
      "y": -12
    },
    {
      "x": 17,
      "y": -12
    },
    {
      "x": 18,
      "y": -12
    },
    {
      "x": 19,
      "y": -12
    },
    {
      "x": 20,
      "y": -12
    },
    {
      "x": 21,
      "y": -12
    },
    {
      "x": 22,
      "y": -12
    },
    {
      "x": 23,
      "y": -12
    },
    {
      "x": 24,
      "y": -12
    },
    {
      "x": 25,
      "y": -12
    },
    {
      "x": 26,
      "y": -12
    },
    {
      "x": 27,
      "y": -12
    },
    {
      "x": 28,
      "y": -12
    },
    {
      "x": 29,
      "y": -12
    },
    {
      "x": 30,
      "y": -12
    }
  ]
]