export default [
  [
    {
      "x": 0,
      "y": 19
    },
    {
      "x": 1,
      "y": 19
    },
    {
      "x": 2,
      "y": 19
    },
    {
      "x": 3,
      "y": 19
    },
    {
      "x": 4,
      "y": 19
    },
    {
      "x": 5,
      "y": 19
    },
    {
      "x": 6,
      "y": 19
    },
    {
      "x": 7,
      "y": 19
    },
    {
      "x": 8,
      "y": 19
    },
    {
      "x": 9,
      "y": 19
    },
    {
      "x": 10,
      "y": 19
    },
    {
      "x": 11,
      "y": 19
    },
    {
      "x": 12,
      "y": 19
    },
    {
      "x": 13,
      "y": 19
    },
    {
      "x": 14,
      "y": 19
    },
    {
      "x": 15,
      "y": 19
    },
    {
      "x": 16,
      "y": 19
    },
    {
      "x": 17,
      "y": 19
    },
    {
      "x": 18,
      "y": 19
    },
    {
      "x": 19,
      "y": 19
    }
  ],
  [
    {
      "x": 0,
      "y": 18
    },
    {
      "x": 1,
      "y": 18
    },
    {
      "x": 2,
      "y": 18
    },
    {
      "x": 3,
      "y": 18
    },
    {
      "x": 4,
      "y": 18
    },
    {
      "x": 5,
      "y": 18
    },
    {
      "x": 6,
      "y": 18
    },
    {
      "x": 7,
      "y": 18
    },
    {
      "x": 8,
      "y": 18
    },
    {
      "x": 9,
      "y": 18
    },
    {
      "x": 10,
      "y": 18
    },
    {
      "x": 11,
      "y": 18
    },
    {
      "x": 12,
      "y": 18
    },
    {
      "x": 13,
      "y": 18
    },
    {
      "x": 14,
      "y": 18
    },
    {
      "x": 15,
      "y": 18
    },
    {
      "x": 16,
      "y": 18
    },
    {
      "x": 17,
      "y": 18
    },
    {
      "x": 18,
      "y": 18
    },
    {
      "x": 19,
      "y": 18
    }
  ],
  [
    {
      "x": 0,
      "y": 17
    },
    {
      "x": 1,
      "y": 17
    },
    {
      "x": 2,
      "y": 17
    },
    {
      "x": 3,
      "y": 17
    },
    {
      "x": 4,
      "y": 17
    },
    {
      "x": 5,
      "y": 17
    },
    {
      "x": 6,
      "y": 17
    },
    {
      "x": 7,
      "y": 17
    },
    {
      "x": 8,
      "y": 17
    },
    {
      "x": 9,
      "y": 17
    },
    {
      "x": 10,
      "y": 17
    },
    {
      "x": 11,
      "y": 17
    },
    {
      "x": 12,
      "y": 17
    },
    {
      "x": 13,
      "y": 17
    },
    {
      "x": 14,
      "y": 17
    },
    {
      "x": 15,
      "y": 17,
      "map": "dt",
      "name": "\u001bWatchful Tower\u001b",
      "description": "\u001bWith four windows, one in each cardinal direction, peering over the broken\u001b\r\n\u001bcold surroundings, this high tower is certainly an excellent look-out. The \u001b\r\n\u001bgrey stone of the walls nearly blends in with the grey stone of the mountains,\u001b\r\n\u001band the grey overcast sky gives everything a dead, grey shade.\u001b\r\n",
      "exits": {
        "down": {
          "door": {},
          "pipeTo": {
            "x": 15,
            "y": 15
          }
        }
      },
      "terrain": "indoors"
    },
    {
      "x": 16,
      "y": 17
    },
    {
      "x": 17,
      "y": 17
    },
    {
      "x": 18,
      "y": 17
    },
    {
      "x": 19,
      "y": 17
    }
  ],
  [
    {
      "x": 0,
      "y": 16
    },
    {
      "x": 1,
      "y": 16
    },
    {
      "x": 2,
      "y": 16
    },
    {
      "x": 3,
      "y": 16
    },
    {
      "x": 4,
      "y": 16
    },
    {
      "x": 5,
      "y": 16
    },
    {
      "x": 6,
      "y": 16
    },
    {
      "x": 7,
      "y": 16
    },
    {
      "x": 8,
      "y": 16
    },
    {
      "x": 9,
      "y": 16
    },
    {
      "x": 10,
      "y": 16
    },
    {
      "x": 11,
      "y": 16
    },
    {
      "x": 12,
      "y": 16
    },
    {
      "x": 13,
      "y": 16
    },
    {
      "x": 14,
      "y": 16
    },
    {
      "x": 15,
      "y": 16
    },
    {
      "x": 16,
      "y": 16
    },
    {
      "x": 17,
      "y": 16
    },
    {
      "x": 18,
      "y": 16
    },
    {
      "x": 19,
      "y": 16
    }
  ],
  [
    {
      "x": 0,
      "y": 15
    },
    {
      "x": 1,
      "y": 15
    },
    {
      "x": 2,
      "y": 15
    },
    {
      "x": 3,
      "y": 15
    },
    {
      "x": 4,
      "y": 15
    },
    {
      "x": 5,
      "y": 15
    },
    {
      "x": 6,
      "y": 15
    },
    {
      "x": 7,
      "y": 15
    },
    {
      "x": 8,
      "y": 15
    },
    {
      "x": 9,
      "y": 15
    },
    {
      "x": 10,
      "y": 15
    },
    {
      "x": 11,
      "y": 15
    },
    {
      "x": 12,
      "y": 15
    },
    {
      "x": 13,
      "y": 15
    },
    {
      "x": 14,
      "y": 15
    },
    {
      "x": 15,
      "y": 15,
      "map": "dt",
      "name": "\u001bDark Tower\u001b",
      "description": "\u001bOnce this tower may have been a proud fortress of the kingdom of Rhudaur,\u001b\r\n\u001bholding at bay the evil armies of the north. In the recent past, however,\u001b\r\n\u001bthe murals of glorious battles and the walls they are on have been desecrated.\u001b\r\n\u001bThe furniture that once would comfort an ever-vigilant sentry must have\u001b\r\n\u001brecently seen crueler occupants. There are some narrow slit windows high above\u001b\r\n\u001bthe floor, through which some grey shafts of light can fall during what passes\u001b\r\n\u001bfor daylight in these grim mountainous wastes.\u001b\r\n",
      "exits": {
        "south": {
          "door": {},
          "pipeTo": {
            "x": 36,
            "y": 22,
            "map": "mmwest"
          }
        },
        "up": {
          "door": {},
          "pipeTo": {
            "x": 15,
            "y": 17
          }
        },
        "down": {
          "door": {},
          "pipeTo": {
            "x": 13,
            "y": 13
          }
        }
      },
      "terrain": "indoors"
    },
    {
      "x": 16,
      "y": 15
    },
    {
      "x": 17,
      "y": 15
    },
    {
      "x": 18,
      "y": 15
    },
    {
      "x": 19,
      "y": 15
    }
  ],
  [
    {
      "x": 0,
      "y": 14
    },
    {
      "x": 1,
      "y": 14
    },
    {
      "x": 2,
      "y": 14
    },
    {
      "x": 3,
      "y": 14
    },
    {
      "x": 4,
      "y": 14
    },
    {
      "x": 5,
      "y": 14
    },
    {
      "x": 6,
      "y": 14
    },
    {
      "x": 7,
      "y": 14
    },
    {
      "x": 8,
      "y": 14
    },
    {
      "x": 9,
      "y": 14
    },
    {
      "x": 10,
      "y": 14
    },
    {
      "x": 11,
      "y": 14
    },
    {
      "x": 12,
      "y": 14
    },
    {
      "x": 13,
      "y": 14,
      "map":  "dt",
      "name": "\u001bFilthy Pen\u001b",
      "description": "\u001bThe scent of musk is most definitely at its peak here. The floor is littered\u001b\r\n\u001bwith straw and filthy cloth, intermixed with bones and rotting meat. Small\u001b\r\n\u001bpiles of offal and rotting scraps fill the air with a smell so thick and\u001b\r\n\u001bdisgusting that you feel sick. The walls that surround you are scarred deeply\u001b\r\n\u001bwith claw marks.\u001b\r\n",
      "exits": {
        "south": {
          "door": {}
        }
      },
      "terrain": "indoors"

    },
    {
      "x": 14,
      "y": 14
    },
    {
      "x": 15,
      "y": 14
    },
    {
      "x": 16,
      "y": 14
    },
    {
      "x": 17,
      "y": 14
    },
    {
      "x": 18,
      "y": 14
    },
    {
      "x": 19,
      "y": 14
    }
  ],
  [
    {
      "x": 0,
      "y": 13
    },
    {
      "x": 1,
      "y": 13
    },
    {
      "x": 2,
      "y": 13
    },
    {
      "x": 3,
      "y": 13
    },
    {
      "x": 4,
      "y": 13
    },
    {
      "x": 5,
      "y": 13
    },
    {
      "x": 6,
      "y": 13
    },
    {
      "x": 7,
      "y": 13
    },
    {
      "x": 8,
      "y": 13
    },
    {
      "x": 9,
      "y": 13
    },
    {
      "x": 10,
      "y": 13
    },
    {
      "x": 11,
      "y": 13,
      "map":  "dt",  
      "name": "\u001bCramped Smithy\u001b",
      "description": "\u001bThis chamber is dominated by the forge at its centre. Its walls are covered\u001b\r\n\u001bin soot and the smell of hot metal fills the air. Three narrow vents, too\u001b\r\n\u001bsmall for even a hobbit to climb through, are evenly spaced around the\u001b\r\n\u001bchamber's roof. They look cunningly-built, but most of the room is rough-hewn.\u001b\r\n\u001bThere is little space here, just the bare minimum necessary for a smith to\u001b\r\n\u001bwork, and nothing not immediately needed for the forge is stored here.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        }
      },
      "terrain": "indoors"  
    },
    {
      "x": 12,
      "y": 13,
      "map":  "dt",  
      "name": "\u001bBarracks\u001b",
      "description": "\u001bObviously this room was not part of the original tower. The walls are\u001b\r\n\u001brough-hewn, and no thought was paid to them after the builders finished\u001b\r\n\u001bexcavating a reasonable sized room. The ground is uneven, and littered with \u001b\r\n\u001bmats of cloth and straw beds. Loose equipment is scattered about, and several \u001b\r\n\u001bitems, rusted away, stick out of various stench-ridden cloth heaps.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "west": {
          "door": false
        },
        "down": {
          "door": {},
          "pipeTo": {
            "x": 9,
            "y": 10,
          }
        }
      },
      "terrain": "indoors"
    },
    {
      "x": 13,
      "y": 13,
      "map": "dt",
      "name": "\u001bDank Dungeon\u001b",
      "description": "\u001bOnce this may have been a carefully constructed section of the tower; now it\u001b\r\n\u001bhas been hacked and hewed until the original walls can barely be seen, and\u001b\r\n\u001bonly the new, expanded walls enclose you. The air is filled with a dank,\u001b\r\n\u001bmusky scent that reminds you of something...better not to dwell on that.\u001b\r\n\u001bA well fitted iron door is in the north wall, in a section of stone that was\u001b\r\n\u001bnot butchered. To the south the wall was broken to form an archway,\u001b\r\n\u001band another doorway is to the west.\u001b\r\n",
      "exits": {
        "north": {
          "door": {}
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        },
        "up": {
          "door": {},
          "pipeTo": {
            "x": 15,
            "y": 15
          }
        }
      },
      "terrain": "cavern"
    },
    {
      "x": 14,
      "y": 13
    },
    {
      "x": 15,
      "y": 13
    },
    {
      "x": 16,
      "y": 13
    },
    {
      "x": 17,
      "y": 13
    },
    {
      "x": 18,
      "y": 13
    },
    {
      "x": 19,
      "y": 13
    }
  ],
  [
    {
      "x": 0,
      "y": 12
    },
    {
      "x": 1,
      "y": 12
    },
    {
      "x": 2,
      "y": 12
    },
    {
      "x": 3,
      "y": 12
    },
    {
      "x": 4,
      "y": 12
    },
    {
      "x": 5,
      "y": 12
    },
    {
      "x": 6,
      "y": 12
    },
    {
      "x": 7,
      "y": 12
    },
    {
      "x": 8,
      "y": 12,
      "map": "dt",
      "name": "\u001bEscape Passage\u001b",
      "description": "\u001bYou are in a hidden chamber buried deep below the rocky pinnacle of the\u001b\r\n\u001btower. Once the personal sleeping chamber of the tower's commander, this\u001b\r\n\u001broom has now become the last safety measure should the garrison fall to\u001b\r\n\u001ban enemy army. Atop a small metal ladder, a passage, blocked by a metal\u001b\r\n\u001bgrille, may allow the last survivors to flee and call reinforcements.\u001b\r\n",
      "exits": {
        "south": {
          "door": {}
        },
        "up": {
          "door": {},
          "pipeTo": {
            "x": 36,
            "y": 22,
            "map": "mmwest"
          }
        }
      },
      "terrain": "indoors"
    },
    {
      "x": 9,
      "y": 12,
      "map": "dt",
      "name": "\u001bMusty Room\u001b",
      "description": "\u001bThis is merely an alcove carved out of the main hall, and the light from the\u001b\r\n\u001bfire does not reach you. The walls are overgrown with mould and fungus, and\u001b\r\n\u001bthe musty smell fills your lungs. Various worthless items are scattered about,\u001b\r\n\u001bbut in one corner a large chest lies, aged and open.\u001b\r\n",
      "exits": {
        "south": {
          "door": false
        }
      },
      "terrain": "indoors" 
    },
    {
      "x": 10,
      "y": 12
    },
    {
      "x": 11,
      "y": 12
    },
    {
      "x": 12,
      "y": 12
    },
    {
      "x": 13,
      "y": 12,
      "map":  "dt",  
      "name": "\u001bLittered Room\u001b",
      "description": "\u001bThis was clearly not in the original tower. In fact, you would be hard pressed\u001b\r\n\u001bto call this even a room, rather it is an alcove chopped into the rock walls\u001b\r\n\u001bsurrounding the dungeon. Over the last few years, it has apparently served as\u001b\r\n\u001ba repository for any junk the orcs wished to get rid of, or even things of\u001b\r\n\u001bvalue they wished to store. There is so much clutter and junk in here it is\u001b\r\n\u001bhard to even start to search for anything worthwhile.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        }
      },
      "terrain": "indoors"
    },
    {
      "x": 14,
      "y": 12
    },
    {
      "x": 15,
      "y": 12
    },
    {
      "x": 16,
      "y": 12
    },
    {
      "x": 17,
      "y": 12
    },
    {
      "x": 18,
      "y": 12
    },
    {
      "x": 19,
      "y": 12
    }
  ],
  [
    {
      "x": 0,
      "y": 11
    },
    {
      "x": 1,
      "y": 11
    },
    {
      "x": 2,
      "y": 11
    },
    {
      "x": 3,
      "y": 11
    },
    {
      "x": 4,
      "y": 11
    },
    {
      "x": 5,
      "y": 11
    },
    {
      "x": 6,
      "y": 11
    },
    {
      "x": 7,
      "y": 11
    },
    {
      "x": 8,
      "y": 11,
      "map": "dt",
      "name": "\u001bQuarters\u001b",
      "description": "\u001bThe stone walls of this room are circular, and carved with more skill or care\u001b\r\n\u001bthan most in this tower. A large desk lies on the northern wall, on top of it\u001b\r\n\u001bare papers and maps. But a table stands in the centre of the room, flanked by\u001b\r\n\u001ba pair of chairs. A larger chair sits at the head, and on it are various\u001b\r\n\u001bweapons of war. A large map hangs on the southern wall.\u001b\r\n",
      "exits": {
        "north": {
          "door": {}
        },
        "east": {
          "door": {}
        }
      },
      "terrain": "indoors"
    },
    {
      "x": 9,
      "y": 11,
      "map": "dt",
      "name": "\u001bCarved Hall\u001b",
      "description": "\u001bThe grey stone of the mountains this hall was carved from seems dead and hard\u001b\r\n\u001bin the shadows cast by the fire in the centre of the hall. On the rough ground\u001b\r\n\u001bare scattered mats and beds of straw where most of the inhabitants of this\u001b\r\n\u001btower spend the night.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "south": {
          "door": {}
        },
        "west": {
          "door": {}
        }
      },
      "terrain": "indoors"
    },
    {
      "x": 10,
      "y": 11
    },
    {
      "x": 11,
      "y": 11
    },
    {
      "x": 12,
      "y": 11
    },
    {
      "x": 13,
      "y": 11
    },
    {
      "x": 14,
      "y": 11
    },
    {
      "x": 15,
      "y": 11
    },
    {
      "x": 16,
      "y": 11
    },
    {
      "x": 17,
      "y": 11
    },
    {
      "x": 18,
      "y": 11
    },
    {
      "x": 19,
      "y": 11
    }
  ],
  [
    {
      "x": 0,
      "y": 10
    },
    {
      "x": 1,
      "y": 10
    },
    {
      "x": 2,
      "y": 10
    },
    {
      "x": 3,
      "y": 10
    },
    {
      "x": 4,
      "y": 10
    },
    {
      "x": 5,
      "y": 10
    },
    {
      "x": 6,
      "y": 10
    },
    {
      "x": 7,
      "y": 10
    },
    {
      "x": 8,
      "y": 10
    },
    {
      "x": 9,
      "y": 10,
      "map": "dt",
      "name": "\u001bHewn Chamber\u001b",
      "description": "\u001bThe walls were not carved, rather they look as if they were hacked away by\u001b\r\n\u001bsome large axe. The stone is a dead grey, but is covered in dried blood of\u001b\r\n\u001bsome sort. The smell of iron is strong here, and floor is rocky and hard to\u001b\r\n\u001bwalk on. A doorway was cleaved from the rock to the north, and in the inky\u001b\r\n\u001bshadows of the ceiling a faint trap-door can be seen.\u001b\r\n",
      "exits": {
        "north": {
          "door": {}
        },
        "up": {
          "door": {},
          "pipeTo": {
            "x": 12,
            "y": 13,
          }
        }
      },
      "terrain": "indoors"
    },
    {
      "x": 10,
      "y": 10
    },
    {
      "x": 11,
      "y": 10
    },
    {
      "x": 12,
      "y": 10
    },
    {
      "x": 13,
      "y": 10
    },
    {
      "x": 14,
      "y": 10
    },
    {
      "x": 15,
      "y": 10
    },
    {
      "x": 16,
      "y": 10
    },
    {
      "x": 17,
      "y": 10
    },
    {
      "x": 18,
      "y": 10
    },
    {
      "x": 19,
      "y": 10
    }
  ],
  [
    {
      "x": 0,
      "y": 9
    },
    {
      "x": 1,
      "y": 9
    },
    {
      "x": 2,
      "y": 9
    },
    {
      "x": 3,
      "y": 9
    },
    {
      "x": 4,
      "y": 9
    },
    {
      "x": 5,
      "y": 9
    },
    {
      "x": 6,
      "y": 9
    },
    {
      "x": 7,
      "y": 9
    },
    {
      "x": 8,
      "y": 9
    },
    {
      "x": 9,
      "y": 9
    },
    {
      "x": 10,
      "y": 9
    },
    {
      "x": 11,
      "y": 9
    },
    {
      "x": 12,
      "y": 9
    },
    {
      "x": 13,
      "y": 9
    },
    {
      "x": 14,
      "y": 9
    },
    {
      "x": 15,
      "y": 9
    },
    {
      "x": 16,
      "y": 9
    },
    {
      "x": 17,
      "y": 9
    },
    {
      "x": 18,
      "y": 9
    },
    {
      "x": 19,
      "y": 9
    }
  ],
  [
    {
      "x": 0,
      "y": 8
    },
    {
      "x": 1,
      "y": 8
    },
    {
      "x": 2,
      "y": 8
    },
    {
      "x": 3,
      "y": 8
    },
    {
      "x": 4,
      "y": 8
    },
    {
      "x": 5,
      "y": 8
    },
    {
      "x": 6,
      "y": 8
    },
    {
      "x": 7,
      "y": 8
    },
    {
      "x": 8,
      "y": 8
    },
    {
      "x": 9,
      "y": 8
    },
    {
      "x": 10,
      "y": 8
    },
    {
      "x": 11,
      "y": 8
    },
    {
      "x": 12,
      "y": 8
    },
    {
      "x": 13,
      "y": 8
    },
    {
      "x": 14,
      "y": 8
    },
    {
      "x": 15,
      "y": 8
    },
    {
      "x": 16,
      "y": 8
    },
    {
      "x": 17,
      "y": 8
    },
    {
      "x": 18,
      "y": 8
    },
    {
      "x": 19,
      "y": 8
    }
  ],
  [
    {
      "x": 0,
      "y": 7
    },
    {
      "x": 1,
      "y": 7
    },
    {
      "x": 2,
      "y": 7
    },
    {
      "x": 3,
      "y": 7
    },
    {
      "x": 4,
      "y": 7
    },
    {
      "x": 5,
      "y": 7
    },
    {
      "x": 6,
      "y": 7
    },
    {
      "x": 7,
      "y": 7
    },
    {
      "x": 8,
      "y": 7
    },
    {
      "x": 9,
      "y": 7
    },
    {
      "x": 10,
      "y": 7
    },
    {
      "x": 11,
      "y": 7
    },
    {
      "x": 12,
      "y": 7
    },
    {
      "x": 13,
      "y": 7
    },
    {
      "x": 14,
      "y": 7
    },
    {
      "x": 15,
      "y": 7
    },
    {
      "x": 16,
      "y": 7
    },
    {
      "x": 17,
      "y": 7
    },
    {
      "x": 18,
      "y": 7
    },
    {
      "x": 19,
      "y": 7
    }
  ],
  [
    {
      "x": 0,
      "y": 6
    },
    {
      "x": 1,
      "y": 6
    },
    {
      "x": 2,
      "y": 6
    },
    {
      "x": 3,
      "y": 6
    },
    {
      "x": 4,
      "y": 6
    },
    {
      "x": 5,
      "y": 6
    },
    {
      "x": 6,
      "y": 6
    },
    {
      "x": 7,
      "y": 6
    },
    {
      "x": 8,
      "y": 6
    },
    {
      "x": 9,
      "y": 6
    },
    {
      "x": 10,
      "y": 6
    },
    {
      "x": 11,
      "y": 6
    },
    {
      "x": 12,
      "y": 6
    },
    {
      "x": 13,
      "y": 6
    },
    {
      "x": 14,
      "y": 6
    },
    {
      "x": 15,
      "y": 6
    },
    {
      "x": 16,
      "y": 6
    },
    {
      "x": 17,
      "y": 6
    },
    {
      "x": 18,
      "y": 6
    },
    {
      "x": 19,
      "y": 6
    }
  ],
  [
    {
      "x": 0,
      "y": 5
    },
    {
      "x": 1,
      "y": 5
    },
    {
      "x": 2,
      "y": 5
    },
    {
      "x": 3,
      "y": 5
    },
    {
      "x": 4,
      "y": 5
    },
    {
      "x": 5,
      "y": 5
    },
    {
      "x": 6,
      "y": 5
    },
    {
      "x": 7,
      "y": 5
    },
    {
      "x": 8,
      "y": 5
    },
    {
      "x": 9,
      "y": 5
    },
    {
      "x": 10,
      "y": 5
    },
    {
      "x": 11,
      "y": 5
    },
    {
      "x": 12,
      "y": 5
    },
    {
      "x": 13,
      "y": 5
    },
    {
      "x": 14,
      "y": 5
    },
    {
      "x": 15,
      "y": 5
    },
    {
      "x": 16,
      "y": 5
    },
    {
      "x": 17,
      "y": 5
    },
    {
      "x": 18,
      "y": 5
    },
    {
      "x": 19,
      "y": 5
    }
  ],
  [
    {
      "x": 0,
      "y": 4
    },
    {
      "x": 1,
      "y": 4
    },
    {
      "x": 2,
      "y": 4
    },
    {
      "x": 3,
      "y": 4
    },
    {
      "x": 4,
      "y": 4
    },
    {
      "x": 5,
      "y": 4
    },
    {
      "x": 6,
      "y": 4
    },
    {
      "x": 7,
      "y": 4
    },
    {
      "x": 8,
      "y": 4
    },
    {
      "x": 9,
      "y": 4
    },
    {
      "x": 10,
      "y": 4
    },
    {
      "x": 11,
      "y": 4
    },
    {
      "x": 12,
      "y": 4
    },
    {
      "x": 13,
      "y": 4
    },
    {
      "x": 14,
      "y": 4
    },
    {
      "x": 15,
      "y": 4
    },
    {
      "x": 16,
      "y": 4
    },
    {
      "x": 17,
      "y": 4
    },
    {
      "x": 18,
      "y": 4
    },
    {
      "x": 19,
      "y": 4
    }
  ],
  [
    {
      "x": 0,
      "y": 3
    },
    {
      "x": 1,
      "y": 3
    },
    {
      "x": 2,
      "y": 3
    },
    {
      "x": 3,
      "y": 3
    },
    {
      "x": 4,
      "y": 3
    },
    {
      "x": 5,
      "y": 3
    },
    {
      "x": 6,
      "y": 3
    },
    {
      "x": 7,
      "y": 3
    },
    {
      "x": 8,
      "y": 3
    },
    {
      "x": 9,
      "y": 3
    },
    {
      "x": 10,
      "y": 3
    },
    {
      "x": 11,
      "y": 3
    },
    {
      "x": 12,
      "y": 3
    },
    {
      "x": 13,
      "y": 3
    },
    {
      "x": 14,
      "y": 3
    },
    {
      "x": 15,
      "y": 3
    },
    {
      "x": 16,
      "y": 3
    },
    {
      "x": 17,
      "y": 3
    },
    {
      "x": 18,
      "y": 3
    },
    {
      "x": 19,
      "y": 3
    }
  ],
  [
    {
      "x": 0,
      "y": 2
    },
    {
      "x": 1,
      "y": 2
    },
    {
      "x": 2,
      "y": 2
    },
    {
      "x": 3,
      "y": 2
    },
    {
      "x": 4,
      "y": 2
    },
    {
      "x": 5,
      "y": 2
    },
    {
      "x": 6,
      "y": 2
    },
    {
      "x": 7,
      "y": 2
    },
    {
      "x": 8,
      "y": 2
    },
    {
      "x": 9,
      "y": 2
    },
    {
      "x": 10,
      "y": 2
    },
    {
      "x": 11,
      "y": 2
    },
    {
      "x": 12,
      "y": 2
    },
    {
      "x": 13,
      "y": 2
    },
    {
      "x": 14,
      "y": 2
    },
    {
      "x": 15,
      "y": 2
    },
    {
      "x": 16,
      "y": 2
    },
    {
      "x": 17,
      "y": 2
    },
    {
      "x": 18,
      "y": 2
    },
    {
      "x": 19,
      "y": 2
    }
  ],
  [
    {
      "x": 0,
      "y": 1
    },
    {
      "x": 1,
      "y": 1
    },
    {
      "x": 2,
      "y": 1
    },
    {
      "x": 3,
      "y": 1
    },
    {
      "x": 4,
      "y": 1
    },
    {
      "x": 5,
      "y": 1
    },
    {
      "x": 6,
      "y": 1
    },
    {
      "x": 7,
      "y": 1
    },
    {
      "x": 8,
      "y": 1
    },
    {
      "x": 9,
      "y": 1
    },
    {
      "x": 10,
      "y": 1
    },
    {
      "x": 11,
      "y": 1
    },
    {
      "x": 12,
      "y": 1
    },
    {
      "x": 13,
      "y": 1
    },
    {
      "x": 14,
      "y": 1
    },
    {
      "x": 15,
      "y": 1
    },
    {
      "x": 16,
      "y": 1
    },
    {
      "x": 17,
      "y": 1
    },
    {
      "x": 18,
      "y": 1
    },
    {
      "x": 19,
      "y": 1
    }
  ],
  [
    {
      "x": 0,
      "y": 0
    },
    {
      "x": 1,
      "y": 0
    },
    {
      "x": 2,
      "y": 0
    },
    {
      "x": 3,
      "y": 0
    },
    {
      "x": 4,
      "y": 0
    },
    {
      "x": 5,
      "y": 0
    },
    {
      "x": 6,
      "y": 0
    },
    {
      "x": 7,
      "y": 0
    },
    {
      "x": 8,
      "y": 0
    },
    {
      "x": 9,
      "y": 0
    },
    {
      "x": 10,
      "y": 0
    },
    {
      "x": 11,
      "y": 0
    },
    {
      "x": 12,
      "y": 0
    },
    {
      "x": 13,
      "y": 0
    },
    {
      "x": 14,
      "y": 0
    },
    {
      "x": 15,
      "y": 0
    },
    {
      "x": 16,
      "y": 0
    },
    {
      "x": 17,
      "y": 0
    },
    {
      "x": 18,
      "y": 0
    },
    {
      "x": 19,
      "y": 0
    }
  ]
]