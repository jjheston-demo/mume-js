export default [
  [
    {
      "x": 44,
      "y": 42
    },
    {
      "x": 45,
      "y": 42
    },
    {
      "x": 46,
      "y": 42
    },
    {
      "x": 47,
      "y": 42
    },
    {
      "x": 48,
      "y": 42
    },
    {
      "x": 49,
      "y": 42
    },
    {
      "x": 50,
      "y": 42
    },
    {
      "x": 51,
      "y": 42
    },
    {
      "x": 52,
      "y": 42
    },
    {
      "x": 53,
      "y": 42
    },
    {
      "x": 54,
      "y": 42
    },
    {
      "x": 55,
      "y": 42
    },
    {
      "x": 56,
      "y": 42
    },
    {
      "x": 57,
      "y": 42
    },
    {
      "x": 58,
      "y": 42
    },
    {
      "x": 59,
      "y": 42
    },
    {
      "x": 60,
      "y": 42
    },
    {
      "x": 61,
      "y": 42
    },
    {
      "x": 62,
      "y": 42
    },
    {
      "x": 63,
      "y": 42
    },
    {
      "x": 64,
      "y": 42
    },
    {
      "x": 65,
      "y": 42
    },
    {
      "x": 66,
      "y": 42
    },
    {
      "x": 67,
      "y": 42
    },
    {
      "x": 68,
      "y": 42
    },
    {
      "x": 69,
      "y": 42
    },
    {
      "x": 70,
      "y": 42
    },
    {
      "x": 71,
      "y": 42
    },
    {
      "x": 72,
      "y": 42
    },
    {
      "x": 73,
      "y": 42
    },
    {
      "x": 74,
      "y": 42
    },
    {
      "x": 75,
      "y": 42
    },
    {
      "x": 76,
      "y": 42
    },
    {
      "x": 77,
      "y": 42
    },
    {
      "x": 78,
      "y": 42
    },
    {
      "x": 79,
      "y": 42
    },
    {
      "x": 80,
      "y": 42
    },
    {
      "x": 81,
      "y": 42
    },
    {
      "x": 82,
      "y": 42
    },
    {
      "x": 83,
      "y": 42
    },
    {
      "x": 84,
      "y": 42
    },
    {
      "x": 85,
      "y": 42
    },
    {
      "x": 86,
      "y": 42,
      "name": "\u001bA Pit\u001b",
      "description": "\u001bYou have found a small pit between the rocks. It is a perfect shelter for those\u001b\r\n\u001bwho like such environment. The walls of the pit are rocky. On the bottom there\u001b\r\n\u001bis some wet soil between the stones. A damp smell fills the air.\u001b\r\n",
      "exits": {
        "up": {
          "door": false,
          "pipeTo": {
            "x": 85,
            "y": 36
          }
        }
      },
      "terrain": "hills",
      "map": "goblins2oc"
    },
    {
      "x": 87,
      "y": 42
    },
    {
      "x": 88,
      "y": 42
    },
    {
      "x": 89,
      "y": 42
    },
    {
      "x": 90,
      "y": 42
    },
    {
      "x": 91,
      "y": 42
    },
    {
      "x": 92,
      "y": 42
    },
    {
      "x": 93,
      "y": 42
    }
  ],
  [
    {
      "x": 44,
      "y": 41
    },
    {
      "x": 45,
      "y": 41
    },
    {
      "x": 46,
      "y": 41
    },
    {
      "x": 47,
      "y": 41
    },
    {
      "x": 48,
      "y": 41
    },
    {
      "x": 49,
      "y": 41
    },
    {
      "x": 50,
      "y": 41
    },
    {
      "x": 51,
      "y": 41
    },
    {
      "x": 52,
      "y": 41
    },
    {
      "x": 53,
      "y": 41
    },
    {
      "x": 54,
      "y": 41
    },
    {
      "x": 55,
      "y": 41
    },
    {
      "x": 56,
      "y": 41
    },
    {
      "x": 57,
      "y": 41
    },
    {
      "x": 58,
      "y": 41
    },
    {
      "x": 59,
      "y": 41
    },
    {
      "x": 60,
      "y": 41
    },
    {
      "x": 61,
      "y": 41
    },
    {
      "x": 62,
      "y": 41
    },
    {
      "x": 63,
      "y": 41
    },
    {
      "x": 64,
      "y": 41
    },
    {
      "x": 65,
      "y": 41
    },
    {
      "x": 66,
      "y": 41
    },
    {
      "x": 67,
      "y": 41
    },
    {
      "x": 68,
      "y": 41
    },
    {
      "x": 69,
      "y": 41
    },
    {
      "x": 70,
      "y": 41
    },
    {
      "x": 71,
      "y": 41
    },
    {
      "x": 72,
      "y": 41
    },
    {
      "x": 73,
      "y": 41
    },
    {
      "x": 74,
      "y": 41
    },
    {
      "x": 75,
      "y": 41
    },
    {
      "x": 76,
      "y": 41
    },
    {
      "x": 77,
      "y": 41
    },
    {
      "x": 78,
      "y": 41,
      "name": "At the Wolf Gate",
      "description": "To the west of you is the Wolf Gate, the main entrance to the fabled Goblin-gate, home of the orcs of the Misty Mountains. A few steps are running down from the gate into a narrow valley between tall mountains.",
      "exits": {
        "east": {
          "door": false
        },
        "west": {
          "door": {},
          "pipeTo": {
            "map": "noc",
            "x": 19,
            "y": 3
          }
        }
      },
      "map": "goblins2oc",
      "terrain": "mountains"
    },
    {
      "x": 79,
      "y": 41,
      "map": "goblins2oc",
      "name": "\u001bBend in the Path\u001b",
      "description": "\u001bThe narrow valley turns here, allowing you to proceed to the west or to\u001b\r\n\u001bthe south. A few tufts of grass grow along the path that is covered with\u001b\r\n\u001bfootprints of orcs.\u001b\r\n",
      "exits": {
        "east": {
          "door": {}
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains"
    },
    {
      "x": 80,
      "y": 41,
      "name": "\u001bAbandoned Hovel\u001b",
      "description": "\u001bThe stupid orc who used to live here got lost in the mountains years ago. His\u001b\r\n\u001bfriends are still looking for him. If you don't like orcs, better not to be\u001b\r\n\u001bhere when they show up. The walls and the floor of the room are bare; no\u001b\r\n\u001bobjects can be seen, yet this place is obviously not totally forgotten. There \u001b\r\n\u001bare many cracks in the walls and the floor. In a big crack on the ground there\u001b\r\n\u001bis some water. Another big crack leads south.\u001b\r\n",
      "exits": {
        "south": {
          "door": false,
          "pipeTo": {
            "x": 81,
            "y": 32
          }
        },
        "west": {
          "door": {}
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 81,
      "y": 41
    },
    {
      "x": 82,
      "y": 41
    },
    {
      "x": 83,
      "y": 41
    },
    {
      "x": 84,
      "y": 41,
      "name": "\u001bOld Tomb\u001b",
      "description": "\u001bThe air is thick, making it hard to breathe. A weakhearted person or creature\u001b\r\n\u001bmight have to fight feelings of terror. What happened to the poor soul buried\u001b\r\n\u001bhere? This is not a burial place of anyone who was liked by his people. The\u001b\r\n\u001bhead has been cut of from the body and turned upside down, and through the \u001b\r\n\u001bheart an iron spike has been driven.\u001b\r\n",
      "exits": {
        "up": {
          "door": {}
        }
      },
      "terrain": "cavern",
      "map": "goblins2oc"
    },
    {
      "x": 85,
      "y": 41
    },
    {
      "x": 86,
      "y": 41
    },
    {
      "x": 87,
      "y": 41
    },
    {
      "x": 88,
      "y": 41
    },
    {
      "x": 89,
      "y": 41
    },
    {
      "x": 90,
      "y": 41
    },
    {
      "x": 91,
      "y": 41
    },
    {
      "x": 92,
      "y": 41
    },
    {
      "x": 93,
      "y": 41
    }
  ],
  [
    {
      "x": 44,
      "y": 40
    },
    {
      "x": 45,
      "y": 40
    },
    {
      "x": 46,
      "y": 40
    },
    {
      "x": 47,
      "y": 40
    },
    {
      "x": 48,
      "y": 40
    },
    {
      "x": 49,
      "y": 40,
      "name": "\u001bA Trail along a River\u001b",
      "description": "\u001bThe river to the north is passing through a shallow gorge, while the trail\u001b\r\n\u001bwinds along on the rim. The trail turns from south to west here, following the\u001b\r\n\u001briver as best it can. To the east, a large ridge rises to the south of the\u001b\r\n\u001btrail, sealing it between the mountainside and the river.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "field",
      "map": "goblins2oc"
    },
    {
      "x": 50,
      "y": 40,
      "name": "\u001bA Trail along a River\u001b",
      "description": "\u001bThe trail is very precarious indeed, poised along a gorge to the north and a\u001b\r\n\u001blooming mountainside to the south. Sometimes boulders from frequent rock-\u001b\r\n\u001bslides almost completely block the path, forcing those who wish to pass into\u001b\r\n\u001bdangerously scrambling over the debris. To the east, the gorge seems to be\u001b\r\n\u001bnearing its head, and the trail begins to head down toward the edge of the\u001b\r\n\u001briver.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "field",
      "map": "goblins2oc"
    },
    {
      "x": 51,
      "y": 40,
      "name": "\u001bA Trail along a River\u001b",
      "description": "\u001bThe head of the gorge lies just to the east, and the sheer rock cliff that lies\u001b\r\n\u001bto the south of the trail seems to end there as well. The trail is often\u001b\r\n\u001blittered with debris from frequent rockslides, but at least it is much wider\u001b\r\n\u001bhere than farther to the west.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "field",
      "map": "goblins2oc"
    },
    {
      "x": 52,
      "y": 40,
      "name": "\u001bA Trail along a River\u001b",
      "description": "\u001bThe trail again runs right along the river here, and due to the caprice of\u001b\r\n\u001bnature, the sharp rocks that usually border the riverbed are not as frequent\u001b\r\n\u001bhere, allowing access to the cold water. To the south is a smaller trail that\u001b\r\n\u001bleads down into a canyon that lies to the south of the ridge that begins just\u001b\r\n\u001bwest of here. The trail itself continues east and west, leaving the river\u001b\r\n\u001bbriefly in the latter direction.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "field",
      "map": "goblins2oc"
    },
    {
      "x": 53,
      "y": 40,
      "name": "\u001bA Trail along a River\u001b",
      "description": "\u001bJust to the east, the river bends slightly, and the trail takes advantage of a\u001b\r\n\u001bmagnificent natural granite bridge to cross the shallow gorge. To the west, the\u001b\r\n\u001btrail continues to follow every twist of the river, eventually leading out of\u001b\r\n\u001bthe Misty Mountains and into the Ettenmoors.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false,
          "pipeTo": {
            "x": 53,
            "y": 33
          }
        },
        "west": {
          "door": false
        }
      },
      "terrain": "field",
      "map": "goblins2oc"
    },
    {
      "x": 54,
      "y": 40,
      "name": "\u001bA Sloping Bridge\u001b",
      "description": "\u001bThis massive rock slab has fortuitously formed a bridge across the river's\u001b\r\n\u001bgorge, a fact taken advantage of by the trail. Below runs the river, still\u001b\r\n\u001bdaunting despite being so near to its source. A small ledge situated along the\u001b\r\n\u001bwestern cliff below the bridge might allow one access to the river, but the\u001b\r\n\u001bcurrents there are bound to be dangerous.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "west": {
          "door": false
        },
        "down": {
          "door": false
        }
      },
      "terrain": "field",
      "map": "goblins2oc"
    },
    {
      "x": 55,
      "y": 40,
      "name": "\u001bNear a Fork\u001b",
      "description": "\u001bThe trail forks here, with one branch heading east and following the river to\u001b\r\n\u001bthe south, while the other heads north deeper into the mountains. To the west,\u001b\r\n\u001bthe trail crosses the river gorge via a natural bridge, formed by a single huge\u001b\r\n\u001bblock of granite that, when shrugged off the side of the mountain, somehow\u001b\r\n\u001blanded to form a bridge.\u001b\r\n",
      "exits": {
        "north": {
          "door": false,
          "pipeTo": {
            "map": "mmnorth",
            "x": 59,
            "y": 47
          }
        },
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "field",
      "map": "goblins2oc"
    },
    {
      "x": 56,
      "y": 40,
      "name": "\u001bTrail along a River\u001b",
      "description": "\u001bThe river flows just to the south, about 10 metres below the level of this\u001b\r\n\u001btrail. Either the trail slopes down to the east, or the riverbed slopes up-\u001b\r\n\u001bward, for the two seem to be much closer together to the east. West of this\u001b\r\n\u001bpoint there is a fork in the trail.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 57,
      "y": 40,
      "name": "\u001bUnder the Cliff\u001b",
      "description": "\u001bA daunting cliff looms ominously to the north, dwarfing the small trail that\u001b\r\n\u001bwinds along between its foot and the river to the south. The trail is wet with\u001b\r\n\u001bspray from the turbulent river, and often the footing is treacherous. To the\u001b\r\n\u001beast, the trail gets much closer to the river's edge -- perhaps it would be\u001b\r\n\u001bpossible to enter the water there.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 58,
      "y": 40,
      "name": "\u001bTrail along a River\u001b",
      "description": "\u001bTo the east, the stream becomes more narrow as its source nears. The trail\u001b\r\n\u001bfollows the northern bank, nestled between the icy water and a bleak cliff\u001b\r\n\u001bto the north. The cold peaks of the Misty Mountains rear up to the east -\u001b\r\n\u001ba seemingly impassable obstacle.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 59,
      "y": 40,
      "name": "\u001bTrail along a River\u001b",
      "description": "\u001bThis trail seems to go forever, with no end in sight. With the exception of\u001b\r\n\u001bthe river flowing south of you, all you see is stone. Mountains and mountains\u001b\r\n\u001bof stone...  Perhaps some ancient secret can be found in those mountains, but \u001b\r\n\u001bunless you have some means to cross the river, you'll have to travel farther \u001b\r\n\u001buntil you find a break from the high walls of stone preventing you from \u001b\r\n\u001bleaving this trail.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 60,
      "y": 40,
      "name": "\u001bTrail along a River\u001b",
      "description": "\u001bMore and more in your journey, you begin to wish you never left home. Although\u001b\r\n\u001ba rock face might change, or a river might flow, the scenery is as bleak as \u001b\r\n\u001bcould be. On this section of the path, you are fortunate enough to have the \u001b\r\n\u001bdreariness broken by the sound of a river to the south. Also, you hear a loud\u001b\r\n\u001bsplashing noise to the southeast. Perhaps some vile creature of these mountains\u001b\r\n\u001bis playing in the water?\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 61,
      "y": 40,
      "name": "\u001bTrail along a River\u001b",
      "description": "\u001bExcept for the loud roaring of the waterfall south of here, the mountains\u001b\r\n\u001bseem quiet and lifeless. Even the trail you're standing on seems devoid of\u001b\r\n\u001blife. Only the hardiest creatures could survive in this land for long, and\u001b\r\n\u001byou sense that you would prefer not to have the misfortune of meeting any\u001b\r\n\u001bof them. East and west, this trail continues, seeming to go deeper into the\u001b\r\n\u001bmountains in both directions.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 62,
      "y": 40,
      "name": "\u001bTrail through the Mountains\u001b",
      "description": "\u001bPassing through the lower mountains to the west, the trail follows the side of\u001b\r\n\u001bthe mountain, just east of here, directly to the top. Glancing south, you\u001b\r\n\u001bnotice what is perhaps a mirage of sorts. How could a pool form in these harsh\u001b\r\n\u001bmountains? Nevertheless, you are drawn to it, even if just to quench your\u001b\r\n\u001bthirst, and fill your skins. After all, this might be the last chance in these\u001b\r\n\u001bharsh, lifeless mountains.\u001b\r\n",
      "exits": {
        "south": {
          "door": false
        },
        "west": {
          "door": false
        },
        "up": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 63,
      "y": 40,
      "name": "\u001bTrail over a Mountain Top\u001b",
      "description": "\u001bWinding along the side of the mountain, the trail can be followed to the peak\u001b\r\n\u001bjust east of here. However, to the west you can see a trail that flows next to\u001b\r\n\u001ba river far below. Although near a peak to the east, south of you is a sheer\u001b\r\n\u001bwall towards yet another, higher peak of the Misty Mountains.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "down": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 64,
      "y": 40,
      "name": "\u001bBend in the Trail\u001b",
      "description": "\u001bYou are standing atop one of the lower peaks of the Misty Mountains. At this\u001b\r\n\u001bheight, you can better see much of your surroundings. To the west, a trail\u001b\r\n\u001bseems to be following alongside a river. Another part of that trail seems to\u001b\r\n\u001bgo southeast of here. Due east, more of the strong mountains of this region\u001b\r\n\u001brise out of the ground. To the southwest, there is another peak higher than\u001b\r\n\u001bthis one.\u001b\r\n",
      "exits": {
        "south": {
          "door": false
        },
        "west": {
          "door": false
        },
        "down": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 65,
      "y": 40,
      "name": "\u001bMisty Mountains\u001b",
      "description": "\u001bA large sheet of rock from the south overhangs this area. As you stare at this\u001b\r\n\u001boverhang, your mind drifts into thoughts of its creation at the hands of the\u001b\r\n\u001bancients. A picture comes into your mind of the massive upheaval Arda suffered\u001b\r\n\u001bwhen Morgoth was finally cast out. Even today, many ages later, signs of that\u001b\r\n\u001bbattle remain throughout the lands.  \u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "up": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 66,
      "y": 40,
      "name": "\u001bMisty Mountains\u001b",
      "description": "\u001bThe thick dust of the mountains makes your way difficult. Both to your north\u001b\r\n\u001band south, the way is impassable. However, it appears that you can climb to\u001b\r\n\u001byet another peak of these mountains from here. Perhaps from there you can find\u001b\r\n\u001byour destination, and a way to get there...\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 67,
      "y": 40,
      "name": "\u001bMountain Peak\u001b",
      "description": "\u001bA long chain of mountain peaks are strung along the north and south. However,\u001b\r\n\u001bbecause of the gaps between them, none are accessible from here. From your\u001b\r\n\u001bperch, you notice two mountains with snow-capped peaks. One is south of here,\u001b\r\n\u001bwhile the other is somewhat east. \u001b\r\n",
      "exits": {
        "west": {
          "door": false
        },
        "down": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 68,
      "y": 40
    },
    {
      "x": 69,
      "y": 40
    },
    {
      "x": 70,
      "y": 40
    },
    {
      "x": 71,
      "y": 40
    },
    {
      "x": 72,
      "y": 40
    },
    {
      "x": 73,
      "y": 40
    },
    {
      "x": 74,
      "y": 40
    },
    {
      "x": 75,
      "y": 40
    },
    {
      "x": 76,
      "y": 40
    },
    {
      "x": 77,
      "y": 40
    },
    {
      "x": 78,
      "y": 40
    },
    {
      "x": 79,
      "y": 40,
      "name": "Small Path",
      "description": "The path leaves the shelter of the valley and leads downwards and out to the road in the High Pass to the south. The path is obviously well-used and several of tracks here come from hungry wargs.",
      "exits": {
        "north": {
          "door": false
        },
        "south": {
          "door": false,
          "pipeTo": {
            "x": 78,
            "y": 38
          }
        }
      },
      "map": "goblins2oc",
      "terrain": "mountains"
    },
    {
      "x": 80,
      "y": 40
    },
    {
      "x": 81,
      "y": 40
    },
    {
      "x": 82,
      "y": 40
    },
    {
      "x": 83,
      "y": 40
    },
    {
      "x": 84,
      "y": 40,
      "name": "\u001bSmall Hill\u001b",
      "description": "\u001bLarge rocks cover the western and the northern sides of this strange hill.\u001b\r\n\u001bSome hands must have put the rocks in their places. Hands belonging to\u001b\r\n\u001bliving or undead creatures... who can tell?\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "down": {
          "door": {}
        }
      },
      "terrain": "hills",
      "map": "goblins2oc"
    },
    {
      "x": 85,
      "y": 40,
      "name": "\u001bGreen Slope\u001b",
      "description": "\u001bThe pleasant grassy slope leads down from the small hill to the road. Looking\u001b\r\n\u001beast, you can see before you only ridges and slopes falling towards lowlands\u001b\r\n\u001band plains glimpsed occasionally between the trees.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "field",
      "map": "goblins2oc"
    },
    {
      "x": 86,
      "y": 40,
      "name": "\u001bMaethel Road\u001b",
      "description": "\u001bThe road can take you north, to the distant Eagle's Eyrie and the small \u001b\r\n\u001btrading centre Maethelburg that lies by the river Anduin. To the east you see \u001b\r\n\u001bthe border of a dark forest, and south of here is a small crossing.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "field",
      "map": "goblins2oc"
    },
    {
      "x": 87,
      "y": 40,
      "name": "\u001bDense Forest\u001b",
      "description": "\u001bYou are on the western border of a dark forest. The trees are standing very \u001b\r\n\u001bclosely here, and the thick undergrowth makes it hard to move. West and south \u001b\r\n\u001byou can catch a glimpse of the road leading through the forested landscape. To\u001b\r\n\u001bthe east the forest gets thicker.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "forest",
      "map": "goblins2oc"
    },
    {
      "x": 88,
      "y": 40,
      "name": "\u001bMixed Forest\u001b",
      "description": "\u001bPines, firs and other trees stand together in this quiet forest. You hear\u001b\r\n\u001bno birds and no animals. You watch carefully for any sign of life,\u001b\r\n\u001bknowing that something is here, waiting for its prey in the shadows.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "forest",
      "map": "goblins2oc"
    },
    {
      "x": 89,
      "y": 40,
      "name": "\u001bMixed Forest\u001b",
      "description": "\u001bThe forest is here, quietly abiding its time. You keep turning around to\u001b\r\n\u001bcheck if anything has sneaked up on you from behind. From the treetops you\u001b\r\n\u001bhear the rattling of the leaves in the light breeze from the valley of the\u001b\r\n\u001bgreat river Anduin somewhere in the east.\u001b\r\n",
      "exits": {
        "east": {
          "door": false,
          "pipeTo": {
            "map": 'valewest',
            "x": 10,
            "y": 40,
          }
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "forest",
      "map": "goblins2oc"
    },
    {
      "x": 90,
      "y": 40,
    },
    {
      "x": 91,
      "y": 40,
    },
    {
      "x": 92,
      "y": 40
    },
    {
      "x": 93,
      "y": 40
    }
  ],
  [
    {
      "x": 44,
      "y": 39
    },
    {
      "x": 45,
      "y": 39
    },
    {
      "x": 46,
      "y": 39
    },
    {
      "x": 47,
      "y": 39
    },
    {
      "x": 48,
      "y": 39,
      "name": "\u001bA Trail along a River\u001b",
      "description": "\u001bThe trail winds along the rim of a shallow gorge, through which the river to\u001b\r\n\u001bthe north makes its way. Any slack in alertness may prove dangerous, for the\u001b\r\n\u001btrail is very narrow and in some places slick with spray rising from small\u001b\r\n\u001bcataracts in the river.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "field",
      "map": "goblins2oc"
    },
    {
      "x": 49,
      "y": 39,
      "name": "\u001bA Trail along a River\u001b",
      "description": "\u001bThe trail forks here, with a small and faint spur leading down to the east into\u001b\r\n\u001ba canyon. The main trail turns north, following the path of the turbulent\u001b\r\n\u001briver. The only vegetation in this sombre landscape are bushes that possess\u001b\r\n\u001bwicked thorns, which grab and tear at all exposed clothing.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "field",
      "map": "goblins2oc"
    },
    {
      "x": 50,
      "y": 39
    },
    {
      "x": 51,
      "y": 39
    },
    {
      "x": 52,
      "y": 39
    },
    {
      "x": 53,
      "y": 39
    },
    {
      "x": 54,
      "y": 39
    },
    {
      "x": 55,
      "y": 39
    },
    {
      "x": 56,
      "y": 39,
      "name": "\u001bOn a River\u001b",
      "description": "\u001bDirectly overhead, a massive slab of granite forms a natural bridge over this\u001b\r\n\u001brushing water, connecting the cliffs on either side of the river. The bridge is\u001b\r\n\u001bat least fifteen metres above the water, but it may be accessible due to a\u001b\r\n\u001bsmall ledge on the western cliff.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "up": {
          "door": false
        }
      },
      "terrain": "water",
      "map": "goblins2oc"
    },
    {
      "x": 57,
      "y": 39,
      "name": "\u001bOn a River\u001b",
      "description": "\u001bThe water here is shallow, but still swift enough to be dangerous, and its icy\u001b\r\n\u001btouch numbs the flesh almost instantly. The source of this stream must not be\u001b\r\n\u001btoo much farther upstream -- the water is crystal clear. There is a trail\u001b\r\n\u001brunning along the northern bank of the river, but it is impossible to reach due\u001b\r\n\u001bto the jagged rocks and swift currents.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "water",
      "map": "goblins2oc"
    },
    {
      "x": 58,
      "y": 39,
      "name": "\u001bRiver\u001b",
      "description": "\u001bHere the river widens slightly, slowing down the strong flow of water. Sheer\u001b\r\n\u001bwalls of stone block any attempt to get out of the river, forcing you to \u001b\r\n\u001bchoose to follow the water west, or to try and follow it east. Perhaps you\u001b\r\n\u001bwill soon be able to leave the water, and continue your journey...\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "water",
      "map": "goblins2oc"
    },
    {
      "x": 59,
      "y": 39,
      "name": "\u001bRiver\u001b",
      "description": "\u001bWith a persistence beyond the powers of Morgoth, the water continues its \u001b\r\n\u001bjourney westwards. The flume through which the water flows here is cut deep\u001b\r\n\u001binto the mountains, cutting off passage to the south. However, you can climb\u001b\r\n\u001bnorth on to a worn path running through the mountains. To the east, you can\u001b\r\n\u001bfollow the river upstream up to a waterfall farther along.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "water",
      "map": "goblins2oc"
    },
    {
      "x": 60,
      "y": 39,
      "name": "\u001bRiver\u001b",
      "description": "\u001bThe river here is thin and shallow. Although very cold, the water appears\u001b\r\n\u001bvery muddy. Almost red in hue, it flows towards the west. To the east, you\u001b\r\n\u001bsee a waterfall. South of here, you see nothing but mountains. Perhaps, the\u001b\r\n\u001btrail north of here leads someplace interesting...\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "water",
      "map": "goblins2oc"
    },
    {
      "x": 61,
      "y": 39,
      "name": "\u001bBottom of a Waterfall\u001b",
      "description": "\u001bWater falling from above splashes you, cleaning some of the dirt caked on\u001b\r\n\u001byour body. Dripping from your clothes, the water (and dirt) flows west, down\u001b\r\n\u001bthrough the mountains. A smile comes to your face when you consider just how\u001b\r\n\u001bfar your dirt will travel before coming to rest at the bottom of the sea.\u001b\r\n",
      "terrain": "shallowWater",
      "exits": {
        "north": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        },
        "up": {
          "door": false
        }
      },
      "map": "goblins2oc"
    },
    {
      "x": 62,
      "y": 39,
      "name": "\u001bSource of the River\u001b",
      "description": "\u001bSo different from the rest of the scenery here, a large pool of water stands\u001b\r\n\u001bin the middle of this landing. It seems to be fed from underground, perhaps\u001b\r\n\u001bfrom somewhere beyond the sheer face of rock to the east...  The water drains\u001b\r\n\u001bdirectly down in a thin, but quickly flowing, waterfall. The falling water\u001b\r\n\u001bflows west to form a thin river through the mountains. There appears to be a\u001b\r\n\u001btrail of sorts directly north of here, and to the east you spy a small camp.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "down": {
          "door": false,
          "pipeTo": {
            "x": 61,
            "y": 39
          }
        }
      },
      "terrain": "mountains",
      "flags": "water",
      "map": "goblins2oc"
    },
    {
      "x": 63,
      "y": 39,
      "name": "\u001bA Crude Camp\u001b",
      "description": "\u001bTaking advantage of the sloping lands, a small camp has been erected here. \u001b\r\n\u001bUsing the rising cusps as walls, the roof has been made of thatched branches\u001b\r\n\u001band pelts. Inside, a small fireplace stands cold, a few bits of broken bones \u001b\r\n\u001bstand testament for its use. Off to one corner, a lantern glows dimly, giving\u001b\r\n\u001benough light to illuminate the surroundings.\u001b\r\n",
      "exits": {
        "west": {
          "door": false
        }
      },
      "terrain": "hills",
      "map": "goblins2oc"
    },
    {
      "x": 64,
      "y": 39,
      "name": "\u001bTrail over a Mountain Top\u001b",
      "description": "\u001bWinding along the side of the mountains, the trail can be followed to a peak\u001b\r\n\u001bjust north of here. As well, there is a very high, snowcapped peak towering\u001b\r\n\u001babove you. You notice signs of the trail also going south and east of here.\u001b\r\n\u001bUnfortunately, the blandness of the Misty Mountains hides from your eyes the\u001b\r\n\u001btrue path.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "down": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 65,
      "y": 39,
      "name": "\u001bTrail through the Mountains\u001b",
      "description": "\u001bHere, the trail becomes faint and appears to continue in several directions.\u001b\r\n\u001bYou can follow it up towards a mountain top, or east and south deeper into the\u001b\r\n\u001bmountains. If only you had a map of the region, things might be easier. \u001b\r\n\u001bUnfortunately, not even a map would help much, as by some evil magic, the true\u001b\r\n\u001bpath seems to change occasionally. Or perhaps that's just a false rumour spread\u001b\r\n\u001bby those creatures who would rather have you as a dinner guest...\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "up": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 66,
      "y": 39,
      "name": "\u001bTrail through the Mountains\u001b",
      "description": "\u001bA rather smooth trail begins here and heads off towards a ridge in the east.\u001b\r\n\u001bAs well, you can follow a smaller path up to the peak of a mountain. Southeast\u001b\r\n\u001bof here, an extremely tall mountain rises high into the clouds. Perhaps if you\u001b\r\n\u001bcould find your way to that peak, you could see the region better, and find \u001b\r\n\u001byour way out of here.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 67,
      "y": 39,
      "name": "\u001bTrail beside a Peak\u001b",
      "description": "\u001bFrom this peak, you have a clear view of a much higher peak rising to the \u001b\r\n\u001bsouth. The way to it, however, seems to be complicated. A trail continues east\u001b\r\n\u001bfrom here, as well as going down from the peak, and west. In the distance, you\u001b\r\n\u001bsee a chain of more peaks in the west. To the north, the long chains all seem\u001b\r\n\u001bto come together.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        },
        "up": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 68,
      "y": 39,
      "name": "\u001bTrail Leading Upwards\u001b",
      "description": "\u001bYou are walking on a trail leading up the side of a mountain. A vast range of\u001b\r\n\u001bmountains spreads out to the west and south, with a magnificent line of peaks\u001b\r\n\u001bweaving down towards the south-east. To the east the path you are on leads \u001b\r\n\u001bstraight as an arrow towards a tall pillar of rock, and to the north lies a\u001b\r\n\u001bdense forest, but no obvious way of getting there.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 69,
      "y": 39,
      "name": "\u001bTrail through the Mountains\u001b",
      "description": "\u001bThe ground underfoot is somewhat rocky, but much easier going than travelling\u001b\r\n\u001bthrough the mountains. Obviously it was built by someone or something walking\u001b\r\n\u001bthis way many times, for it is both relatively worn and wide. To the east the\u001b\r\n\u001bpath seems to lead towards a pillar of rock, while several miles to the west\u001b\r\n\u001bthe path starts to lead up the side of a mountain.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 70,
      "y": 39,
      "name": "\u001bTrail through the Mountains\u001b",
      "description": "\u001bStanding in the middle of this rocky trail, the magnificence of the surrounding\u001b\r\n\u001bmountains is obvious for anyone who would care to look. The wandering line\u001b\r\n\u001bof mountain-tops leads your eyes in a mesmerising dance, light glistening off\u001b\r\n\u001bthe caps like sun on wet skin. The trail you are on leads west and east.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 71,
      "y": 39,
      "name": "\u001bTrail through the Mountains\u001b",
      "description": "\u001bLeading east and west, this trail seems to meet another at some short distance\u001b\r\n\u001bin the east. In the east there appears to be a large pillar of black stone, \u001b\r\n\u001bstanding out in sharp contrast to the whiteness of the snow-clad mountains\u001b\r\n\u001bbehind.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 72,
      "y": 39,
      "name": "\u001bTrail through the Mountains\u001b",
      "description": "\u001bDirectly in the east the path you are on intersects another leading north and\u001b\r\n\u001bsouth. In the east stands a tall pillar of stone, contrasting with the snow-\u001b\r\n\u001bclad mountain tops behind it. The shape of the pillar is not obvious from this \u001b\r\n\u001bfar away, but it definitely seems to have been carved, either by hand or by the\u001b\r\n\u001bweather. To the west the path leads up a mountain in the distance.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 73,
      "y": 39,
      "name": "\u001bCrossing of the Paths\u001b",
      "description": "\u001bThis is the intersection of the well travelled path leading east-west, and an\u001b\r\n\u001bolder but less used path leading north-south. The mountainside slopes steeply\u001b\r\n\u001bdownwards in the north, and rises slightly to the south. In the east stands a\u001b\r\n\u001bblack pillar which has obviously been shaped by something, but its precise\u001b\r\n\u001bform cannot be told from this distance.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 74,
      "y": 39,
      "name": "\u001bTrail through the Mountains\u001b",
      "description": "\u001bIn the east the path seems to make a bend to the south, veering around a\u001b\r\n\u001blarge, imposing pillar of black rock. In the south the cloud-wreathed tops of\u001b\r\n\u001bthe mountains rear high, while in the west the path leads up the side of a\u001b\r\n\u001bsteep mountain.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 75,
      "y": 39,
      "name": "\u001bBend in the Trail\u001b",
      "description": "\u001bThe trail bends south and west here, around a large pillar of black rock\u001b\r\n\u001bstanding at the side of the road. To the south the trail seems to make a sharp\u001b\r\n\u001bbend, and in the west the trail continues, rising up the side of a far off\u001b\r\n\u001bmountain. The pillar of rock here appears to have been crudely carved in the\u001b\r\n\u001bshape of a humanoid, wearing some sort of crown, but the ravages of the\u001b\r\n\u001bweather make further detail impossible to see.\u001b\r\n",
      "exits": {
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 76,
      "y": 39
    },
    {
      "x": 77,
      "y": 39
    },
    {
      "x": 78,
      "y": 39
    },
    {
      "x": 79,
      "y": 39
    },
    {
      "x": 80,
      "y": 39
    },
    {
      "x": 81,
      "y": 39
    },
    {
      "x": 82,
      "y": 39,
      "name": "\u001bHidden Valley\u001b",
      "description": "\u001bThis small valley is nestled in the foothills of the Misty Mountains, and\u001b\r\n\u001bit has a quiet unspoiled demeanour. On either side steep walls guard its\u001b\r\n\u001bperimeter - the only means of egress appears to be a small tunnel behind\u001b\r\n\u001ba rock to the south.\u001b\r\n",
      "exits": {
        "south": {
          "door": {}
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 83,
      "y": 39
    },
    {
      "x": 84,
      "y": 39,
      "name": "\u001bTrail to the High Pass\u001b",
      "description": "\u001bLooking south, you see the mountain range that separates the upper and lower\u001b\r\n\u001bbranches of the High Pass. To the west the trail is climbing up into the\u001b\r\n\u001bmountains, and to the east it becomes wider. North of here is something that\u001b\r\n\u001blooks like a hill.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "up": {
          "door": false,
          "pipeTo": {
            "map": "mmnorth",
            "x": 89,
            "y": 45
          }
        }
      },
      "terrain": "road",
      "map": "goblins2oc"
    },
    {
      "x": 85,
      "y": 39,
      "name": "\u001bRoad to the Lower Pass\u001b",
      "description": "\u001bThe road has now left the high mountains to the west and allows you to travel \u001b\r\n\u001bmuch more easily. To the east you see a road intersection in the middle of the\u001b\r\n\u001bwilderness. On the north side of the road pleasant green slopes invite you to \u001b\r\n\u001bcome and visit.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "road",
      "map": "goblins2oc"
    },
    {
      "x": 86,
      "y": 39,
      "name": "\u001bCrossing\u001b",
      "description": "\u001bThe road from Maethelburg connects here to the road leading to the Lower\u001b\r\n\u001bPass and to the Old Dwarven Road which starts to the east of here.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "road",
      "map": "goblins2oc"
    },
    {
      "x": 87,
      "y": 39,
      "name": "\u001bNorth of a Ford\u001b",
      "description": "\u001bYou are on the remnants of the Old Dwarven Road which connects Mount Erebor far\u001b\r\n\u001bin the east with Eriador to the west. Now only the fearless Beornings prevent\u001b\r\n\u001bthe road from disappearing completely. A bit west the road splits into two\u001b\r\n\u001bparts, each leading through one branch of the High Pass.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "road",
      "map": "goblins2oc"
    },
    {
      "x": 88,
      "y": 39,
      "name": "\u001bMixed Forest\u001b",
      "description": "\u001bThis mixed forest at the foot of the Misty Mountains consists of tall pines,\u001b\r\n\u001bold oaks and stout elms. Just south of here, you hear the sound of running\u001b\r\n\u001bwater. It is a small stream carrying fresh water from the mountains to feed\u001b\r\n\u001bthe Anduin somewhere in the south.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "forest",
      "map": "goblins2oc"
    },
    {
      "x": 89,
      "y": 39,
      "name": "\u001bForest\u001b",
      "description": "\u001bThis is the east rim of the dark pine forest at the foot of the mountain\u001b\r\n\u001brange. Trees obscure your view to the west, and it seems to you that\u001b\r\n\u001bthere is something moving in their shadow. To the south you can see a \u001b\r\n\u001bsmall forest river.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false,
          "pipeTo": {
            "map": 'valewest',
            "x": 10,
            "y": 39,
          } 
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "forest",
      "map": "goblins2oc"
    },
    {
      "x": 90,
      "y": 39,
    },
    {
      "x": 91,
      "y": 39,
    },
    {
      "x": 92,
      "y": 39
    },
    {
      "x": 93,
      "y": 39
    }
  ],
  [
    {
      "x": 44,
      "y": 38
    },
    {
      "x": 45,
      "y": 38
    },
    {
      "x": 46,
      "y": 38,
    },
    {
      "x": 47,
      "y": 38,
      "name": "\u001bA Trail along a River\u001b",
      "description": "\u001bStill next to the river, the trail has become a bit wider. It is made of dirt\u001b\r\n\u001band rock, and anything that used to grow on it has long since been killed off\u001b\r\n\u001bby the countless passing of unknown creatures. Due to the trail's good\u001b\r\n\u001bcondition, you are able to pass by this area quickly.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "west": {
          "door": false,
          "pipeTo": {
            "map": "mmwest",
            "x": 45,
            "y": 20
          }
        }
      },
      "terrain": "field",
      "map": "goblins2oc"
    },
    {
      "x": 48,
      "y": 38,
      "name": "\u001bA Trail along a River\u001b",
      "description": "\u001bThe trail continues alongside the river, bending here from west to north. The\u001b\r\n\u001bpath is sometimes obstructed by large boulders that have been dislodged from\u001b\r\n\u001bthe mountain to the south -- whether by natural forces or by the near-legendary\u001b\r\n\u001bstone giants may never be known. The river to the north is inaccessible from \u001b\r\n\u001bhere because of the treacherous rocks that line its course.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "field",
      "map": "goblins2oc"
    },
    {
      "x": 49,
      "y": 38
    },
    {
      "x": 50,
      "y": 38
    },
    {
      "x": 51,
      "y": 38
    },
    {
      "x": 52,
      "y": 38
    },
    {
      "x": 53,
      "y": 38
    },
    {
      "x": 54,
      "y": 38
    },
    {
      "x": 55,
      "y": 38
    },
    {
      "x": 56,
      "y": 38
    },
    {
      "x": 57,
      "y": 38
    },
    {
      "x": 58,
      "y": 38
    },
    {
      "x": 59,
      "y": 38
    },
    {
      "x": 60,
      "y": 38,
      "name": "\u001bMisty Mountains\u001b",
      "description": "\u001bYou find yourself inside a U-shaped valley in the mountains. North of here, a\u001b\r\n\u001bthin river flows westwards, but there is no way to pass it without a boat of\u001b\r\n\u001bsome sort. South, the valley tends to gain altitude. If you are unable to pass\u001b\r\n\u001bthe river, perhaps your destiny lies to the south.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 61,
      "y": 38,
      "name": "\u001bMisty Mountains\u001b",
      "description": "\u001bThe river to the north must have dug this alcove in the side of the mountains.\u001b\r\n\u001bThe only exit appears to be back to the river that you came from. However, this\u001b\r\n\u001balcove does seem to offer protection from the creatures of the mountains. \u001b\r\n\u001bPerhaps this would be a good place to rest for a while.  \u001b\r\n",
      "exits": {
        "north": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 62,
      "y": 38,
      "name": "\u001bMountain Peak\u001b",
      "description": "\u001bRising high from the ground below, the mountains tower over the land. Higher\u001b\r\n\u001bstill, the peaks of the mountains look down upon the terrain with a view of\u001b\r\n\u001ball that can be seen. Unfortunately, there isn't much. Just north, you can\u001b\r\n\u001bsee a large pool of water that falls into a river. East of that pool, a\u001b\r\n\u001bsingle peak rises high above the rest. In all other directions, you see\u001b\r\n\u001bnothing but the cold stone of the Misty Mountains.\u001b\r\n",
      "exits": {
        "down": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 63,
      "y": 38,
      "name": "\u001bMisty Mountains\u001b",
      "description": "\u001bYou find yourself on a slope that rises higher into the mountains, coming to a\u001b\r\n\u001bpeak above you. The slope flows south, falling into a lower part of the\u001b\r\n\u001bmountains. Sharp overhangs from the north and east taunt you. Passage in those\u001b\r\n\u001bdirections is impossible.   \u001b\r\n",
      "exits": {
        "south": {
          "door": false
        },
        "up": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 64,
      "y": 38,
      "name": "\u001bMisty Mountains\u001b",
      "description": "\u001bIf you've become tired of wandering aimlessly through the mountains, perhaps\u001b\r\n\u001byou are in luck. East of here you notice a trail made in this treacherous\u001b\r\n\u001bquarry called the Misty Mountains. Of course, if exploration is on your mind,\u001b\r\n\u001bfollow the slope down to the south.  Perhaps you can find some lost treasure\u001b\r\n\u001bdeep within the mountains.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 65,
      "y": 38,
      "name": "\u001bNarrow Trail\u001b",
      "description": "\u001bYou find that the trail runs through a valley at this point. You are able to\u001b\r\n\u001bmake your way along the trail north and south. As well, you can follow a slope\u001b\r\n\u001bup towards the west. A large pile of rocks and boulders block any chance of\u001b\r\n\u001bpassing to east.  \u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "hills",
      "map": "goblins2oc"
    },
    {
      "x": 66,
      "y": 38,
      "name": "\u001bAmong the Mountains\u001b",
      "description": "\u001bThe wind in this region of the mountains is much stronger than elsewhere. \u001b\r\n\u001bWhipping about you, it desperately tries to knock you off balance. Just south\u001b\r\n\u001bof you, you see a trail that travels through the mountains. Perhaps it leads\u001b\r\n\u001bto refuge of some sort. As well, you can climb to a mountain peak from here.  \u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "south": {
          "door": false
        },
        "up": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 67,
      "y": 38,
      "name": "\u001bMountain Peak\u001b",
      "description": "\u001bAs with the slope leading up here, the wind is stronger then you'd like. \u001b\r\n\u001bHowever, from this height, the threat of falling is much greater. You decide\u001b\r\n\u001byou should make haste and leave from this mountain top as soon as you can. \u001b\r\n\u001bBefore you leave, however, you notice that the peak just south of you rises \u001b\r\n\u001bhigher than most of the mountains in this area. Curiosity drives you to\u001b\r\n\u001bexplore it further.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "down": {
          "door": false,
          "pipeTo": {
            "x": 66,
            "y": 38
          }
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 68,
      "y": 38,
      "name": "\u001bOn the Mountainside\u001b",
      "description": "\u001bYou carefully pick your way between the rocks, knowing that one small slip\u001b\r\n\u001bcould mean death. Looking up you can see the majestic peak of this mountain\u001b\r\n\u001bwith the line of peaks snaking away to the south east like a worm glistening \u001b\r\n\u001bin the sun.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "up": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 69,
      "y": 38
    },
    {
      "x": 70,
      "y": 38
    },
    {
      "x": 71,
      "y": 38
    },
    {
      "x": 72,
      "y": 38
    },
    {
      "x": 73,
      "y": 38,
      "name": "\u001bTrail on a Mountainside\u001b",
      "description": "\u001bThis rough trail leads south along a narrow ledge on the side of a mountain.\u001b\r\n\u001bThe way both up and down is too steep for you to attempt to climb, so you must\u001b\r\n\u001beither follow this trail south, or go north onto the path leading east and\u001b\r\n\u001bwest.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "hills",
      "map": "goblins2oc"
    },
    {
      "x": 74,
      "y": 38,
      "map": "goblins2oc",
      "name": "A Slag Pit",
      "description": "The path slopes downward quickly into a large pit filled with loose rocks and gravel.  Smoke rises from a tunnel in the mountainside, only to be dispersed by the cool mountain air.  Broken tools and scraps of cloth litter the area, along with some bones.",
      "exits": {
        "south": {
          "door": false
        },
        "down": {
          "door": false,
          "pipeTo": {
            "map": "noc",
            "x": 17,
            "y": -4
          }
        }
      },
      "terrain": "mountains"
    },
    {
      "x": 75,
      "y": 38,
      "map": "goblins2oc",
      "name": "Bend in the Trail",
      "description": "The trail bends north and east here, around a large pillar of black rock standing at the side of the road. In the east the trail continues further into the range of mountains, and to the north the trail seems to make another sharp bend. From this distance the pillar of rock appears to have been crudely carved in the shape of a humanoid, wearing some sort of crown, but the ravages of the weather make further detail impossible to see. To the south there is a rocky path leading to a wooden hut.",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "mountains"
    },
    {
      "x": 76,
      "y": 38,
      "map": "goblins2oc",
      "name": "Trail through the Mountains",
      "description": "The trail leads east and west. In the west there stands an imposing black pillar of rock, that seems to have some sort of discernible shape, but it is too far away for its exact shape to be seen. To the east the path carries on further into the foothills, in the west the path bends to the north.",
      "exits": {
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains"
    },
    {
      "x": 77,
      "y": 38,
      "map": "goblins2oc",
      "name": "Trail through the Mountains",
      "description": "The trail leads east and west here, and in the west there stands a large pillar of black stone, silhouetted against the white background of the snow-capped misty mountains. North of you rises an impenetrable wall of rock, while in the south the mountains fall away into a valley before rising again to form a magnificent line of peaks.",
      "exits": {
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains"
    },
    {
      "x": 78,
      "y": 38,
      "map": "goblins2oc",
      "name": "Small Crossing",
      "description": "To the north you can see a narrow valley between two high mountain peaks. The trail continues to the west, to the High Pass, and to the east, down the mountain slope. You notice a trampled path leading north into the small valley. Examining it closer, you understand that it is well-used.",
      "exits": {
        "north": {
          "pipeTo": {
            "x": 79,
            "y": 40
          },
          "door": false
        },
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "road"
    },
    {
      "x": 79,
      "y": 38,
      "name": "Trail through the Mountains",
      "description": "The road takes you over a ridge. Occasional tufts of grass are spread along the road. There is no other vegetation in this very hostile environment.",
      "exits": {
        "west": {
          "door": false
        },
        "down": {
          "door": false,
          "pipeTo": {
            "x": 81,
            "y": 36
          }
        }
      },
      "terrain": "road",
      "map": "goblins2oc"
    },
    {
      "x": 80,
      "y": 38
    },
    {
      "x": 81,
      "y": 38,
      "name": "\u001bRoad through the Lower Pass\u001b",
      "description": "\u001bEntering through the mouth of the valley to the east, the road climbs steadily\u001b\r\n\u001bup the southern mountain slope, faithfully following all the bends of the\u001b\r\n\u001bmountainside.\u001b\r\n",
      "terrain": "road",
      "exits": {
        "east": {
          "door": false
        },
        "up": {
          "door": false,
          "pipeTo": {
            "x": 80,
            "y": 36
          }
        }
      },
      "map": "goblins2oc"
    },
    {
      "x": 82,
      "y": 38,
      "name": "\u001bRoad through the Lower Pass\u001b",
      "description": "\u001bThe road is quite intact here, making travel a lot easier. It runs along the\u001b\r\n\u001bsouthern side of the mountain, allowing you to climb the pass to the west or\u001b\r\n\u001bto leave the mountains and head to the Anduin vale somewhere east of here.\u001b\r\n\u001bThe mountainside to the north looks very smooth.\u001b\r\n",
      "exits": {
        "north": {
          "door": {}
        },
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "road",
      "map": "goblins2oc"
    },
    {
      "x": 83,
      "y": 38,
      "name": "\u001bRoad through the Lower Pass\u001b",
      "description": "\u001bYou are on a stony path winding downwards with a rocky wall to the north.\u001b\r\n\u001bTo the south the ground slopes away and there is a dell below the level\u001b\r\n\u001bof the path overhung with bushes and low trees.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "road",
      "map": "goblins2oc"
    },
    {
      "x": 84,
      "y": 38,
      "name": "\u001bRoad through the Lower Pass\u001b",
      "description": "\u001bYou have now reached the bottom of the valley. To the north, a rather high\u001b\r\n\u001bcliff towers above you. From the south, you hear the splashing of water, and\u001b\r\n\u001blooking there, you see the source of a small stream, flowing slowly along\u001b\r\n\u001bthe road to the east. To the west a narrow trail leads up to an even narrower\u001b\r\n\u001bgap in the Misty Mountains.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "road",
      "map": "goblins2oc"
    },
    {
      "x": 85,
      "y": 38,
      "name": "\u001bBend in the Road\u001b",
      "description": "\u001bThe trail bends sharply here, rising higher to the west. East and south there\u001b\r\n\u001bis a small cliff, below which can be seen a rapidly moving mountain stream.\u001b\r\n\u001bNorthward, the path continues its journey among the eastern foothills of the\u001b\r\n\u001bMisty Mountains. \u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "road",
      "map": "goblins2oc"
    },
    {
      "x": 86,
      "y": 38,
      "name": "\u001bMountain Stream\u001b",
      "description": "\u001bThe stream takes a sharp bend here, flowing beneath a small cliff to the north.\u001b\r\n\u001bThe water is frigid, fed by snowmelt from the peaks to the far west. To the \u001b\r\n\u001beast, the stream widens a bit and becomes shallower before disappearing into a\u001b\r\n\u001bforest.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "water",
      "map": "goblins2oc"
    },
    {
      "x": 87,
      "y": 38,
      "name": "\u001bThe Ford\u001b",
      "description": "\u001bHere the Old Dwarven Road leads across the little forest stream running east\u001b\r\n\u001bfrom the mountains towards the Anduin. The water looks cold, clear and quite\u001b\r\n\u001bsafe to drink.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "shallowWater",
      "map": "goblins2oc"
    },
    {
      "x": 88,
      "y": 38,
      "name": "\u001bForest River\u001b",
      "description": "\u001bThe one who cups his hand for a sip and plunges it in the water might quickly\u001b\r\n\u001bpull it back. The water is freezing even during the summer. On the north and\u001b\r\n\u001bthe south banks huge trees guard the river. Looking upstream, to the west,\u001b\r\n\u001byou see a small path crossing the stream.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "water",
      "map": "goblins2oc"
    },
    {
      "x": 89,
      "y": 38,
      "name": "\u001bForest River\u001b",
      "description": "\u001bThe small river cheerfully carries its waters through the sombre forest.\u001b\r\n\u001bThe low mossy banks are covered with tall pine-trees which drop their cones,\u001b\r\n\u001bneedles and branches into the clear water of the current.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false,
          "pipeTo": {
            "map": 'valewest',
            "x": 10,
            "y": 38,
          }

        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "water",
      "map": "goblins2oc"
    },
    {
      "x": 90,
      "y": 38,
    },
    {
      "x": 91,
      "y": 38,
    },
    {
      "x": 92,
      "y": 38
    },
    {
      "x": 93,
      "y": 38
    }
  ],
  [
    {
      "x": 44,
      "y": 37,
    },
    {
      "x": 45,
      "y": 37,
    },
    {
      "x": 46,
      "y": 37,
    },
    {
      "x": 47,
      "y": 37
    },
    {
      "x": 48,
      "y": 37
    },
    {
      "x": 49,
      "y": 37
    },
    {
      "x": 50,
      "y": 37
    },
    {
      "x": 51,
      "y": 37
    },
    {
      "x": 52,
      "y": 37
    },
    {
      "x": 53,
      "y": 37
    },
    {
      "x": 54,
      "y": 37
    },
    {
      "x": 55,
      "y": 37
    },
    {
      "x": 56,
      "y": 37
    },
    {
      "x": 57,
      "y": 37
    },
    {
      "x": 58,
      "y": 37
    },
    {
      "x": 59,
      "y": 37
    },
    {
      "x": 60,
      "y": 37,
      "name": "\u001bMisty Mountains\u001b",
      "description": "\u001bAs you walk through the valley, you trip on a pile of rocks on the rocky\u001b\r\n\u001bground. Looking around, you can see that this debris must have fallen from the\u001b\r\n\u001bmountain tops east of you. A rock slide must have occurred recently. Hopefully,\u001b\r\n\u001bthis valley will be spared another while you are here. The valley begins to\u001b\r\n\u001bslope upwards south of here, while going down towards the north where you\u001b\r\n\u001bthink you hear the sounds of splashing water.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 61,
      "y": 37,
      "name": "\u001bMountain Peak\u001b",
      "description": "\u001bThe climb up the side of the mountain to the peak was more difficult than you\u001b\r\n\u001bthought at first. Luckily, you made it safely to the top. The humidity of the\u001b\r\n\u001bair here is quite high, and your vision is blurred in the mist. Looking back\u001b\r\n\u001bdown the direction you came, the mountainside is littered with rubble.\u001b\r\n",
      "exits": {
        "west": {
          "door": false
        },
        "down": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 62,
      "y": 37,
      "name": "\u001bMisty Mountains\u001b",
      "description": "\u001bBoth north and west of you, the mountains come to peaks. You can also see a\u001b\r\n\u001bwall of peaks going from the southwest to the northeast. Although you can\u001b\r\n\u001bclimb up the western peak, the one to the north offers nothing but a vertical\u001b\r\n\u001bwall to its top. Perhaps it can be accessed from another side. A small\u001b\r\n\u001bcave-like alcove is south of you. \u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "up": {
          "door": false,
          "pipeTo": {
            "x": 61,
            "y": 37
          }
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 63,
      "y": 37,
      "name": "\u001bMisty Mountains\u001b",
      "description": "\u001bFear strikes you as you recall horror stories of large stone giants that \u001b\r\n\u001bwander deep in these mountains. Sloping upwards from the east, the mountains\u001b\r\n\u001bcontinue north and west of here. From your location low in the mountains, not\u001b\r\n\u001bmuch is visible here except for the endless ranges of stone and rock. With the\u001b\r\n\u001btrail lost, you might never find your way out of the mountains. \u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 64,
      "y": 37,
      "name": "\u001bMisty Mountains\u001b",
      "description": "\u001bYou find yourself wandering in a deep valley.  All around you, the hard, grey\u001b\r\n\u001bstone walls of the mountains look down upon you. Both to your east and west\u001b\r\n\u001byou see the high peaks of mountains. Above you, you notice a shelf that you\u001b\r\n\u001bcan reach. North and west, the valley slopes upwards, rising towards the high\u001b\r\n\u001bmountain tops. \u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "west": {
          "door": false
        },
        "up": {
          "door": false,
          "pipeTo": {
            "x": 65,
            "y": 37
          }
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 65,
      "y": 37,
      "name": "\u001bAmong the Mountains\u001b",
      "description": "\u001bThe trail is stronger here, giving you a sense of confidence. A slope to the\u001b\r\n\u001bsouth appears to lead up to a mountain peak. However, your trail travels from\u001b\r\n\u001bthe north, continuing east. The eastward path, which flanks the steep, bare,\u001b\r\n\u001bdangerous-looking flank of a mountain, goes up towards another peak in the\u001b\r\n\u001bMisty Mountains.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "hills",
      "map": "goblins2oc"
    },
    {
      "x": 66,
      "y": 37,
      "name": "\u001bAmong the Mountains\u001b",
      "description": "\u001bAs you follow the trail, the passage to the west is suddenly filled with large\u001b\r\n\u001bboulders! Luckily, you dodge the falling rocks. Unfortunately, passage that way\u001b\r\n\u001bis now hopelessly blocked. However, the trail appears to continue high up to a\u001b\r\n\u001bsnow capped peak. Although it's a steep climb, you should be able to manage it\u001b\r\n\u001bwithout too much problem.   \u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "hills",
      "map": "goblins2oc"
    },
    {
      "x": 67,
      "y": 37,
      "name": "\u001bAtop a Peak\u001b",
      "description": "\u001bAfter a tough climb, you finally manage to reach the peak of this mountain. All\u001b\r\n\u001baround you, ageless snow rests. There is some fresh snow sifted on the very top\u001b\r\n\u001bof the snow cap. From this height, you can see that a long chain of mountains\u001b\r\n\u001bare strung from the north to south. Looking back down, you realise that your\u001b\r\n\u001bclimb up must have dislodged some of the mountainside, as returning below is\u001b\r\n\u001bnow impossible.\u001b\r\n",
      "exits": {
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 68,
      "y": 37,
      "name": "\u001bOn Top of a Mountain\u001b",
      "description": "\u001bStanding on top of one of the highest peaks in this range, you are overwhelmed\u001b\r\n\u001bby the power of the sight that greets you. In the north you catch a glimpse\u001b\r\n\u001bof a path leading east to west, and beyond that a dense forest. To the south\u001b\r\n\u001beast this line of peaks leads your eyes in a dazzling sinuous dance.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "down": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 69,
      "y": 37,
      "map": "goblins2oc",
      "name": "\u001bOn Top of a Mountain\u001b",
      "description": "\u001bThe snow crunches underfoot as you travel precariously along a narrow ridge.\u001b\r\n\u001bThe way down the west side of this mountain looks impossible, but in the apex\u001b\r\n\u001bof this peak and the one further east, you may be able to descend safely to the\u001b\r\n\u001bvalley below.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "west": {
          "door": false
        },
        "down": {
          "door": false
        }
      },
      "terrain": "mountains"
    },
    {
      "x": 70,
      "y": 37,
      "map": "goblins2oc",
      "name": "\u001bOn Top of a Mountain\u001b",
      "description": "\u001bStanding on the top of this mountain gives you a commanding view of the area\u001b\r\n\u001baround you. This line of peaks weaves sinuously down to the south east, In the\u001b\r\n\u001bnorth a path leads east-west, and further north you can see a dense forest.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains"
    },
    {
      "x": 71,
      "y": 37,
      "map": "goblins2oc",
      "name": "\u001bOn the Mountainside\u001b",
      "description": "\u001bStanding on the side of this mountain makes you feel slightly uneasy, since you\u001b\r\n\u001brealize that you could possibly lose your footing at any time and fall down the\u001b\r\n\u001bslope. To the west you can climb further up this magnificent mountain, or you\u001b\r\n\u001bcan traverse the mountainside to the south.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "pipeTo": {
            "x": 71,
            "y": 33
          },
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains"
    },
    {
      "x": 72,
      "y": 37,
      "map": "goblins2oc",
      "name": "\u001bTrail on a Mountainside\u001b",
      "description": "\u001bThe trail bends south and east here, dipping down to a valley bottom in the \u001b\r\n\u001bsouth, leading along the side of a mountain in the east. To the west the peaks\u001b\r\n\u001bof this mountain chain rise majestically to their full height, glistening in\u001b\r\n\u001bthe sun.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "pipeTo": {
            "x": 72,
            "y": 33
          },
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "hills"
    },
    {
      "x": 73,
      "y": 37,
      "name": "\u001bTrail on a Mountainside\u001b",
      "description": "\u001bThe trail bends north and west here, around a small outcrop of rock. To the \u001b\r\n\u001bwest, a mountain peak looms high above your head, but you can see no way of\u001b\r\n\u001bclimbing up. In the east the mountains dip down to a valley before rising again\u001b\r\n\u001bto form some sightly less formidable hills.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "hills",
      "map": "goblins2oc"
    },
    {
      "x": 74,
      "y": 37,
      "map": "goblins2oc",
      "name": "Rocky Path",
      "description": "The path leads north, south and east here. In the west the sheer size of the mountains rising above you makes you feel very inadequate, but the sight of the homely hut in the east cheers you up, with the small wisp of smoke curling from the chimney.",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "mountains"
    },
    {
      "x": 75,
      "y": 37,
      "map": "goblins2oc",
      "name": "Rocky Path",
      "description": "You can travel along this rocky path to the east and west, or you can rejoin the main road in the north. Underfoot you hear the crunch of fine rocks and gravel, and this path certainly looks like it has been built by someone or something. Further evidence of habitation is given by the smoke curling lazily from the chimney of the small hut to the south.",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains"
    },
    {
      "x": 76,
      "y": 37,
      "name": "Rocky Path",
      "description": "Rocks and stones crunch happily under your feet as you travel along this path. In the west the Misty Mountains reach up to touch the clouds, while closer you can see a small wooden hut situated just south of here. You can go east and west along this trail.",
      "exits": {
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 77,
      "y": 37,
      "name": "Rocky Path",
      "description": "The path ends in the south at the entrance to that neat little hut which you have been admiring for so long. You can also follow the path to the west, where it rejoins the main road. Looking west you again stare in wonder at the great magnificence of those towering mountains.",
      "exits": {
        "south": {
          "door": false,
          "pipeTo": {
            "x": 77,
            "y": 35
          }
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 78,
      "y": 37
    },
    {
      "x": 79,
      "y": 37
    },
    {
      "x": 80,
      "y": 37
    },
    {
      "x": 81,
      "y": 37
    },
    {
      "x": 82,
      "y": 37
    },
    {
      "x": 83,
      "y": 37,
      "name": "\u001bSmall Dell\u001b",
      "description": "\u001bBushes and low trees overhang the dell which lies just below the level of the\u001b\r\n\u001broad through the Lower Pass. Long bright-green grasses cover the ground like\u001b\r\n\u001ba thick carpet with a couple of thinner patches where the grass has been\u001b\r\n\u001bcropped by rabbits. Two big boulders rise at the northern end of the dell \u001b\r\n\u001bas if keeping watch, the ground around them decorated with yellow rockroses.\u001b\r\n\u001bTo the east you can see the source of the small stream, the waters of which\u001b\r\n\u001bfeed the grasses around you. A small path leaves the dell to the south.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "field",
      "map": "goblins2oc"
    },
    {
      "x": 84,
      "y": 37,
      "name": "\u001bSource of the Stream\u001b",
      "description": "\u001bA small stream bubbles out of a little hole in the rocks to the west. It \u001b\r\n\u001bruns along the road to the north. Green grass and occasional flowers\u001b\r\n\u001bsurround it, feeding on the nutritious fresh water.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "flags": "water",
      "terrain": "field",
      "map": "goblins2oc"
    },
    {
      "x": 85,
      "y": 37,
      "name": "\u001bCascades\u001b",
      "description": "\u001bThe stream plunges through a series of short waterfalls. The current is very\u001b\r\n\u001bswift, and the channel is full of sharp rocks. Northward there is a small\u001b\r\n\u001bcliff, above which runs a road. To the west, the land tilts sharply upwards,\u001b\r\n\u001brising to the first peaks of the Mountains. You can climb the rocky bank to\u001b\r\n\u001bthe south.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "rapids",
      "map": "goblins2oc"
    },
    {
      "x": 86,
      "y": 37,
      "name": "\u001bCascades\u001b",
      "description": "\u001bThis frigid mountain stream swifly plunges through a series of waterfalls\u001b\r\n\u001band rapids on its journey to the Anduin. There is a hairpin turn here, and\u001b\r\n\u001bthe banks are so steep that it is very hard to find purchase there. To the\u001b\r\n\u001bnorth the terrain becomes more gentle, but the current is still very fast.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "rapids",
      "map": "goblins2oc"
    },
    {
      "x": 87,
      "y": 37,
      "name": "\u001bSouth of a Ford\u001b",
      "description": "\u001bTo the north, the road winds its way into the lower branch of the High Pass, \u001b\r\n\u001band to the south, it can take you down to the great river Anduin. To the east\u001b\r\n\u001bis a dark, green forest.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "road",
      "map": "goblins2oc"
    },
    {
      "x": 88,
      "y": 37,
      "name": "\u001bDark Green Forest\u001b",
      "description": "\u001bThis small forest at the foot of the mountains is full of green trees and\u001b\r\n\u001bcolourful wild flowers. Listening to the singing birds and admiring the\u001b\r\n\u001bbeauty of the forest makes you want to sit down and take a break from the\u001b\r\n\u001bdangerous world around you.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "forest",
      "map": "goblins2oc"
    },
    {
      "x": 89,
      "y": 37,
      "name": "\u001bDeep inside the Forest\u001b",
      "description": "\u001bYou are now deep within the forest. Tall pines and sturdy oaks surround you, \u001b\r\n\u001band you have trouble moving around due to the thick undergrowth. Listening\u001b\r\n\u001bcarefully, you hear a grunting sound. Was it a boar, or something bigger \u001b\r\n\u001bsneaking around looking for fresh meat?\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false,
          "pipeTo": {
            "map": 'valewest',
            "x": 10,
            "y": 37,
          }

        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        },
        "up": {
          "door": false
        }
      },
      "terrain": "forest",
      "map": "goblins2oc"
    },
    {
      "x": 90,
      "y": 37,
    },
    {
      "x": 91,
      "y": 37,
    },
    {
      "x": 92,
      "y": 37,
    },
    {
      "x": 93,
      "y": 37,
    }
  ],
  [
    {
      "x": 44,
      "y": 36
    },
    {
      "x": 45,
      "y": 36
    },
    {
      "x": 46,
      "y": 36
    },
    {
      "x": 47,
      "y": 36
    },
    {
      "x": 48,
      "y": 36
    },
    {
      "x": 49,
      "y": 36
    },
    {
      "x": 50,
      "y": 36
    },
    {
      "x": 51,
      "y": 36
    },
    {
      "x": 52,
      "y": 36
    },
    {
      "x": 53,
      "y": 36
    },
    {
      "x": 54,
      "y": 36
    },
    {
      "x": 55,
      "y": 36
    },
    {
      "x": 56,
      "y": 36
    },
    {
      "x": 57,
      "y": 36
    },
    {
      "x": 58,
      "y": 36
    },
    {
      "x": 59,
      "y": 36
    },
    {
      "x": 60,
      "y": 36,
      "name": "\u001bMisty Mountains\u001b",
      "description": "\u001bContinuing up the slope, you find yourself near two mountain peaks rising\u001b\r\n\u001babove you. The southern one is inaccessible, but the one to the east is a\u001b\r\n\u001bfairly easy climb. Your mind ponders for a moment what might lie on the other\u001b\r\n\u001bside of that peak. Of course, there is only one way to find out. \u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "up": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 61,
      "y": 36,
      "name": "\u001bMountain Peak\u001b",
      "description": "\u001bHigh on the top of this mountain, the lack of air causes you to become dizzy.\u001b\r\n\u001bYou quickly lose your sense of direction for a brief moment, before becoming\u001b\r\n\u001baccustomed to the thinness of air. You shudder when you look south, as several\u001b\r\n\u001bof the peaks there are even higher. Perhaps you'll be able to avoid them in\u001b\r\n\u001byour journey...\u001b\r\n",
      "exits": {
        "south": {
          "door": false
        },
        "down": {
          "door": false,
          "pipeTo": {
            "x": 60,
            "y": 36
          }
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 62,
      "y": 36,
      "name": "\u001bMisty Mountains\u001b",
      "description": "\u001bYou wonder what force could have dug this alcove in the side of the mountains.\u001b\r\n\u001bThe only exit appears to be back to the north where you came from. However,\u001b\r\n\u001bthis alcove does seem to offer protection from the creatures of the mountains.\u001b\r\n\u001bPerhaps this would be a good place to rest for a while.  \u001b\r\n",
      "exits": {
        "north": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 63,
      "y": 36
    },
    {
      "x": 64,
      "y": 36
    },
    {
      "x": 65,
      "y": 36,
      "name": "\u001bMisty Mountains\u001b",
      "description": "\u001bWith the path sloping upwards, you are able to reach another peak of the\u001b\r\n\u001bmountains to the east. To the north, you see a strong trail leading through\u001b\r\n\u001bthe mountains. All other directions are blocked by high walls of stone which\u001b\r\n\u001bslant over you slightly, threatening to fall and crush you.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "up": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 66,
      "y": 36,
      "name": "\u001bMountain Peak\u001b",
      "description": "\u001bFrom your perch on the top of this mountain, you can see that all but one side\u001b\r\n\u001bis very steep. To the south, you can see that the same holds true for most of\u001b\r\n\u001bthe peaks in that direction. Far in the west, another row of mountain tops\u001b\r\n\u001brises in the air. As there is nothing of interest here, you decide that you\u001b\r\n\u001bshould go back the way you came, and continue your journey.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "down": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 67,
      "y": 36,
      "name": "\u001bMisty Mountains\u001b",
      "description": "\u001bAs you climb down, you lose your footing and fall. Although you land \u001b\r\n\u001bcomfortably, you find that you have no way of getting back up. Rising before\u001b\r\n\u001byou, a mountain peak stands high in the clouds. Climbing shouldn't be too\u001b\r\n\u001bdifficult, and you quickly decide that standing here might not be very safe.\u001b\r\n\u001bPerhaps you can find a way down the other side of the mountain top.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 68,
      "y": 36
    },
    {
      "x": 69,
      "y": 36
    },
    {
      "x": 70,
      "y": 36
    },
    {
      "x": 71,
      "y": 36
    },
    {
      "x": 72,
      "y": 36
    },
    {
      "x": 73,
      "y": 36
    },
    {
      "x": 74,
      "y": 36,
      "name": "Rocky Path",
      "description": "The path is rocky, but not so rocky as to make it hard to walk. In the south you can hear the gurgling of water from a spring, in the east you see a small wooden hut, and to the west you see the large range of Misty Mountains rising far above your head.",
      "exits": {
        "north": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 75,
      "y": 36,
      "name": "\u001bKitchen\u001b",
      "description": "\u001bA black metal stove is the centerpiece of this kitchen, with a chimney climbing\u001b\r\n\u001bthe wall to speed the smoke on its way. On the wall a few shelves hold a large\u001b\r\n\u001bvariety of spices and seasonings. A large cupboard adorns the eastern wall, and\u001b\r\n\u001bthrough the slightly open door you can see some choice cuts of meat hanging up\u001b\r\n\u001bto cure.\u001b\r\n",
      "exits": {
        "south": {
          "door": false
        }
      },
      "terrain": "cavern",
      "map": "goblins2oc"
    },
    {
      "x": 76,
      "y": 36
    },
    {
      "x": 77,
      "y": 36
    },
    {
      "x": 78,
      "y": 36
    },
    {
      "x": 79,
      "y": 36
    },
    {
      "x": 80,
      "y": 36,
      "name": "\u001bSharp Bend\u001b",
      "description": "\u001bWhat used to be a road leading up the steep slope is almost completely gone.\u001b\r\n\u001bThe remainders of it are sprinkled with fallen rocks, making movement very\u001b\r\n\u001bcumbersome.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "down": {
          "door": false,
          "pipeTo": {
            "x": 81,
            "y": 38
          }
        }
      },
      "terrain": "road",
      "map": "goblins2oc"
    },
    {
      "x": 81,
      "y": 36,
      "name": "\u001bSharp Bend\u001b",
      "description": "\u001bThe terrain to the east is far too rough to travel, and you can only proceed \u001b\r\n\u001bby going up the mountain side, or further into the mountains to the west.\u001b\r\n\u001bTo the north, a high mountain rises, and looking south you see another of the\u001b\r\n\u001bhigh peaks of the Hithaeglir.\u001b\r\n",
      "exits": {
        "west": {
          "door": false
        },
        "up": {
          "door": false,
          "pipeTo": {
            "x": 79,
            "y": 38
          }
        }
      },
      "terrain": "road",
      "map": "goblins2oc"
    },
    {
      "x": 82,
      "y": 36
    },
    {
      "x": 83,
      "y": 36,
      "name": "\u001bRough Path\u001b",
      "description": "\u001bThe path leaves the small dell to the north and winds its way up the green \u001b\r\n\u001bslope. Bushes and boulders seem to be all around you, both beside the path\u001b\r\n\u001band almost directly under your feet, as if trying to make you stumble. You\u001b\r\n\u001bnotice some plants and berries which look as if they might be edible.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        }
      },
      "terrain": "hills",
      "map": "goblins2oc"
    },
    {
      "x": 84,
      "y": 36,
      "name": "\u001bRough Path\u001b",
      "description": "\u001bThe path turns west-south here, rising up the rocky slope to the south.\u001b\r\n\u001bPebbles and lumps of rock litter the path, making it hard to move. Some\u001b\r\n\u001bboulders rise on the sides of the path, giving a shelter from the wind\u001b\r\n\u001band falling rocks to a few patches of grass and an occasional bright\u001b\r\n\u001bflower. To the north you can see a small stream.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "hills",
      "map": "goblins2oc"
    },
    {
      "x": 85,
      "y": 36,
      "name": "\u001bRocky Plains\u001b",
      "description": "\u001bRocks of all possible shapes and sizes are strewn around you. A couple\u001b\r\n\u001bof fir trees decorate the monotonous landscape. To the east, the ground\u001b\r\n\u001bslopes up a little. To the north there is a steep drop to a small stream.\u001b\r\n\u001bTo the south the slope rises all the way to the crest of the mountain\u001b\r\n\u001bridge.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        },
        "down": {
          "door": false,
          "pipeTo": {
            "x": 86,
            "y": 42
          }
        }
      },
      "terrain": "field",
      "map": "goblins2oc"
    },
    {
      "x": 86,
      "y": 36,
      "name": "\u001bSlope above a Stream\u001b",
      "description": "\u001bThe slope is covered with fir trees that provide some shelter from the wind.\u001b\r\n\u001bTo the north you can see the top of a cliff and a series of waterfalls below.\u001b\r\n\u001bTo the east, the trees give way to a road, and beyond the road there is a \u001b\r\n\u001bdense forest. To the west the ground becomes more rocky.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "field",
      "map": "goblins2oc"
    },
    {
      "x": 87,
      "y": 36,
      "name": "\u001bOld Dwarven Road\u001b",
      "description": "\u001bThis very old road was once built by dwarves to support trade between Arnor\u001b\r\n\u001band the Lonely Mountain. Now it is mainly used by foul creatures of the night.\u001b\r\n\u001bThe road leads north and south passing a slope you can see if you look west. On\u001b\r\n\u001bthe east side is a green but very dark forest. \u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "road",
      "map": "goblins2oc"
    },
    {
      "x": 88,
      "y": 36,
      "name": "\u001bDark Green Forest\u001b",
      "description": "\u001bTo the north and to the east you can see nothing but tall green trees.\u001b\r\n\u001bWest and south you can catch a glimpse of an overgrown road making its way\u001b\r\n\u001bthrough the forest.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "forest",
      "map": "goblins2oc"
    },
    {
      "x": 89,
      "y": 36,
      "name": "\u001bDeep inside the Forest\u001b",
      "description": "\u001bTrees surround you on all the sides, hiding the sky above. The forest is dark\u001b\r\n\u001band gloomy with thick undergrowth, making it very hard to move around. All\u001b\r\n\u001baround you, you hear the sounds of the forest, birds singing and boars\u001b\r\n\u001bgrunting.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false,
          "pipeTo": {
            "map": 'valewest',
            "x": 10,
            "y": 36,
          }

        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "forest",
      "map": "goblins2oc"
    },
    {
      "x": 90,
      "y": 36,
    },
    {
      "x": 91,
      "y": 36,
    },
    {
      "x": 92,
      "y": 36,
    },
    {
      "x": 93,
      "y": 36
    }
  ],
  [
    {
      "x": 44,
      "y": 35
    },
    {
      "x": 45,
      "y": 35
    },
    {
      "x": 46,
      "y": 35
    },
    {
      "x": 47,
      "y": 35
    },
    {
      "x": 48,
      "y": 35
    },
    {
      "x": 49,
      "y": 35
    },
    {
      "x": 50,
      "y": 35
    },
    {
      "x": 51,
      "y": 35
    },
    {
      "x": 52,
      "y": 35
    },
    {
      "x": 53,
      "y": 35
    },
    {
      "x": 54,
      "y": 35
    },
    {
      "x": 55,
      "y": 35
    },
    {
      "x": 56,
      "y": 35
    },
    {
      "x": 57,
      "y": 35
    },
    {
      "x": 58,
      "y": 35
    },
    {
      "x": 59,
      "y": 35
    },
    {
      "x": 60,
      "y": 35,
      "name": "\u001bMountain Peak\u001b",
      "description": "\u001bHigh on the top of this mountain, the lack of air causes you to become dizzy.\u001b\r\n\u001bYou quickly lose your sense of direction for a brief moment, before becoming\u001b\r\n\u001baccustomed to the thinness of air. You shudder when you look south, as several\u001b\r\n\u001bof the peaks there are even higher. Perhaps you'll be able to avoid them in\u001b\r\n\u001byour journey...\u001b\r\n",
      "exits": {
        "west": {
          "door": false
        },
        "down": {
          "door": false,
          "pipeTo": {
            "x": 61,
            "y": 35
          }
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 61,
      "y": 35,
      "name": "\u001bMisty Mountains\u001b",
      "description": "\u001bAs you look around at the vast reaches of grey mountains which surround you\u001b\r\n\u001bon all sides, and contemplate the sameness of it all, you can't help but\u001b\r\n\u001bwonder if you haven't been in this very spot already.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "up": {
          "door": false,
          "pipeTo": {
            "x": 60,
            "y": 35
          }
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 62,
      "y": 35
    },
    {
      "x": 63,
      "y": 35
    },
    {
      "x": 64,
      "y": 35
    },
    {
      "x": 65,
      "y": 35
    },
    {
      "x": 66,
      "y": 35
    },
    {
      "x": 67,
      "y": 35
    },
    {
      "x": 68,
      "y": 35
    },
    {
      "x": 69,
      "y": 35
    },
    {
      "x": 70,
      "y": 35
    },
    {
      "x": 71,
      "y": 35
    },
    {
      "x": 72,
      "y": 35
    },
    {
      "x": 73,
      "y": 35
    },
    {
      "x": 74,
      "y": 35,
      "name": "Bubbling Spring",
      "description": "The water bubbles merrily from a crack in the rocks and flows into a little pond. There must be an underground outlet for the water, because the pool never seems to get any fuller, yet the water doesn't leave here in any observable manner. This looks like a good place to rest your weary bones, or maybe the hut you can see in the east would be more comfortable?",
      "exits": {
        "north": {
          "door": false
        }
      },
      "flags": "water",
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 75,
      "y": 35,
      "name": "\u001bLiving Room\u001b",
      "description": "\u001bSome chairs sit here placed neatly around a table in the centre of the room. On\u001b\r\n\u001bthe walls you see an assortment of furs and animal heads, obviously trophies\u001b\r\n\u001bbelonging to the occupants of this hut. The animals represented are quite an\u001b\r\n\u001bimpressive collection, the prowess of this hunter must be almost legendary.\u001b\r\n\u001bYou can see a kitchen to the north, and south there is what looks like some\u001b\r\n\u001bsleeping quarters.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "indoors",
      "map": "goblins2oc"
    },
    {
      "x": 76,
      "y": 35,
      "name": "\u001bInside the Hut\u001b",
      "description": "\u001bThe walls are made of closely fitted split logs, the floor laid in wooden\u001b\r\n\u001bplanks. The workmanship is good, so whoever built this place obviously knew\u001b\r\n\u001bhow to work with wood. The hut extends to the west, where you can see a room\u001b\r\n\u001bwith a few chairs and a table, obviously where the occupants spend most of the\u001b\r\n\u001btime they are here.\u001b\r\n",
      "exits": {
        "east": {
          "door": {}
        },
        "west": {
          "door": false
        }
      },
      "terrain": "indoors",
      "map": "goblins2oc"
    },
    {
      "x": 77,
      "y": 35,
      "name": "End of the Path",
      "description": "The path ends here in a small, tidy garden, with most of the plants being in a rockery, surprisingly enough. The hut to the west looks solidly built from wood, which must have been hauled here from the forest you spotted way in the north, and smoke curls lazily away from the chimney on a light breeze.",
      "exits": {
        "north": {
          "door": false,
          "pipeTo": {
            "x": 77,
            "y": 37
          }
        },
        "south": {
          "door": {}
        },
        "west": {
          "door": {}
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 78,
      "y": 35
    },
    {
      "x": 79,
      "y": 35
    },
    {
      "x": 80,
      "y": 35
    },
    {
      "x": 81,
      "y": 35
    },
    {
      "x": 82,
      "y": 35
    },
    {
      "x": 83,
      "y": 35
    },
    {
      "x": 84,
      "y": 35,
      "name": "\u001bMountain Slope\u001b",
      "description": "\u001bYou are climbing up the steep rocky slope to the top of the mountain ridge.\u001b\r\n\u001bThe rough path, which has been leading up the slope, has disappeared. The \u001b\r\n\u001bbushes and the long grasses between the boulders, the patches of \u001b\r\n\u001brabbit-cropped turf and the bright flowers have all vanished. Pebbles, rubbish,\u001b\r\n\u001brocks and boulders cover the slope in uneven piles, as if waiting for something\u001b\r\n\u001bor someone to start an avalanche.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "up": {
          "door": false,
          "pipeTo": {
            "x": 84,
            "y": 34
          }
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 85,
      "y": 35,
      "name": "\u001bMountain Slope\u001b",
      "description": "\u001bYou are on the slope which leads to the top of a mountain ridge. A few \u001b\r\n\u001bbushes are the only larger growing things here. A small forest can be seen \u001b\r\n\u001bbelow the ridge to the east.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "west": {
          "door": false
        },
        "up": {
          "door": false,
          "pipeTo": {
            "x": 85,
            "y": 34
          }
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 86,
      "y": 35,
      "name": "\u001bSmall Forest\u001b",
      "description": "\u001bThis forest, on the eastern foothills of the Misty Mountains consists mainly\u001b\r\n\u001bof high, strong pines, mixed with an occasional birch and some oaks. Every\u001b\r\n\u001bstep you take brings you further up on the mountains, that rise to heights\u001b\r\n\u001bof over eighteen thousand feet above you.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        },
        "up": {
          "door": false
        }
      },
      "terrain": "forest",
      "map": "goblins2oc"
    },
    {
      "x": 87,
      "y": 35,
      "name": "\u001bBend in the Old Dwarven Road\u001b",
      "description": "\u001bThe old road turns north and east here. To the east of the road the forest is\u001b\r\n\u001bso thick that you can barely see two steps ahead. To the west the pine-forest\u001b\r\n\u001brises almost to the top of the mountain slope.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "road",
      "map": "goblins2oc"
    },
    {
      "x": 88,
      "y": 35,
      "name": "\u001bOld Dwarven Road\u001b",
      "description": "\u001bThis is yet another turn in the twisting road, which is getting more and more\u001b\r\n\u001bovergrown. Obviously, travellers are not common guests here. Maybe the\u001b\r\n\u001binhabitants of the surrounding forests do not need tracks or roads?\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "road",
      "map": "goblins2oc"
    },
    {
      "x": 89,
      "y": 35,
      "name": "\u001bThick Forest\u001b",
      "description": "\u001bThe thick undergrowth is making it hard to walk. To the north the forest is so\u001b\r\n\u001bdense that you can barely see two steps ahead. The trees grow less dense south\u001b\r\n\u001band west. \u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false,
          "pipeTo": {
            "map": 'valewest',
            "x": 10,
            "y": 35,
          }

        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "forest",
      "map": "goblins2oc"
    },
    {
      "x": 90,
      "y": 35,
    },
    {
      "x": 91,
      "y": 35,
    },
    {
      "x": 92,
      "y": 35,
    },
    {
      "x": 93,
      "y": 35,
    }
  ],
  [
    {
      "x": 44,
      "y": 34
    },
    {
      "x": 45,
      "y": 34
    },
    {
      "x": 46,
      "y": 34
    },
    {
      "x": 47,
      "y": 34
    },
    {
      "x": 48,
      "y": 34
    },
    {
      "x": 49,
      "y": 34
    },
    {
      "x": 50,
      "y": 34
    },
    {
      "x": 51,
      "y": 34
    },
    {
      "x": 52,
      "y": 34
    },
    {
      "x": 53,
      "y": 34
    },
    {
      "x": 54,
      "y": 34
    },
    {
      "x": 55,
      "y": 34
    },
    {
      "x": 56,
      "y": 34
    },
    {
      "x": 57,
      "y": 34
    },
    {
      "x": 58,
      "y": 34
    },
    {
      "x": 59,
      "y": 34
    },
    {
      "x": 60,
      "y": 34
    },
    {
      "x": 61,
      "y": 34
    },
    {
      "x": 62,
      "y": 34
    },
    {
      "x": 63,
      "y": 34
    },
    {
      "x": 64,
      "y": 34
    },
    {
      "x": 65,
      "y": 34
    },
    {
      "x": 66,
      "y": 34
    },
    {
      "x": 67,
      "y": 34
    },
    {
      "x": 68,
      "y": 34
    },
    {
      "x": 69,
      "y": 34
    },
    {
      "x": 70,
      "y": 34
    },
    {
      "x": 71,
      "y": 34
    },
    {
      "x": 72,
      "y": 34
    },
    {
      "x": 73,
      "y": 34
    },
    {
      "x": 74,
      "y": 34
    },
    {
      "x": 75,
      "y": 34,
      "name": "\u001bLiving Room\u001b",
      "description": "\u001bSome chairs sit here placed neatly around a table in the centre of the room. On\u001b\r\n\u001bthe walls you see an assortment of furs and animal heads, obviously trophies\u001b\r\n\u001bbelonging to the occupants of this hut. The animals represented are quite an\u001b\r\n\u001bimpressive collection, the prowess of this hunter must be almost legendary.\u001b\r\n\u001bYou can see a kitchen to the north, and south there is what looks like some\u001b\r\n\u001bsleeping quarters.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "indoors",
      "map": "goblins2oc"
    },
    {
      "x": 76,
      "y": 34
    },
    {
      "x": 77,
      "y": 34,
      "name": "\u001bAnimal Pen\u001b",
      "description": "\u001bThis small enclosed yard lies adjacent to a crude homestead, and obviously\u001b\r\n\u001bprovides shelter to a few of the animals kept by whomever dwells in the\u001b\r\n\u001bcabin. The only fencing is along the northern edge of the pen - in every\u001b\r\n\u001bother direction natural granite forms the sides.\u001b\r\n",
      "exits": {
        "north": {
          "door": {}
        }
      },
      "terrain": "indoors",
      "map": "goblins2oc"
    },
    {
      "x": 78,
      "y": 34
    },
    {
      "x": 79,
      "y": 34
    },
    {
      "x": 80,
      "y": 34
    },
    {
      "x": 81,
      "y": 34
    },
    {
      "x": 82,
      "y": 34
    },
    {
      "x": 83,
      "y": 34,
      "name": "\u001bCrest of the Ridge\u001b",
      "description": "\u001bYou have reached the crest of the ridge. The steep mountain slopes facing\u001b\r\n\u001bwest, south and north make this place look like a fortress. The air is thin\u001b\r\n\u001band clear, and even the smell of the pine trees does not reach so far up in\u001b\r\n\u001bthe mountains. During the short and intense spring, the mountain flowers\u001b\r\n\u001bcelebrate it with colours and smells.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 84,
      "y": 34,
      "name": "\u001bMountain Ridge\u001b",
      "description": "\u001bThe ridge turns west-south here. To the west you can see its crest and the \u001b\r\n\u001bsnow-capped peaks of the mountains beyond. To the east and the north you can \u001b\r\n\u001btry to make your way down the rocky slopes to the green valleys below.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        },
        "down": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 85,
      "y": 34,
      "name": "\u001bTop of the Mountain Slope\u001b",
      "description": "\u001bYou are on the top of a wide steep slope of fallen stones, the remains of\u001b\r\n\u001ba landslide. When you try to move down it, rubbish and small pebbles roll\u001b\r\n\u001baway from your feet. Soon larger bits of split stone go clattering down\u001b\r\n\u001band start other pieces below them slithering and rolling, and you are forced\u001b\r\n\u001bto walk very carefully to avoid setting the whole slope above and below you\u001b\r\n\u001bon the move.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        },
        "down": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 86,
      "y": 34,
      "name": "\u001bEdge of a Small Forest\u001b",
      "description": "\u001bYou are on the edge of a climbing wood of pines that stand here right up the\u001b\r\n\u001bmountain slope from the deeper darker forests of the valleys below. Looking\u001b\r\n\u001bwest, you can see the rocky slope continue till it reaches the top of the\u001b\r\n\u001bmountain ridge. A path goes down the slope to the south.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "forest",
      "map": "goblins2oc"
    },
    {
      "x": 87,
      "y": 34,
      "name": "\u001bPine Forest\u001b",
      "description": "\u001bYou are on the edge of a green pine forest at the eastern foothills of the\u001b\r\n\u001blargest mountain-range in all Arda - the Misty Mountains. Just east of here\u001b\r\n\u001byou can barely make out a small road. To the west the terrain starts to\u001b\r\n\u001bslope up. Some tracks lead south.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "forest",
      "map": "goblins2oc"
    },
    {
      "x": 88,
      "y": 34,
      "name": "\u001bOld Dwarven Road\u001b",
      "description": "\u001bThe road bends north and east here. Green hills rise to the west of the road,\u001b\r\n\u001btheir slopes covered by various trees and bushes. Some tall birches grow just\u001b\r\n\u001bto the south of the road. To the northeast you can see a dark forest.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "road",
      "map": "goblins2oc"
    },
    {
      "x": 89,
      "y": 34,
      "name": "\u001bOld Dwarven Road\u001b",
      "description": "\u001bZigzagging its way through a dark forest, this overgrown road can take you into\u001b\r\n\u001bthe Misty Mountains, or south towards the Anduin. On all sides, high trees \u001b\r\n\u001bsurrounds you.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false,
          "pipeTo": {
            "map": 'valewest',
            "x": 10,
            "y": 34,
          }

        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "road",
      "map": "goblins2oc"
    },
    {
      "x": 90,
      "y": 34,
    },
    {
      "x": 91,
      "y": 34,
    },
    {
      "x": 92,
      "y": 34,
    },
    {
      "x": 93,
      "y": 34,
    }
  ],
  [
    {
      "x": 44,
      "y": 33
    },
    {
      "x": 45,
      "y": 33
    },
    {
      "x": 46,
      "y": 33
    },
    {
      "x": 47,
      "y": 33
    },
    {
      "x": 48,
      "y": 33
    },
    {
      "x": 49,
      "y": 33
    },
    {
      "x": 50,
      "y": 33
    },
    {
      "x": 51,
      "y": 33
    },
    {
      "x": 52,
      "y": 33
    },
    {
      "x": 53,
      "y": 33,
      "name": "\u001bA Dangerous Road\u001b",
      "description": "\u001bThis trail cuts between two large peaks, leaving a river to the north and\u001b\r\n\u001bheading south deeper into the mountains. The air is permanently chilled here,\u001b\r\n\u001band the rarefied atmosphere makes breathing laborious. To the south the road\u001b\r\n\u001bseems to head down slightly, before twisting out of sight.\u001b\r\n",
      "exits": {
        "north": {
          "door": false,
          "pipeTo": {
            "x": 53,
            "y": 40
          }
        },
        "south": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 54,
      "y": 33
    },
    {
      "x": 55,
      "y": 33
    },
    {
      "x": 56,
      "y": 33
    },
    {
      "x": 57,
      "y": 33
    },
    {
      "x": 58,
      "y": 33
    },
    {
      "x": 59,
      "y": 33
    },
    {
      "x": 60,
      "y": 33
    },
    {
      "x": 61,
      "y": 33
    },
    {
      "x": 62,
      "y": 33
    },
    {
      "x": 63,
      "y": 33
    },
    {
      "x": 64,
      "y": 33
    },
    {
      "x": 65,
      "y": 33
    },
    {
      "x": 66,
      "y": 33
    },
    {
      "x": 67,
      "y": 33
    },
    {
      "x": 68,
      "y": 33
    },
    {
      "x": 69,
      "y": 33
    },
    {
      "x": 70,
      "y": 33
    },
    {
      "x": 71,
      "y": 33,
      "name": "\u001bOn the Mountainside\u001b",
      "description": "\u001bThe rocks underfoot are rather loose, but thankfully you managed to find a\u001b\r\n\u001brelatively compact patch on which to walk with safety. From here you can climb\u001b\r\n\u001bthe mountain to the west, or traverse north along the side of the mountain.\u001b\r\n",
      "exits": {
        "north": {
          "door": false,
          "pipeTo": {
            "x": 71,
            "y": 37
          }
        },
        "south": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 72,
      "y": 33,
      "name": "\u001bThe Trail\u001b",
      "description": "\u001bThe trail is almost at an end here, dropping down into a valley which leads\u001b\r\n\u001bsouth-east. You can see mountains in the east and west, although those in the\u001b\r\n\u001beast don't seem to be so formidable.\u001b\r\n",
      "exits": {
        "north": {
          "pipeTo": {
            "x": 72,
            "y": 37
          },
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "hills",
      "map": "goblins2oc"
    },
    {
      "x": 73,
      "y": 33
    },
    {
      "x": 74,
      "y": 33
    },
    {
      "x": 75,
      "y": 33
    },
    {
      "x": 76,
      "y": 33
    },
    {
      "x": 77,
      "y": 33
    },
    {
      "x": 78,
      "y": 33
    },
    {
      "x": 79,
      "y": 33
    },
    {
      "x": 80,
      "y": 33
    },
    {
      "x": 81,
      "y": 33
    },
    {
      "x": 82,
      "y": 33
    },
    {
      "x": 83,
      "y": 33
    },
    {
      "x": 84,
      "y": 33,
      "name": "\u001bMountain Ridge\u001b",
      "description": "\u001bWow! The view from here is breathtaking. You have reached one of the lower\u001b\r\n\u001beastern ridges of the Misty Mountains. Looking east, you see a green\u001b\r\n\u001bforest and beyond it the mighty Anduin with its surrounding lowlands. Even\u001b\r\n\u001bfurther away lies a dark-green sea, the Mirkwood. To the west you see a deep\u001b\r\n\u001bvalley far below.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "down": {
          "door": false,
          "pipeTo": {
            "x": 84,
            "y": 31
          }
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 85,
      "y": 33,
      "name": "\u001bMountain Ridge\u001b",
      "description": "\u001bYou have finally reached the eastern and lower side of the ridge. Looking \u001b\r\n\u001baround you, you see the Misty Mountains to the west, and a small forest below\u001b\r\n\u001byou to the northeast. A few dwarf birches are the only growing things up here \u001b\r\n\u001bexcept for the low grass that covers the ridge.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 86,
      "y": 33,
      "name": "\u001bRocky Path\u001b",
      "description": "\u001bYou have stumbled across a small path among the rocks. It leads east, from\u001b\r\n\u001bwhere you can hear thick voices speaking a foul orkish dialect.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 87,
      "y": 33,
      "name": "\u001bOrc Camp\u001b",
      "description": "\u001bUnder an overhanging cliff, there is a dirty, smelly, stinking orc camp among\u001b\r\n\u001bsharp rocks. Filthy piles of debris lies around it, and some unsanitary pots\u001b\r\n\u001bwith black, oily stews can be seen hanging over fires.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "forest",
      "map": "goblins2oc"
    },
    {
      "x": 88,
      "y": 33,
      "name": "\u001bBirches\u001b",
      "description": "\u001bSlender white birches rise all around you, a marvel in this dully coloured\u001b\r\n\u001blandscape. To the north and east you can see the old dwarven road running\u001b\r\n\u001balong the border of a dense forest. The ground is very wet. To the west there\u001b\r\n\u001bis an opening between the trees that does not look natural.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "west": {
          "door": false
        },
        "down": {
          "door": {}
        }
      },
      "terrain": "forest",
      "map": "goblins2oc"
    },
    {
      "x": 89,
      "y": 33,
      "name": "\u001bOld Dwarven Road\u001b",
      "description": "\u001bThe road makes its way through a very dark forest. It is possible to continue\u001b\r\n\u001bon the road north and east. A thick forest to the south does not let anyone\u001b\r\n\u001bpass. The trees to the west are less dense.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false,
          "pipeTo": {
            "map": 'valewest',
            "x": 10,
            "y": 33,
          }

        },
        "west": {
          "door": false
        },
        "down": {
          "door": false
        }
      },
      "terrain": "road",
      "map": "goblins2oc"
    },
    {
      "x": 90,
      "y": 33,
    },
    {
      "x": 91,
      "y": 33,
    },
    {
      "x": 92,
      "y": 33,
    },
    {
      "x": 93,
      "y": 33,
    }
  ],
  [
    {
      "x": 44,
      "y": 32
    },
    {
      "x": 45,
      "y": 32,
      "name": "\u001bUnder the Mountains\u001b",
      "description": "\u001bThis seems to be the deepest part of the tunnel. There is a puddle of water in\u001b\r\n\u001bthe middle of the room with a reddish tint. After examining the water it\u001b\r\n\u001bbecomes clear that elves have recently been stewed here. Clearly this is not a\u001b\r\n\u001bplace friendly to elves and their friends. No sound can be heard from the\u001b\r\n\u001btunnels, so maybe the attackers are hiding in the darkness.\u001b\r\n",
      "exits": {
        "south": {
          "door": false
        },
        "up": {
          "door": false
        }
      },
      "terrain": "tunnel",
      "map": "goblins2oc"
    },
    {
      "x": 46,
      "y": 32
    },
    {
      "x": 47,
      "y": 32
    },
    {
      "x": 48,
      "y": 32
    },
    {
      "x": 49,
      "y": 32
    },
    {
      "x": 50,
      "y": 32
    },
    {
      "x": 51,
      "y": 32
    },
    {
      "x": 52,
      "y": 32
    },
    {
      "x": 53,
      "y": 32,
      "name": "\u001bA Dangerous Road\u001b",
      "description": "\u001bThis trail takes advantage of a narrow flat area between two towering peaks to\u001b\r\n\u001bthe east and west. To the north, the trail rises higher while it leads to lower\u001b\r\n\u001belevations to the south. The hard and rocky soil bears a few foot- prints --\u001b\r\n\u001bsomeone has been here not long ago.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 54,
      "y": 32
    },
    {
      "x": 55,
      "y": 32
    },
    {
      "x": 56,
      "y": 32
    },
    {
      "x": 57,
      "y": 32
    },
    {
      "x": 58,
      "y": 32
    },
    {
      "x": 59,
      "y": 32
    },
    {
      "x": 60,
      "y": 32
    },
    {
      "x": 61,
      "y": 32
    },
    {
      "x": 62,
      "y": 32
    },
    {
      "x": 63,
      "y": 32
    },
    {
      "x": 64,
      "y": 32
    },
    {
      "x": 65,
      "y": 32
    },
    {
      "x": 66,
      "y": 32
    },
    {
      "x": 67,
      "y": 32
    },
    {
      "x": 68,
      "y": 32
    },
    {
      "x": 69,
      "y": 32
    },
    {
      "x": 70,
      "y": 32
    },
    {
      "x": 71,
      "y": 32,
      "name": "\u001bOn Top of a Mountain\u001b",
      "description": "\u001bSmall breaks in the snow here allow you to descend the side of the mountain if\u001b\r\n\u001byou wish, or you can continue along this ridge which leads to the south east.\u001b\r\n\u001bIt is difficult to see clearly, but you think there is a traversable valley in\u001b\r\n\u001bthe east.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 72,
      "y": 32,
      "name": "\u001bOn a Mountainside\u001b",
      "description": "\u001bThe rock underfoot is unstable, but you manage to keep your balance. The valley\u001b\r\n\u001bin the east seems to roughly follow the line of peaks to the south-east, or\u001b\r\n\u001byou can descend into the valley and then back up the side of another mountain.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 73,
      "y": 32,
      "name": "\u001bIn a Valley\u001b",
      "description": "\u001bThe valley bottom is covered in rocks and debris which have broken from the\u001b\r\n\u001bmountain and come tumbling down to end up here. The valley bends here to the\u001b\r\n\u001bnorth and east, or you can climb the mountain to the west.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 74,
      "y": 32,
      "name": "\u001bIn a Valley\u001b",
      "description": "\u001bRocks from above lie strewn all over the floor, some large rocks, some small.\u001b\r\n\u001bMost look like they have been lying here for many years, ravaged by wind and\u001b\r\n\u001brain. The valley bends west and south; to the east a smaller valley climbs\u001b\r\n\u001bup towards a narrow gap in the Hithaeglir.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 75,
      "y": 32,
      "name": "\u001bNarrow Gap\u001b",
      "description": "\u001bThe trail you are following leads east and west here. Westwards you can see\u001b\r\n\u001bmountains rising to high peaks, while to the east you can see the wide valley\u001b\r\n\u001bof the Anduin. In the far north you spot an area of darkness which may herald\u001b\r\n\u001ba forest of some sort, but you can't be sure from this distance. Still in the\u001b\r\n\u001bnorth but rather closer you see a path winding its way east to west.\u001b\r\n",
      "exits": {
        "east": {
          "door": false,
          "pipeTo": {
            "x": 78,
            "y": 32
          }
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 76,
      "y": 32
    },
    {
      "x": 77,
      "y": 32
    },
    {
      "x": 78,
      "y": 32,
      "name": "\u001bSteep Path\u001b",
      "description": "\u001bHere is the lowest part of a slippery and steep path through the mountain. It\u001b\r\n\u001bis only possible to travel north. Some lichens grows here but else this place\u001b\r\n\u001bis deserted of any form of permanent life.\u001b\r\n",
      "exits": {
        "north": {
          "door": false,
          "pipeTo": {
            "x": 78,
            "y": 38
          }
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 79,
      "y": 32
    },
    {
      "x": 80,
      "y": 32
    },
    {
      "x": 81,
      "y": 32,
      "name": "\u001bSteep Path\u001b",
      "description": "\u001bIt is an extremely steep and slippery path leading southwards. An other option\u001b\r\n\u001bis to go through the big crack to the west. There is no sign of any life except\u001b\r\n\u001bfor lichens on the dark rocks.\u001b\r\n",
      "exits": {
        "south": {
          "door": false,
          "pipeTo": {
            "x": 80,
            "y": 36
          }
        },
        "west": {
          "door": false,
          "pipeTo": {
            "x": 78,
            "y": 32
          }
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 82,
      "y": 32
    },
    {
      "x": 83,
      "y": 32
    },
    {
      "x": 84,
      "y": 32
    },
    {
      "x": 85,
      "y": 32
    },
    {
      "x": 86,
      "y": 32
    },
    {
      "x": 87,
      "y": 32
    },
    {
      "x": 88,
      "y": 32
    },
    {
      "x": 89,
      "y": 32
    },
    {
      "x": 90,
      "y": 32
    },
    {
      "x": 91,
      "y": 32
    },
    {
      "x": 92,
      "y": 32
    },
    {
      "x": 93,
      "y": 32
    }
  ],
  [
    {
      "x": 44,
      "y": 31
    },
    {
      "x": 45,
      "y": 31,
      "name": "\u001bUnder the Mountains\u001b",
      "description": "\u001bThe tunnels extend on and on. Though the wind can be felt, its origin can not\u001b\r\n\u001bbe discerned. A strange whistling sound can be heard coming from above. At\u001b\r\n\u001bfirst, this sound could be mistaken as artificial, but after careful\u001b\r\n\u001bexamination of the walls, it becomes obvious that the sound is merely the wind\u001b\r\n\u001bblowing through the little holes in the wall.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "tunnel",
      "map": "goblins2oc"
    },
    {
      "x": 46,
      "y": 31
    },
    {
      "x": 47,
      "y": 31
    },
    {
      "x": 48,
      "y": 31
    },
    {
      "x": 49,
      "y": 31
    },
    {
      "x": 50,
      "y": 31
    },
    {
      "x": 51,
      "y": 31
    },
    {
      "x": 52,
      "y": 31,
      "name": "\u001bA Dangerous Road\u001b",
      "description": "\u001bThe road passes through a narrow gully here, sharply turning from east to\u001b\r\n\u001bsouth. Piled in the middle of the trail are the bodies of two unfortunate\u001b\r\n\u001btravellers -- elven hunters perhaps -- who look to have succumbed to an ambush\u001b\r\n\u001bof some sort. The rocky sides of the gully could easily hide dozens of un- seen\u001b\r\n\u001benemies. Whoever these travellers were, they gave a good accounting of\u001b\r\n\u001bthemselves -- the presence of several goblin corpses attests to this.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 53,
      "y": 31,
      "name": "\u001bA Dangerous Road\u001b",
      "description": "\u001bThe road takes a turn here to the west. It leads deeper south and maybe into\u001b\r\n\u001bsome unknown regions of the mountains. When you look towards the top of the\u001b\r\n\u001bmountains, all you see is fog everywhere which hides most of the mountains.\u001b\r\n\u001bOccasionally you hear the sound of great big eagles high up in the sky. You\u001b\r\n\u001bwonder what kind of magic is hidden in the niches of these mountains.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 54,
      "y": 31
    },
    {
      "x": 55,
      "y": 31
    },
    {
      "x": 56,
      "y": 31
    },
    {
      "x": 57,
      "y": 31
    },
    {
      "x": 58,
      "y": 31,
      "name": "\u001bAbove a Mineshaft\u001b",
      "description": "\u001bA dwindling mineshaft descends deep into the mountain. Narrow, with rough walls\u001b\r\n\u001band ceiling, it twists its way downwards. The cavern itself is not very large,\u001b\r\n\u001band appears to have been dug out carelessly. Sharp edges protrude from the\u001b\r\n\u001bwalls; rocks and rubble lie scattered on the caverns floor, covered in a layer\u001b\r\n\u001bof dust. The air is damp and heavy. There is a myriad of footprints, leading to\u001b\r\n\u001band fro between the mineshaft and the eastern wall.\u001b\r\n",
      "exits": {
        "down": {
          "door": false,
          "pipeTo": {
            "x": 57,
            "y": 29
          }
        },
        "east": {
          "door": {}
        }
      },
      "terrain": "cavern",
      "map": "goblins2oc"
    },
    {
      "x": 59,
      "y": 31
    },
    {
      "x": 60,
      "y": 31
    },
    {
      "x": 61,
      "y": 31
    },
    {
      "x": 62,
      "y": 31
    },
    {
      "x": 63,
      "y": 31
    },
    {
      "x": 64,
      "y": 31
    },
    {
      "x": 65,
      "y": 31
    },
    {
      "x": 66,
      "y": 31
    },
    {
      "x": 67,
      "y": 31
    },
    {
      "x": 68,
      "y": 31
    },
    {
      "x": 69,
      "y": 31
    },
    {
      "x": 70,
      "y": 31,
      "name": "\u001bOn the Mountainside\u001b",
      "description": "\u001bAhh, at last, some shelter from the wind. Although the sides of this mountain\u001b\r\n\u001bare rather slippery, you don't have too much trouble keeping your footing. You\u001b\r\n\u001bcan travel further south, or climb back up to the mountain top.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 71,
      "y": 31,
      "name": "\u001bOn Top of a Mountain\u001b",
      "description": "\u001bMountains, mountains and more mountains. If they weren't so beautiful, I guess\u001b\r\n\u001byou would be getting sick of them by now. Standing on the peak of this one, you\u001b\r\n\u001bhave an excellent view all around. You can follow this trail along the peaks\u001b\r\n\u001bnorth or east. To the west you can descend a little to the side of the mountain\u001b\r\n\u001bto get some shelter from the harsh winds.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 72,
      "y": 31,
      "name": "\u001bOn Top of a Mountain\u001b",
      "description": "\u001bThe ridge of peaks along which you are walking extends further to the south\u001b\r\n\u001band west. You can descend the mountain to the east, but then you would not be\u001b\r\n\u001bable to see these mountains in their full magnificence. You spot a valley in\u001b\r\n\u001bthe east following roughly the same direction as the ridge you are on.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 73,
      "y": 31,
      "name": "\u001bOn a Mountainside\u001b",
      "description": "\u001bThe rocks on this side of the mountain are loose, but not loose enough to make\u001b\r\n\u001byou lose your balance and fall. You can descend further into the valley bottom,\u001b\r\n\u001btraverse the mountainside to the south, or climb back up to the ridge in the\u001b\r\n\u001bnorth or west.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false,
          "pipeTo": {
            "x": 74,
            "y": 29
          }
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 74,
      "y": 31,
      "name": "\u001bIn a Valley\u001b",
      "description": "\u001bThe valley continues north and south here. Picking your way carefully through\u001b\r\n\u001bshattered rocks, you wonder if you will ever find a way out of here.. when\u001b\r\n\u001bsuddenly you spot a trail leading up the side of the mountain to the west.\u001b\r\n\u001bAll around you can look up and see the magnificent snow-capped peaks towering\u001b\r\n\u001bhigh above you.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "south": {
          "door": false,
          "pipeTo": {
            "x": 75,
            "y": 29
          }
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 75,
      "y": 31
    },
    {
      "x": 76,
      "y": 31
    },
    {
      "x": 77,
      "y": 31
    },
    {
      "x": 78,
      "y": 31
    },
    {
      "x": 79,
      "y": 31
    },
    {
      "x": 80,
      "y": 31
    },
    {
      "x": 81,
      "y": 31
    },
    {
      "x": 82,
      "y": 31
    },
    {
      "x": 83,
      "y": 31,
      "name": "\u001bThe Mountainside\u001b",
      "description": "\u001bRocks again... what a surprise. The rough path you are following along the\u001b\r\n\u001bside  of this mountain makes a bend to the east and south here: to the east\u001b\r\n\u001bthe path descends to the bottom of a valley, while to the south it enters a\u001b\r\n\u001bnarrow canyon. Mountains rise up majestically in the north and west, but you\u001b\r\n\u001bcan spot no way of climbing them from here.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 84,
      "y": 31,
      "name": "\u001bVale in the Hithaeglir\u001b",
      "description": "\u001bYou have now reached the bottom of the valley. A few bushes and some grass\u001b\r\n\u001bcover the slopes, feeding the few animals that dwell here. The mountain \u001b\r\n\u001bslopes bear footmarks of humanoid beings.\u001b\r\n",
      "exits": {
        "west": {
          "door": false
        },
        "up": {
          "door": false,
          "pipeTo": {
            "x": 84,
            "y": 33
          }
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 85,
      "y": 31
    },
    {
      "x": 86,
      "y": 31
    },
    {
      "x": 87,
      "y": 31
    },
    {
      "x": 88,
      "y": 31
    },
    {
      "x": 89,
      "y": 31
    },
    {
      "x": 90,
      "y": 31
    },
    {
      "x": 91,
      "y": 31
    },
    {
      "x": 92,
      "y": 31
    },
    {
      "x": 93,
      "y": 31
    }
  ],
  [
    {
      "x": 44,
      "y": 30
    },
    {
      "x": 45,
      "y": 30,
      "name": "\u001bBackdoor\u001b",
      "description": "\u001bThe tunnel is very narrow. The sides are lined with jagged rocks that protrude\u001b\r\n\u001bfrom the side. Through a crack in the boulders, a valley can be seen to the\u001b\r\n\u001bsouth while the tunnel continues to the north. There is a powerful smell of\u001b\r\n\u001bgoblin to the north.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "south": {
          "door": {},
          "pipeTo": {
            "x": 49,
            "y": 28
          }
        }
      },
      "terrain": "tunnel",
      "map": "goblins2oc"
    },
    {
      "x": 46,
      "y": 30
    },
    {
      "x": 47,
      "y": 30
    },
    {
      "x": 48,
      "y": 30
    },
    {
      "x": 49,
      "y": 30
    },
    {
      "x": 50,
      "y": 30
    },
    {
      "x": 51,
      "y": 30
    },
    {
      "x": 52,
      "y": 30,
      "name": "\u001bA Dangerous Road\u001b",
      "description": "\u001bTo the south, the road slopes downward through a narrow gap between two large\u001b\r\n\u001bmountains. In that direction, the mountains look to give way eventually to\u001b\r\n\u001brocky hills. To the north, the road slants upward, again following the only\u001b\r\n\u001bpath between the two peaks, before it makes a sharp turn to the east and is\u001b\r\n\u001blost from view.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 53,
      "y": 30
    },
    {
      "x": 54,
      "y": 30
    },
    {
      "x": 55,
      "y": 30
    },
    {
      "x": 56,
      "y": 30
    },
    {
      "x": 57,
      "y": 30
    },
    {
      "x": 58,
      "y": 30
    },
    {
      "x": 59,
      "y": 30
    },
    {
      "x": 60,
      "y": 30
    },
    {
      "x": 61,
      "y": 30
    },
    {
      "x": 62,
      "y": 30
    },
    {
      "x": 63,
      "y": 30
    },
    {
      "x": 64,
      "y": 30
    },
    {
      "x": 65,
      "y": 30
    },
    {
      "x": 66,
      "y": 30
    },
    {
      "x": 67,
      "y": 30
    },
    {
      "x": 68,
      "y": 30
    },
    {
      "x": 69,
      "y": 30
    },
    {
      "x": 70,
      "y": 30,
      "name": "\u001bOn the Mountainside\u001b",
      "description": "\u001bYou can travel east or north, but you can see no way of climbing back up to the\u001b\r\n\u001btop of the mountain from here. Rising majestically above you to both the north\u001b\r\n\u001band south, the snow covered peaks make you feel very small and insignificant\u001b\r\n\u001bin comparison.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 71,
      "y": 30,
      "name": "\u001bOn the Mountainside\u001b",
      "description": "\u001bA small trail leads up the mountain to the east allowing to climb back up to \u001b\r\n\u001bthe ridge, or you can stay sheltered and travel west or south along the sides\u001b\r\n\u001bof this large mountain, being careful not to stand on any loose scree which may\u001b\r\n\u001bcause you to fall to your death hundreds of feet below.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "pipeTo": {
            "x": 70,
            "y": 28
          },
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 72,
      "y": 30,
      "name": "\u001bOn Top of a Mountain\u001b",
      "description": "\u001bThe mountains extend in a long sinuous chain to the north-west and south-east.\u001b\r\n\u001bFar to the north you spot a dark line which could only be a path leading east\u001b\r\n\u001band west. Closer, you can see a valley in the east following this ridge of\u001b\r\n\u001bpeaks which you are travelling on.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 73,
      "y": 30
    },
    {
      "x": 74,
      "y": 30
    },
    {
      "x": 75,
      "y": 30
    },
    {
      "x": 76,
      "y": 30
    },
    {
      "x": 77,
      "y": 30
    },
    {
      "x": 78,
      "y": 30
    },
    {
      "x": 79,
      "y": 30
    },
    {
      "x": 80,
      "y": 30
    },
    {
      "x": 81,
      "y": 30
    },
    {
      "x": 82,
      "y": 30
    },
    {
      "x": 83,
      "y": 30,
      "name": "\u001bNarrow Canyon\u001b",
      "description": "\u001bYou are on the bottom of a rocky canyon, about two meters wide. High, smooth\u001b\r\n\u001bcliffs, with no handholds in sight, flank you on both sides, leaving almost no\u001b\r\n\u001bchoice on where to proceed: you can either follow the canyon to the north, or\u001b\r\n\u001bsqueeze through a crevice westwards.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "west": {
          "door": {},
          "pipeTo": {
            "x": 75,
            "y": 28
          }
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 84,
      "y": 30
    },
    {
      "x": 85,
      "y": 30
    },
    {
      "x": 86,
      "y": 30
    },
    {
      "x": 87,
      "y": 30
    },
    {
      "x": 88,
      "y": 30
    },
    {
      "x": 89,
      "y": 30
    },
    {
      "x": 90,
      "y": 30
    },
    {
      "x": 91,
      "y": 30
    },
    {
      "x": 92,
      "y": 30
    },
    {
      "x": 93,
      "y": 30
    }
  ],
  [
    {
      "x": 44,
      "y": 29
    },
    {
      "x": 45,
      "y": 29
    },
    {
      "x": 46,
      "y": 29
    },
    {
      "x": 47,
      "y": 29
    },
    {
      "x": 48,
      "y": 29
    },
    {
      "x": 49,
      "y": 29
    },
    {
      "x": 50,
      "y": 29
    },
    {
      "x": 51,
      "y": 29
    },
    {
      "x": 52,
      "y": 29,
      "name": "\u001bA Dangerous Road\u001b",
      "description": "\u001bThe road continues south from here. To your west you see a small cutout in the\u001b\r\n\u001bmountain side which allows you to step out of the road into comparative secrecy\u001b\r\n\u001baway from the eyes of other curious travellers. The cold is beginning to get to\u001b\r\n\u001byou and you wish you had more clothes and some ale to warm yourself with.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 53,
      "y": 29
    },
    {
      "x": 54,
      "y": 29
    },
    {
      "x": 55,
      "y": 29
    },
    {
      "x": 56,
      "y": 29,
      "name": "\u001bStorage Room\u001b",
      "description": "\u001bCrates, chests and barrels crowd this small room. A table stands along the\u001b\r\n\u001bsouthern wall with shards of clay and broken bottles scattered on it. An iron\u001b\r\n\u001bshackle, nailed to the ceiling, holds a skeleton covered in ragged clothing.\u001b\r\n\u001bThe floor, walls and ceiling are smooth and heavy beams supports the ceiling,\u001b\r\n\u001btightly bound together by thick, black rope, creating a supporting arch. The\u001b\r\n\u001broom exits through a door-opening in the east.\u001b\r\n",
      "exits": {
        "east": {
          "door": {}
        }
      },
      "terrain": "cavern",
      "map": "goblins2oc"
    },
    {
      "x": 57,
      "y": 29,
      "name": "\u001bNexus\u001b",
      "description": "\u001bThe mines twist back and forth, with tunnels leading off in four directions.\u001b\r\n\u001bDamp and dark, the excavation expands further to the east, south and west. A\u001b\r\n\u001bhole in the northern wall leads into a steep, inclining tunnel. Cracked timber\u001b\r\n\u001bshores the ceiling, keeping the tunnels from collapsing. Broken tools lie\u001b\r\n\u001babandoned in deep layers of dust along the tunnel's walls. A lantern hanging\u001b\r\n\u001bfrom one of the beams in the ceiling emits a faint light.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": {}
        },
        "up": {
          "door": false,
          "pipeTo": {
            "x": 58,
            "y": 31
          }
        }
      },
      "terrain": "tunnel",
      "map": "goblins2oc"
    },
    {
      "x": 58,
      "y": 29,
      "name": "\u001bExcavation Site\u001b",
      "description": "\u001bA wooden trough stands centrally placed in the damp cavern. An abundance of\u001b\r\n\u001bpickaxes and heavy hammers are lined up in a rack on the northern wall. Wooden\u001b\r\n\u001bbeams steady the arched ceiling, keeping it from collapsing. Shackles connected\u001b\r\n\u001bto heavy balls and chains lie grouped in different piles, just next to the\u001b\r\n\u001bmining areas. The mine opens up to the west while a narrowing tunnel leads\u001b\r\n\u001bdeeper into the mountain to the south.\u001b\r\n",
      "exits": {
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "flags": "water",
      "terrain": "tunnel",
      "map": "goblins2oc"
    },
    {
      "x": 59,
      "y": 29
    },
    {
      "x": 60,
      "y": 29
    },
    {
      "x": 61,
      "y": 29
    },
    {
      "x": 62,
      "y": 29
    },
    {
      "x": 63,
      "y": 29
    },
    {
      "x": 64,
      "y": 29
    },
    {
      "x": 65,
      "y": 29
    },
    {
      "x": 66,
      "y": 29
    },
    {
      "x": 67,
      "y": 29
    },
    {
      "x": 68,
      "y": 29
    },
    {
      "x": 69,
      "y": 29
    },
    {
      "x": 70,
      "y": 29
    },
    {
      "x": 71,
      "y": 29
    },
    {
      "x": 72,
      "y": 29,
      "name": "\u001bOn Top of a Mountain\u001b",
      "description": "\u001bThe mountain drops down to a valley in the east, or you can travel along this\u001b\r\n\u001bridge to the south and west. On one of the highest peaks in this range, you\u001b\r\n\u001bcan see for miles around, even spotting a dark forest many miles to the north.\u001b\r\n",
      "exits": {
        "east": {
          "door": false,
          "pipeTo": {
            "x": 72,
            "y": 29
          }
        },
        "south": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 73,
      "y": 29
    },
    {
      "x": 74,
      "y": 29,
      "name": "\u001bOn a Mountainside\u001b",
      "description": "\u001bStanding on the loose rocks you wonder if this was a wise move, but the ground\u001b\r\n\u001bunderfoot seems to be solid enough for the moment. You can traverse the side of\u001b\r\n\u001bthe mountain to the north or south, descend to a valley in the east, or climb\u001b\r\n\u001bup to a ridge on the top of the mountain in the west.\u001b\r\n",
      "exits": {
        "north": {
          "door": false,
          "pipeTo": {
            "x": 73,
            "y": 31
          }
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false,
          "pipeTo": {
            "x": 72,
            "y": 29
          }
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 75,
      "y": 29,
      "name": "\u001bIn a Valley\u001b",
      "description": "\u001bA rock wall towers up on the east, casting its long shadow over the valley \u001b\r\n\u001bfloor. In the west you can start to climb up the side of one of the tallest \u001b\r\n\u001bpeaks in this range, or you can follow this barren valley either north or\u001b\r\n\u001bsouth.\u001b\r\n",
      "exits": {
        "north": {
          "door": false,
          "pipeTo": {
            "x": 74,
            "y": 31
          }
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 76,
      "y": 29
    },
    {
      "x": 77,
      "y": 29
    },
    {
      "x": 78,
      "y": 29
    },
    {
      "x": 79,
      "y": 29
    },
    {
      "x": 80,
      "y": 29
    },
    {
      "x": 81,
      "y": 29
    },
    {
      "x": 82,
      "y": 29
    },
    {
      "x": 83,
      "y": 29
    },
    {
      "x": 84,
      "y": 29
    },
    {
      "x": 85,
      "y": 29
    },
    {
      "x": 86,
      "y": 29
    },
    {
      "x": 87,
      "y": 29
    },
    {
      "x": 88,
      "y": 29
    },
    {
      "x": 89,
      "y": 29
    },
    {
      "x": 90,
      "y": 29
    },
    {
      "x": 91,
      "y": 29
    },
    {
      "x": 92,
      "y": 29
    },
    {
      "x": 93,
      "y": 29
    }
  ],
  [
    {
      "x": 44,
      "y": 28
    },
    {
      "x": 45,
      "y": 28
    },
    {
      "x": 46,
      "y": 28
    },
    {
      "x": 47,
      "y": 28
    },
    {
      "x": 48,
      "y": 28,
      "name": "\u001bDeep Valley\u001b",
      "description": "\u001bThe deep valley comes to an end here -- it is impossible to head farther west\u001b\r\n\u001bor north. To the south, more of the forest stretches, but in that direction as\u001b\r\n\u001bwell a cliff is visible. The forest is dark -- very little sunlight can filter\u001b\r\n\u001bdown through the thick canopy of pine needles -- and the howling of wolves\u001b\r\n\u001bindicates that this area may not be a good place to tarry.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "forest",
      "map": "goblins2oc"
    },
    {
      "x": 49,
      "y": 28,
      "name": "\u001bDeep Valley\u001b",
      "description": "\u001bThere is a large notch in the cliff directly to the north, and many boulders\u001b\r\n\u001bhave been piled there to obscure whatever lies within it. The ancient pines and\u001b\r\n\u001bfirs are dense here, their black trunks giving this area a gloomy and\u001b\r\n\u001bforeboding atmosphere.\u001b\r\n",
      "exits": {
        "north": {
          "door": {},
          "pipeTo": {
            "x": 45,
            "y": 30
          }
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "forest",
      "map": "goblins2oc"
    },
    {
      "x": 50,
      "y": 28,
      "name": "\u001bDeep Valley\u001b",
      "description": "\u001bA rock cliff looms to the north, and the multitude of large boulders scattered\u001b\r\n\u001bamong the ancient trees hints at frequent rockslides. The ground here is\u001b\r\n\u001bcovered with old pine-needles and the tracks of many wolves. Howling and\u001b\r\n\u001boccasional sounds of padding feet just out of sight heightens the sense of\u001b\r\n\u001bdanger here.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "forest",
      "map": "goblins2oc"
    },
    {
      "x": 51,
      "y": 28,
      "name": "\u001bDeep Valley\u001b",
      "description": "\u001bThe valley slopes steeply upward to the east, eventually meeting a trail that\u001b\r\n\u001bruns along the top of a ridge. The trees, mainly pine and fir, are old, perhaps\u001b\r\n\u001bold enough to remember the icy winters of the Witch-king's rule over Angmar to\u001b\r\n\u001bthe north. The valley floor lies to the west, enclosed on three sides by steep\u001b\r\n\u001bmountain slopes.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "forest",
      "map": "goblins2oc"
    },
    {
      "x": 52,
      "y": 28,
      "name": "\u001bA Dangerous Road\u001b",
      "description": "\u001bThe narrow road seemingly ends here, as its route south quickly becomes lost\u001b\r\n\u001bamong some low hills. To the west is a deep valley filled with a dark pine\u001b\r\n\u001bforest. Occasional lonely howls echo up from the ominous forest, but the trees\u001b\r\n\u001bare too dense to locate their source.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 53,
      "y": 28
    },
    {
      "x": 54,
      "y": 28
    },
    {
      "x": 55,
      "y": 28
    },
    {
      "x": 56,
      "y": 28,
      "name": "\u001bSloping Tunnel\u001b",
      "description": "\u001bA long tunnel, more or less rounded, has been dug through the rock here. Sharp\u001b\r\n\u001bedges of the cold, grey stone interrupt what could be a proper passageway.\u001b\r\n\u001bAlong the walls lie cracked pickaxes and abandoned broken ore carts. A thick\u001b\r\n\u001blayer of dust covers the ground. Beams of dried, cracking wood support the\u001b\r\n\u001btunnel, keeping it from collapsing. A faint flickering light can be discerned\u001b\r\n\u001bat the end of a passage that leads deeper down under the mountain.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "down": {
          "door": false,
          "pipeTo": {
            "x": 55,
            "y": 27
          }
        }
      },
      "terrain": "tunnel",
      "map": "goblins2oc"
    },
    {
      "x": 57,
      "y": 28,
      "name": "\u001bTwisting and Turning Tunnel\u001b",
      "description": "\u001bThe ground is rough and uneven. Footprints of various sizes and other\u001b\r\n\u001bunidentified tracks run deep in the heavy layer of dust which covers the area.\u001b\r\n\u001bRocks lie heaped in big piles along the rounded tunnels. The ceiling and walls\u001b\r\n\u001bare supported by thick, dark logs. A pile of boulders and rubble blocks the\u001b\r\n\u001bpassage to the east, while the darkness to the north hints at another cave or\u001b\r\n\u001btunnel in that direction. A tunnel slopes slightly downwards to the west.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": {}
        },
        "west": {
          "door": false
        }
      },
      "terrain": "tunnel",
      "map": "goblins2oc"
    },
    {
      "x": 58,
      "y": 28,
      "name": "\u001bCollapsed Mine\u001b",
      "description": "\u001bAn old mining tunnel comes to an end here, partially collapsed and completely\u001b\r\n\u001bin ruins. From pebbles to boulders, broken planks to cracked wooden beams,\u001b\r\n\u001beverything lie scattered about on the floor of the mine. The ceiling appears to\u001b\r\n\u001bhave caved in completely to the west. Humanoid bones poke out from the rubble,\u001b\r\n\u001bcrushed from the weight of the collapsed mine. The air is damp, and dust lies\u001b\r\n\u001bheavily on the ground. A small passageway to north seems to be the only route\u001b\r\n\u001bfrom this tunnel.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "west": {
          "door": {}
        },
        "down": {
          "door": {},
          "pipeTo": {
            "x": 58,
            "y": 26
          }
        }
      },
      "terrain": "tunnel",
      "map": "goblins2oc"
    },
    {
      "x": 59,
      "y": 28
    },
    {
      "x": 60,
      "y": 28
    },
    {
      "x": 61,
      "y": 28
    },
    {
      "x": 62,
      "y": 28
    },
    {
      "x": 63,
      "y": 28
    },
    {
      "x": 64,
      "y": 28
    },
    {
      "x": 65,
      "y": 28
    },
    {
      "x": 66,
      "y": 28
    },
    {
      "x": 67,
      "y": 28
    },
    {
      "x": 68,
      "y": 28
    },
    {
      "x": 69,
      "y": 28
    },
    {
      "x": 70,
      "y": 28,
      "name": "\u001bOn a Mountainside\u001b",
      "description": "\u001bYou can travel east or north, but you can see no way of climbing back up to the\u001b\r\n\u001btop of the mountain from here. Rising majestically above you to both the north\u001b\r\n\u001band south, the snow covered peaks make you feel very small and insignificant\u001b\r\n\u001bin comparison.\u001b\r\n",
      "exits": {
        "north": {
          "door": false,
          "pipeTo": {
            "x": 71,
            "y": 30
          }
        },
        "east": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 71,
      "y": 28,
      "name": "\u001bOn the Mountainside\u001b",
      "description": "\u001bYou can either climb back up the mountain to the east, or travel west along\u001b\r\n\u001bthe mountainside. A large rock wall looms in the south, with a dark hole in\u001b\r\n\u001bthe middle of it probably indicating the entrance to a cave.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 72,
      "y": 28,
      "name": "\u001bOn Top of a Mountain\u001b",
      "description": "\u001bThe wind here chills you to the bone, and the rocks move beneath your feet.\u001b\r\n\u001bBalancing precariously, you see that you can follow this ridge to the north\u001b\r\n\u001band east, or you can go into the shelter of the mountainside to the west.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 73,
      "y": 28,
      "name": "\u001bOn Top of a Mountain\u001b",
      "description": "\u001bStanding here you are dwarfed by the size of the peak which rises in the south.\u001b\r\n\u001bIt looks impossibly steep for climbing, with many sharp rocks which could cut\u001b\r\n\u001byour hands to shreds. You can travel west and follow the ridge of smaller peaks\u001b\r\n\u001bwhich snakes out to the north-west, or you can descend the mountain to a valley\u001b\r\n\u001bin the east.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 74,
      "y": 28,
      "name": "\u001bOn a Mountainside\u001b",
      "description": "\u001bTravel further south is curtailed by a large outcrop of rocks, which is almost\u001b\r\n\u001bimpossible to pass. You can however descend into a valley to the east, or climb\u001b\r\n\u001bback up the mountain in the west.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 75,
      "y": 28,
      "name": "\u001bIn a Valley\u001b",
      "description": "\u001bThe valley ends here, dropping so sharply to the south that it would be folly\u001b\r\n\u001bto even try going down it. You can climb the mountains to the west, where you\u001b\r\n\u001bsee the highest peak you have ever beheld rising far above your head, or you\u001b\r\n\u001bcan follow the valley northwards. To the east a tall cliff blocks your way.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": true,
          "pipeTo": {
            "x": 83,
            "y": 30
          }
        },
        "west": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 76,
      "y": 28
    },
    {
      "x": 77,
      "y": 28
    },
    {
      "x": 78,
      "y": 28
    },
    {
      "x": 79,
      "y": 28
    },
    {
      "x": 80,
      "y": 28
    },
    {
      "x": 81,
      "y": 28
    },
    {
      "x": 82,
      "y": 28
    },
    {
      "x": 83,
      "y": 28
    },
    {
      "x": 84,
      "y": 28
    },
    {
      "x": 85,
      "y": 28
    },
    {
      "x": 86,
      "y": 28
    },
    {
      "x": 87,
      "y": 28
    },
    {
      "x": 88,
      "y": 28
    },
    {
      "x": 89,
      "y": 28
    },
    {
      "x": 90,
      "y": 28
    },
    {
      "x": 91,
      "y": 28
    },
    {
      "x": 92,
      "y": 28
    },
    {
      "x": 93,
      "y": 28
    }
  ],
  [
    {
      "x": 44,
      "y": 27
    },
    {
      "x": 45,
      "y": 27
    },
    {
      "x": 46,
      "y": 27
    },
    {
      "x": 47,
      "y": 27
    },
    {
      "x": 48,
      "y": 27
    },
    {
      "x": 49,
      "y": 27
    },
    {
      "x": 50,
      "y": 27
    },
    {
      "x": 51,
      "y": 27
    },
    {
      "x": 52,
      "y": 27,
      "name": "\u001bRocky Hills\u001b",
      "description": "\u001bThe hills are little more than jagged outcroppings of rock thrusting upward\u001b\r\n\u001bthrough the earth. On all sides, the high peaks of the Misty Mountains rise,\u001b\r\n\u001benclosing this oasis of rocky hills. There is a road to the north that once led\u001b\r\n\u001bthrough this area, but its path has become lost so there is no indication of a\u001b\r\n\u001bcorrect direction to proceed.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "hills",
      "map": "goblins2oc"
    },
    {
      "x": 53,
      "y": 27
    },
    {
      "x": 54,
      "y": 27,
      "name": "\u001bMine Entrance\u001b",
      "description": "\u001bThe entrance is a small space and littered with rubble. Along the grey rock\u001b\r\n\u001bwalls stand benches and a few tables. The roof is jagged, with sharp edges\u001b\r\n\u001bprotruding in different directions. The cave is damp, and moss has spread\u001b\r\n\u001bacross the ceiling. Brown bottles and clay jars lie cracked open on the ground.\u001b\r\n\u001bA small desk with a stool stands next to the entrance. A narrow tunnel burrows\u001b\r\n\u001bdeeper beneath the Misty Mountains to the east.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "west": {
          "door": {}
        }
      },
      "terrain": "tunnel",
      "map": "goblins2oc"
    },
    {
      "x": 55,
      "y": 27,
      "name": "\u001bHollow in the Mountain\u001b",
      "description": "\u001bA vast cave opens up inside the mountain, rising more than thirty feet tall.\u001b\r\n\u001bWater trickles down the eastern wall into a small pond located centrally in the\u001b\r\n\u001bcavern. Stalagmites and stalactites cover much of the cavern floor and ceiling.\u001b\r\n\u001bScattered about the cave lie crumbling bones and rotting pelts. A stone stair\u001b\r\n\u001bhas been carved out from the mountain, with a steep incline from the tunnel in\u001b\r\n\u001bthe west, to a tunnel entrance in the north. Small torches sit lit in iron\u001b\r\n\u001bclaws attached to the wall along the staircase.\u001b\r\n",
      "exits": {
        "west": {
          "door": false
        },
        "up": {
          "door": false,
          "pipeTo": {
            "x": 56,
            "y": 28
          }
        }
      },
      "flags": "water",
      "terrain": "tunnel",
      "map": "goblins2oc"
    },
    {
      "x": 56,
      "y": 27
    },
    {
      "x": 57,
      "y": 27
    },
    {
      "x": 58,
      "y": 27
    },
    {
      "x": 59,
      "y": 27
    },
    {
      "x": 60,
      "y": 27
    },
    {
      "x": 61,
      "y": 27
    },
    {
      "x": 62,
      "y": 27
    },
    {
      "x": 63,
      "y": 27
    },
    {
      "x": 64,
      "y": 27
    },
    {
      "x": 65,
      "y": 27
    },
    {
      "x": 66,
      "y": 27
    },
    {
      "x": 67,
      "y": 27
    },
    {
      "x": 68,
      "y": 27
    },
    {
      "x": 69,
      "y": 27,
      "name": "\u001bSmelly Corner\u001b",
      "description": "\u001bThe light in here is eerie, and you see it comes from the luminous fungi\u001b\r\n\u001bgrowing all over the walls and roof, in many shades of green, orange, red and\u001b\r\n\u001byellow. Obviously it is the decay of dead fungi that causes the awful stench in\u001b\r\n\u001bhere. Or is it... unidentifiable remains lie scattered about the floor.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "tunnel",
      "map": "goblins2oc"
    },
    {
      "x": 70,
      "y": 27,
      "name": "\u001bDark Cave\u001b",
      "description": "\u001bThe constant drip..drip..drip of water puts your nerves on edge. The darkness\u001b\r\n\u001ball around does nothing to allay any fears you may have about the inhabitants\u001b\r\n\u001bof this cave complex, and neither does the powdered bones on the floor.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "cavern",
      "map": "goblins2oc"
    },
    {
      "x": 71,
      "y": 27,
      "name": "\u001bCave Entrance\u001b",
      "description": "\u001bThis cave has been worn from the rock by the passage of many millions of quarts\u001b\r\n\u001bof water. This room is lit by the light streaming in from outside, but you can\u001b\r\n\u001bsee that the cave extending further back into the mountain is shrouded in\u001b\r\n\u001bdarkness. A foul odor avails your senses as you sniff around looking to see\u001b\r\n\u001bwhat might be living here.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "tunnel",
      "map": "goblins2oc"
    },
    {
      "x": 72,
      "y": 27
    },
    {
      "x": 73,
      "y": 27,
      "name": "\u001bA Very High Peak\u001b",
      "description": "\u001bThis is one of the highest peaks in the area, and from here you can look\u001b\r\n\u001ball around and see the Misty Mountains in all their glorious splendour.\u001b\r\n\u001bTo the north, the mountains slowly descend into a shallow valley, cut by\u001b\r\n\u001ba brown ribbon which must be a road, running east-west. To the south the\u001b\r\n\u001bmountains close ranks, forming an impassable barrier of rock, while to\u001b\r\n\u001bthe east and west the mountains continue on for miles in their snow-capped,\u001b\r\n\u001bmist-wreathed splendour.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        }
      },
      "terrain": "mountains",
      "map": "goblins2oc"
    },
    {
      "x": 74,
      "y": 27
    },
    {
      "x": 75,
      "y": 27
    },
    {
      "x": 76,
      "y": 27
    },
    {
      "x": 77,
      "y": 27
    },
    {
      "x": 78,
      "y": 27
    },
    {
      "x": 79,
      "y": 27
    },
    {
      "x": 80,
      "y": 27
    },
    {
      "x": 81,
      "y": 27
    },
    {
      "x": 82,
      "y": 27
    },
    {
      "x": 83,
      "y": 27
    },
    {
      "x": 84,
      "y": 27
    },
    {
      "x": 85,
      "y": 27
    },
    {
      "x": 86,
      "y": 27
    },
    {
      "x": 87,
      "y": 27
    },
    {
      "x": 88,
      "y": 27
    },
    {
      "x": 89,
      "y": 27
    },
    {
      "x": 90,
      "y": 27
    },
    {
      "x": 91,
      "y": 27
    },
    {
      "x": 92,
      "y": 27
    },
    {
      "x": 93,
      "y": 27
    }
  ],
  [
    {
      "x": 44,
      "y": 26
    },
    {
      "x": 45,
      "y": 26
    },
    {
      "x": 46,
      "y": 26
    },
    {
      "x": 47,
      "y": 26
    },
    {
      "x": 48,
      "y": 26
    },
    {
      "x": 49,
      "y": 26
    },
    {
      "x": 50,
      "y": 26
    },
    {
      "x": 51,
      "y": 26
    },
    {
      "x": 52,
      "y": 26,
      "name": "\u001bRocky Hills\u001b",
      "description": "\u001bThe rocky hills continue in all directions, until they eventually give way to\u001b\r\n\u001bmountains once more. These hills are like slanted rock tables, all tilted up\u001b\r\n\u001bfrom west to east at steep angles. The sharp rocks and rocky soil make walk-\u001b\r\n\u001bing difficult, threatening to shred even the toughest of boots. Some distance\u001b\r\n\u001bto the east, there is a hill noticeably larger and of different appearance than\u001b\r\n\u001bthe others.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "hills",
      "map": "goblins2oc"
    },
    {
      "x": 53,
      "y": 26
    },
    {
      "x": 54,
      "y": 26,
      "name": "\u001bCircle of Stones\u001b",
      "description": "\u001bThis hill, though smaller than the mountains to the east and south, is still\u001b\r\n\u001bvery impressive. It is crowned by a small circle of stones, probably the\u001b\r\n\u001bremnant of some ancient tower -- perhaps a road or trail once led through this\u001b\r\n\u001barea. In every direction save west, the hillside is too steep and dangerous to\u001b\r\n\u001battempt to negotiate, and any attempt to do so would surely send one sprawling\u001b\r\n\u001bto one's death on the jagged rocks below.\u001b\r\n",
      "exits": {
        "west": {
          "door": false
        },
        "down": {
          "door": {},
          "pipeTo": {
            "x": 54,
            "y": 23
          }
        }
      },
      "terrain": "hills",
      "map": "goblins2oc"
    },
    {
      "x": 55,
      "y": 26
    },
    {
      "x": 56,
      "y": 26
    },
    {
      "x": 57,
      "y": 26
    },
    {
      "x": 58,
      "y": 26,
      "name": "\u001bCollapsed Mineshaft\u001b",
      "description": "\u001bSeveral large boulders, surrounded by smaller rocks and shattered wooden beams,\u001b\r\n\u001bcreate an upwards slope to the north. A charred skeleton has been crushed\u001b\r\n\u001bunderneath scorched timber. The cave was extensively mined in the past, but by\u001b\r\n\u001bthe looks of it has been abandoned for a long time. Carts have fallen over,\u001b\r\n\u001bbroken into many pieces. Some of their load has a small shine to it, beneath\u001b\r\n\u001bthe covering dust.\u001b\r\n",
      "exits": {
        "up": {
          "door": {},
          "pipeTo": {
            "x": 58,
            "y": 28
          }
        }
      },
      "terrain": "cavern",
      "map": "goblins2oc"
    },
    {
      "x": 59,
      "y": 26
    },
    {
      "x": 60,
      "y": 26
    },
    {
      "x": 61,
      "y": 26
    },
    {
      "x": 62,
      "y": 26
    },
    {
      "x": 63,
      "y": 26
    },
    {
      "x": 64,
      "y": 26
    },
    {
      "x": 65,
      "y": 26
    },
    {
      "x": 66,
      "y": 26
    },
    {
      "x": 67,
      "y": 26
    },
    {
      "x": 68,
      "y": 26
    },
    {
      "x": 69,
      "y": 26,
      "name": "\u001bDark Cave\u001b",
      "description": "\u001bA foul stench comes from the water to the south, and the steam rising from it\u001b\r\n\u001bhas a sulfurous smell. You spot a glimmer of surreal light from the north, but\u001b\r\n\u001bto the east all you can see is the ever present, all pervading darkness.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "cavern",
      "map": "goblins2oc"
    },
    {
      "x": 70,
      "y": 26,
      "name": "\u001bDark Cave\u001b",
      "description": "\u001bA sulfurous stench comes from the water to the south, a lake which is wreathed\u001b\r\n\u001bin mist. Peering into the gloom you can see the cave extending east and west, \u001b\r\n\u001band the ever present sound of dripping water un-nerves you in the darkness.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "cavern",
      "map": "goblins2oc"
    },
    {
      "x": 71,
      "y": 26,
      "name": "\u001bBack of the Cave\u001b",
      "description": "\u001bShrouded in darkness the gloom here is depressing. Peering around you, you can\u001b\r\n\u001bsee that the cave extends to the west, or you can go back to the light in the\u001b\r\n\u001bnorth where the entrance is.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "cavern",
      "map": "goblins2oc"
    },
    {
      "x": 72,
      "y": 26
    },
    {
      "x": 73,
      "y": 26
    },
    {
      "x": 74,
      "y": 26
    },
    {
      "x": 75,
      "y": 26
    },
    {
      "x": 76,
      "y": 26
    },
    {
      "x": 77,
      "y": 26
    },
    {
      "x": 78,
      "y": 26
    },
    {
      "x": 79,
      "y": 26
    },
    {
      "x": 80,
      "y": 26
    },
    {
      "x": 81,
      "y": 26
    },
    {
      "x": 82,
      "y": 26
    },
    {
      "x": 83,
      "y": 26
    },
    {
      "x": 84,
      "y": 26
    },
    {
      "x": 85,
      "y": 26
    },
    {
      "x": 86,
      "y": 26
    },
    {
      "x": 87,
      "y": 26
    },
    {
      "x": 88,
      "y": 26
    },
    {
      "x": 89,
      "y": 26
    },
    {
      "x": 90,
      "y": 26
    },
    {
      "x": 91,
      "y": 26
    },
    {
      "x": 92,
      "y": 26
    },
    {
      "x": 93,
      "y": 26
    }
  ],
  [
    {
      "x": 44,
      "y": 25
    },
    {
      "x": 45,
      "y": 25
    },
    {
      "x": 46,
      "y": 25
    },
    {
      "x": 47,
      "y": 25
    },
    {
      "x": 48,
      "y": 25
    },
    {
      "x": 49,
      "y": 25
    },
    {
      "x": 50,
      "y": 25
    },
    {
      "x": 51,
      "y": 25
    },
    {
      "x": 52,
      "y": 25
    },
    {
      "x": 53,
      "y": 25
    },
    {
      "x": 54,
      "y": 25
    },
    {
      "x": 55,
      "y": 25
    },
    {
      "x": 56,
      "y": 25
    },
    {
      "x": 57,
      "y": 25
    },
    {
      "x": 58,
      "y": 25
    },
    {
      "x": 59,
      "y": 25
    },
    {
      "x": 60,
      "y": 25
    },
    {
      "x": 61,
      "y": 25
    },
    {
      "x": 62,
      "y": 25
    },
    {
      "x": 63,
      "y": 25
    },
    {
      "x": 64,
      "y": 25
    },
    {
      "x": 65,
      "y": 25
    },
    {
      "x": 66,
      "y": 25
    },
    {
      "x": 67,
      "y": 25
    },
    {
      "x": 68,
      "y": 25
    },
    {
      "x": 69,
      "y": 25,
      "name": "\u001bSteaming Lake\u001b",
      "description": "\u001bRipples bounce backward and forward from the shore. The mist quickly rises up \u001b\r\n\u001bto wrap itself around your throat, and you can almost feel it trying to \u001b\r\n\u001bstrangle you. The lake extends east, and to the south you spot a low island \u001b\r\n\u001bjutting above the surface.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "water",
      "map": "goblins2oc"
    },
    {
      "x": 70,
      "y": 25,
      "name": "\u001bSteaming Lake\u001b",
      "description": "\u001bThe mist-shrouded water moves gently around you. Strange sounds are echoed and\u001b\r\n\u001bamplified, distorted beyond recognition by the vast chamber in which this lake \u001b\r\n\u001bsits. The water smells foul, the stench of sulfur and decay ever-present in \u001b\r\n\u001byour poor nostrils. The lake extends to the south and west. You can also climb\u001b\r\n\u001bback to the cave in the north.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "water",
      "map": "goblins2oc"
    },
    {
      "x": 71,
      "y": 25
    },
    {
      "x": 72,
      "y": 25
    },
    {
      "x": 73,
      "y": 25
    },
    {
      "x": 74,
      "y": 25
    },
    {
      "x": 75,
      "y": 25
    },
    {
      "x": 76,
      "y": 25
    },
    {
      "x": 77,
      "y": 25
    },
    {
      "x": 78,
      "y": 25
    },
    {
      "x": 79,
      "y": 25
    },
    {
      "x": 80,
      "y": 25
    },
    {
      "x": 81,
      "y": 25
    },
    {
      "x": 82,
      "y": 25
    },
    {
      "x": 83,
      "y": 25
    },
    {
      "x": 84,
      "y": 25
    },
    {
      "x": 85,
      "y": 25
    },
    {
      "x": 86,
      "y": 25
    },
    {
      "x": 87,
      "y": 25
    },
    {
      "x": 88,
      "y": 25
    },
    {
      "x": 89,
      "y": 25
    },
    {
      "x": 90,
      "y": 25
    },
    {
      "x": 91,
      "y": 25
    },
    {
      "x": 92,
      "y": 25
    },
    {
      "x": 93,
      "y": 25
    }
  ],
  [
    {
      "x": 44,
      "y": 24
    },
    {
      "x": 45,
      "y": 24
    },
    {
      "x": 46,
      "y": 24
    },
    {
      "x": 47,
      "y": 24
    },
    {
      "x": 48,
      "y": 24
    },
    {
      "x": 49,
      "y": 24
    },
    {
      "x": 50,
      "y": 24
    },
    {
      "x": 51,
      "y": 24
    },
    {
      "x": 52,
      "y": 24
    },
    {
      "x": 53,
      "y": 24,
      "name": "\u001bCrumbling Passage\u001b",
      "description": "\u001bThe walls here are in better shape than elsewhere, and some faint carvings in\u001b\r\n\u001ban unknown language can be made out high on the western wall. This age of this\u001b\r\n\u001bplace is an almost tangible force, seeming to mock those who dare invade with\u001b\r\n\u001btheir own insignificance in the grand pageant of time.\u001b\r\n",
      "exits": {
        "east": {
          "door": false
        },
        "south": {
          "door": false
        }
      },
      "terrain": "tunnel",
      "map": "goblins2oc"
    },
    {
      "x": 54,
      "y": 24,
      "name": "\u001bCrumbling Passage\u001b",
      "description": "\u001bThe purpose behind this place is unclear. The walls are lined with ancient\u001b\r\n\u001bmasonry that crumbles at the slightest touch, indicating great age. Perhaps\u001b\r\n\u001bthis hill once housed a tower for guarding the dangerous road that led to these\u001b\r\n\u001brocky hills, or perhaps this place is the tomb of some ancient and forgotten\u001b\r\n\u001bking.\u001b\r\n",
      "exits": {
        "south": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "tunnel",
      "map": "goblins2oc"
    },
    {
      "x": 55,
      "y": 24
    },
    {
      "x": 56,
      "y": 24
    },
    {
      "x": 57,
      "y": 24
    },
    {
      "x": 58,
      "y": 24
    },
    {
      "x": 59,
      "y": 24
    },
    {
      "x": 60,
      "y": 24
    },
    {
      "x": 61,
      "y": 24
    },
    {
      "x": 62,
      "y": 24
    },
    {
      "x": 63,
      "y": 24
    },
    {
      "x": 64,
      "y": 24
    },
    {
      "x": 65,
      "y": 24
    },
    {
      "x": 66,
      "y": 24
    },
    {
      "x": 67,
      "y": 24
    },
    {
      "x": 68,
      "y": 24
    },
    {
      "x": 69,
      "y": 24,
      "name": "\u001bRocky Island\u001b",
      "description": "\u001bThis rocky island must be where the inhabitants of the caves kept their most\u001b\r\n\u001bprecious possessions, for in the middle of the island sits a very heavy stone\u001b\r\n\u001bchest with a very sturdy lock on it. The passage of many feet has worn the \u001b\r\n\u001bsurface of the rock smooth, and the island is just high enough to prevent the\u001b\r\n\u001bmist covering it. Looking around, you see the lake to the east and north is\u001b\r\n\u001bcovered with mist, making you feel truly isolated here.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        }
      },
      "terrain": "cavern",
      "map": "goblins2oc"
    },
    {
      "x": 70,
      "y": 24,
      "name": "\u001bSteaming Lake\u001b",
      "description": "\u001bYou are gently rocked by ripples on the surface of this underground lake. Mist\u001b\r\n\u001band foul-smelling gases rise from the surface of the water, seemingly reaching\u001b\r\n\u001bfor your throat, trying to tear your lungs from your body. The lake extends to\u001b\r\n\u001bthe north. In the west you see a low island jutting above the water.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "west": {
          "door": false
        }
      },
      "terrain": "water",
      "map": "goblins2oc"
    },
    {
      "x": 71,
      "y": 24
    },
    {
      "x": 72,
      "y": 24
    },
    {
      "x": 73,
      "y": 24
    },
    {
      "x": 74,
      "y": 24
    },
    {
      "x": 75,
      "y": 24
    },
    {
      "x": 76,
      "y": 24
    },
    {
      "x": 77,
      "y": 24
    },
    {
      "x": 78,
      "y": 24
    },
    {
      "x": 79,
      "y": 24
    },
    {
      "x": 80,
      "y": 24
    },
    {
      "x": 81,
      "y": 24
    },
    {
      "x": 82,
      "y": 24
    },
    {
      "x": 83,
      "y": 24
    },
    {
      "x": 84,
      "y": 24
    },
    {
      "x": 85,
      "y": 24
    },
    {
      "x": 86,
      "y": 24
    },
    {
      "x": 87,
      "y": 24
    },
    {
      "x": 88,
      "y": 24
    },
    {
      "x": 89,
      "y": 24
    },
    {
      "x": 90,
      "y": 24
    },
    {
      "x": 91,
      "y": 24
    },
    {
      "x": 92,
      "y": 24
    },
    {
      "x": 93,
      "y": 24
    }
  ],
  [
    {
      "x": 44,
      "y": 23
    },
    {
      "x": 45,
      "y": 23
    },
    {
      "x": 46,
      "y": 23
    },
    {
      "x": 47,
      "y": 23
    },
    {
      "x": 48,
      "y": 23
    },
    {
      "x": 49,
      "y": 23
    },
    {
      "x": 50,
      "y": 23
    },
    {
      "x": 51,
      "y": 23
    },
    {
      "x": 52,
      "y": 23
    },
    {
      "x": 53,
      "y": 23,
      "name": "\u001bCrumbling Passage\u001b",
      "description": "\u001bTo the east is the the exit from this underground chamber. The walls of this\u001b\r\n\u001bplace exude a sense of age -- perhaps even before the Witchking of Angmar first\u001b\r\n\u001bmade his power felt and fell things gathered from all corners of Arda.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "east": {
          "door": false
        }
      },
      "terrain": "tunnel",
      "map": "goblins2oc"
    },
    {
      "x": 54,
      "y": 23,
      "name": "\u001bBeneath the Hill\u001b",
      "description": "\u001bThis entire hill seems to be hollow, an amazing accomplishment considering the\u001b\r\n\u001bsolid granite exterior. The purpose of this place has been lost in antiquity.\u001b\r\n\u001bMaybe this was once a watchtower of old Angmar, or perhaps this place predates\u001b\r\n\u001beven the Witchking's rule.\u001b\r\n",
      "exits": {
        "north": {
          "door": false
        },
        "west": {
          "door": false
        },
        "up": {
          "door": {},
          "pipeTo": {
            "x": 54,
            "y": 26
          }
        }
      },
      "terrain": "cavern",
      "map": "goblins2oc"
    },
    {
      "x": 55,
      "y": 23
    },
    {
      "x": 56,
      "y": 23
    },
    {
      "x": 57,
      "y": 23
    },
    {
      "x": 58,
      "y": 23
    },
    {
      "x": 59,
      "y": 23
    },
    {
      "x": 60,
      "y": 23
    },
    {
      "x": 61,
      "y": 23
    },
    {
      "x": 62,
      "y": 23
    },
    {
      "x": 63,
      "y": 23
    },
    {
      "x": 64,
      "y": 23
    },
    {
      "x": 65,
      "y": 23
    },
    {
      "x": 66,
      "y": 23
    },
    {
      "x": 67,
      "y": 23
    },
    {
      "x": 68,
      "y": 23
    },
    {
      "x": 69,
      "y": 23
    },
    {
      "x": 70,
      "y": 23
    },
    {
      "x": 71,
      "y": 23
    },
    {
      "x": 72,
      "y": 23
    },
    {
      "x": 73,
      "y": 23
    },
    {
      "x": 74,
      "y": 23
    },
    {
      "x": 75,
      "y": 23
    },
    {
      "x": 76,
      "y": 23
    },
    {
      "x": 77,
      "y": 23
    },
    {
      "x": 78,
      "y": 23
    },
    {
      "x": 79,
      "y": 23
    },
    {
      "x": 80,
      "y": 23
    },
    {
      "x": 81,
      "y": 23
    },
    {
      "x": 82,
      "y": 23
    },
    {
      "x": 83,
      "y": 23
    },
    {
      "x": 84,
      "y": 23
    },
    {
      "x": 85,
      "y": 23
    },
    {
      "x": 86,
      "y": 23
    },
    {
      "x": 87,
      "y": 23
    },
    {
      "x": 88,
      "y": 23
    },
    {
      "x": 89,
      "y": 23
    },
    {
      "x": 90,
      "y": 23
    },
    {
      "x": 91,
      "y": 23
    },
    {
      "x": 92,
      "y": 23
    },
    {
      "x": 93,
      "y": 23
    }
  ],
  [
    {
      "x": 44,
      "y": 22
    },
    {
      "x": 45,
      "y": 22
    },
    {
      "x": 46,
      "y": 22
    },
    {
      "x": 47,
      "y": 22
    },
    {
      "x": 48,
      "y": 22
    },
    {
      "x": 49,
      "y": 22
    },
    {
      "x": 50,
      "y": 22
    },
    {
      "x": 51,
      "y": 22
    },
    {
      "x": 52,
      "y": 22
    },
    {
      "x": 53,
      "y": 22
    },
    {
      "x": 54,
      "y": 22
    },
    {
      "x": 55,
      "y": 22
    },
    {
      "x": 56,
      "y": 22
    },
    {
      "x": 57,
      "y": 22
    },
    {
      "x": 58,
      "y": 22
    },
    {
      "x": 59,
      "y": 22
    },
    {
      "x": 60,
      "y": 22
    },
    {
      "x": 61,
      "y": 22
    },
    {
      "x": 62,
      "y": 22
    },
    {
      "x": 63,
      "y": 22
    },
    {
      "x": 64,
      "y": 22
    },
    {
      "x": 65,
      "y": 22
    },
    {
      "x": 66,
      "y": 22
    },
    {
      "x": 67,
      "y": 22
    },
    {
      "x": 68,
      "y": 22
    },
    {
      "x": 69,
      "y": 22
    },
    {
      "x": 70,
      "y": 22
    },
    {
      "x": 71,
      "y": 22
    },
    {
      "x": 72,
      "y": 22
    },
    {
      "x": 73,
      "y": 22
    },
    {
      "x": 74,
      "y": 22
    },
    {
      "x": 75,
      "y": 22
    },
    {
      "x": 76,
      "y": 22
    },
    {
      "x": 77,
      "y": 22
    },
    {
      "x": 78,
      "y": 22
    },
    {
      "x": 79,
      "y": 22
    },
    {
      "x": 80,
      "y": 22
    },
    {
      "x": 81,
      "y": 22
    },
    {
      "x": 82,
      "y": 22
    },
    {
      "x": 83,
      "y": 22
    },
    {
      "x": 84,
      "y": 22
    },
    {
      "x": 85,
      "y": 22
    },
    {
      "x": 86,
      "y": 22
    },
    {
      "x": 87,
      "y": 22
    },
    {
      "x": 88,
      "y": 22
    },
    {
      "x": 89,
      "y": 22
    },
    {
      "x": 90,
      "y": 22
    },
    {
      "x": 91,
      "y": 22
    },
    {
      "x": 92,
      "y": 22
    },
    {
      "x": 93,
      "y": 22
    }
  ]
]