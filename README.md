# README #
mumejs is a MUD client that runs in your web browser. It was created specifically for the game [MUME](https://mume.org).
mumejs is coded on top of the create-react-app codebase.

### Quick Start Instructions (Install and Play) ###
```sh
git clone https://bitbucket.org/jjheston-demo/mume-js
cd mume-js
npm install
npm run server
# leave `npm run server` running and open up another terminal/shell tab and also run:
npm run start
# the start command above will open up your browser to localhost:3000 and connect to MUME.
```

### Features! ###
* Mapper
* user input history scrolling with up arrow and down arrow keys
* user input command splitting with `;`
* triggers
* multi-line aliases
* auto login
* ANSI color support


### Upcoming Features ###
* automapping
* global minimap
* user accounts with multiple character configs

