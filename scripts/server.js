// Libraries from socket.io chat tutorial
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
// Libraries required from original muddy file
var fs = require('fs');
var net = require('net');
var express = require('express'); 
var createResponse = function(command, data) {
  return { command: command, data: data }
}

var mud_server_port = '4242';
var mud_server_url = 'mume.org';

app.set('views', __dirname, '/public');
app.use(express.static(__dirname + '/public'));

app.get('/', function(request, response) {
  res.render('index.html', {
    layout: false,
    locals: { mud: config.name }
  })
});

io.on('connection', function(socket){

  // connect to MUD server
  var mud = net.createConnection(mud_server_port, mud_server_url, function() {
    console.log('connected to mume.org:4242');
  });

  mud.setEncoding('utf8'); 

  mud.addListener('data', function(data) { // data events from MUD server
    console.log('mud.addListener `data` data');
    console.log(data);
    socket.emit('mudOutput', data);
  });

  socket.on('finalUserInput', function(data) {
    console.log('socket.on `finalUserInput` data');
    console.log(data);
    mud.write(data + '\r');// if you don't include at least the readline char, sent message isn't picked up by remote MUD server
  });

  socket.on('disconnect', function(){
    console.log('user disconnected');
  });

});

const _port = 3002;

http.listen(_port, function(){
  console.log('listening on *:' + _port);
});